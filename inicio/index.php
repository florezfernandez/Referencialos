<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Referencialos</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="icon" type="image/png" href="img/logo.png" />
		<link href="https://bootswatch.com/4/cerulean/bootstrap.css" rel="stylesheet" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css" rel="stylesheet">
		<link href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" rel="stylesheet" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script src="https://d3js.org/d3.v4.js"></script>
		<script src="https://cdn.jsdelivr.net/gh/holtzy/D3-graph-gallery@master/LIB/d3.layout.cloud.js"></script>
		<script charset="utf-8">
			$(function () { 
				$("[data-toggle='tooltip']").tooltip(); 
			});
		</script>
		<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-7016374989309129"
     crossorigin="anonymous"></script>
	</head>
<body>
	<div class="container-fluid"
		style="background-image: url('img/fondo1.jpeg'); background-size: cover; align-items: center; justify-content: left; display: flex;">
		<div class="row">
			<div class="col-md-12 text-center">&nbsp;</div>
			<div class="col-12 text-center" style="margin-top: 50px;">
				<h1
					style="font-weight: 700; font-size: 50px; color: #FFFFFF; text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);">Referencialos</h1>
			</div>
			<div class="col-md-12 text-center" style="margin-top: 20px;">&nbsp;</div>
			<div class="col-1"></div>
			<div class="col-5 text-center"
				style="width: 500px; height: 200px; margin-top: 50px; background-color: #FFFFFF; border-radius: 15px; box-shadow: 0 4px 30px rgba(0, 0, 0, 0.5); padding: 20px; transition: transform 0.3s; opacity: 0.6;">
				<h4 class="mb-2 text-center text-dark">
					¡Explora los beneficios de <strong>Referencialos</strong>!
				</h4>
				<p class="text-center p-2">Referencialos es una red social académica
					que permite registrar referencias de profesores universitarios con
					el fin de tener un instrumento que ofrezca un soporte a los
					estudiantes para la toma de decisiones en el momento de inscribir
					cursos.</p>
			</div>
			<div class="col-4" style="margin-left: 100px; margin-bottom: 20px;">
				<a href="?"><img src="img/logo2.png" width="350px" /></a>
			</div>
			<div class="col-md-12 text-center">&nbsp;</div>
		</div>
	</div>
	<div class="container-fluid"
		style="height: 100vh; align-items: center; justify-content: left; display: flex;">
		<div class="row">
			<div class="col-md-12 text-center">&nbsp;</div>
			<div class="col-4 text-center">
				<h1
					style="margin-left: 100px; margin-top: 50px; font-weight: 700; font-size: 50px; color: #005581; text-shadow: 2px 2px 5px rgba(0, 0, 0, 0.7);">
					Como ingresar a <i>Referencialos</i>
				</h1>
			</div>
			<div class="col-6" style="margin-left: 100px">
				<video width="100%" height="100%" controls>
					<source src="img/Registrarse.mp4" type="video/mp4">
				</video>
			</div>
			<div class="col-md-12 text-center">&nbsp;</div>
		</div>
	</div>
	<div class="container-fluid"
		style="height: 100vh; background-color: #005581">
		<div class="col-md-12 text-center">&nbsp;</div>
		<div class="col-12 text-center">
			<h1
				style="font-weight: 700; font-size: 50px; color: #FFFFFF; text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);">¿De
				que universidad eres?</h1>
		</div>
		<div class="col-md-12 text-center">&nbsp;</div>
		<div class="cards"
			style="display: flex; align-items: center; justify-content: center;">
			<div class="card"
				style="position: relative; width: 300px; margin: 25px; border-radius: 15px; box-shadow: 0 10px 15px rgba(0, 0, 0, 0.5); overflow: hidden; height: 415px; transition: transform 0.3s ease, box-shadow 0.3s ease;"
				onmouseover="this.style.transform='translateY(-10px)'; this.style.boxShadow='0 15px 25px rgba(0, 0, 0, 0.7)';"
				onmouseout="this.style.transform='translateY(0)'; this.style.boxShadow='0 10px 15px rgba(0, 0, 0, 0.5)';">
				<a href="https://ud.referencialos.org/"
					style="position: absolute; inset: 0;"></a> <img
					src="img/distrital.jpeg" style="width: 100%; border-radius: 15px;" />
				<h5 class="card-title text-center text-dark"
					style="position: absolute; bottom: 20px; left: 33px; text-align: center;">
					Universidad Distrital<br>Francisco José de Caldas
				</h5>
			</div>
			<div class="card"
				style="position: relative; width: 300px; margin: 25px; border-radius: 15px; box-shadow: 0 10px 15px rgba(0, 0, 0, 0.5); overflow: hidden; height: 415px; transition: transform 0.3s ease, box-shadow 0.3s ease;"
				onmouseover="this.style.transform='translateY(-10px)'; this.style.boxShadow='0 15px 25px rgba(0, 0, 0, 0.7)';"
				onmouseout="this.style.transform='translateY(0)'; this.style.boxShadow='0 10px 15px rgba(0, 0, 0, 0.5)';">
				<a href="https://uniandes.referencialos.org/"
					style="position: absolute; inset: 0;"></a> <img
					src="img/andes.jpeg" style="width: 100%; border-radius: 15px;" />
				<h5 class="card-title text-center text-dark"
					style="position: absolute; bottom: 25px; left: 50%; transform: translateX(-50%);">
					Universidad de los Andes</h5>
			</div>
		</div>
		<div class="col-md-12 text-center">&nbsp;</div>
	</div>
</body>
</html>