<?php
require("business/Administrador.php");
require("business/LogAdministrador.php");
require("business/LogEstudiante.php");
require("business/Estudiante.php");
require("business/Genero.php");
require("business/Facultad.php");
require("business/Programa.php");
require("business/Curso.php");
require("business/Profesor.php");
require("business/RangoDeNota.php");
require("business/Referencia.php");
require("business/Criterio.php");
require("business/CategoriaDeCriterio.php");
require("business/ReferenciaCriterio.php");
require("business/Votacion.php");
require("business/TipoDeDenuncia.php");
require("business/Denuncia.php");
require_once("persistence/Connection.php");
$idCriterio = $_GET ['idCriterio'];
$criterio = new Criterio($idCriterio);
$criterio -> select();
?>
<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	}); 
</script>
<div class="modal-header">
	<h4 class="modal-title">Criterio</h4>
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
	<table class="table table-striped table-hover">
		<tr>
			<th>Nombre</th>
			<td><?php echo $criterio -> getNombre() ?></td>
		</tr>
		<tr>
			<th>Descripcion</th>
			<td><?php echo $criterio -> getDescripcion() ?></td>
		</tr>
		<tr>
			<th>Categoria De Criterio</th>
			<td><?php echo $criterio -> getCategoriaDeCriterio() -> getNombre() ?></td>
		</tr>
	</table>
</div>
