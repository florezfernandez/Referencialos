<?php 
session_start();
require("business/Administrador.php");
require("business/LogAdministrador.php");
require("business/LogEstudiante.php");
require("business/Estudiante.php");
require("business/Genero.php");
require("business/Facultad.php");
require("business/Programa.php");
require("business/Curso.php");
require("business/Profesor.php");
require("business/RangoDeNota.php");
require("business/Referencia.php");
require("business/Criterio.php");
require("business/CategoriaDeCriterio.php");
require("business/ReferenciaCriterio.php");
require("business/Votacion.php");
require("business/TipoDeDenuncia.php");
require("business/Denuncia.php");
require("addons/facultad/FacultadExt.php");
require("addons/programa/ProgramaExt.php");
require("addons/profesor/ProfesorExt.php");
require("addons/genero/GeneroExt.php");
require("addons/curso/CursoExt.php");
require("addons/estudiante/EstudianteExt.php");
require("addons/referencia/ReferenciaExt.php");
require("addons/votacion/VotacionExt.php");
ini_set("display_errors","1");
date_default_timezone_set("America/Bogota");
$webPagesNoAuthentication = array(
	'ui/recoverPassword.php',
    'addons/estudiante/registerEstudiante.php',
    'addons/terminos.php',
    'addons/politica.php',
    'addons/estudiante/activateEstudiante.php',
    'addons/referencia/searchReferenciaProfesorNoAuth.php',
    'addons/referencia/viewSearchReferenciaProfesorNoAuth.php',
    'addons/referencia/viewReferenciasProfesorCursoNoAuth.php',
);
$webPages = array(
	'ui/sessionAdministrador.php',
	'ui/administrador/insertAdministrador.php',
	'ui/administrador/updateAdministrador.php',
	'ui/administrador/selectAllAdministrador.php',
	'ui/administrador/searchAdministrador.php',
	'ui/administrador/updateProfileAdministrador.php',
	'ui/administrador/updatePasswordAdministrador.php',
	'ui/administrador/updateProfilePictureAdministrador.php',
	'ui/administrador/updateFotoAdministrador.php',
	'ui/logAdministrador/searchLogAdministrador.php',
	'ui/logEstudiante/searchLogEstudiante.php',
	'ui/sessionEstudiante.php',
	'ui/estudiante/insertEstudiante.php',
	'ui/estudiante/updateEstudiante.php',
	'ui/estudiante/selectAllEstudiante.php',
	'ui/estudiante/searchEstudiante.php',
	'ui/estudiante/updateProfileEstudiante.php',
	'ui/estudiante/updatePasswordEstudiante.php',
	'ui/estudiante/updateProfilePictureEstudiante.php',
	'ui/referencia/selectAllReferenciaByEstudiante.php',
	'ui/denuncia/selectAllDenunciaByEstudiante.php',
	'ui/votacion/selectAllVotacionByEstudiante.php',
	'ui/estudiante/updateFotoEstudiante.php',
	'ui/genero/insertGenero.php',
	'ui/genero/updateGenero.php',
	'ui/genero/selectAllGenero.php',
	'ui/genero/searchGenero.php',
	'ui/estudiante/selectAllEstudianteByGenero.php',
	'ui/profesor/selectAllProfesorByGenero.php',
	'ui/facultad/insertFacultad.php',
	'ui/facultad/updateFacultad.php',
	'ui/facultad/selectAllFacultad.php',
	'ui/facultad/searchFacultad.php',
	'ui/programa/selectAllProgramaByFacultad.php',
	'ui/programa/insertPrograma.php',
	'ui/programa/updatePrograma.php',
	'ui/programa/selectAllPrograma.php',
	'ui/programa/searchPrograma.php',
	'ui/referencia/selectAllReferenciaByPrograma.php',
	'ui/curso/insertCurso.php',
	'ui/curso/updateCurso.php',
	'ui/curso/selectAllCurso.php',
	'ui/curso/searchCurso.php',
	'ui/referencia/selectAllReferenciaByCurso.php',
	'ui/profesor/insertProfesor.php',
	'ui/profesor/updateProfesor.php',
	'ui/profesor/selectAllProfesor.php',
	'ui/profesor/searchProfesor.php',
	'ui/referencia/selectAllReferenciaByProfesor.php',
	'ui/rangoDeNota/insertRangoDeNota.php',
	'ui/rangoDeNota/updateRangoDeNota.php',
	'ui/rangoDeNota/selectAllRangoDeNota.php',
	'ui/rangoDeNota/searchRangoDeNota.php',
	'ui/referencia/selectAllReferenciaByRangoDeNota.php',
	'ui/referencia/insertReferencia.php',
	'ui/referencia/updateReferencia.php',
	'ui/referencia/selectAllReferencia.php',
	'ui/referencia/searchReferencia.php',
	'ui/referenciaCriterio/selectAllReferenciaCriterioByReferencia.php',
	'ui/denuncia/selectAllDenunciaByReferencia.php',
	'ui/votacion/selectAllVotacionByReferencia.php',
	'ui/criterio/insertCriterio.php',
	'ui/criterio/updateCriterio.php',
	'ui/criterio/selectAllCriterio.php',
	'ui/criterio/searchCriterio.php',
	'ui/referenciaCriterio/selectAllReferenciaCriterioByCriterio.php',
	'ui/categoriaDeCriterio/insertCategoriaDeCriterio.php',
	'ui/categoriaDeCriterio/updateCategoriaDeCriterio.php',
	'ui/categoriaDeCriterio/selectAllCategoriaDeCriterio.php',
	'ui/categoriaDeCriterio/searchCategoriaDeCriterio.php',
	'ui/criterio/selectAllCriterioByCategoriaDeCriterio.php',
	'ui/referenciaCriterio/insertReferenciaCriterio.php',
	'ui/referenciaCriterio/updateReferenciaCriterio.php',
	'ui/referenciaCriterio/selectAllReferenciaCriterio.php',
	'ui/referenciaCriterio/searchReferenciaCriterio.php',
	'ui/votacion/insertVotacion.php',
	'ui/votacion/updateVotacion.php',
	'ui/votacion/selectAllVotacion.php',
	'ui/votacion/searchVotacion.php',
	'ui/tipoDeDenuncia/insertTipoDeDenuncia.php',
	'ui/tipoDeDenuncia/updateTipoDeDenuncia.php',
	'ui/tipoDeDenuncia/selectAllTipoDeDenuncia.php',
	'ui/tipoDeDenuncia/searchTipoDeDenuncia.php',
	'ui/denuncia/selectAllDenunciaByTipoDeDenuncia.php',
	'ui/denuncia/insertDenuncia.php',
	'ui/denuncia/updateDenuncia.php',
	'ui/denuncia/selectAllDenuncia.php',
	'ui/denuncia/searchDenuncia.php',
    'addons/programa/insertProgramaArchivo.php',
    'addons/profesor/insertProfesorArchivo.php',
    'addons/curso/insertCursoArchivo.php',
    'addons/referencia/insertReferenciaEstudiante.php',
    'addons/referencia/selectAllReferenciaEstudiante.php',
    'addons/referencia/updateReferenciaEstudiante.php',
    'addons/referencia/selectRankingProfesorCurso.php',
    'addons/referencia/viewRankingProfesorCurso.php',
    'addons/referencia/viewReferenciasProfesorCurso.php',
    'addons/referencia/searchReferenciaProfesor.php',
    'addons/referencia/viewReferenciaProfesor.php',
    'addons/referencia/viewSearchReferenciaProfesor.php',
    'addons/referencia/selectRankingProfesorPrograma.php',
    'addons/referencia/viewRankingProfesorPrograma.php',
    'addons/profesor/solicitarAgregarProfesor.php',
    'addons/curso/solicitarAgregarCurso.php',
    'addons/referencia/solicitarAgregarReferencia.php',
    'addons/denuncia/denunciarReferencia.php',
    'addons/actividad/actividadGeneral.php',
    'addons/actividad/actividadEstudiante.php',
    'addons/reportes/reportes.php',
    'addons/contacto/contacto.php',
);
if(isset($_GET['logOut'])){
	$_SESSION['id']="";
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Referencialos</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="icon" type="image/png" href="img/logo.png" />
		<link href="https://bootswatch.com/4/cerulean/bootstrap.css" rel="stylesheet" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css" rel="stylesheet">
		<link href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" rel="stylesheet" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script src="https://d3js.org/d3.v4.js"></script>
		<script src="https://cdn.jsdelivr.net/gh/holtzy/D3-graph-gallery@master/LIB/d3.layout.cloud.js"></script>
		<script charset="utf-8">
			$(function () { 
				$("[data-toggle='tooltip']").tooltip(); 
			});
		</script>
		<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-7016374989309129"
     crossorigin="anonymous"></script>
	</head>
	<body>
		<?php
		if(empty($_GET['pid'])){
			include('ui/home.php' );
		}else{
			$pid=base64_decode($_GET['pid']);
			if(in_array($pid, $webPagesNoAuthentication)){
				include($pid);
			}else{
				if($_SESSION['id']==""){
					header("Location: ?");
					die();
				}
				if($_SESSION['entity']=="Administrador"){
					include('ui/menuAdministrador.php');
				}
				if($_SESSION['entity']=="Estudiante"){
					include('ui/menuEstudiante.php');
				}
				if (in_array($pid, $webPages)){
					include($pid);
				}else{
					include('ui/error.php');
				}
			}
		}
		?>
		<div class="text-center text-muted">Referencialos &copy; <?php echo date("Y")?></div>
	</body>
</html>
