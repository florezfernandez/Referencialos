<?php
require("business/Administrador.php");
require("business/LogAdministrador.php");
require("business/LogEstudiante.php");
require("business/Estudiante.php");
require("business/Genero.php");
require("business/Facultad.php");
require("business/Programa.php");
require("business/Curso.php");
require("business/Profesor.php");
require("business/RangoDeNota.php");
require("business/Referencia.php");
require("business/Criterio.php");
require("business/CategoriaDeCriterio.php");
require("business/ReferenciaCriterio.php");
require("business/Votacion.php");
require("business/TipoDeDenuncia.php");
require("addons/referencia/ReferenciaExt.php");
require("business/Denuncia.php");
require_once("persistence/Connection.php");
$idReferencia = $_GET ['idReferencia'];
$referencia = new Referencia($idReferencia);
$referencia -> select();
?>
<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	}); 
</script>
<div class="modal-header">
	<h4 class="modal-title">Referencia</h4>
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
	<table class="table table-striped table-hover">
		<tr>
			<th>Comentario</th>
			<td colspan="2"><?php echo $referencia -> getComentario() ?></td>
		</tr>
		<tr>
			<th>Pros</th>
			<td colspan="2"><?php echo $referencia -> getPros() ?></td>
		</tr>
		<tr>
			<th>Contras</th>
			<td colspan="2"><?php echo $referencia -> getContras() ?></td>
		</tr>
		<tr>
			<th>Periodo</th>
			<td colspan="2"><?php echo $referencia -> getPeriodo() ?></td>
		</tr>
		<tr>
			<th>Fecha</th>
			<td colspan="2"><?php echo $referencia -> getFecha() ?></td>
		</tr>
		<tr>
			<th>Profesor</th>
			<td colspan="2"><?php echo $referencia -> getProfesor() -> getNombre() ?></td>
		</tr>
		<tr>
			<th>Curso</th>
			<td colspan="2"><?php echo $referencia -> getCurso() -> getNombre() ?></td>
		</tr>
		<tr>
			<th>Programa</th>
			<td colspan="2"><?php echo $referencia -> getPrograma() -> getNombre() ?></td>
		</tr>
		<tr>
			<th>Rango De Nota</th>
			<td colspan="2"><?php echo $referencia -> getRangoDeNota() -> getValor() ?></td>
		</tr>
		<tr>
            <th>Criterios</th>
            <td><?php
                $referenciaExt = new ReferenciaExt($idReferencia);
                $criteriosAcademicos = $referenciaExt -> criteriosAcademicosByReferencia();
                $criteriosSociales = $referenciaExt -> criteriosSocialesByReferencia();
                echo "<strong>Criterios Académicos:</strong>";
                echo "<ul>";
                foreach ($criteriosAcademicos as $currentCriterioAcademico) {
                    echo "<li>" . $currentCriterioAcademico[0] . ": <strong>" . $currentCriterioAcademico[1] . "</strong></li>";
                };
                echo "</ul>";
                echo "</div></td>";
                echo "<td><div class='col'>";
                echo "<strong>Criterios Sociales:</strong>";
                echo "<ul>";
                foreach ($criteriosSociales as $currentCriterioSocial) {
                    echo "<li>" . $currentCriterioSocial[0] . ": <strong>" . $currentCriterioSocial[1] . "</strong></li>";
                };
                echo "</ul>";
                echo "</div>";
            ?></td>
        </tr>
	</table>
		
</div>