import { wordList } from "../js/dic.js";
$(document).ready(function() {
  $("title, span, label, h1, h2, h3, h4, h5, h6, th, a, button").each(function() {
    for (let i = 0; i < wordList.length; i++) {
      const item = wordList[i];
      var originalText = $(this).text();
      var newText = originalText.replace(item.search, item.replacement);
      if (originalText !== newText) {
        $(this).text(newText);
      }
    }
  });  
});

/*
Create the file js/dic.js with a constant dictionari and include all words to replace:

Example:
export const wordList = [
  { search: 'Academico', replacement: 'Académico' },
];
*/