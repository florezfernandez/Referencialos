<?php
require("business/Administrador.php");
require("business/LogAdministrador.php");
require("business/LogEstudiante.php");
require("business/Estudiante.php");
require("business/Genero.php");
require("business/Facultad.php");
require("business/Programa.php");
require("business/Curso.php");
require("business/Profesor.php");
require("business/RangoDeNota.php");
require("business/Referencia.php");
require("business/Criterio.php");
require("business/CategoriaDeCriterio.php");
require("business/ReferenciaCriterio.php");
require("business/Votacion.php");
require("business/TipoDeDenuncia.php");
require("business/Denuncia.php");
require_once("persistence/Connection.php");
$idReferencia = $_GET ['idReferencia'];
$referencia = new Referencia($idReferencia);
$referencia -> select();
?>
<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	}); 
</script>
<div class="modal-header">
	<h4 class="modal-title">Detalles de Referencia</h4>
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
	<table class="table table-striped table-hover">
		<tr>
			<th>Profesor</th>
			<td><?php echo $referencia -> getProfesor() -> getNombre() ?></td>
		</tr>
		<tr>
			<th>Curso</th>
			<td><?php echo $referencia -> getCurso() -> getNombre() ?></td>
		</tr>
		<tr>
			<th>Programa</th>
			<td><?php echo $referencia -> getPrograma() -> getNombre() ?></td>
		</tr>
		<tr>
			<th>Periodo</th>
			<td><?php echo $referencia -> getPeriodo() ?></td>
		</tr>
		<tr>
			<th>Rango De Nota</th>
			<td><?php echo $referencia -> getRangoDeNota() -> getValor() ?></td>
		</tr>
		<tr>
			<th>Comentario</th>
			<td><?php echo $referencia -> getComentario() ?></td>
		</tr>
		<tr>
			<th>Pros</th>
			<td><?php echo $referencia -> getPros() ?></td>
		</tr>
		<tr>
			<th>Contras</th>
			<td><?php echo $referencia -> getContras() ?></td>
		</tr>
		<tr>
			<th>Fecha</th>
			<td><?php echo $referencia -> getFecha() ?></td>
		</tr>
		<tr>
			<th>Estado</th>
			<td><?php echo ($referencia -> getEstado()==1?"Habilitado":"Deshabilitado") ?> </td>
		</tr>
		<tr>
			<th>Criterios</th>
			<td>
			<?php 
			$referenciaCriterio = new ReferenciaCriterio("", "", $_GET['idReferencia'], "");
		    $referenciaCriterios = $referenciaCriterio -> selectAllByReferencia();
		    $calificaciones = array("Muy en desacuerdo (1)", "En desacuerdo	(2)", "Neutral (3)", "De acuerdo (4)", "Muy de acuerdo (5)");
			echo "<ul>";
			foreach ($referenciaCriterios as $currentReferenciaCriterio) {
			    echo "<li>" . $currentReferenciaCriterio -> getCriterio() -> getNombre() . ": <strong>" . $calificaciones[$currentReferenciaCriterio -> getValor()-1] . "</strong></li>";
			};
			echo "</ul>";
			
			?>			
			</td>
		</tr>
	</table>
</div>
