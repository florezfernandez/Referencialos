<?php
require_once ("addons/votacion/VotacionDAOExt.php");


class VotacionExt extends Votacion {
    private $votacionDAOExt;
    
    function __construct($pIdVotacion = "", $pVoto = "", $pFecha = "", $pReferencia = "", $pEstudiante = ""){
        parent::__construct($pIdVotacion, $pVoto, $pFecha, $pReferencia, $pEstudiante);
        $this -> votacionDAOExt = new VotacionDAOExt($this -> idVotacion, $this -> voto, $this -> fecha, $this -> referencia, $this -> estudiante);
        
    }
    
    function existVoto(){
        $this -> connection -> open();
        $this -> connection -> run($this -> votacionDAOExt -> existVoto());
        $result = $this -> connection -> fetchRow();
        $this -> connection -> close();
        return ($result[0]==1)?true:false;
    }
    
    function getVoto(){
        $this -> connection -> open();
        $this -> connection -> run($this -> votacionDAOExt -> getVoto());
        $result = $this -> connection -> fetchRow();
        $this -> connection -> close();
        return $result[0];
    }
    
    function deleteVoto(){
        $this -> connection -> open();
        $this -> connection -> run($this -> votacionDAOExt -> deleteVoto());
        $this -> connection -> close();
    }    
    
    function updateVoto(){
        $this -> connection -> open();
        $this -> connection -> run($this -> votacionDAOExt -> updateVoto());
        $this -> connection -> close();
    }
    
    function insertVoto(){
        $this -> connection -> open();
        $this -> connection -> run($this -> votacionDAOExt -> insert());
        $this -> connection -> close();
    }
    
    function getCantidadVotos($voto){
        $this -> connection -> open();
        $this -> connection -> run($this -> votacionDAOExt -> getCantidadVotos($voto));
        $result = $this -> connection -> fetchRow();
        $this -> connection -> close();
        return $result[0];
    }
    
    function votar(){
        if($this -> existVoto()){
            $votoAnterior = $this -> getVoto();
            if($votoAnterior == $this -> voto){
                $this -> deleteVoto();
            }else{
                $this -> updateVoto();   
            }
        }else{
            $this -> insertVoto();
        }
    }
    
    function votoByReferenciaAndEstudiante(){
        $this -> connection -> open();
        $this -> connection -> run($this -> votacionDAOExt -> votoByReferenciaAndEstudiante());
        if($this -> connection -> numRows() == 0){
            return 0;
        }else{
            $result = $this -> connection -> fetchRow();            
        }
        $this -> connection -> close();
        return $result[0];
    }
    
}
?>