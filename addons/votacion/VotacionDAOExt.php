<?php


class VotacionDAOExt extends VotacionDAO{
       
    function __construct($pIdVotacion = "", $pVoto = "", $pFecha = "", $pReferencia = "", $pEstudiante = ""){
        parent::__construct($pIdVotacion, $pVoto, $pFecha, $pReferencia, $pEstudiante);
    }
    
    function existVoto(){
        return "select count(idVotacion)
                from Votacion
                where referencia_idReferencia = '" . $this -> referencia . "' and estudiante_idEstudiante = '" . $this -> estudiante . "'";        
    }
    
    function getVoto(){
        return "select voto
                from Votacion
                where referencia_idReferencia = '" . $this -> referencia . "' and estudiante_idEstudiante = '" . $this -> estudiante . "'";
    }
    
    function deleteVoto(){
        return "delete 
                from Votacion
                where referencia_idReferencia = '" . $this -> referencia . "' and estudiante_idEstudiante = '" . $this -> estudiante . "'";
    }
    
    function updateVoto(){
        return "update Votacion
                set voto = '" . $this -> voto . "'
                where referencia_idReferencia = '" . $this -> referencia . "' and estudiante_idEstudiante = '" . $this -> estudiante . "'";
    }
    
    function insertVoto(){
        return "select count(idVotacion)
                from Votacion
                where referencia_idReferencia = '" . $this -> referencia . "'";
    }
    
    function getCantidadVotos($voto){
        return "select count(idVotacion)
                from Votacion
                where referencia_idReferencia = '" . $this -> referencia . "' and voto = '" . $voto . "'";
    }
    function votoByReferenciaAndEstudiante(){
        return "select voto
                from Votacion
                where referencia_idReferencia = '" . $this -> referencia . "' and estudiante_idEstudiante = '" . $this -> estudiante . "'";
    }
    
}
?>