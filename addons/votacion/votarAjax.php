<script charset="utf-8">
$(function () { 
	$("[data-toggle='tooltip']").tooltip(); 
});
</script>
<?php 
$votacion = new VotacionExt("", $_GET["voto"], date("Y-m-d"), $_GET["idReferencia"], $_GET["idEstudiante"]);
$votacion -> votar();
$votos = array(
    "-1" => "Estoy en desacuerdo",
    "0" => "No he votado",
    "1" => "Estoy de acuerdo"
);
echo "<span class='fas fa-thumbs-up' data-toggle='tooltip' data-placement='top' data-original-title='De acuerdo' ></span> " . $votacion -> getCantidadVotos(1) . " - ";
echo "<span class='fas fa-thumbs-down' data-toggle='tooltip' data-placement='top' data-original-title='Desacuerdo' ></span> " . $votacion -> getCantidadVotos(-1) . " - ";
echo "Mi voto: " . $votos[$votacion -> votoByReferenciaAndEstudiante()];
?>