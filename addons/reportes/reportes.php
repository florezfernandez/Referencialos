<div class="container">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Reportes</h4>
				</div>
				<div class="card-body">
				<?php 
				    include ("addons/reportes/referenciasPorPrograma.php");
				    echo "<br>";
				    include ("addons/reportes/referenciasPorCurso.php");
				    echo "<br>";
				    include ("addons/reportes/referenciasPorProfesor.php");
				    echo "<br>";
				    include ("addons/reportes/referenciasPorRangoDeNota.php");
				    echo "<br>";
				    include ("addons/reportes/referenciasPorPeriodo.php");
				?>
				</div>
			</div>
		</div>
	</div>
</div>