<?php 
$referencia = new ReferenciaExt();
$referencias = $referencia -> cantidadDeReferenciasPorPrograma();
$data = "['Programa','Cantidad'],";
$total = 0;
foreach ($referencias as $currentReferencia){
    $data .= "['" . $currentReferencia[0] . "', " . $currentReferencia[1] . "],";
    $total += $currentReferencia[1];
}
?>
<div class="card">
	<div class="card-header">
		<h4 class="card-title">Referencias por rango de nota</h4>
	</div>
	<div class="card-body">
		<h5>Cantidad de referencias: <?php echo $total ?></h5>
		<div id="pieReferenciasPorPrograma" style="height: 400px; width=100%"
			class="text-center">
			<img src="img/loading.gif">
		</div>
		<div id="barReferenciasPorPrograma" style="height: 400px; width=100%"
			class="text-center">
			<img src="img/loading.gif">
		</div>
	</div>
</div>
<script type="text/javascript">
google.charts.load("current", {"packages":["corechart", "bar"]});
google.charts.setOnLoadCallback(drawChart);                        
function drawChart() {
	var data = google.visualization.arrayToDataTable([
		<?php echo $data ?>
    ]);
    var options = {
        is3D: true,
        legend: {position: "right", alignment: "center"}
    };
	var chart = new google.visualization.PieChart(document.getElementById("pieReferenciasPorPrograma"));                    
	chart.draw(data, options);
	var options = {
        bars: 'horizontal',
    };
    var chart = new google.charts.Bar(document.getElementById("barReferenciasPorPrograma"));                    
	chart.draw(data, options);
}
</script>