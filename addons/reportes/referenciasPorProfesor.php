<?php 
$referencia = new ReferenciaExt();
$referencias = $referencia -> cantidadDeReferenciasPorProfesor();
$data = "['Profesor','Cantidad'],";
$total = 0;
foreach ($referencias as $currentReferencia){
    $data .= "['" . $currentReferencia[0] . "', " . $currentReferencia[1] . "],";
    $total += $currentReferencia[1];
}
?>
<div class="card">
	<div class="card-header">
		<h4 class="card-title">Referencias por profesor</h4>
	</div>
	<div class="card-body">
		<h5>Profesores con mayor cantidad de referencias</h5>
		<div id="barReferenciasPorProfesor" style="height: 400px; width=100%"
			class="text-center">
			<img src="img/loading.gif">
		</div>
	</div>
</div>
<script type="text/javascript">
google.charts.load("current", {"packages":["corechart", "bar"]});
google.charts.setOnLoadCallback(drawChart);                        
function drawChart() {
	var data = google.visualization.arrayToDataTable([
		<?php echo $data ?>
    ]);
	var options = {
        bars: 'horizontal',
    };
    var chart = new google.charts.Bar(document.getElementById("barReferenciasPorProfesor"));                    
	chart.draw(data, options);
}
</script>