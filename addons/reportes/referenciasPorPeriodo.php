<?php 
$referencia = new ReferenciaExt();
$referencias = $referencia -> cantidadDeReferenciasPorPeriodo();
$data = "['Rango de Nota','Cantidad'],";
$total = 0;
foreach ($referencias as $currentReferencia){
    $data .= "['" . $currentReferencia[0] . "', " . $currentReferencia[1] . "],";
    $total += $currentReferencia[1];
}
?>
<div class="card">
	<div class="card-header">
		<h4 class="card-title">Referencias por periodo</h4>
	</div>
	<div class="card-body">
		<h5>Cantidad de referencias: <?php echo $total ?></h5>
		<div id="pieReferenciasPorPeriodo" style="height: 400px; width=100%"
			class="text-center">
			<img src="img/loading.gif">
		</div>
	</div>
</div>
<script type="text/javascript">
google.charts.load("current", {"packages":["corechart", "bar"]});
google.charts.setOnLoadCallback(drawChart);                        
function drawChart() {
	var data = google.visualization.arrayToDataTable([
		<?php echo $data ?>
    ]);
    var options = {
        is3D: true,
        legend: {position: "right", alignment: "center"}
    };
	var chart = new google.visualization.PieChart(document.getElementById("pieReferenciasPorPeriodo"));                    
	chart.draw(data, options);
}
</script>