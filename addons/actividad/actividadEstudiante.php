<?php
$referencia = new ReferenciaExt("", "", "", "", "", "", "",  "",  "", $_SESSION["id"]);
$referencias = $referencia -> getActividadReferenciaEstudiante();
$votaciones = $referencia -> getActividadVotacionEstudiante();
$denuncias = $referencia -> getActividadDenunciaEstudiante();
$solicitudes = $referencia -> getActividadSolicitudEstudiante();
?>
<div class="container">
	<div class="card">
		<div class="card-header">
			<h3>Mi Actividad</h3>
		</div>
		<div class="card-body">
			<div class="accordion" id="accordionActividad">
				<div class="card">
					<div class="card-header" id="headingOne">
						<h2 class="mb-0">
							<button class="btn btn-link btn-block text-left" type="button"
								data-toggle="collapse" data-target="#collapseOne"
								aria-expanded="true" aria-controls="collapseOne">Mis Referencias <i class="fas fa-angle-double-down"></i></button>
						</h2>
					</div>

					<div id="collapseOne" class="collapse"
						aria-labelledby="headingOne" data-parent="#accordionActividad">
						<div class="card-body">
            			<?php 
            			if(count($referencias) > 0) {
                            foreach ($referencias as $currentReferencia) {
                                echo "<p>";
                                echo "<small>" . $currentReferencia[7] . "</small><br>";
                                echo "Referencia para el profesor ";
                                echo "<a href='?pid=" . base64_encode("addons/referencia/viewSearchReferenciaProfesor.php") . "&idProfesor=" . $currentReferencia[1] . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver referencias del profesor " . $currentReferencia[2] . "'>" . $currentReferencia[2] . "</a> ";
                			    echo "en el curso ";
                			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorCurso.php") . "&idCurso=" . $currentReferencia[3] . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver ranking de profesores del curso " . $currentReferencia[4] . "'>" . $currentReferencia[4] . "</a> ";
                			    echo "del programa ";
                			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorPrograma.php") . "&idPrograma=" . $currentReferencia[5] . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver ranking de profesores del programa " . $currentReferencia[6] . "'>" . $currentReferencia[6] . "</a><br>";
                			    echo "<em>" . $currentReferencia[0] . "</em>";
                			    echo "</p>";
                			    echo "<hr>";			    
                			}
            			}else{
            			    echo "<p>Aun no ha hecho ninguna referencia</p>";
            			}
            			?>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingTwo">
						<h2 class="mb-0">
							<button class="btn btn-link btn-block text-left collapsed"
								type="button" data-toggle="collapse" data-target="#collapseTwo"
								aria-expanded="false" aria-controls="collapseTwo">Mis Solicitudes de Referencias <i class="fas fa-angle-double-down"></i></button>
						</h2>
					</div>
					<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
						data-parent="#accordionActividad">
						<div class="card-body">
						<?php 
						if(count($solicitudes) > 0){
						    foreach ($solicitudes as $currentSolicitud){
						        echo "<p>";
						        echo "<small>" . $currentSolicitud[0] . "</small><br>";
						        echo "Solicitud de referencia del profesor ";
						        echo "<a href='?pid=" . base64_encode("addons/referencia/viewSearchReferenciaProfesor.php") . "&idProfesor=" . $currentSolicitud[1] -> getIdProfesor() . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver referencias del profesor " . $currentSolicitud[1] -> getNombre() . "'>" . $currentSolicitud[1] -> getNombre() . "</a> ";
						        echo "en el curso ";
						        echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorCurso.php") . "&idCurso=" . $currentSolicitud[2] -> getIdCurso() . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver ranking de profesores del curso " . $currentSolicitud[2] -> getNombre() . "'>" . $currentSolicitud[2] -> getNombre() . "</a> ";
						        echo "del programa ";
						        echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorPrograma.php") . "&idPrograma=" . $currentSolicitud[4] -> getIdPrograma() . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver ranking de profesores del programa " . $currentSolicitud[4] -> getNombre() . "'>" . $currentSolicitud[4] -> getNombre() . "</a><br>";
						        echo "<hr></p>";
						    }
						}else{
						    echo "<p>Aun no ha hecho ninguna solicitud</p>";
						}
						?>

						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingThree">
						<h2 class="mb-0">
							<button class="btn btn-link btn-block text-left collapsed"
								type="button" data-toggle="collapse"
								data-target="#collapseThree" aria-expanded="false"
								aria-controls="collapseThree">Mis Votaciones <i class="fas fa-angle-double-down"></i></button>
						</h2>
					</div>
					<div id="collapseThree" class="collapse"
						aria-labelledby="headingThree" data-parent="#accordionActividad">
						<div class="card-body">
						<?php  if(count($votaciones) > 0) {
                			$voto = array(
                			    1 => "<span class='fas fa-thumbs-up' data-toggle='tooltip' data-placement='top' data-original-title='De acuerdo' ></span>",    
                			    -1 => "<span class='fas fa-thumbs-down' data-toggle='tooltip' data-placement='top' data-original-title='Desacuerdo' ></span>"
                			);
                			foreach ($votaciones as $currentVotacion){
                			    echo "<p>";
                			    echo "<small>" . $currentVotacion[7] . "</small><br>";
                			    echo "Mi voto " . $voto[$currentVotacion[8]] . "<br>";
                			    echo "Referencia para el profesor ";
                			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewSearchReferenciaProfesor.php") . "&idProfesor=" . $currentVotacion[1] . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver referencias del profesor " . $currentVotacion[2] . "'>" . $currentVotacion[2] . "</a> ";
                			    echo "en el curso ";
                			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorCurso.php") . "&idCurso=" . $currentVotacion[3] . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver ranking de profesores del curso " . $currentVotacion[4] . "'>" . $currentVotacion[4] . "</a> ";
                			    echo "del programa ";
                			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorPrograma.php") . "&idPrograma=" . $currentVotacion[5] . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver ranking de profesores del programa " . $currentVotacion[6] . "'>" . $currentVotacion[6] . "</a><br>";
                			    echo "<em>" . $currentVotacion[0] . "</em>";
                			    echo "</p>";
                			    echo "<hr>";			    
                			}
						} else {
						    echo "<p>Aun no ha hecho ninguna votación</p>";
						}
            			?>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingFour">
						<h2 class="mb-0">
							<button class="btn btn-link btn-block text-left collapsed"
								type="button" data-toggle="collapse"
								data-target="#collapseFour" aria-expanded="false"
								aria-controls="collapseFour">Mis Denuncias <i class="fas fa-angle-double-down"></i></button>
						</h2>
					</div>
					<div id="collapseFour" class="collapse"
						aria-labelledby="headingFour" data-parent="#accordionActividad">
						<div class="card-body">
						<?php  if(count($denuncias) > 0) { 				             			
                			foreach ($denuncias as $currentDenuncia){
                			    echo "<p>";
                			    echo "<small>" . $currentDenuncia[7] . "</small><br>";
                			    echo "Referencia para el profesor ";
                			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewSearchReferenciaProfesor.php") . "&idProfesor=" . $currentDenuncia[1] . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver referencias del profesor " . $currentDenuncia[2] . "'>" . $currentDenuncia[2] . "</a> ";
                			    echo "en el curso ";
                			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorCurso.php") . "&idCurso=" . $currentDenuncia[3] . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver ranking de profesores del curso " . $currentDenuncia[4] . "'>" . $currentDenuncia[4] . "</a> ";
                			    echo "del programa ";
                			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorPrograma.php") . "&idPrograma=" . $currentDenuncia[5] . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver ranking de profesores del programa " . $currentDenuncia[6] . "'>" . $currentDenuncia[6] . "</a><br>";
                			    echo "<em>" . $currentDenuncia[0] . "</em>";
                			    echo "</p>";
                			    echo "Tipo de denuncia: <em>" . $currentDenuncia[9] . "</em><br>";
                			    echo "Argumento: <em>" . $currentDenuncia[8] . "</em><br>";
                			    echo "<hr>";			    
                			}
						} else {
						    echo "<p>Aun no ha hecho ninguna denuncia</p>";
						}
            			?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
