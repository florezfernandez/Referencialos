<?php 
$pos = $_GET["n"];
$referenciaExt = new ReferenciaExt();
$referencias = $referenciaExt -> getActividadGeneralCincoMas($pos);
if($referencias != null){
    echo "<table>";
    foreach ($referencias as $currentReferencia){
        echo "<tr>";
        echo "<td><p>";
        echo "<small>" . $currentReferencia[8] . "</small><br>";
        echo "Referencia para el profesor ";
        echo "<a href='?pid=" . base64_encode("addons/referencia/viewSearchReferenciaProfesor.php") . "&idProfesor=" . $currentReferencia[2] . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver referencias del profesor " . $currentReferencia[3] . "'>" . $currentReferencia[3] . "</a> ";
        echo "en el curso ";
        echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorCurso.php") . "&idCurso=" . $currentReferencia[4] . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver ranking de profesores del curso " . $currentReferencia[5] . "'>" . $currentReferencia[5] . "</a> ";
        echo "del programa ";
        echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorPrograma.php") . "&idPrograma=" . $currentReferencia[6] . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver ranking de profesores del programa " . $currentReferencia[7] . "'>" . $currentReferencia[7] . "</a><br>";
        echo "<em>" . $currentReferencia[1] . "</em>";
        echo "</p>";
        echo "<hr></td>";
        echo "<td class='text-right' nowrap>";
        echo "<a href='modalReferenciaActividad.php?idReferencia=" . $currentReferencia[0] . "'  data-toggle='modal' data-target='#modalReferenciaActividad' ><span class='fas fa-eye' data-toggle='tooltip' data-placement='top' data-original-title='Ver más información'></span></a> ";
        echo "</td>";
        echo "</tr>";
    }
    echo "</table>";
}
?>