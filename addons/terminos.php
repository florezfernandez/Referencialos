<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Referencialos</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="icon" type="image/png" href="img/logo.png" />
		<link href="https://bootswatch.com/4/litera/bootstrap.css" rel="stylesheet" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css" rel="stylesheet">
		<link href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" rel="stylesheet" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
		<script charset="utf-8">
			$(function () { 
				$("[data-toggle='tooltip']").tooltip(); 
			});
		</script>
	</head>
	<body>
        <?php include "ui/header.php" ?>
        <div class="container">
			<div class="row">
				<div class="col">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Terminos de Servicio</h4>
						</div>
						<div class="card-body">
							<ul>
								<li><i>Referencialos</i> es un proyecto desarrollado de forma independiente, 
								por lo tanto no está asociado con ninguna institución.</li>
								<li><i>Referencialos</i> es una herramienta de recomendación de profesores 
								universitarios, creada con el fin de apoyar la toma de decisiones de los 
								estudiantes en el momento de inscribir cursos, para que los estudiantes 
								puedan lograr sus objetivos académicos.</li>
								<li>Los administradores de <i>Referencialos</i> tendrán a cargo la tarea 
								de incluir nueva información de facultades, programas, cursos y profesores.
								</li>
								<li>Los usuarios de <i>Referencialos</i> son estudiantes universitarios, quienes 
								deben registrarse con correo institucional.</li>
								<li>Los profesores no podrán ser usuarios de <i>Referencialos</i>.</li>
								<li>Los usuarios registrados pueden agregar información de referencias, 
								hacer votaciones de referencias y crear denuncias de referencias.</li>
								<li>Cada usuario es responsable por el contenido ingresado en 
								<i>Referencialos</i>. Por lo tanto, <i>Referencialos</i> no se hace 
								responsable por la información ingresada por los usuarios.</li>								
								<li>Las referencias pueden tener votación mediante los botones <i>Estoy de 
								acuerdo</i> y <i>Estoy en desacuerdo</i>. Si una referencia 
								obtiene una cantidad de 10 votos de tipo <i>Estoy en desacuerdo</i>, la 
								referencia se ocultará automáticamente.</li>
								<li>Las referencias pueden recibir denuncias por falsedad, vocabulario 
								inapropiado, etc. En el momento de recibir una denuncia, un administrador 
								de <i>Referencialos</i> revisará la denuncia y podrá ocultar la referencia.
								Además, el administrador podrá inhabilitar al estudiante que agregó la 
								referencia denunciada.</li>
								<li><i>Referencialos</i> se reserva el derecho de otorgar o negar el acceso a cualquier 
								usuario en cualquier momento.</li>								
								<li><i>Referencialos</i> no cobra ningún valor económico por su uso.</li>
								<li><i>Referencialos</i> se financia mediante publicidad.</li>
								<li><i>Referencialos</i> se reserva el derecho de modificar los <i>Términos de 
								Servicio</i> en cualquier momento y sin previo aviso.</li>
								<li>El uso de <i>Referencialos</i> posterior a cualquier modificación de 
								los <i>Términos de Servicio</i>, genera la aceptación automática del contenido 
								completo de los <i>Términos de Servicio</i>.</li>
							</ul>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</body>
</html>

