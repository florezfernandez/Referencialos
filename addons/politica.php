<!DOCTYPE html>
<html lang="en">
<head>
<title>Referencialos</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" type="image/png" href="img/logo.png" />
<link href="https://bootswatch.com/4/litera/bootstrap.css"
	rel="stylesheet" />
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css"
	rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.11.1/css/all.css"
	rel="stylesheet" />
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css"
	rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<script charset="utf-8">
			$(function () { 
				$("[data-toggle='tooltip']").tooltip(); 
			});
		</script>
</head>
<body>
        <?php include "ui/header.php" ?>
        <div class="container">
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Política de Privacidad</h4>
					</div>
					<div class="card-body">
						<ul>
							<li>La <i>Política de Privacidad</i> establece las prácticas de
								tratamiento de datos personales de los usuarios que
								voluntariamente se registran en <i>Referencialos</i>. El
								tratamiento de los datos personales consiste en su recolección,
								procesamiento, transferencia y uso.
							</li>
							<li>La <i>Política de Privacidad</i> se aplica a todos los datos
								personales almacenados y procesados en <i>Referencialos</i>
							
							<li>Los usuarios de <i>Referencialos</i> declaran conocer esta <i>Política
									de Privacidad</i> y autorizan el tratamiento de sus datos
								personales.
							</li>
							<li>Los usuarios de datos personales pueden ejercer los
								siguientes derechos:
								<ul>
									<li>Conocer, actualizar y rectificar los datos personales.</li>
									<li>Revocar la autorización o solicitar la eliminación de los
										datos personales de las bases de datos de <i>Referencialos</i>
									</li>
								</ul>
							</li>
							<li><i>Referencialos</i> se reserva el derecho de modificar la <i>Política
									de Privacidad</i> en cualquier momento y sin previo aviso.</li>
							<li>El uso de <i>Referencialos</i> posterior a cualquier
								modificación de la <i>Política de Privacidad</i>, genera la
								aceptación automática del contenido completo de la <i>Política
									de Privacidad</i>.
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

