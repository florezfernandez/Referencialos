<?php
$processed=false;
$error = 0;
$mensaje="";
if(isset($_POST['mensaje'])){
    $mensaje=$_POST['mensaje'];
}

if(isset($_POST['enviar'])){
    if($mensaje == ""){
        $error = 1;
    }else{
    $user_ip = getenv('REMOTE_ADDR');
    $agent = $_SERVER["HTTP_USER_AGENT"];
    $browser = "-";
    if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
        $browser = "Internet Explorer";
    } else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Chrome";
    } else if (preg_match('/Edge\/\d+/', $agent) ) {
        $browser = "Edge";
    } else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Firefox";
    } else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Opera";
    } else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Safari";
    }
    $estudiante = new Estudiante($_SESSION['id']);
    $estudiante -> select();
    $subject="Mensaje para Referencialos";
    $header = array(
        "From" => "Referencialos <contacto@referencialos.org>",
        "Reply-To" => $estudiante -> getNombre(). " " . $estudiante -> getApellido() . "<" . $estudiante -> getCorreo(). ">",
        "Cc" => $estudiante -> getNombre(). " " . $estudiante -> getApellido() . "<" . $estudiante -> getCorreo(). ">",
    );
    $message="El estudiante " . $estudiante -> getNombre() . " " . $estudiante -> getApellido() . " ha enviado el siguiente mensaje:\n";
    $message.=$mensaje . "\n";
    $to = "Referencialos <contacto@referencialos.org>";
    mail($to, $subject, $message, $header);
    $logEstudiante = new LogEstudiante("","Contacto", "Mensaje: " . $mensaje, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
    $logEstudiante -> insert();
    $processed=true;
    }
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Contacto</h4>
				</div>
				<div class="card-body">
				<?php if($processed){ ?>
					    <div class="alert alert-success" >Su mensaje ha sido enviado correctamente. Un administrador de <i>Referencialos</i> le responderá su mensaje.
					    </div>
					<?php } else { if($error == 1){ ?>
					<div class="alert alert-danger" >Por favor ingrese un mensaje. 
					</div>                    
                    <?php } ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("addons/contacto/contacto.php") ?>" class="bootstrap-form needs-validation" enctype="multipart/form-data"  >
						<div class="form-group">
						<label>Mensaje*<br><span class="text-muted"><i>Ingrese un mensaje para Referencialos</i></span></label>
						<textarea class="form-control" id="mensaje" name="mensaje" required><?php echo $mensaje ?></textarea>						
					</div>				
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" required=>
                          <label class="form-check-label">Confirmo que la información que he ingresado es verdadera. En caso contrario, soy consciente que los administradores de <i>Referencialos</i> podrían inhabilitar mi cuenta.	</label>
                        </div>
						<button type="submit" class="btn btn-info" name="enviar">Enviar Mensaje</button>
					</form>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>