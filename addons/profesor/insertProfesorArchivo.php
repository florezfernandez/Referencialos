<?php
$processed=false;
if(isset($_POST['insert'])){
    $user_ip = getenv('REMOTE_ADDR');
    $agent = $_SERVER["HTTP_USER_AGENT"];
    $browser = "-";
    if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
        $browser = "Internet Explorer";
    } else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Chrome";
    } else if (preg_match('/Edge\/\d+/', $agent) ) {
        $browser = "Edge";
    } else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Firefox";
    } else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Opera";
    } else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Safari";
    }    
    $localPath=$_FILES['archivo']['tmp_name'];
    $file = fopen($localPath, "r");
    $cantidadProfesoresAgregados = 0;
    $cantidadProfesoresNoAgregados = 0;
    $profesoresAgregados = "Profesores agregados:<br>";
    $profesoresNoAgregados = "Profesores no agregados:<br>";
    while(($linea = fgets($file)) != null) {
        $datos = explode(";", str_replace("\n", "", $linea));
        $newProfesor = new ProfesorExt("", $datos[1], $datos[2], $datos[0]);
        if(!$newProfesor -> existByCorreo()){
            $newProfesor -> insert();
            $profesoresAgregados .= "Profesor: " . $datos[1] . ", correo: " . $datos[2] . ", genero: " . $datos[0] . "<br>";
            $cantidadProfesoresAgregados++;
        }else{
            $profesoresNoAgregados .= "Profesor: " . $datos[1] . ", correo: " . $datos[2] . ", genero: " . $datos[0] . "<br>";
            $cantidadProfesoresNoAgregados++;
        }        
    }
    if($cantidadProfesoresAgregados > 0){
        $logAdministrador = new LogAdministrador("","Crear Profesor Archivo", "Cantidad: " . $cantidadProfesoresAgregados, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
        $logAdministrador -> insert();
    }    
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Crear Profesor desde Archivo CSV</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ 
					    if($cantidadProfesoresAgregados > 0){
					        echo "<div class='alert alert-success' >" . $profesoresAgregados . "<br>Cantidad: " . $cantidadProfesoresAgregados . "</div>";					        
					    }
					    if($cantidadProfesoresNoAgregados > 0){
					        echo "<div class='alert alert-info' >" . $profesoresNoAgregados . "<br>Cantidad: " . $cantidadProfesoresNoAgregados . "</div>";
					    }
					    
					}?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("addons/profesor/insertProfesorArchivo.php") ?>" class="bootstrap-form needs-validation" enctype="multipart/form-data"  >
						<div class="form-group">
							<label>Archivo CVS (Genero; Profesor; Correo)*</label>
							<input type="file" class="form-control-file" name="archivo" required />
						</div>
						<button type="submit" class="btn btn-info" name="insert">Crear</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
