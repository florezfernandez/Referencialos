<?php
require_once ("addons/profesor/ProfesorDAOExt.php");


class ProfesorExt extends Profesor {
    private $profesorDAOExt;
    
    function __construct($pIdProfesor = "", $pNombre = "", $pCorreo = "", $pGenero = ""){
        parent::__construct($pIdProfesor, $pNombre, $pCorreo, $pGenero);
        $this -> profesorDAOExt = new ProfesorDAOExt($this -> idProfesor, $this -> nombre, $this -> correo, $this -> genero);
        
    }
    
    function existByCorreo(){
        $this -> connection -> open();
        $this -> connection -> run($this -> profesorDAOExt -> existByCorreo());        
        $numRows = $this -> connection -> numRows();
        $this -> connection -> close();
        return $numRows == 1;
    }

    function existByEmail(){
        $this -> connection -> open();
        $this -> connection -> run($this -> profesorDAOExt -> existByEmail());
        $numRows = $this -> connection -> numRows();
        $this -> connection -> close();
        return $numRows == 1;
    }
    function searchMultipleWords($searchTexts){
        $this -> connection -> open();
        $this -> connection -> run($this -> profesorDAOExt -> searchMultipleWords($searchTexts));
        $profesors = array();
        while ($result = $this -> connection -> fetchRow()){
            $genero = new Genero($result[3]);
            $genero -> select();
            array_push($profesors, new Profesor($result[0], $result[1], $result[2], $genero));
        }
        $this -> connection -> close();
        return $profesors;
    }
    
}

?>