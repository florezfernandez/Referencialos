-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 07, 2024 at 12:28 AM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `referencialos`
--

-- --------------------------------------------------------

--
-- Table structure for table `Administrador`
--

CREATE TABLE `Administrador` (
  `idAdministrador` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Administrador`
--

INSERT INTO `Administrador` (`idAdministrador`, `nombre`, `apellido`, `correo`, `clave`, `foto`, `telefono`, `celular`, `estado`) VALUES
(1, 'Hector', 'Florez', 'iti@udistrital.edu.co', '7acf5826ff4e666aec5e01c0f5ac5961', NULL, '123', '123', 1),
(2, 'Dana', 'Macias', 'dsofiamrojasd6419@gmail.com\n', '202cb962ac59075b964b07152d234b70', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `CategoriaDeCriterio`
--

CREATE TABLE `CategoriaDeCriterio` (
  `idCategoriaDeCriterio` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `CategoriaDeCriterio`
--

INSERT INTO `CategoriaDeCriterio` (`idCategoriaDeCriterio`, `nombre`) VALUES
(1, 'Académico'),
(2, 'Social');

-- --------------------------------------------------------

--
-- Table structure for table `Criterio`
--

CREATE TABLE `Criterio` (
  `idCriterio` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `categoriaDeCriterio_idCategoriaDeCriterio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Criterio`
--

INSERT INTO `Criterio` (`idCriterio`, `nombre`, `descripcion`, `categoriaDeCriterio_idCategoriaDeCriterio`) VALUES
(1, 'Dominio del curso', 'El profesor conoce todos los temas del curso', 1),
(2, 'Metodología', 'La forma en que el profesor enseña permite a los estudiantes comprender los temas del curso', 1),
(3, 'Cumplimiento de los temas', 'El profesor explica todos los temas propuestos en el curso', 1),
(4, 'Examenes justos', 'El profesor plantea exámenes solubles que incluyen preguntas de temas presentados previamente al examen', 1),
(5, 'Exigencia', 'El profesor exige a los estudiantes lo adecuado para lograr los objetivos del curso', 1),
(6, 'Amabilidad', 'El profesor se expresa amablemente con los estudiantes', 2),
(7, 'Asistencia', 'El profesor asiste a todas las clases. En caso de no asistir por fuerza mayor, el profesor informa con anticipación y propone la recuperación de la clase', 2),
(8, 'Puntualidad', 'El profesor inicia y termina cada clase a la hora establecida en el horario', 2),
(9, 'Imparcialidad', 'El profesor tiene un trato equitativo con todos los estudiantes', 2),
(10, 'Actitud', 'El profesor es apasinado, motivado, atento, reflexivo, solidario, colaborativo, respetuoso, etc', 2);

-- --------------------------------------------------------

--
-- Table structure for table `Curso`
--

CREATE TABLE `Curso` (
  `idCurso` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Curso`
--

INSERT INTO `Curso` (`idCurso`, `nombre`) VALUES
(1, 'ACCIONAMIENTOS NEUMATICOS-HIDRAULICOS'),
(2, 'ACUEDUCTO Y ALCANTARILLADO'),
(3, 'ADMINISTRACION'),
(4, 'ADQUISICION DE DATOS'),
(5, 'ALGEBRA LINEAL'),
(6, 'ANALISIS DE CIRCUITOS I'),
(7, 'ANALISIS DE CIRCUITOS II'),
(8, 'ANALISIS DE CIRCUITOS III'),
(9, 'ANALISIS DE DATOS'),
(10, 'ANALISIS DE ESTRUCTURAS I'),
(11, 'ANALISIS DE ESTRUCTURAS II'),
(12, 'ANALISIS DE FALLAS Y PROTECCIONES'),
(13, 'ANALISIS DE FOURIER'),
(14, 'ANALISIS DE SISTEMAS'),
(15, 'ANALISIS FINANCIERO'),
(16, 'ANALISIS SOCIAL COLOMBIANO'),
(17, 'ANALISIS Y METODOS NUMERICOS'),
(18, 'ANTENAS Y PROPAGACION'),
(19, 'APLICACIONES COMPUTACIONALES'),
(20, 'APLICACIONES DE LA BIOMASA Y OTRAS FUENTES ALTERNAS'),
(21, 'APLICACIONES DE LA ENERGIA SOLAR FOTOVOLTAICA'),
(22, 'APLICACIONES PARA DISPOSITIVOS DE TELECOMUNICACIONES'),
(23, 'APLICACIONES PARA INTERNET'),
(24, 'ARQUITECTURA DE COMPUTADORES'),
(25, 'ARQUITECTURA DE MICROCONTROLADORES'),
(26, 'ASEGURAMIENTO METROLOGICO'),
(27, 'AUTOMATICA DSC'),
(28, 'AUTOMATICA I'),
(29, 'AUTOMATICA II'),
(30, 'AUTOMATICA III'),
(31, 'AUTOMATISMOS'),
(32, 'AUTOMATIZACION'),
(33, 'AUTOMATIZACION DE MAQUINARIA'),
(34, 'AVANCES EN PROCESOS DE MANUFACTURA'),
(35, 'BASES DE DATOS'),
(36, 'BASES DE DATOS AVANZADAS'),
(37, 'BASES DE DATOS DISTRIBUIDAS'),
(38, 'BASES DE DATOS EN INGENIERIA'),
(39, 'CAD/CAM'),
(40, 'CALCULO DIFERENCIAL'),
(41, 'CALCULO INTEGRAL'),
(42, 'CALCULO MULTIVARIADO'),
(43, 'CAMPOS ELECTROMAGNETICOS'),
(44, 'CATEDRA DE CONTEXTO'),
(45, 'CATEDRA DEMOCRACIA Y CIUDADANIA'),
(46, 'CATEDRA FRANCISCO JOSE DE CALDAS'),
(47, 'CIENCIA TECNOLOGIA Y SOCIEDAD'),
(48, 'CIRCUITOS DIGITALES'),
(49, 'CIRCUITOS DIGITALES I'),
(50, 'CIRCUITOS DIGITALES II'),
(51, 'CIRCUITOS ELECTRICOS I'),
(52, 'CIRCUITOS ELECTRICOS II'),
(53, 'COMPUTACION CUANTICA'),
(54, 'COMUNICACIONES MOVILES'),
(55, 'CONSTRUCCION DE EDIFICACIONES'),
(56, 'CONSTRUCCION DE VIAS Y URBANISMO'),
(57, 'CONSTRUCCION SOSTENIBLE'),
(58, 'CONTABILIDAD'),
(59, 'CONTABILIDAD GENERAL'),
(60, 'CONTROL DE MOVIMIENTO'),
(61, 'CONTROL DE PROCESOS'),
(62, 'CONTROL ESTADISTICO DE CALIDAD'),
(63, 'CONTROL I'),
(64, 'CONTROL II'),
(65, 'CONTROL III'),
(66, 'CONTROL INTELIGENTE'),
(67, 'CONVERSION ELECTROMAGNETICA'),
(68, 'COORDINACION DE AISLAMIENTO'),
(69, 'COSTOS DE PRODUCCIÓN'),
(70, 'COSTOS Y PRESUPUESTOS'),
(71, 'CRIPTOGRAFIA Y SEGURIDAD EN REDES'),
(72, 'CRIPTOLOGIA'),
(73, 'CULTURA Y SOCIEDAD EN AMERICA LATINA'),
(74, 'CULTURAS Y RELIGIONES'),
(75, 'DESARROLLO DE APLICACIONES MULTIPLATAFORMA'),
(76, 'DIBUJO DE ELEMENTOS DE MAQUINAS'),
(77, 'DIBUJO DE TALLER INDUSTRIAL'),
(78, 'DIBUJO TECNICO'),
(79, 'DINAMICA DE MECANISMOS'),
(80, 'DINAMICA ESTRUCTURAL'),
(81, 'DIRECCION DE PRODUCCION'),
(82, 'DISEÑO DE ESPECIFICACION DE PROCESOS DE SOLDADURA'),
(83, 'DISEÑO DE ESTRUCTURAS'),
(84, 'DISEÑO DE ESTRUCTURAS METALICAS'),
(85, 'DISEÑO DE EXPERIMENTOS'),
(86, 'DISEÑO DE INSTALACIONES'),
(87, 'DISEÑO DE INSTALACIONES INDUSTRIALES'),
(88, 'DISEÑO DE MAQUINAS'),
(89, 'DISEÑO DE PROCESOS DE FABRICACION'),
(90, 'DISEÑO DE PROCESOS DE PRODUCCION'),
(91, 'DISEÑO DE PUESTOS DE TRABAJO'),
(92, 'DISEÑO DE RECIPIENTES A PRESION'),
(93, 'DISEÑO DE SISTEMAS DE PUESTA A TIERRA'),
(94, 'DISEÑO DE VEHICULOS IMPULSADOS POR POTENCIA HUMANA'),
(95, 'DISEÑO DIGITAL AVANZADO'),
(96, 'DISEÑO EXPERIMENTAL'),
(97, 'DISEÑO GEOMETRICO DE VIAS'),
(98, 'DISEÑO INDUSTRIAL'),
(99, 'DISEÑO LOGICO'),
(100, 'DISEÑO POR ELEMENTOS FINITOS'),
(101, 'DISEÑO VIAL COMPUTARIZADO'),
(102, 'DISEÑO Y CONSTRUCCION DE CANALES'),
(103, 'DISEÑO Y CONSTRUCCION DE INFRAESTRUCTURA AEROPORTUORIA'),
(104, 'DISEÑO Y CONSTRUCCION DE PAVIMENTOS'),
(105, 'DISEÑO Y PLANEACION DE REDES'),
(106, 'DISPOSITIVOS SEMICONDUCTORES'),
(107, 'ECONOMIA'),
(108, 'ECUACIONES DIFERENCIALES'),
(109, 'EL LENGUAJE DE LA CIENCIA: TALLER DE TEXTO CIENTIFICO'),
(110, 'ELECTIVA SOCIOHUMANISTICA'),
(111, 'ELECTRONICA APLICADA'),
(112, 'ELECTRONICA DE POTENCIA'),
(113, 'ELECTRONICA I'),
(114, 'ELECTRONICA II'),
(115, 'ELECTRONICA INDUSTRIAL'),
(116, 'ELECTRONICA INDUSTRITAL'),
(117, 'ELEMENTOS DE MAQUINAS I'),
(118, 'ELEMENTOS DE MAQUINAS II'),
(119, 'EMPRENDIMIENTO'),
(120, 'EMPRENDIMIENTO EMPRESARIAL'),
(121, 'ENERGIA SOLAR Y MEDIO AMBIENTE'),
(122, 'ENERGIA Y MEDIO AMBIENTE'),
(123, 'ENERGIAS ALTERNATIVAS'),
(124, 'ENERGIAS RENOVABLES'),
(125, 'ESTABILIDAD DE TALUDES'),
(126, 'ESTADISTICA DESCRIPTIVA'),
(127, 'ESTADISTICA Y PROBABILIDAD'),
(128, 'ESTATICA'),
(129, 'ESTRUCTURA DE DATOS'),
(130, 'ESTRUCTURAS HIDRAULICAS'),
(131, 'ETICA Y SOCIEDAD'),
(132, 'EVALUACION DE IMPACTO AMBIENTAL'),
(133, 'EXPRESION GRAFICA'),
(134, 'FISICA DE ONDAS'),
(135, 'FISICA I: MECANICA NEWTONIANA'),
(136, 'FISICA II: ELECTROMAGNETISMO'),
(137, 'FISICA III: ONDAS Y FISICA '),
(138, 'FISICA III: ONDAS Y FISICA MODERNA'),
(139, 'FORMULACION DE PROYECTOS TECNOLOGICOS'),
(140, 'FORMULACION Y EVALUACION DE PROYECTOS'),
(141, 'FUNDACIONES'),
(142, 'FUNDAMENTOS DE MATEMATICAS'),
(143, 'FUNDAMENTOS DE ORGANIZACIÓN'),
(144, 'FUNDAMENTOS DE PROGRAMACION'),
(145, 'FUNDAMENTOS DE SELECCION DE MATERIALES'),
(146, 'FUNDAMENTOS DE TELEMATICA'),
(147, 'GENERACION DE ENERGIA ELECTRICA'),
(148, 'GEOFISICA'),
(149, 'GEOLOGIA'),
(150, 'GEOMETRIA ANALITICA'),
(151, 'GEOMETRIA DESCRIPTIVA'),
(152, 'GEOMETRIA EUCLIDIANA'),
(153, 'GERENCIA Y AUDITORIA EN REDES'),
(154, 'GESTION AMBIENTAL DE LA PRODUCCION'),
(155, 'GESTION DE CALIDAD'),
(156, 'GESTION DE LA CALIDAD'),
(157, 'GESTION DE REDES TELEMATICAS'),
(158, 'GESTION DE SEGURIDAD Y SALUD OCUPACIONAL'),
(159, 'GESTION DE TECNOLOGIA'),
(160, 'GESTION DEL DESARROLLO HUMANO'),
(161, 'GESTION DEL ESPECTRO RADIOELECTRICO'),
(162, 'GESTION HUMANA'),
(163, 'GESTION TECNOLOGICA'),
(164, 'GLOBALIZACION'),
(165, 'HABILIDADES GERENCIALES'),
(166, 'HIDROLOGIA'),
(167, 'HISTORIA Y CULTURA COLOMBIANA'),
(168, 'INDUSTRIA 4.0 APLICADA'),
(169, 'INFORMATICA Y ALGORITMOS'),
(170, 'INFORMATICA Y SOCIEDAD'),
(171, 'INGENIERIA AMBIENTAL'),
(172, 'INGENIERIA DE MANUFACTURA'),
(173, 'INGENIERIA DE SOFTWARE'),
(174, 'INGENIERIA DE TRAFICO'),
(175, 'INGENIERIA DE TRANSITO Y TRANSPORTE'),
(176, 'INGENIERIA DEL AUTOMOVIL'),
(177, 'INGENIERIA ECONOMICA'),
(178, 'INSTALACIONES ELECTRICAS'),
(179, 'INSTRUMENTACION DE PROCESOS I'),
(180, 'INSTRUMENTACION DE PROCESOS II'),
(181, 'INSTRUMENTACION INDUSTRIAL'),
(182, 'INTELIGENCIA ARTIFICIAL'),
(183, 'INTRODUCCION A ALGORITMOS'),
(184, 'INTRODUCCION A LA CEM'),
(185, 'INTRODUCCIÓN A LA ELECTRICIDAD'),
(186, 'INTRODUCCION A LA ELECTRONICA'),
(187, 'INTRODUCCION A LA MECANICA INDUSTRIAL'),
(188, 'INTRODUCCION A LA OPTIMIZACION'),
(189, 'INTRODUCCION A LA PRODUCCION INDUSTRIAL'),
(190, 'INTRODUCCION A LA ROBOTICA'),
(191, 'INTRODUCCION A LAS CONSTRUCCIONES CIVILES'),
(192, 'INTRODUCCION AL CONTROL DE LOS SISTEMAS ROBOTICOS'),
(193, 'INVESTIGACION DE MERCADOS'),
(194, 'INVESTIGACION DE OPERACIONES'),
(195, 'LABORATORIO DE AISLAMIENTO'),
(196, 'LEGISLACION DE TELECOMUNICACIONES'),
(197, 'LEGISLACION E INTERVENTORIA'),
(198, 'LENGUAJE DE PROGRAMACION'),
(199, 'LOGICA MATEMATICA'),
(200, 'LOGICA PROPOSICIONAL'),
(201, 'LOGISTICA INTEGAL'),
(202, 'MANTENIMIENTO DE MAQUINAS'),
(203, 'MANTENIMIENTO DE MAQUINAS INDUSTRIALES'),
(204, 'MANTENIMIENTO INDUSTRIAL'),
(205, 'MANUFACTURA ESBELTA'),
(206, 'MAQUINARIA Y EQUIPOS'),
(207, 'MAQUINAS DE ELEVACION Y TRANSPORTE'),
(208, 'MAQUINAS ELECTRICAS'),
(209, 'MAQUINAS HIDRAULICAS'),
(210, 'MAQUINAS TERMICAS'),
(211, 'MATEMATICAS ESPECIALES'),
(212, 'MATERIALES DE CONSTRUCCION'),
(213, 'MATERIALES INDUSTRIALES'),
(214, 'MATERIALES METALICOS'),
(215, 'MATERIALES POLIMERICOS Y COMPUESTOS'),
(216, 'MECANICA DE FLUIDOS'),
(217, 'MECANICA DE SUELOS'),
(218, 'MEDIDAS ELECTRICAS'),
(219, 'MEDIOS DE COMUNICACIÓN'),
(220, 'MEDIOS DE TRANSMISION'),
(221, 'METODOS DE BUSQUEDA ESTADISTICA'),
(222, 'METODOS DE BUSQUEDA ESTOCASTICA'),
(223, 'METODOS NUMERICOS'),
(224, 'METROLOGIA DIMENSIONAL'),
(225, 'MICROECONOMIA'),
(226, 'MICROONDAS'),
(227, 'MICROREDES Y REDES INTELIGENTES'),
(228, 'MINERIA DE DATOS'),
(229, 'MODELOS DE INTEGRACION LOGISTICA'),
(230, 'MODELOS DETERMINISTICOS DE LA PRODUCCION'),
(231, 'MODELOS ESTOCASTICOS'),
(232, 'MODERNIDAD Y HUMANISMO CIENTIFICO'),
(233, 'MOTORES DE COMBUSTION INTERNA'),
(234, 'MUESTREO Y MEDICION DEL TRABAJO'),
(235, 'NEUMATICA E HIDRAULICA'),
(236, 'NEUMATICA Y ELECTRONEUMATICA'),
(237, 'OPERACIONES Y MANTENIMIENTO DE EQUIPOS INDUSTRIALES'),
(238, 'PARADIGMAS MODERNOS DE LA ADMINISTRACION'),
(239, 'PATOLOGIA DEL CONCRETO'),
(240, 'PENSAMIENTO CIENTIFICO'),
(241, 'PLANEACION DE LA PRODUCCION'),
(242, 'PLANEACION ESTRATEGICA GERENCIAL'),
(243, 'PLANEAMIENTO ENERGETICO'),
(244, 'PLANIFICACION Y DISEÑO DE REDES'),
(245, 'PLANTAS DE TRATAMIENTO DE AGUA POTABLE Y RESIDUAL'),
(246, 'PRINCIPIOS DE ROBOTICA'),
(247, 'PROBABILIDAD Y ESTADISTICA'),
(248, 'PROCESAMIENTO DE IMAGENES MEDICAS'),
(249, 'PROCESAMIENTO DIGITAL DE SEÑALES DE AUDIO Y VIDEO'),
(250, 'PROCESOS DE CONFORMADO'),
(251, 'PROCESOS DE MECANIZADO I'),
(252, 'PROCESOS DE MECANIZADO II'),
(253, 'PROCESOS INDUSTRIALES'),
(254, 'PRODUCCION DE TEXTOS CIENTIFICOS Y ACADEMICOS'),
(255, 'PRODUCCION MAS LIMPIA'),
(256, 'PRODUCCION Y COMPRENSION DE TEXTOS I'),
(257, 'PRODUCCION Y COMPRENSION DE TEXTOS II'),
(258, 'PROGRAMACION'),
(259, 'PROGRAMACION AVANZADA'),
(260, 'PROGRAMACION COSTOS Y PRESUPUESTOS'),
(261, 'PROGRAMACION GRAFICA LABVIEW'),
(262, 'PROGRAMACION II'),
(263, 'PROGRAMACION LINEAL'),
(264, 'PROGRAMACION MULTINIVEL'),
(265, 'PROGRAMACION ORIENTADA A OBJETOS'),
(266, 'PROGRAMACION POR COMPONENTES'),
(267, 'PROGRAMACION WEB'),
(268, 'PROTOTIPOS ELECTRONICOS'),
(269, 'QUIMICA GENERAL'),
(270, 'QUIMICA INDUSTRIAL'),
(271, 'REDES CORPORATIVAS'),
(272, 'REDES DE ALTA VELOCIDAD'),
(273, 'REDES DE COMUNICACIÓN'),
(274, 'REDES DE COMUNICACIONES OPTICAS'),
(275, 'REDES DE CONVERGENCIA'),
(276, 'REDES DE DATOS'),
(277, 'REDES ELECTRICAS'),
(278, 'REDES INALAMBRICAS'),
(279, 'REDES Y AUTOMATIZACION INDUSTRIAL'),
(280, 'REGULACION AMBIENTAL'),
(281, 'RESISTENCIA DE MATERIALES'),
(282, 'ROBOTICA'),
(283, 'SEGUNDA LENGUA I - INGLES'),
(284, 'SEGURIDAD EN REDES'),
(285, 'SEGURIDAD Y SALUD EN EL TRABAJO'),
(286, 'SELECCION DE MATERIALES'),
(287, 'SEMINARIO DE ECOLOGIA'),
(288, 'SEMINARIO DE TELEMATICA'),
(289, 'SEÑALES Y SISTEMAS'),
(290, 'SENSORES Y ACTUADORES'),
(291, 'SERVICIOS TELEMATICOS'),
(292, 'SERVOHIDRAULICA Y SERVONEUMATICA'),
(293, 'SIMULACION'),
(294, 'SIMULACION DE SISTEMAS'),
(295, 'SIMULACION VIRTUAL DE SISTEMAS MECANICOS'),
(296, 'SISTEMAS ABIERTOS'),
(297, 'SISTEMAS DE COMUNICACIONES'),
(298, 'SISTEMAS DE CONTROL'),
(299, 'SISTEMAS DE INFORMACION LOGISTICA'),
(300, 'SISTEMAS DE POTENCIA'),
(301, 'SISTEMAS DINAMICOS'),
(302, 'SISTEMAS DISTRIBUIDOS'),
(303, 'SISTEMAS DNAMICOS Y DE CONTROL'),
(304, 'SISTEMAS FLEXIBLES DE MANUFACTURA'),
(305, 'SISTEMAS OPERACIONALES'),
(306, 'SUBESTACIONES DE POTENCIA'),
(307, 'TALLER DE INVESTIGACION'),
(308, 'TALLER DE INVESTIGACION I'),
(309, 'TALLER DE MECANICA'),
(310, 'TALLER DE SOLDADURA'),
(311, 'TECNICA DE LICITACION'),
(312, 'TECNOCIENCIAS'),
(313, 'TECNOLOGIA DEL CONCRETO'),
(314, 'TECNOLOGIA Y MEDIO AMBIENTE'),
(315, 'TECNOLOGIAS SOBRE IP'),
(316, 'TELECOMUNICACIONES AERONAUTICAS'),
(317, 'TEORIA DE CONTROL'),
(318, 'TEORIA DE CORTE'),
(319, 'TEORIA DE LA DECISION'),
(320, 'TEORIA DE LA INFORMACION'),
(321, 'TEORIA GENERAL DE SISTEMAS'),
(322, 'TEORIA Y LOGICA DE PROGRAMACION'),
(323, 'TERMODINAMICA'),
(324, 'TERMODINAMICA APLICADA'),
(325, 'TERMODINAMICA Y FLUIDOS'),
(326, 'TICS EN LAS ORGANIZACIONES'),
(327, 'TOPOGRAFIA I: PLANIMETRIA'),
(328, 'TOPOGRAFIA II: ALTIMETRIA'),
(329, 'TRABAJO DE GRADO I'),
(330, 'TRABAJO DE GRADO II'),
(331, 'TRABAJO DE GRADO TECNOLOGICO'),
(332, 'TRANSFERENCIA DE CALOR'),
(333, 'TRANSMISION DE DATOS'),
(334, 'TRANSMISION DIGITAL'),
(335, 'TRATAMIENTO DE AGUAS RESIDUALES'),
(336, 'TRATAMIENTO DE RESIDUOS SOLIDOS'),
(337, 'TRATAMIENTOS TERMICOS'),
(338, 'TUBERIAS Y BOMBAS'),
(339, 'VIBRACIONES MECANICAS');

-- --------------------------------------------------------

--
-- Table structure for table `Denuncia`
--

CREATE TABLE `Denuncia` (
  `idDenuncia` int(11) NOT NULL,
  `argumento` text NOT NULL,
  `referencia_idReferencia` int(11) NOT NULL,
  `estudiante_idEstudiante` int(11) NOT NULL,
  `tipoDeDenuncia_idTipoDeDenuncia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Estudiante`
--

CREATE TABLE `Estudiante` (
  `idEstudiante` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `estado` tinyint(4) NOT NULL,
  `periodoDeIngreso` varchar(45) NOT NULL,
  `fechaDeNacimiento` date NOT NULL,
  `fechaDeRegistro` date NOT NULL,
  `genero_idGenero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Facultad`
--

CREATE TABLE `Facultad` (
  `idFacultad` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Facultad`
--

INSERT INTO `Facultad` (`idFacultad`, `nombre`) VALUES
(1, 'Facultad de Artes - ASAB'),
(2, 'Facultad de Ciencias Matemáticas y Naturales'),
(3, 'Facultad de Ciencias y Educación'),
(4, 'Facultad de Ingenieria'),
(5, 'Facultad del Medio Ambiente y Recursos Naturales'),
(6, 'Facultad Tecnológica - Politecnica / Tecnológica');

-- --------------------------------------------------------

--
-- Table structure for table `Genero`
--

CREATE TABLE `Genero` (
  `idGenero` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Genero`
--

INSERT INTO `Genero` (`idGenero`, `nombre`) VALUES
(1, 'Femenino'),
(2, 'Masculino'),
(3, 'Otro');

-- --------------------------------------------------------

--
-- Table structure for table `LogAdministrador`
--

CREATE TABLE `LogAdministrador` (
  `idLogAdministrador` int(11) NOT NULL,
  `accion` varchar(100) NOT NULL,
  `informacion` text NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(45) NOT NULL,
  `so` varchar(45) NOT NULL,
  `explorador` varchar(45) NOT NULL,
  `administrador_idAdministrador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `LogEstudiante`
--

CREATE TABLE `LogEstudiante` (
  `idLogEstudiante` int(11) NOT NULL,
  `accion` varchar(100) NOT NULL,
  `informacion` text NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(45) NOT NULL,
  `so` varchar(45) NOT NULL,
  `explorador` varchar(45) NOT NULL,
  `estudiante_idEstudiante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Profesor`
--

CREATE TABLE `Profesor` (
  `idProfesor` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `genero_idGenero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Profesor`
--

INSERT INTO `Profesor` (`idProfesor`, `nombre`, `correo`, `genero_idGenero`) VALUES
(1, 'ACEVEDO PEREZ JHON VLADIMIR', 'jvacevedop@udistrital.edu.co', 2),
(2, 'ACOSTA SOLARTE PABLO ANDRES', 'paacostas@udistrital.edu.co', 2),
(3, 'AGREDA BASTIDAS ERNESTO', 'eagredab@udistrital.edu.co', 2),
(4, 'AGUDELO CARDENAS ALEXANDER', 'aagudeloc@udistrital.edu.co', 2),
(5, 'ALBA PULIDO RODRIGO', 'ralbap@udistrital.edu.co', 1),
(6, 'ALFEREZ RIVAS LUIS ERNESTO', 'lealferezr@udistrital.edu.co', 2),
(7, 'ALGARRA FAGUA DIANA STELLA', 'dsalgarraf@udistrital.edu.co', 1),
(8, 'ALVARADO MORENO ALEXANDER', 'aalvaradom@udistrital.edu.co', 2),
(9, 'ALVAREZ VIZCAINO NESTOR EDGAR', 'nealvarezv@udistrital.edu.co', 2),
(10, 'ALVIS FONSECA DIEGO ARMANDO', 'daalvisf@udistrital.edu.co', 2),
(11, 'APONTE GUTIERREZ JUAN CARLOS', 'jcaponteg@udistrital.edu.co', 2),
(12, 'ARAUJO OVIEDO CLAUDIA MARINA', 'cmaraujoo@udistrital.edu.co', 1),
(13, 'ARCO MUÑOZ NOE', 'narcom@udistrital.edu.co', 2),
(14, 'ARDILA ISMAEL ANTONIO', 'iaardila@udistrital.edu.co', 2),
(15, 'AREVALO YEISON ANDRES', 'aarevaloy@udistrital.edu.co', 2),
(16, 'ARIAS HENAO CAMILO ANDRES', 'carias@udistrital.edu.co', 2),
(17, 'AVELLANEDA LEAL ROSA MYRIAM', 'rmavellanedal@udistrital.edu.co', 1),
(18, 'AVENDAÑO AVENDAÑO CARLOS ALBERTO', 'caavendanoa@udistrital.edu.co', 2),
(19, 'AVENDAÑO AVENDAÑO EUSEBIO', 'eavendanoa@udistrital.edu.co', 2),
(20, 'AVENDAÑO AVENDAÑO MARY SOL', 'masavendanoa@udistrital.edu.co', 1),
(21, 'BALANTA CASTILLA NEVIS DE JESUS', 'nbalantac@udistrital.edu.co', 1),
(22, 'BAREÑO ROMERO EDICSON FERNEY', 'efbarenor@udistrital.edu.co', 2),
(23, 'BARRAGAN VIZCAYA FLOR ALBA', 'fabarraganb@udistrital.edu.co', 1),
(24, 'BARRANCO JONNY FERNELY', 'jfbarranco@udistrital.edu.co', 2),
(25, 'BARROS SANCHEZ RICARDO DE JESUS', 'rjbarross@udistrital.edu.co', 2),
(26, 'BAUTISTA HERRERA WILLIAM', 'wbautistah@udistrital.edu.co', 2),
(27, 'BECERRA CORREA NELSON REYNALDO', 'nrbecerrac@udistrital.edu.co', 2),
(28, 'BEJARANO BARRETO EDWARD HERNANDO', 'ehbejaranob@udistrital.edu.co', 2),
(29, 'BENAVIDES VEGA OSCAR ENRIQUE', 'oebenavidesv@udistrital.edu.co', 2),
(30, 'BENITEZ SAZA CLAUDIA ROCIO', 'crbenitezs@udistrital.edu.co', 1),
(31, 'BERDUGO ROMERO EDWING OSWALDO', 'eoberdugor@udistrital.edu.co', 2),
(32, 'BERMUDEZ BOHORQUEZ GIOVANNI RODRIGO', 'gbermudez@udistrital.edu.co', 2),
(33, 'BERNAL GARZON EILEEN', 'eibernalg@udistrital.edu.co', 1),
(34, 'BERNAL GOMEZ MIREYA', 'mbernalg@udistrital.edu.co', 1),
(35, 'BLANCO SIERRA FABIAN DARIO', 'fdblancos@udistrital.edu.co', 2),
(36, 'BONILLA ISAZA RUBEN DARIO', 'rdbonillai@udistrital.edu.co', 2),
(37, 'BUENO PINZON MAURICIO', 'mbueno@udistrital.edu.co', 2),
(38, 'BUITRAGO VALERO CARLOS JULIO', 'cjbuitrago@udistrital.edu.co', 2),
(39, 'BULLA PEREIRA EDWIN ALBERTO', 'eabullap@udistrital.edu.co', 2),
(40, 'BURITICA ARBOLEDA CLARA INES', 'ciburiticaa@udistrital.edu.co', 1),
(41, 'CABRALES CONTRERAS HUBER', 'hcabralesc@udistrital.edu.co', 2),
(42, 'CABRERA SANCHEZ JUAN DAVID', 'jdcabreras@udistrital.edu.co', 2),
(43, 'CADENA MUÑOZ ERNESTO', 'ecadenam@udistrital.edu.co', 2),
(44, 'CAICEDO JOSE ALEXANDER', 'jacaicedo@udistrital.edu.co', 2),
(45, 'CAMACHO VELANDIA MARISOL', 'mcamachov@udistrital.edu.co', 1),
(46, 'CAMACHO WILLIAM ALEXANDER', 'wacamacho@udistrital.edu.co', 2),
(47, 'CAMARGO CASALLAS ESPERANZA', 'ecamargoc@udistrital.edu.co', 1),
(48, 'CAÑAS VARON DAVID LEONARDO', 'dlcanasv@udistrital.edu.co', 2),
(49, 'CARANTON VELOZA JHON FREDDY', 'jfcarantonv@udistrital.edu.co', 2),
(50, 'CARDONA PERDOMO LUZ DINED', 'ldcardonap@udistrital.edu.co', 2),
(51, 'CARDOZO CHAUX MARIO JAVIER', 'mjcardozoc@udistrital.edu.co', 2),
(52, 'CARVAJAL MARQUEZ EDILMO', 'ecarvajalm@udistrital.edu.co', 2),
(53, 'CASTANG MONTIEL GERARDO ALBERTO', 'gacastangm@udistrital.edu.co', 2),
(54, 'CASTAÑO ARCILA MAURICIO', 'mcastanoa@udistrital.edu.co', 2),
(55, 'CASTAÑO TAMARA RICARDO', 'rcastanot@udistrital.edu.co', 2),
(56, 'CASTELLANOS MORENO FABIO HERNANDO', 'fcastellanosm@udistrital.edu.co', 2),
(57, 'CASTIBLANCO HERNANDE DIEGO ALFREDO', 'diacastiblancoh@udistrital.edu.co', 2),
(58, 'CASTILLO HERNANDEZ JAIRO ERNESTO', 'jcastillo@udistrital.edu.co', 2),
(59, 'CASTRILLON CAMACHO ARJUNA', 'acastrillonc@udistrital.edu.co', 2),
(60, 'CAVANZO NISO DORIS GUISELA', 'dgcavanzon@udistrital.edu.co', 1),
(61, 'CAVANZO NISSO GLORIA ANDREA', 'gacavanzon@udistrital.edu.co', 1),
(62, 'CELIS MARIN NORVELY', 'ncelism@udistrital.edu.co', 2),
(63, 'CELY CALLEJAS JOSE DAVID', 'jdcely@udistrital.edu.co', 2),
(64, 'CENDALES ROMERO URIAS', 'ucendalesr@udistrital.edu.co', 2),
(65, 'CHACON CEPEDA NORBERTO', 'nchaconc@udistrital.edu.co', 2),
(66, 'CHALA BUSTAMANTE EDGAR ARTURO', 'eachalab@udistrital.edu.co', 2),
(67, 'CHAPARRO ARBOLEDA ARMANDO ALFREDO', 'aachaparroa@udistrital.edu.co', 2),
(68, 'CHAPARRO CHAPARRO FAOLAIN', 'fchaparroc@udistrital.edu.co', 2),
(69, 'CHIQUIZA OCHOA MARIBEL', 'mchiquizao@udistrital.edu.co', 1),
(70, 'CIPAGAUTA LARA ELSY CAROLINA', 'eccipagautal@udistrital.edu.co', 1),
(71, 'CLAVIJO JOYA JULIAN RENE', 'jrclavijoj@udistrital.edu.co', 2),
(72, 'CORONEL SEGURA CESAR AUGUSTO', 'cacoronels@udistrital.edu.co', 2),
(73, 'CORREA MURILLO LUIS HERNANDO', 'lhcorream@udistrital.edu.co', 2),
(74, 'CRUZ GUAYACUNDO WILMER', 'wcruzg@udistrital.edu.co', 2),
(75, 'CRUZ RAMIREZ JOHN ALEXANDER', 'jacruzr@udistrital.edu.co', 2),
(76, 'DAZA TORRES JAVIER ORLANDO', 'jdazat@udistrital.edu.co', 2),
(77, 'DE ARMAS COSTA RICARDO', 'rjdearmasc@udistrital.edu.co', 2),
(78, 'DE LA ROSA SILVA LOURDES CLARISSA', 'lrosas@udistrital.edu.co', 1),
(79, 'DEAZA TRIANA NELSON JAVIER', 'njdeazat@udistrital.edu.co', 2),
(80, 'DELGADILLO GOMEZ EDUARDO ALBERTO', 'eadelgadillog@udistrital.edu.co', 2),
(81, 'DIAZ AREVALO JOSE LUIS', 'jldiaza@udistrital.edu.co', 2),
(82, 'DIAZ MONTAÑO EVELYN IVONNE', 'eidiazm@udistrital.edu.co', 1),
(83, 'DIAZ ORTIZ VICTOR HUGO', 'vdiaz@udistrital.edu.co', 2),
(84, 'DIAZ OSORIO MARCELA', 'mdiazo@udistrital.edu.co', 1),
(85, 'DIAZ OSSA WILMAR ALBERTO', 'wadiazo@udistrital.edu.co', 2),
(86, 'DIAZ RODRIGUEZ MIRTA ROCIO', 'mrdiazr@udistrital.edu.co', 1),
(87, 'DUEÑAS ROJAS JONNY RICARDO', 'jrduenasr@udistrital.edu.co', 2),
(88, 'ESCOBAR DIAZ ANDRES', 'aescobard@udistrital.edu.co', 2),
(89, 'ESLAVA BLANCO HERMES JAVIER', 'hjeslavab@udistrital.edu.co', 2),
(90, 'ESPEJO MOJICA OSCAR GABRIEL', 'ogespejom@udistrital.edu.co', 2),
(91, 'ESQUIVEL RAMIREZ RODRIGO ELIAS', 'reesquivelr@udistrital.edu.co', 2),
(92, 'FANDIÑO JORGE ENRIQUE', 'jefandinot@udistrital.edu.co', 2),
(93, 'FELIZZOLA CONTRERAS RODOLFO', 'rfelizzolac@udistrital.edu.co', 2),
(94, 'FERNANDEZ CASTILLO RAFAEL ENRIQUE', 'refernandezc@udistrital.edu.co', 2),
(95, 'FIERRO CASTAÑO PETER NELSON', 'pfierroc@udistrital.edu.co', 2),
(96, 'FINO SANDOVAL RAFAEL ALBERTO', 'rfinos@udistrital.edu.co', 2),
(97, 'FLOREZ FERNANDEZ HECTOR ARTURO', 'haflorezf@udistrital.edu.co', 2),
(98, 'FONSECA MEDARDO', 'mfonseca@udistrital.edu.co', 2),
(99, 'FONSECA VELASQUEZ ALDEMAR', 'afonseca@udistrital.edu.co', 2),
(100, 'FONSECA VELASQUEZ JIMMY', 'jfonsecav@udistrital.edu.co', 2),
(101, 'FORERO CASALLAS JOHN ALEJANDRO', 'jaforeroc@udistrital.edu.co', 2),
(102, 'FORERO CLAVIJO JORGE ENRIQUE', 'joeforeroc@udistrital.edu.co', 2),
(103, 'FUQUENE ARDILA HECTOR JULIO', 'hfuquene@udistrital.edu.co', 2),
(104, 'GARATEJO ESCOBAR OLGA CECILIA', 'ocgaratejoe@udistrital.edu.co', 1),
(105, 'GARAVITO LEON NELSON EDUARDO', 'negaravitol@udistrital.edu.co', 2),
(106, 'GARCES RENDON HUMBERTO ANTONIO', 'hagarcesr@udistrital.edu.co', 2),
(107, 'GARCIA ARRAZOLA ENRIQUE JOSE', 'ejgarciaa@udistrital.edu.co', 2),
(108, 'GARCIA MATEUS EDICSON GABRIEL', 'eggarciam@udistrital.edu.co', 2),
(109, 'GARCIA QUEVEDO FRANCYA INES', 'figarciaq@udistrital.edu.co', 1),
(110, 'GARZON CARREÑO PABLO EMILIO', 'pegarzonc@udistrital.edu.co', 2),
(111, 'GARZON CORREA MAGDA LORENA', 'mlgarzonc@udistrital.edu.co', 1),
(112, 'GAVILANES JOSE DARIO', 'jdgavilanes@udistrital.edu.co', 2),
(113, 'GIRAL RAMIREZ DIEGO ARMANDO', 'dagiralr@udistrital.edu.co', 2),
(114, 'GIRALDO RAMOS FRANK NIXON', 'fngiraldor@udistrital.edu.co', 2),
(115, 'GOMEZ CASTILLO HARVEY', 'hagomezca@udistrital.edu.co', 2),
(116, 'GOMEZ GOMEZ EDGAR LEONARDO', 'elgomezg@udistrital.edu.co', 2),
(117, 'GOMEZ JIMENEZ CARLOS ARTURO', 'cagomezj@udistrital.edu.co', 2),
(118, 'GOMEZ MORA MILLER', 'mgomezm@udistrital.edu.co', 2),
(119, 'GONZALEZ COLMENARES MAURICIO', 'mgonzalezc@udistrital.edu.co', 2),
(120, 'GONZALEZ MALDONADO GEOVANNY', 'ggonzalezm@udistrital.edu.co', 2),
(121, 'GONZALEZ SILVA RONALD', 'rgonzalezs@udistrital.edu.co', 2),
(122, 'GORDO MUSKUS RICARDO', 'rgordom@udistrital.edu.co', 2),
(123, 'GUASCA GONZALEZ ANDRES GUILLERMO', 'agguascag@udistrital.edu.co', 2),
(124, 'GUERRERO SALAS HUMBERTO', 'hguerreros@udistrital.edu.co', 2),
(125, 'GUEVARA BOLAÑOS JUAN CARLOS', 'jcguevarab@udistrital.edu.co', 2),
(126, 'GUEVARA VELANDIA GERMAN ANTONIO', 'gaguevarav@udistrital.edu.co', 2),
(127, 'GUTIERREZ MONTAÑA RUBEN EDUARDO', 'ruegutierrezm@udistrital.edu.co', 2),
(128, 'GUZMAN LAVERDE JORGE', 'jvguzmanl@udistrital.edu.co', 2),
(129, 'HERNANDEZ BELTRAN LEONARDO ANDRES', 'lahernandezb@udistrital.edu.co', 2),
(130, 'HERNANDEZ GARCIA CLAUDIA LILIANA', 'clhernandezg@udistrital.edu.co', 1),
(131, 'HERNANDEZ GUTIERREZ JAIRO', 'jhernandezg@udistrital.edu.co', 2),
(132, 'HERNANDEZ JAIRO ORLANDO', 'jaohernandez@udistrital.edu.co', 2),
(133, 'HERNANDEZ MARTINEZ HENRY ALBERTO', 'heahernandezm@udistrital.edu.co', 2),
(134, 'HERNANDEZ MORA JOHN RODRIGO', 'jrhernandezm@udistrital.edu.co', 2),
(135, 'HERNANDEZ RODRIGUEZ JORGE EDUARDO', 'jehernandezr@udistrital.edu.co', 2),
(136, 'HERNANDEZ SUAREZ CESAR AUGUSTO', 'cahernandezs@udistrital.edu.co', 2),
(137, 'HERRERA LUIS JORGE', 'ljherrera@udistrital.edu.co', 2),
(138, 'HIGUERA APARICIO JOSE MANUEL', 'jomhigueraa@udistrital.edu.co', 2),
(139, 'HUERTAS MARQUEZ DIEGO FERNANDO', 'dfhuertasm@udistrital.edu.co', 2),
(140, 'HUERTAS SANABRIA JOSE RAFAEL', 'jrhuertass@udistrital.edu.co', 2),
(141, 'HURTADO CORTES LUINI LEONARDO', 'coordinador-otri@udistrital.edu.co', 2),
(142, 'HURTADO MOJICA ROGER ANDERSON', 'rahurtadom@udistrital.edu.co', 2),
(143, 'IBAÑEZ OLAYA HENRY FELIPE', 'hfibanezo@udistrital.edu.co', 2),
(144, 'INFANTE MORENO WILSON', 'winfantem@udistrital.edu.co', 2),
(145, 'JACINTO GOMEZ EDWAR', 'ejacintog@udistrital.edu.co', 2),
(146, 'JAIMES CONTRERAS LUIS ALBERTO', 'lajaimesc@udistrital.edu.co', 2),
(147, 'JARAMILLO VILLAMIZAR LENIN OSWALDO', 'lojaramillov@udistrital.edu.co', 2),
(148, 'JIMENEZ TRIANA ALEXANDER', 'ajimenezt2@udistrital.edu.co', 2),
(149, 'LADINO MORENO EDGAR ORLANDO', 'eoladinom@udistrital.edu.co', 2),
(150, 'LEGUIZAMON PAEZ MIGUEL ANGEL', 'maleguizamonp@udistrital.edu.co', 2),
(151, 'LEON ESPINOSA EDNA ALEJANDRA', 'ealeone@udistrital.edu.co', 1),
(152, 'LEON GARCIA JUAN CARLOS', 'jcleong@udistrital.edu.co', 2),
(153, 'LEYTON VASQUEZ HERNANDO EVELIO', 'heleytonv@udistrital.edu.co', 2),
(154, 'LOPEZ CHACON YERSON ALEJANDRO', 'yalopezc@udistrital.edu.co', 2),
(155, 'LOPEZ GONZALEZ ROSENDO', 'rlopezg@udistrital.edu.co', 2),
(156, 'LOPEZ LEZAMA JUAN CARLOS', 'jclopezl@udistrital.edu.co', 2),
(157, 'LOPEZ MACIAS JAVIER', 'jlopezm@udistrital.edu.co', 2),
(158, 'LOPEZ MARTINEZ GERMAN ARTURO', 'galopezm@udistrital.edu.co', 2),
(159, 'LOPEZ PALOMINO PAULO MARCELO', 'pmlopezp@udistrital.edu.co', 2),
(160, 'LOSADA FIGUEROA CESAR FERNANDO', 'cflosadaf@udistrital.edu.co', 2),
(161, 'LOZADA ROMERO ROXMERY', 'rmlozadar@udistrital.edu.co', 1),
(162, 'LUENGAS CONTRERAS LELY ADRIANA', 'laluengasc@udistrital.edu.co', 1),
(163, 'LUGO GONZALEZ ARMANDO', 'alugog@udistrital.edu.co', 2),
(164, 'LUGO GONZALEZ CARLOS', 'clugog@udistrital.edu.co', 2),
(165, 'LUGO GONZALEZ LUZ MARINA', 'lmlugog@udistrital.edu.co', 1),
(166, 'MADRID SOTO NANCY ESPERANZA', 'nemadrids@udistrital.edu.co', 1),
(167, 'MANJARRES GARCIA GUILLERMO ANTONIO', 'gamanjarresg@udistrital.edu.co', 2),
(168, 'MANRIQUE MUÑOZ ALVARO', 'amanriquem@udistrital.edu.co', 2),
(169, 'MANTILLA BAUTISTA EDGAR JAVIER', 'ejmantillab@udistrital.edu.co', 2),
(170, 'MARTINEZ BUITRAGO DIANA ISABEL', 'dimartinezb@udistrital.edu.co', 1),
(171, 'MARTINEZ CAMARGO DORA MARCELA', 'dmmartinezc@udistrital.edu.co', 1),
(172, 'MARTINEZ GONZALEZ RICARDO', 'rmartinezg@udistrital.edu.co', 2),
(173, 'MARTINEZ RICAURTE OSCAR JAVIER', 'ojmartinezr@udistrital.edu.co', 2),
(174, 'MARTINEZ SANTA FERNANDO', 'fmartinezs@udistrital.edu.co', 2),
(175, 'MARTINEZ SARMIENTO FREDY HERNAN', 'fhmartinezs@udistrital.edu.co', 2),
(176, 'MARULANDA CELEITA JORGE ', 'jamarulandac@udistrital.edu.co', 2),
(177, 'MAYORGA MORATO MANUEL ALFONSO', 'mamayorgam@udistrital.edu.co', 2),
(178, 'MEDINA GAMBA ANDRES FELIPE', 'afmedinag@udistrital.edu.co', 2),
(179, 'MEDINA GONZALEZ MAGDA ', 'mlmedinag@udistrital.edu.co', 1),
(180, 'MEDINA GONZALEZ MAGDA LILIANA', 'mlmedinag@udistrital.edu.co', 1),
(181, 'MEDINA MONROY OSCAR MAURICIO', 'ommedinam@udistrital.edu.co', 2),
(182, 'MEJIA ALVAREZ BIBIANA FARLLEY', 'ifmejiaa@udistrital.edu.co', 1),
(183, 'MEJIA VILLAMIL ANDRES ERNESTO', 'aemejiav@udistrital.edu.co', 2),
(184, 'MENA SERNA MILTON', 'mmenas@udistrital.edu.co', 2),
(185, 'MONCADA ESPITIA ANDRES', 'amoncadae@udistrital.edu.co', 2),
(186, 'MONROY CASTRO JUAN CARLOS', 'jucmonroyc@udistrital.edu.co', 2),
(187, 'MONROY DIAZ JOSE ISRAEL', 'jimonroyd@udistrital.edu.co', 2),
(188, 'MONTANA MESA JORGE ENRIQUE', 'jemontanam@udistrital.edu.co', 2),
(189, 'MONTAÑA QUINTERO NILZON', 'nmontanaq@udistrital.edu.co', 2),
(190, 'MONTEALEGRE MARTINEZ JOSELIN', 'jmontealiegrem@udistrital.edu.co', 2),
(191, 'MONTEZUMA OBANDO GERMAN', 'gmontezumao@udistrital.edu.co', 2),
(192, 'MONTIEL ARIZA HOLMAN', 'hmontiela@udistrital.edu.co', 2),
(193, 'MORENO ACOSTA HENRY', 'hmorenoa@udistrital.edu.co', 2),
(194, 'MORENO FLAUTERO LUIS ALEJANDRO', 'lamorenof@udistrital.edu.co', 2),
(195, 'MORENO HERRERA DIEGO ARMANDO', 'damorenoh@udistrital.edu.co', 2),
(196, 'MORENO HURTADO ALEJANDRO', 'amorenoh@udistrital.edu.co', 2),
(197, 'MORENO PENAGOS CLAUDIA MABEL', 'cmmorenop@udistrital.edu.co', 1),
(198, 'MORENO TORRES CARLOS HUMBERTO', 'chmorenot@udistrital.edu.co', 2),
(199, 'MOSQUERA PALACIOS DARIN JAIRO', 'djmosquerap@udistrital.edu.co', 2),
(200, 'MUÑOZ BELLO NICOLAS GABRIEL', 'ngmunozb@udistrital.edu.co', 2),
(201, 'MURCIA RODRIGUEZ WILLIAM ALEXANDER', 'wamurciar@udistrital.edu.co', 2),
(202, 'MURILLO RONDON FRED GEOVANNY', 'fgmurillor@udistrital.edu.co', 2),
(203, 'NAICIPA OTALORA JAIRO', 'jnaicipao@udistrital.edu.co', 2),
(204, 'NAVARRO MEJIA DAVID RAFAEL', 'drnavarrom@udistrital.edu.co', 2),
(205, 'NAVARRO MEJIA WILMAN ENRIQUE', 'wnavarro@udistrital.edu.co', 2),
(206, 'NIVIA VARGAS ANGELICA MERCEDES', 'amniviav@udistrital.edu.co', 1),
(207, 'NOGUERA VEGA LUIS ANTONIO', 'lanoguerav@udistrital.edu.co', 2),
(208, 'NOVOA ROLDAN KRISTEL SOLANGE', 'ksnovoar@udistrital.edu.co', 1),
(209, 'NOVOA TORRES NORBERTO', 'nnovoat@udistrital.edu.co', 2),
(210, 'OJEDA MARULANDA DAVID', 'dojedam@udistrital.edu.co', 2),
(211, 'OLEA SUAREZ DORIS MARLENE', 'dmoleas@udistrital.edu.co', 1),
(212, 'ORDOÑEZ RODRIGUEZ PAOLA DOLORES', 'pdordonezr@udistrital.edu.co', 1),
(213, 'ORTIZ SUAREZ HELMUT EDGARDO', 'heortizs@udistrital.edu.co', 2),
(214, 'OSPINA USAQUEN MIGUEL ANGEL', 'maospinau@udistrital.edu.co', 2),
(215, 'PALMA VANEGAS NELLY PAOLA', 'nppalmav@udistrital.edu.co', 1),
(216, 'PANTOJA BENAVIDES JAIME FRANCISCO', 'jfpantojab@udistrital.edu.co', 2),
(217, 'PARDO AMAYA DIEGO ALEJANDRO', 'dpardoa@udistrital.edu.co', 2),
(218, 'PARDO HEREDIA ANGELA', 'apardoh@udistrital.edu.co', 1),
(219, 'PARRA PEÑA JAVIER', 'jparrap@udistrital.edu.co', 2),
(220, 'PASTRAN BELTRAN CARLOS GREGORIO', 'cgpastranb@udistrital.edu.co', 2),
(221, 'PASTRAN BELTRAN OSWALDO', 'opastranb@udistrital.edu.co', 2),
(222, 'PATERNINA DURAN JESUS MANUEL', 'jmpaterninad@udistrital.edu.co', 2),
(223, 'PATIÑO BERNAL MARLON', 'marlonpb@udistrital.edu.co', 2),
(224, 'PATIÑO SANCHEZ DANIEL FRANCISCO', 'dfpatinos@udistrital.edu.co', 2),
(225, 'PEDRAZA MARTINEZ LUIS FERNANDO', 'lfpedrazam@udistrital.edu.co', 2),
(226, 'PEÑA TRIANA JEAN YECID', 'jypenat@udistrital.edu.co', 2),
(227, 'PEÑA TRIANA ROSA MARGARITA', 'rmpenat@udistrital.edu.co', 1),
(228, 'PEREZ CARO HERMENT', 'hperezc@udistrital.edu.co', 2),
(229, 'PEREZ MEDINA ELISEO', 'eperezm@udistrital.edu.co', 2),
(230, 'PEREZ PEREZ PEDRO JAVIER', 'pjperezp@udistrital.edu.co', 2),
(231, 'PEREZ SANTOS ALEXANDRA SASHENKA', 'asperezs@udistrital.edu.co', 1),
(232, 'PEREZ TORRES YONATHAN ANDRES', 'yaperezt@udistrital.edu.co', 2),
(233, 'PINEDA JAIMES JORGE ARTURO', 'japinedaj@udistrital.edu.co', 2),
(234, 'PINILLA SUAREZ HECTOR ORLANDO', 'hopinillas@udistrital.edu.co', 2),
(235, 'PINTO CRUZ EDGAR ANTONIO', 'eapintoc@udistrital.edu.co', 2),
(236, 'PINZON LOPEZ HECTOR ALFONSO', 'hapinzonl@udistrital.edu.co', 2),
(237, 'PINZON NUÑEZ SONIA ALEXANDRA', 'spinzon@udistrital.edu.co', 1),
(238, 'PINZON RUEDA MARTHA EDITH', 'mepinzonr@udistrital.edu.co', 1),
(239, 'PINZON RUEDA WILSON ALEXANDER', 'wapinzon@udistrital.edu.co', 2),
(240, 'PORRAS BOADA RICARDO ENRIQUE', 'reporrasb@udistrital.edu.co', 2),
(241, 'PORRAS BOHADA JORGE EDUARDO', 'jeporrasb@udistrital.edu.co', 2),
(242, 'PUENTES JAIRO HERNANDO', 'jhpuentes@udistrital.edu.co', 2),
(243, 'QUINTERO REYES RODRIGO', 'rquinteror@udistrital.edu.co', 2),
(244, 'QUITIAN BUSTOS RUTH MERY', 'rmquitianb@udistrital.edu.co', 1),
(245, 'RAHIM GARZON GLADYS P. ABDEL', 'garahimg@udistrital.edu.co', 1),
(246, 'RAIRAN ANTOLINES JOSE DANILO', 'drairan@udistrital.edu.co', 2),
(247, 'RAMIREZ ACERO JULIO CESAR', 'jcramireza@udistrital.edu.co', 2),
(248, 'RAMIREZ ESCOBAR JORGE FEDERICO', 'jframirez@udistrital.edu.co', 2),
(249, 'RAMIREZ SANCHEZ GLORIA', 'gramirezs@udistrital.edu.co', 1),
(250, 'REYES MOZO JOSE VICENTE', 'jvreyesm@udistrital.edu.co', 2),
(251, 'RIAÑO PULIDO ANDRES JHOVANY', 'ajrianop@udistrital.edu.co', 2),
(252, 'RINCON GUALDRON JOHAN ALEXANDER', 'jarincong@udistrital.edu.co', 2),
(253, 'RIOS MONTOYA JENNY ALEXANDRA', 'jariosm@udistrital.edu.co', 1),
(254, 'RIVERA AGUILAR FREDY ALEXANDER', 'rariveraa@udistrital.edu.co', 2),
(255, 'RIVEROS GOMEZ VICTOR HUGO', 'vhriverosg@udistrital.edu.co', 2),
(256, 'ROCHA CASTELLANOS DAIRO', 'dgrochac@udistrital.edu.co', 2),
(257, 'ROCHA SALAMANCA MAURICIO FERNANDO', 'mfrochas@udistrital.edu.co', 2),
(258, 'RODRIGUEZ ARANGO EMILIANO', 'erodrigueza@udistrital.edu.co', 2),
(259, 'RODRIGUEZ BARRERA MARIO ALBERTO', 'marodriguezb@udistrital.edu.co', 2),
(260, 'RODRIGUEZ FLORIAN ROMAN LEONARDO', 'rlrodriguezf@udistrital.edu.co', 2),
(261, 'RODRIGUEZ GUERRERO ROCIO', 'rrodriguezg@udistrital.edu.co', 2),
(262, 'RODRIGUEZ LEON NAYIVER', 'narodriguezl@udistrital.edu.co', 1),
(263, 'RODRIGUEZ MONDRAGON LUIS FERNANDO', 'luferodriguezm@udistrital.edu.co', 2),
(264, 'RODRIGUEZ MONTANA FERY PATRICIA', 'fprodriguezm@udistrital.edu.co', 1),
(265, 'RODRIGUEZ MONTAÑA NELSON EDUARDO', 'nerodriguezm@udistrital.edu.co', 2),
(266, 'RODRIGUEZ QUINTERO JAVIER GIOVANNY', 'jgrodriguezq@udistrital.edu.co', 2),
(267, 'RODRIGUEZ RINCON BRAHIAN FABIANS', 'bfrodriguezr@udistrital.edu.co', 2),
(268, 'RODRIGUEZ RODRIGUEZ JORGE ENRIQUE', 'jerodriguezr@udistrital.edu.co', 2),
(269, 'RODRIGUEZ TEQUI YUDY MARCELA', 'ymrodriguezt@udistrital.edu.co', 1),
(270, 'ROJAS CASTELLAR LUIS ALEJANDRO', 'larojasc@udistrital.edu.co', 2),
(271, 'ROJAS OBANDO LUIS CARLOS', 'lucrojaso@udistrital.edu.co', 2),
(272, 'ROJAS QUIROGA ANIBAL NICOLAS', 'narojasq@udistrital.edu.co', 2),
(273, 'ROMAN CASTILLO RUTH ESPERANZA', 'rroman@udistrital.edu.co', 2),
(274, 'ROMERO ARIZA CARLOS ANDRES', 'caaromeroa@udistrital.edu.co', 2),
(275, 'ROMERO DUQUE GUSTAVO ANDRES', 'garomerod@udistrital.edu.co', 2),
(276, 'ROMERO GARCIA MARILUZ', 'mromerog@udistrital.edu.co', 1),
(277, 'ROMERO MESTRE HENRY ALFONSO', 'haromerom@udistrital.edu.co', 2),
(278, 'ROMERO RODRIGUEZ JORGE ENRIQUE', 'joeromeror@udistrital.edu.co', 2),
(279, 'ROMERO SUAREZ WILSON LEONARDO', 'wlromeros@udistrital.edu.co', 2),
(280, 'RUA YANEZ LAURA MARCELA', 'lmruay@udistrital.edu.co', 1),
(281, 'RUBIANO CRUZ JONATHAN JAIR', 'jjrubianoc@udistrital.edu.co', 2),
(282, 'RUIZ CAICEDO JAIRO ALFONSO', 'jruiz@udistrital.edu.co', 2),
(283, 'RUSINQUE PADILLA ALEJANDRA MARITZA', 'amrusinquep@udistrital.edu.co', 1),
(284, 'SABY BELTRAN JORGE ENRIQUE', 'jesabyb@udistrital.edu.co', 2),
(285, 'SALAS RUIZ ROBERTO EMILIO', 'resalasr@udistrital.edu.co', 2),
(286, 'SALAZAR GUALDRON JUAN CARLOS', 'jcsalazarg@udistrital.edu.co', 2),
(287, 'SALGADO DURAN OLGA PATRICIA', 'opsalgadod@udistrital.edu.co', 1),
(288, 'SANABRIA VERGARA YOANNY', 'ysanabriav@udistrital.edu.co', 2),
(289, 'SANCHEZ COTTE EDGAR HUMBERTO', 'gi-viasypavimentos@udistrital.edu.co', 2),
(290, 'SANCHEZ GONZALEZ DIEGO', 'dsanchezg@udistrital.edu.co', 2),
(291, 'SEGURA BOLIVAR JOHN ALEXANDER', 'jasegurab@udistrital.edu.co', 2),
(292, 'SERNA DIAZ JOSE LEONARDO', 'jlsernad@udistrital.edu.co', 2),
(293, 'SICACHA ROJAS GERMAN', 'gsicachar@udistrital.edu.co', 2),
(294, 'SOLER CUBIDES FREIL EDUARDO', 'fesolerc@udistrital.edu.co', 2),
(295, 'SUAREZ DIAZ LUIS FRANCISCO', 'lfsuarezd@udistrital.edu.co', 2),
(296, 'SUAREZ SERRANO MONICA YINETTE', 'mysuarezs@udistrital.edu.co', 1),
(297, 'TELLO CASTAÑEDA MARTHA LUCIA', 'mtelloc@udistrital.edu.co', 1),
(298, 'TENORIO BAUTISTA JULIETH ALEXANDRA', 'jatenoriob@udistrital.edu.co', 1),
(299, 'TERREROS CANTOR LUIS ORLANDO', 'loterrerosc@udistrital.edu.co', 2),
(300, 'TOLEDO BALLEN ALFONSO', 'atoledob@udistrital.edu.co', 2),
(301, 'TORRES RIVEROS ALVARO ROGELIO', 'artorresr@udistrital.edu.co', 2),
(302, 'TORRES SUAREZ SERGIO ANDRES', 'satorress@udistrital.edu.co', 2),
(303, 'UBAQUE BRITO KAROL YOBANI', 'kyubaqueb@udistrital.edu.co', 2),
(304, 'ULLOA RODRIGUEZ MANUEL ALFREDO', 'maulloar@udistrital.edu.co', 2),
(305, 'URIBE BECERRA JOSE ERNESTO', 'euribeb@udistrital.edu.co', 2),
(306, 'URIBE SUAREZ GERMAN', 'guribes@udistrital.edu.co', 2),
(307, 'URREGO RIVILLAS LIBIA SUSANA', 'lsurregor@udistrital.edu.co', 1),
(308, 'VACCA GONZALEZ HAROLD', 'hvacca@udistrital.edu.co', 2),
(309, 'VANEGAS CARLOS ALBERTO', 'cavanegas@udistrital.edu.co', 2),
(310, 'VANEGAS PARRA HENRY SAMIR', 'hsvanegasp@udistrital.edu.co', 2),
(311, 'VARGAS HERNANDEZ NAZLY', 'nvargash@udistrital.edu.co', 1),
(312, 'VARGAS MADRID JUAN RAMON', 'jrvargas@udistrital.edu.co', 2),
(313, 'VARGAS SANCHEZ NELSON ARMANDO', 'navargass@udistrital.edu.co', 2),
(314, 'VASQUEZ ARRIETA TOMAS ANTONIO', 'tavasquez@udistrital.edu.co', 2),
(315, 'VELANDIA RODRIGUEZ CARLOS HERBERTY', 'chvelandiar@udistrital.edu.co', 2),
(316, 'VELASCO PEÑA MARCO ANTONIO', 'mavelascop@udistrital.edu.co', 2),
(317, 'VELASQUEZ MOYA XIMENA AUDREY', 'xavelasquezm@udistrital.edu.co', 1),
(318, 'VILLALOBOS MEDINA JUAN DE JESUS', 'jdvillalobosm@udistrital.edu.co', 2),
(319, 'VILLARRAGA RIAÑO ANDRES FELIPE', 'afvillarragar@udistrital.edu.co', 2),
(320, 'VILLOTA POSSO HERNANDO ANTONIO', 'havillotap@udistrital.edu.co', 2),
(321, 'WANUMEN SILVA LUIS FELIPE', 'lwanumen@udistrital.edu.co', 2),
(322, 'WILCHES CAÑON CARLOS FERNANDO', 'cfwilchesc@udistrital.edu.co', 2),
(323, 'ZABALA ALVAREZ JOHN FREDY', 'jfzabalaa@udistrital.edu.co', 2),
(324, 'ZAMBRANO BERRIO REYNALDO', 'rzambranob@udistrital.edu.co', 2),
(325, 'ZAMBRANO CASTRO MARTIN GERMAN', 'mgzambranoc@udistrital.edu.co', 2),
(326, 'ZAMBRANO CAVIEDES JUAN NEPOMUCENO', 'jzambranoc@udistrital.edu.co', 2),
(327, 'ZAMBRANO URBANO HAROLD LEON', 'hlzambranou@udistrital.edu.co', 2),
(328, 'ZAMUDIO HUERTAS EDUARDO', 'ezamudioh@udistrital.edu.co', 2),
(329, 'ZULUAGA ATEHORTUA IVAN DARIO', 'dzulu@udistrital.edu.co', 2);

-- --------------------------------------------------------

--
-- Table structure for table `Programa`
--

CREATE TABLE `Programa` (
  `idPrograma` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `facultad_idFacultad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Programa`
--

INSERT INTO `Programa` (`idPrograma`, `nombre`, `facultad_idFacultad`) VALUES
(1, 'Arte Danzario', 1),
(2, 'Artes Escénicas', 1),
(3, 'Artes Musicales', 1),
(4, 'Artes Plásticas y Visuales', 1),
(5, 'Biología', 2),
(6, 'Física', 2),
(7, 'Matemáticas', 2),
(8, 'Química', 2),
(9, 'Archivística y Gestión de la Información Digital', 3),
(10, 'Comunicación Social y Periodismo', 3),
(11, 'Licenciatura en Biología', 3),
(12, 'Licenciatura en Ciencias Sociales', 3),
(13, 'Licenciatura en Educación Artística', 3),
(14, 'Licenciatura en Educación Infantil', 3),
(15, 'Licenciatura en Física', 3),
(16, 'Licenciatura en Humanidades y Lengua Castellana', 3),
(17, 'Licenciatura en Lenguas Extranjeras con Énfasis en Inglés', 3),
(18, 'Licenciatura en Matemáticas', 3),
(19, 'Licenciatura en Química', 3),
(20, 'Ingeniería Catastral y Geodesia', 4),
(21, 'Ingeniería de Sistemas', 4),
(22, 'Ingeniería Eléctrica', 4),
(23, 'Ingeniería Electrónica', 4),
(24, 'Ingeniería Industrial', 4),
(25, 'Administración Ambiental', 5),
(26, 'Administración Deportiva', 5),
(27, 'Ingeniería Ambiental', 5),
(28, 'Ingeniería Forestal', 5),
(29, 'Ingeniería Sanitaria', 5),
(30, 'Ingenieria Topografica', 5),
(31, 'Tecnología en Gestión Ambiental y Servicios Públicos', 5),
(32, 'Tecnología en Levantamientos Topográficos', 5),
(33, 'Tecnología en Saneamiento Ambiental', 5),
(34, 'Ingeniería Civil (por ciclos propedéuticos)', 6),
(35, 'Ingeniería de Producción (Por ciclos propedéuticos)', 6),
(36, 'Ingeniería Eléctrica (Por ciclos propedéuticos)', 6),
(37, 'Ingeniería en Control y Automatización (por ciclos propedéuticos)', 6),
(38, 'Ingeniería en Telecomunicaciones (por ciclos propedéuticos)', 6),
(39, 'Ingeniería en Telemática (por ciclos propedéuticos)', 6),
(40, 'Ingeniería Mecánica (por ciclos propedéuticos)', 6),
(41, 'Tecnología en Construcciones Civiles (Por ciclos propedéuticos)', 6),
(42, 'Tecnología en Electricidad de Media y Baja Tensión (por ciclos propedéuticos)', 6),
(43, 'Tecnología en Electrónica (Por ciclos propedéuticos)', 6),
(44, 'Tecnología en Electrónica Industrial (Por ciclos propedéuticos)', 6),
(45, 'Tecnología en Gestión de la Producción Industrial (Por ciclos propedéuticos)', 6),
(46, 'Tecnología en Mecánica Industrial (Por ciclos propedéuticos)', 6),
(47, 'Tecnología en Sistematización de Datos (Por ciclos propedéuticos)', 6);

-- --------------------------------------------------------

--
-- Table structure for table `RangoDeNota`
--

CREATE TABLE `RangoDeNota` (
  `idRangoDeNota` int(11) NOT NULL,
  `valor` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `RangoDeNota`
--

INSERT INTO `RangoDeNota` (`idRangoDeNota`, `valor`) VALUES
(1, '50'),
(2, 'Entre 40 y 49'),
(3, 'Entre 30 y 39'),
(4, 'Entre 20 y 29'),
(5, 'Menor a 20');

-- --------------------------------------------------------

--
-- Table structure for table `Referencia`
--

CREATE TABLE `Referencia` (
  `idReferencia` int(11) NOT NULL,
  `comentario` text NOT NULL,
  `pros` text DEFAULT NULL,
  `contras` text DEFAULT NULL,
  `periodo` varchar(45) NOT NULL,
  `fecha` date NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `profesor_idProfesor` int(11) NOT NULL,
  `curso_idCurso` int(11) NOT NULL,
  `estudiante_idEstudiante` int(11) NOT NULL,
  `programa_idPrograma` int(11) NOT NULL,
  `rangoDeNota_idRangoDeNota` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ReferenciaCriterio`
--

CREATE TABLE `ReferenciaCriterio` (
  `idReferenciaCriterio` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `referencia_idReferencia` int(11) NOT NULL,
  `criterio_idCriterio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `TipoDeDenuncia`
--

CREATE TABLE `TipoDeDenuncia` (
  `idTipoDeDenuncia` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Votacion`
--

CREATE TABLE `Votacion` (
  `idVotacion` int(11) NOT NULL,
  `voto` int(11) NOT NULL,
  `referencia_idReferencia` int(11) NOT NULL,
  `estudiante_idEstudiante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Administrador`
--
ALTER TABLE `Administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indexes for table `CategoriaDeCriterio`
--
ALTER TABLE `CategoriaDeCriterio`
  ADD PRIMARY KEY (`idCategoriaDeCriterio`);

--
-- Indexes for table `Criterio`
--
ALTER TABLE `Criterio`
  ADD PRIMARY KEY (`idCriterio`),
  ADD KEY `categoriaDeCriterio_idCategoriaDeCriterio` (`categoriaDeCriterio_idCategoriaDeCriterio`);

--
-- Indexes for table `Curso`
--
ALTER TABLE `Curso`
  ADD PRIMARY KEY (`idCurso`);

--
-- Indexes for table `Denuncia`
--
ALTER TABLE `Denuncia`
  ADD PRIMARY KEY (`idDenuncia`),
  ADD KEY `referencia_idReferencia` (`referencia_idReferencia`),
  ADD KEY `estudiante_idEstudiante` (`estudiante_idEstudiante`),
  ADD KEY `tipoDeDenuncia_idTipoDeDenuncia` (`tipoDeDenuncia_idTipoDeDenuncia`);

--
-- Indexes for table `Estudiante`
--
ALTER TABLE `Estudiante`
  ADD PRIMARY KEY (`idEstudiante`),
  ADD KEY `genero_idGenero` (`genero_idGenero`);

--
-- Indexes for table `Facultad`
--
ALTER TABLE `Facultad`
  ADD PRIMARY KEY (`idFacultad`);

--
-- Indexes for table `Genero`
--
ALTER TABLE `Genero`
  ADD PRIMARY KEY (`idGenero`);

--
-- Indexes for table `LogAdministrador`
--
ALTER TABLE `LogAdministrador`
  ADD PRIMARY KEY (`idLogAdministrador`),
  ADD KEY `administrador_idAdministrador` (`administrador_idAdministrador`);

--
-- Indexes for table `LogEstudiante`
--
ALTER TABLE `LogEstudiante`
  ADD PRIMARY KEY (`idLogEstudiante`),
  ADD KEY `estudiante_idEstudiante` (`estudiante_idEstudiante`);

--
-- Indexes for table `Profesor`
--
ALTER TABLE `Profesor`
  ADD PRIMARY KEY (`idProfesor`),
  ADD KEY `genero_idGenero` (`genero_idGenero`);

--
-- Indexes for table `Programa`
--
ALTER TABLE `Programa`
  ADD PRIMARY KEY (`idPrograma`),
  ADD KEY `facultad_idFacultad` (`facultad_idFacultad`);

--
-- Indexes for table `RangoDeNota`
--
ALTER TABLE `RangoDeNota`
  ADD PRIMARY KEY (`idRangoDeNota`);

--
-- Indexes for table `Referencia`
--
ALTER TABLE `Referencia`
  ADD PRIMARY KEY (`idReferencia`),
  ADD KEY `profesor_idProfesor` (`profesor_idProfesor`),
  ADD KEY `curso_idCurso` (`curso_idCurso`),
  ADD KEY `estudiante_idEstudiante` (`estudiante_idEstudiante`),
  ADD KEY `programa_idPrograma` (`programa_idPrograma`),
  ADD KEY `rangoDeNota_idRangoDeNota` (`rangoDeNota_idRangoDeNota`);

--
-- Indexes for table `ReferenciaCriterio`
--
ALTER TABLE `ReferenciaCriterio`
  ADD PRIMARY KEY (`idReferenciaCriterio`),
  ADD KEY `referencia_idReferencia` (`referencia_idReferencia`),
  ADD KEY `criterio_idCriterio` (`criterio_idCriterio`);

--
-- Indexes for table `TipoDeDenuncia`
--
ALTER TABLE `TipoDeDenuncia`
  ADD PRIMARY KEY (`idTipoDeDenuncia`);

--
-- Indexes for table `Votacion`
--
ALTER TABLE `Votacion`
  ADD PRIMARY KEY (`idVotacion`),
  ADD KEY `referencia_idReferencia` (`referencia_idReferencia`),
  ADD KEY `estudiante_idEstudiante` (`estudiante_idEstudiante`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Administrador`
--
ALTER TABLE `Administrador`
  MODIFY `idAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `CategoriaDeCriterio`
--
ALTER TABLE `CategoriaDeCriterio`
  MODIFY `idCategoriaDeCriterio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Criterio`
--
ALTER TABLE `Criterio`
  MODIFY `idCriterio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `Curso`
--
ALTER TABLE `Curso`
  MODIFY `idCurso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=340;

--
-- AUTO_INCREMENT for table `Denuncia`
--
ALTER TABLE `Denuncia`
  MODIFY `idDenuncia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Estudiante`
--
ALTER TABLE `Estudiante`
  MODIFY `idEstudiante` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Facultad`
--
ALTER TABLE `Facultad`
  MODIFY `idFacultad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `Genero`
--
ALTER TABLE `Genero`
  MODIFY `idGenero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `LogAdministrador`
--
ALTER TABLE `LogAdministrador`
  MODIFY `idLogAdministrador` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `LogEstudiante`
--
ALTER TABLE `LogEstudiante`
  MODIFY `idLogEstudiante` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Profesor`
--
ALTER TABLE `Profesor`
  MODIFY `idProfesor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=330;

--
-- AUTO_INCREMENT for table `Programa`
--
ALTER TABLE `Programa`
  MODIFY `idPrograma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `RangoDeNota`
--
ALTER TABLE `RangoDeNota`
  MODIFY `idRangoDeNota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `Referencia`
--
ALTER TABLE `Referencia`
  MODIFY `idReferencia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ReferenciaCriterio`
--
ALTER TABLE `ReferenciaCriterio`
  MODIFY `idReferenciaCriterio` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `TipoDeDenuncia`
--
ALTER TABLE `TipoDeDenuncia`
  MODIFY `idTipoDeDenuncia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Votacion`
--
ALTER TABLE `Votacion`
  MODIFY `idVotacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Criterio`
--
ALTER TABLE `Criterio`
  ADD CONSTRAINT `Criterio_ibfk_1` FOREIGN KEY (`categoriaDeCriterio_idCategoriaDeCriterio`) REFERENCES `CategoriaDeCriterio` (`idCategoriaDeCriterio`);

--
-- Constraints for table `Denuncia`
--
ALTER TABLE `Denuncia`
  ADD CONSTRAINT `Denuncia_ibfk_1` FOREIGN KEY (`referencia_idReferencia`) REFERENCES `Referencia` (`idReferencia`),
  ADD CONSTRAINT `Denuncia_ibfk_2` FOREIGN KEY (`estudiante_idEstudiante`) REFERENCES `Estudiante` (`idEstudiante`),
  ADD CONSTRAINT `Denuncia_ibfk_3` FOREIGN KEY (`tipoDeDenuncia_idTipoDeDenuncia`) REFERENCES `TipoDeDenuncia` (`idTipoDeDenuncia`);

--
-- Constraints for table `Estudiante`
--
ALTER TABLE `Estudiante`
  ADD CONSTRAINT `Estudiante_ibfk_1` FOREIGN KEY (`genero_idGenero`) REFERENCES `Genero` (`idGenero`);

--
-- Constraints for table `LogAdministrador`
--
ALTER TABLE `LogAdministrador`
  ADD CONSTRAINT `LogAdministrador_ibfk_1` FOREIGN KEY (`administrador_idAdministrador`) REFERENCES `Administrador` (`idAdministrador`);

--
-- Constraints for table `LogEstudiante`
--
ALTER TABLE `LogEstudiante`
  ADD CONSTRAINT `LogEstudiante_ibfk_1` FOREIGN KEY (`estudiante_idEstudiante`) REFERENCES `Estudiante` (`idEstudiante`);

--
-- Constraints for table `Profesor`
--
ALTER TABLE `Profesor`
  ADD CONSTRAINT `Profesor_ibfk_1` FOREIGN KEY (`genero_idGenero`) REFERENCES `Genero` (`idGenero`);

--
-- Constraints for table `Programa`
--
ALTER TABLE `Programa`
  ADD CONSTRAINT `Programa_ibfk_1` FOREIGN KEY (`facultad_idFacultad`) REFERENCES `Facultad` (`idFacultad`);

--
-- Constraints for table `Referencia`
--
ALTER TABLE `Referencia`
  ADD CONSTRAINT `Referencia_ibfk_1` FOREIGN KEY (`profesor_idProfesor`) REFERENCES `Profesor` (`idProfesor`),
  ADD CONSTRAINT `Referencia_ibfk_2` FOREIGN KEY (`curso_idCurso`) REFERENCES `Curso` (`idCurso`),
  ADD CONSTRAINT `Referencia_ibfk_3` FOREIGN KEY (`estudiante_idEstudiante`) REFERENCES `Estudiante` (`idEstudiante`),
  ADD CONSTRAINT `Referencia_ibfk_4` FOREIGN KEY (`programa_idPrograma`) REFERENCES `Programa` (`idPrograma`),
  ADD CONSTRAINT `Referencia_ibfk_5` FOREIGN KEY (`rangoDeNota_idRangoDeNota`) REFERENCES `RangoDeNota` (`idRangoDeNota`);

--
-- Constraints for table `ReferenciaCriterio`
--
ALTER TABLE `ReferenciaCriterio`
  ADD CONSTRAINT `ReferenciaCriterio_ibfk_1` FOREIGN KEY (`referencia_idReferencia`) REFERENCES `Referencia` (`idReferencia`),
  ADD CONSTRAINT `ReferenciaCriterio_ibfk_2` FOREIGN KEY (`criterio_idCriterio`) REFERENCES `Criterio` (`idCriterio`);

--
-- Constraints for table `Votacion`
--
ALTER TABLE `Votacion`
  ADD CONSTRAINT `Votacion_ibfk_1` FOREIGN KEY (`referencia_idReferencia`) REFERENCES `Referencia` (`idReferencia`),
  ADD CONSTRAINT `Votacion_ibfk_2` FOREIGN KEY (`estudiante_idEstudiante`) REFERENCES `Estudiante` (`idEstudiante`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
