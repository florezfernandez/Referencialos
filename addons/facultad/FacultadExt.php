<?php
require_once ("addons/facultad/FacultadDAOExt.php");


class FacultadExt extends Facultad {
    private $facultadDAOExt;
    
    function __construct($pIdFacultad = "", $pNombre = ""){
        parent::__construct($pIdFacultad, $pNombre);
        $this -> facultadDAOExt = new FacultadDAOExt($this -> idFacultad, $this -> nombre);
        
    }
    
    function selectByNombre(){
        $this -> connection -> open();
        $this -> connection -> run($this -> facultadDAOExt -> selectByNombre());        
        $result = $this -> connection -> fetchRow();
        $this -> connection -> close();
        $this -> idFacultad = $result[0];
        $this -> nombre = $result[1];
    }
    
}

?>