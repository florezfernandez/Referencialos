-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 03, 2024 at 05:14 AM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `referencialos`
--

-- --------------------------------------------------------

--
-- Table structure for table `Administrador`
--

CREATE TABLE `Administrador` (
  `idAdministrador` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Administrador`
--

INSERT INTO `Administrador` (`idAdministrador`, `nombre`, `apellido`, `correo`, `clave`, `foto`, `telefono`, `celular`, `estado`) VALUES
(1, 'Hector', 'Florez', 'iti@udistrital.edu.co', '7acf5826ff4e666aec5e01c0f5ac5961', NULL, '123', '123', 1),
(2, 'Dana', 'Macias', 'dsmaciasr@udistrital.edu.co', '202cb962ac59075b964b07152d234b70', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `CategoriaDeCriterio`
--

CREATE TABLE `CategoriaDeCriterio` (
  `idCategoriaDeCriterio` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Criterio`
--

CREATE TABLE `Criterio` (
  `idCriterio` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `categoriaDeCriterio_idCategoriaDeCriterio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Curso`
--

CREATE TABLE `Curso` (
  `idCurso` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Curso`
--

INSERT INTO `Curso` (`idCurso`, `nombre`) VALUES
(1, 'FISICA II: ELECTROMAGNETISMO'),
(2, 'CALCULO MULTIVARIADO'),
(3, 'ECUACIONES DIFERENCIALES'),
(4, 'PROBABILIDAD Y ESTADISTICA'),
(5, 'ADMINISTRACION'),
(6, 'FISICA III: ONDAS Y FISICA MODERNA'),
(7, 'FORMULACION Y EVALUACION DE PROYECTOS'),
(8, 'INGENIERIA ECONOMICA'),
(9, 'TALLER DE INVESTIGACION'),
(10, 'METODOS NUMERICOS'),
(11, 'INGENIERIA AMBIENTAL'),
(12, 'HIDROLOGIA'),
(13, 'ANALISIS DE ESTRUCTURAS I'),
(14, 'EMPRENDIMIENTO EMPRESARIAL'),
(15, 'ANALISIS DE ESTRUCTURAS II'),
(16, 'DISEÑO Y CONSTRUCCION DE PAVIMENTOS'),
(17, 'TUBERIAS Y BOMBAS'),
(18, 'DISEÑO DE ESTRUCTURAS'),
(19, 'DISEÑO DE INSTALACIONES'),
(20, 'DISEÑO Y CONSTRUCCION DE CANALES'),
(21, 'FUNDACIONES'),
(22, 'ACUEDUCTO Y ALCANTARILLADO'),
(23, 'DINAMICA ESTRUCTURAL'),
(24, 'INGENIERIA DE TRANSITO Y TRANSPORTE'),
(25, 'LEGISLACION E INTERVENTORIA'),
(26, 'MAQUINARIA Y EQUIPOS'),
(27, 'GESTION DE SEGURIDAD Y SALUD OCUPACIONAL'),
(28, 'PLANTAS DE TRATAMIENTO DE AGUA POTABLE Y RESIDUAL'),
(29, 'ESTRUCTURAS HIDRAULICAS'),
(30, 'CONSTRUCCION SOSTENIBLE'),
(31, 'GEOFISICA'),
(32, 'TEORIA Y LOGICA DE PROGRAMACION'),
(33, 'PROGRAMACION II'),
(34, 'ESTABILIDAD DE TALUDES'),
(35, 'DISEÑO Y CONSTRUCCION DE INFRAESTRUCTURA AEROPORTUORIA'),
(36, 'TRATAMIENTO DE AGUAS RESIDUALES'),
(37, 'DISEÑO DE PROCESOS DE PRODUCCION'),
(38, 'GESTION DE LA CALIDAD'),
(39, 'AUTOMATIZACION'),
(40, 'TRATAMIENTO DE RESIDUOS SOLIDOS'),
(41, 'MODELOS DETERMINISTICOS DE LA PRODUCCION'),
(42, 'CONTROL DE PROCESOS'),
(43, 'EVALUACION DE IMPACTO AMBIENTAL'),
(44, 'SISTEMAS FLEXIBLES DE MANUFACTURA'),
(45, 'PRODUCCION MAS LIMPIA'),
(46, 'PARADIGMAS MODERNOS DE LA ADMINISTRACION'),
(47, 'DISEÑO DE INSTALACIONES INDUSTRIALES'),
(48, 'MEDIOS DE COMUNICACIÓN'),
(49, 'CULTURAS Y RELIGIONES'),
(50, 'DISEÑO DE EXPERIMENTOS'),
(51, 'MODELOS ESTOCASTICOS'),
(52, 'GESTION TECNOLOGICA'),
(53, 'DISEÑO INDUSTRIAL'),
(54, 'SIMULACION'),
(55, 'DIRECCION DE PRODUCCION'),
(56, 'MODELOS DE INTEGRACION LOGISTICA'),
(57, 'SISTEMAS DE INFORMACION LOGISTICA'),
(58, 'METODOS DE BUSQUEDA ESTADISTICA'),
(59, 'MANUFACTURA ESBELTA'),
(60, 'TEORIA DE LA DECISION'),
(61, 'METODOS DE BUSQUEDA ESTOCASTICA'),
(62, 'HISTORIA Y CULTURA COLOMBIANA'),
(63, 'FISICA III: ONDAS Y FISICA '),
(64, 'DISEÑO DE SISTEMAS DE PUESTA A TIERRA'),
(65, 'SUBESTACIONES DE POTENCIA'),
(66, 'COORDINACION DE AISLAMIENTO'),
(67, 'TRABAJO DE GRADO I'),
(68, 'TRABAJO DE GRADO II'),
(69, 'COSTOS Y PRESUPUESTOS'),
(70, 'REGULACION AMBIENTAL'),
(71, 'CULTURA Y SOCIEDAD EN AMERICA LATINA'),
(72, 'PLANEAMIENTO ENERGETICO'),
(73, 'MICROREDES Y REDES INTELIGENTES'),
(74, 'LABORATORIO DE AISLAMIENTO'),
(75, 'CAMPOS ELECTROMAGNETICOS'),
(76, 'TEORIA DE CONTROL'),
(77, 'MECANICA DE FLUIDOS'),
(78, 'TERMODINAMICA'),
(79, 'ELECTRONICA DE POTENCIA'),
(80, 'ANALISIS DE FALLAS Y PROTECCIONES'),
(81, 'REDES Y AUTOMATIZACION INDUSTRIAL'),
(82, 'GENERACION DE ENERGIA ELECTRICA'),
(83, 'INTRODUCCION A LA CEM'),
(84, 'ENERGIA Y MEDIO AMBIENTE'),
(85, 'BASES DE DATOS EN INGENIERIA'),
(86, 'REDES DE DATOS'),
(87, 'MEDIOS DE TRANSMISION'),
(88, 'REDES DE COMUNICACIONES OPTICAS'),
(89, 'ANTENAS Y PROPAGACION'),
(90, 'CRIPTOGRAFIA Y SEGURIDAD EN REDES'),
(91, 'LEGISLACION DE TELECOMUNICACIONES'),
(92, 'REDES DE CONVERGENCIA'),
(93, 'COMUNICACIONES MOVILES'),
(94, 'TECNOLOGIAS SOBRE IP'),
(95, 'MODERNIDAD Y HUMANISMO CIENTIFICO'),
(96, 'GESTION DE TECNOLOGIA'),
(97, 'APLICACIONES PARA DISPOSITIVOS DE TELECOMUNICACIONES'),
(98, 'FISICA DE ONDAS'),
(99, 'TRANSMISION DIGITAL'),
(100, 'INGENIERIA DE TRAFICO'),
(101, 'TEORIA DE LA INFORMACION'),
(102, 'PROCESAMIENTO DIGITAL DE SEÑALES DE AUDIO Y VIDEO'),
(103, 'REDES INALAMBRICAS'),
(104, 'SERVICIOS TELEMATICOS'),
(105, 'DISEÑO Y PLANEACION DE REDES'),
(106, 'MICROONDAS'),
(107, 'GESTION DEL ESPECTRO RADIOELECTRICO'),
(108, 'TELECOMUNICACIONES AERONAUTICAS'),
(109, 'SISTEMAS DISTRIBUIDOS'),
(110, 'TEORIA GENERAL DE SISTEMAS'),
(111, 'ANALISIS DE FOURIER'),
(112, 'REDES CORPORATIVAS'),
(113, 'SISTEMAS ABIERTOS'),
(114, 'PLANIFICACION Y DISEÑO DE REDES'),
(115, 'REDES DE ALTA VELOCIDAD'),
(116, 'SEGURIDAD EN REDES'),
(117, 'GERENCIA Y AUDITORIA EN REDES'),
(118, 'TECNOCIENCIAS'),
(119, 'COMPUTACION CUANTICA'),
(120, 'CRIPTOLOGIA'),
(121, 'INVESTIGACION DE OPERACIONES'),
(122, 'ANALISIS DE DATOS'),
(123, 'SEMINARIO DE TELEMATICA'),
(124, 'GESTION DE CALIDAD'),
(125, 'GESTION DE REDES TELEMATICAS'),
(126, 'SISTEMAS DNAMICOS Y DE CONTROL'),
(127, 'DISEÑO POR ELEMENTOS FINITOS'),
(128, 'TRANSFERENCIA DE CALOR'),
(129, 'DISEÑO DE MAQUINAS'),
(130, 'MAQUINAS HIDRAULICAS'),
(131, 'MAQUINAS TERMICAS'),
(132, 'EL LENGUAJE DE LA CIENCIA: TALLER DE TEXTO CIENTIFICO'),
(133, 'MOTORES DE COMBUSTION INTERNA'),
(134, 'ASEGURAMIENTO METROLOGICO'),
(135, 'PRODUCCION DE TEXTOS CIENTIFICOS Y ACADEMICOS'),
(136, 'TERMODINAMICA APLICADA'),
(137, 'MANTENIMIENTO DE MAQUINAS INDUSTRIALES'),
(138, 'DISEÑO EXPERIMENTAL'),
(139, 'INGENIERIA DE MANUFACTURA'),
(140, 'DISEÑO DE RECIPIENTES A PRESION'),
(141, 'SELECCION DE MATERIALES'),
(142, 'VIBRACIONES MECANICAS'),
(143, 'TEORIA DE CORTE'),
(144, 'DISEÑO DE ESTRUCTURAS METALICAS'),
(145, 'INGENIERIA DEL AUTOMOVIL'),
(146, 'AVANCES EN PROCESOS DE MANUFACTURA'),
(147, 'AUTOMATIZACION DE MAQUINARIA'),
(148, 'SERVOHIDRAULICA Y SERVONEUMATICA'),
(149, 'APLICACIONES DE LA BIOMASA Y OTRAS FUENTES ALTERNAS'),
(150, 'DISEÑO DE ESPECIFICACION DE PROCESOS DE SOLDADURA'),
(151, 'DISEÑO DE VEHICULOS IMPULSADOS POR POTENCIA HUMANA'),
(152, 'MAQUINAS DE ELEVACION Y TRANSPORTE'),
(153, 'SIMULACION DE SISTEMAS'),
(154, 'TICS EN LAS ORGANIZACIONES'),
(155, 'MATEMATICAS ESPECIALES'),
(156, 'ANALISIS SOCIAL COLOMBIANO'),
(157, 'INSTRUMENTACION DE PROCESOS I'),
(158, 'INSTRUMENTACION DE PROCESOS II'),
(159, 'CONTROL INTELIGENTE'),
(160, 'AUTOMATICA DSC'),
(161, 'ENERGIAS ALTERNATIVAS'),
(162, 'DESARROLLO DE APLICACIONES MULTIPLATAFORMA'),
(163, 'PENSAMIENTO CIENTIFICO'),
(164, 'PROCESAMIENTO DE IMAGENES MEDICAS'),
(165, 'MINERIA DE DATOS'),
(166, 'SEÑALES Y SISTEMAS'),
(167, 'SISTEMAS DINAMICOS'),
(168, 'AUTOMATICA I'),
(169, 'TERMODINAMICA Y FLUIDOS'),
(170, 'CONTROL DE MOVIMIENTO'),
(171, 'SENSORES Y ACTUADORES'),
(172, 'AUTOMATICA II'),
(173, 'CONTROL I'),
(174, 'INSTRUMENTACION INDUSTRIAL'),
(175, 'ROBOTICA'),
(176, 'AUTOMATICA III'),
(177, 'CONTROL II'),
(178, 'CONTROL III'),
(179, 'INDUSTRIA 4.0 APLICADA'),
(180, 'CALCULO DIFERENCIAL'),
(181, 'FISICA I: MECANICA NEWTONIANA'),
(182, 'CATEDRA FRANCISCO JOSE DE CALDAS'),
(183, 'CALCULO INTEGRAL'),
(184, 'ALGEBRA LINEAL'),
(185, 'INTRODUCCION A LAS CONSTRUCCIONES CIVILES'),
(186, 'PRODUCCION Y COMPRENSION DE TEXTOS I'),
(187, 'PRODUCCION Y COMPRENSION DE TEXTOS II'),
(188, 'CIENCIA TECNOLOGIA Y SOCIEDAD'),
(189, 'GEOMETRIA DESCRIPTIVA'),
(190, 'CONTABILIDAD'),
(191, 'ESTATICA'),
(192, 'ETICA Y SOCIEDAD'),
(193, 'MECANICA DE SUELOS'),
(194, 'RESISTENCIA DE MATERIALES'),
(195, 'PROGRAMACION COSTOS Y PRESUPUESTOS'),
(196, 'ELECTIVA SOCIOHUMANISTICA'),
(197, 'GEOMETRIA EUCLIDIANA'),
(198, 'LOGICA PROPOSICIONAL'),
(199, 'GEOMETRIA ANALITICA'),
(200, 'GEOLOGIA'),
(201, 'PLANEACION ESTRATEGICA GERENCIAL'),
(202, 'GESTION DEL DESARROLLO HUMANO'),
(203, 'ESTADISTICA DESCRIPTIVA'),
(204, 'EXPRESION GRAFICA'),
(205, 'FUNDAMENTOS DE MATEMATICAS'),
(206, 'TOPOGRAFIA I: PLANIMETRIA'),
(207, 'TECNOLOGIA DEL CONCRETO'),
(208, 'TOPOGRAFIA II: ALTIMETRIA'),
(209, 'DISEÑO GEOMETRICO DE VIAS'),
(210, 'MATERIALES DE CONSTRUCCION'),
(211, 'SEMINARIO DE ECOLOGIA'),
(212, 'PATOLOGIA DEL CONCRETO'),
(213, 'DISEÑO VIAL COMPUTARIZADO'),
(214, 'CONSTRUCCION DE EDIFICACIONES'),
(215, 'CONSTRUCCION DE VIAS Y URBANISMO'),
(216, 'QUIMICA GENERAL'),
(217, 'TECNICA DE LICITACION, SEGUIMIENTO Y CONTROL DE PROYECTOS DE INGENIERIA'),
(218, 'ELECTRONICA APLICADA'),
(219, 'MAQUINAS ELECTRICAS'),
(220, 'ELECTRONICA INDUSTRIAL'),
(221, 'TALLER DE INVESTIGACION I'),
(222, 'DISEÑO DIGITAL AVANZADO'),
(223, 'ADQUISICION DE DATOS'),
(224, 'ACCIONAMIENTOS NEUMATICOS-HIDRAULICOS'),
(225, 'INVESTIGACION DE MERCADOS'),
(226, 'DIBUJO TECNICO'),
(227, 'INTRODUCCION A LA PRODUCCION INDUSTRIAL'),
(228, 'QUIMICA INDUSTRIAL'),
(229, 'TALLER DE MECANICA'),
(230, 'MUESTREO Y MEDICION DEL TRABAJO'),
(231, 'NEUMATICA E HIDRAULICA'),
(232, 'GESTION HUMANA'),
(233, 'HABILIDADES GERENCIALES'),
(234, 'CAD/CAM'),
(235, 'MANTENIMIENTO INDUSTRIAL'),
(236, 'MICROECONOMIA'),
(237, 'PROCESOS INDUSTRIALES'),
(238, 'GESTION AMBIENTAL DE LA PRODUCCION'),
(239, 'TALLER DE SOLDADURA'),
(240, 'TECNOLOGIA Y MEDIO AMBIENTE'),
(241, 'COSTOS DE PRODUCCIÓN'),
(242, 'SEGURIDAD Y SALUD EN EL TRABAJO'),
(243, 'ESTADISTICA Y PROBABILIDAD'),
(244, 'ANALISIS FINANCIERO'),
(245, 'PLANEACION DE LA PRODUCCION'),
(246, 'DISEÑO DE PUESTOS DE TRABAJO'),
(247, 'CONTROL ESTADISTICO DE CALIDAD'),
(248, 'LOGISTICA INTEGAL'),
(249, 'APLICACIONES COMPUTACIONALES'),
(250, 'PROGRAMACION LINEAL'),
(251, 'MATERIALES INDUSTRIALES'),
(252, 'SEGUNDA LENGUA I - INGLES'),
(253, 'INTRODUCCION A LA MECANICA INDUSTRIAL'),
(254, 'MATERIALES METALICOS'),
(255, 'DIBUJO DE ELEMENTOS DE MAQUINAS'),
(256, 'FUNDAMENTOS DE PROGRAMACION'),
(257, 'MATERIALES POLIMERICOS Y COMPUESTOS'),
(258, 'DIBUJO DE TALLER INDUSTRIAL'),
(259, 'METROLOGIA DIMENSIONAL'),
(260, 'PROCESOS DE MECANIZADO I'),
(261, 'PROCESOS DE MECANIZADO II'),
(262, 'DINAMICA DE MECANISMOS'),
(263, 'MANTENIMIENTO DE MAQUINAS'),
(264, 'PROCESOS DE CONFORMADO'),
(265, 'ELEMENTOS DE MAQUINAS I'),
(266, 'DISEÑO DE PROCESOS DE FABRICACION'),
(267, 'ELEMENTOS DE MAQUINAS II'),
(268, 'INTRODUCCION A LA OPTIMIZACION'),
(269, 'NEUMATICA Y ELECTRONEUMATICA'),
(270, 'FUNDAMENTOS DE SELECCION DE MATERIALES'),
(271, 'APLICACIONES DE LA ENERGIA SOLAR FOTOVOLTAICA'),
(272, 'SIMULACION VIRTUAL DE SISTEMAS MECANICOS'),
(273, 'TRATAMIENTOS TERMICOS'),
(274, 'ENERGIAS RENOVABLES'),
(275, 'INTRODUCCION A LA ROBOTICA'),
(276, 'CATEDRA DEMOCRACIA Y CIUDADANIA'),
(277, 'EMPRENDIMIENTO'),
(278, 'CONTABILIDAD GENERAL'),
(279, 'INTRODUCCION A ALGORITMOS'),
(280, 'LOGICA MATEMATICA'),
(281, 'FUNDAMENTOS DE ORGANIZACIÓN'),
(282, 'ESTRUCTURA DE DATOS'),
(283, 'PROGRAMACION ORIENTADA A OBJETOS'),
(284, 'ANALISIS Y METODOS NUMERICOS'),
(285, 'PROGRAMACION MULTINIVEL'),
(286, 'BASES DE DATOS'),
(287, 'PROGRAMACION AVANZADA'),
(288, 'BASES DE DATOS AVANZADAS'),
(289, 'DISEÑO LOGICO'),
(290, 'ANALISIS DE SISTEMAS'),
(291, 'INTELIGENCIA ARTIFICIAL'),
(292, 'SISTEMAS OPERACIONALES'),
(293, 'TRANSMISION DE DATOS'),
(294, 'PROGRAMACION WEB'),
(295, 'INGENIERIA DE SOFTWARE'),
(296, 'ARQUITECTURA DE COMPUTADORES'),
(297, 'GLOBALIZACION'),
(298, 'FUNDAMENTOS DE TELEMATICA'),
(299, 'PROGRAMACION POR COMPONENTES'),
(300, 'APLICACIONES PARA INTERNET'),
(301, 'BASES DE DATOS DISTRIBUIDAS'),
(302, 'INFORMATICA Y SOCIEDAD'),
(303, 'PROGRAMACION'),
(304, 'PRINCIPIOS DE ROBOTICA'),
(305, 'ECONOMIA'),
(306, 'SISTEMAS DE POTENCIA'),
(307, 'DISPOSITIVOS SEMICONDUCTORES'),
(308, 'MEDIDAS ELECTRICAS'),
(309, 'REDES ELECTRICAS'),
(310, 'ARQUITECTURA DE MICROCONTROLADORES'),
(311, 'ANALISIS DE CIRCUITOS I'),
(312, 'ANALISIS DE CIRCUITOS II'),
(313, 'CONVERSION ELECTROMAGNETICA'),
(314, 'ANALISIS DE CIRCUITOS III'),
(315, 'CIRCUITOS DIGITALES'),
(316, 'INSTALACIONES ELECTRICAS'),
(317, 'FORMULACION DE PROYECTOS TECNOLOGICOS'),
(318, 'AUTOMATISMOS'),
(319, 'INTRODUCCIÓN A LA ELECTRICIDAD'),
(320, 'CATEDRA DE CONTEXTO'),
(321, 'INFORMATICA Y ALGORITMOS'),
(322, 'INTRODUCCION A LA ELECTRONICA'),
(323, 'LENGUAJE DE PROGRAMACION'),
(324, 'CIRCUITOS DIGITALES II'),
(325, 'ELECTRONICA II'),
(326, 'TRABAJO DE GRADO TECNOLOGICO'),
(327, 'ENERGIA SOLAR Y MEDIO AMBIENTE'),
(328, 'REDES DE COMUNICACIÓN'),
(329, 'INTRODUCCION AL CONTROL DE LOS SISTEMAS ROBOTICOS'),
(330, 'CIRCUITOS ELECTRICOS I'),
(331, 'CIRCUITOS ELECTRICOS II'),
(332, 'ELECTRONICA I'),
(333, 'CIRCUITOS DIGITALES I'),
(334, 'OPERACIONES Y MANTENIMIENTO DE EQUIPOS INDUSTRIALES'),
(335, 'ELECTRONICA INDUSTRITAL'),
(336, 'SISTEMAS DE CONTROL'),
(337, 'SISTEMAS DE COMUNICACIONES'),
(338, 'PROGRAMACION GRAFICA LABVIEW'),
(339, 'PROTOTIPOS ELECTRONICOS');

-- --------------------------------------------------------

--
-- Table structure for table `Denuncia`
--

CREATE TABLE `Denuncia` (
  `idDenuncia` int(11) NOT NULL,
  `argumento` text NOT NULL,
  `referencia_idReferencia` int(11) NOT NULL,
  `estudiante_idEstudiante` int(11) NOT NULL,
  `tipoDeDenuncia_idTipoDeDenuncia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Estudiante`
--

CREATE TABLE `Estudiante` (
  `idEstudiante` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `estado` tinyint(4) NOT NULL,
  `periodoDeIngreso` varchar(45) NOT NULL,
  `fechaDeNacimiento` date NOT NULL,
  `fechaDeRegistro` date NOT NULL,
  `genero_idGenero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Facultad`
--

CREATE TABLE `Facultad` (
  `idFacultad` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Facultad`
--

INSERT INTO `Facultad` (`idFacultad`, `nombre`) VALUES
(1, 'Facultad de Artes - ASAB'),
(2, 'Facultad de Ciencias Matemáticas y Naturales'),
(3, 'Facultad de Ciencias y Educación'),
(4, 'Facultad de Ingenieria'),
(5, 'Facultad del Medio Ambiente y Recursos Naturales'),
(6, 'Facultad Tecnológica - Politecnica / Tecnológica');

-- --------------------------------------------------------

--
-- Table structure for table `Genero`
--

CREATE TABLE `Genero` (
  `idGenero` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Genero`
--

INSERT INTO `Genero` (`idGenero`, `nombre`) VALUES
(1, 'Femenino'),
(2, 'Masculino'),
(3, 'Otro');

-- --------------------------------------------------------

--
-- Table structure for table `LogAdministrador`
--

CREATE TABLE `LogAdministrador` (
  `idLogAdministrador` int(11) NOT NULL,
  `accion` varchar(100) NOT NULL,
  `informacion` text NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(45) NOT NULL,
  `so` varchar(45) NOT NULL,
  `explorador` varchar(45) NOT NULL,
  `administrador_idAdministrador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `LogAdministrador`
--

INSERT INTO `LogAdministrador` (`idLogAdministrador`, `accion`, `informacion`, `fecha`, `hora`, `ip`, `so`, `explorador`, `administrador_idAdministrador`) VALUES
(1, 'Log In', '', '2024-05-02', '21:22:39', '::1', 'Linux', 'Chrome', 1),
(2, 'Crear Profesor Archivo', 'Nombre: CAICEDO JOSE ALEXANDER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(3, 'Crear Profesor Archivo', 'Nombre: OJEDA MARULANDA DAVID; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(4, 'Crear Profesor Archivo', 'Nombre: RIAÑO PULIDO ANDRES JHOVANY; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(5, 'Crear Profesor Archivo', 'Nombre: GARATEJO ESCOBAR OLGA CECILIA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(6, 'Crear Profesor Archivo', 'Nombre: GARCIA ARRAZOLA ENRIQUE JOSE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(7, 'Crear Profesor Archivo', 'Nombre: MEJIA ALVAREZ BIBIANA FARLLEY; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(8, 'Crear Profesor Archivo', 'Nombre: HURTADO MOJICA ROGER ANDERSON; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(9, 'Crear Profesor Archivo', 'Nombre: CIPAGAUTA LARA ELSY CAROLINA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(10, 'Crear Profesor Archivo', 'Nombre: SANCHEZ COTTE EDGAR HUMBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(11, 'Crear Profesor Archivo', 'Nombre: SABY BELTRAN JORGE ENRIQUE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(12, 'Crear Profesor Archivo', 'Nombre: MEDINA GONZALEZ MAGDA ; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(13, 'Crear Profesor Archivo', 'Nombre: SEGURA BOLIVAR JOHN ALEXANDER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(14, 'Crear Profesor Archivo', 'Nombre: BARRANCO JONNY FERNELY; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(15, 'Crear Profesor Archivo', 'Nombre: FELIZZOLA CONTRERAS RODOLFO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(16, 'Crear Profesor Archivo', 'Nombre: AREVALO YEISON ANDRES; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(17, 'Crear Profesor Archivo', 'Nombre: ZAMBRANO URBANO HAROLD LEON; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(18, 'Crear Profesor Archivo', 'Nombre: PATIÑO SANCHEZ DANIEL FRANCISCO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(19, 'Crear Profesor Archivo', 'Nombre: LADINO MORENO EDGAR ORLANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(20, 'Crear Profesor Archivo', 'Nombre: DIAZ AREVALO JOSE LUIS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(21, 'Crear Profesor Archivo', 'Nombre: TERREROS CANTOR LUIS ORLANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(22, 'Crear Profesor Archivo', 'Nombre: ZAMUDIO HUERTAS EDUARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(23, 'Crear Profesor Archivo', 'Nombre: LOPEZ PALOMINO PAULO MARCELO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(24, 'Crear Profesor Archivo', 'Nombre: CARDOZO CHAUX MARIO JAVIER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(25, 'Crear Profesor Archivo', 'Nombre: ALVIS FONSECA DIEGO ARMANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(26, 'Crear Profesor Archivo', 'Nombre: VILLALOBOS MEDINA JUAN DE JESUS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(27, 'Crear Profesor Archivo', 'Nombre: LOSADA FIGUEROA CESAR FERNANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(28, 'Crear Profesor Archivo', 'Nombre: ALGARRA FAGUA DIANA STELLA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(29, 'Crear Profesor Archivo', 'Nombre: VARGAS MADRID JUAN RAMON; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(30, 'Crear Profesor Archivo', 'Nombre: ROCHA SALAMANCA MAURICIO FERNANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(31, 'Crear Profesor Archivo', 'Nombre: GOMEZ JIMENEZ CARLOS ARTURO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(32, 'Crear Profesor Archivo', 'Nombre: MONTAÑA QUINTERO NILZON; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(33, 'Crear Profesor Archivo', 'Nombre: LEON GARCIA JUAN CARLOS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(34, 'Crear Profesor Archivo', 'Nombre: HERRERA LUIS JORGE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(35, 'Crear Profesor Archivo', 'Nombre: CHACON CEPEDA NORBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(36, 'Crear Profesor Archivo', 'Nombre: ROMAN CASTILLO RUTH ESPERANZA; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(37, 'Crear Profesor Archivo', 'Nombre: MARULANDA CELEITA JORGE ; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(38, 'Crear Profesor Archivo', 'Nombre: SOLER CUBIDES FREIL EDUARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(39, 'Crear Profesor Archivo', 'Nombre: LOPEZ LEZAMA JUAN CARLOS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(40, 'Crear Profesor Archivo', 'Nombre: RINCON GUALDRON JOHAN ALEXANDER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(41, 'Crear Profesor Archivo', 'Nombre: RODRIGUEZ MONDRAGON LUIS FERNANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(42, 'Crear Profesor Archivo', 'Nombre: RIVEROS GOMEZ VICTOR HUGO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(43, 'Crear Profesor Archivo', 'Nombre: CARDONA PERDOMO LUZ DINED; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(44, 'Crear Profesor Archivo', 'Nombre: HERNANDEZ BELTRAN LEONARDO ANDRES; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(45, 'Crear Profesor Archivo', 'Nombre: BONILLA ISAZA RUBEN DARIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(46, 'Crear Profesor Archivo', 'Nombre: BEJARANO BARRETO EDWARD HERNANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(47, 'Crear Profesor Archivo', 'Nombre: ROMERO SUAREZ WILSON LEONARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(48, 'Crear Profesor Archivo', 'Nombre: GUERRERO SALAS HUMBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(49, 'Crear Profesor Archivo', 'Nombre: RODRIGUEZ FLORIAN ROMAN LEONARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(50, 'Crear Profesor Archivo', 'Nombre: PINZON RUEDA MARTHA EDITH; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(51, 'Crear Profesor Archivo', 'Nombre: MAYORGA MORATO MANUEL ALFONSO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(52, 'Crear Profesor Archivo', 'Nombre: SANCHEZ GONZALEZ DIEGO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(53, 'Crear Profesor Archivo', 'Nombre: TOLEDO BALLEN ALFONSO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(54, 'Crear Profesor Archivo', 'Nombre: LUGO GONZALEZ CARLOS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(55, 'Crear Profesor Archivo', 'Nombre: ROJAS OBANDO LUIS CARLOS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(56, 'Crear Profesor Archivo', 'Nombre: PARRA PEÑA JAVIER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(57, 'Crear Profesor Archivo', 'Nombre: GARZON CARREÑO PABLO EMILIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(58, 'Crear Profesor Archivo', 'Nombre: ROMERO DUQUE GUSTAVO ANDRES; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(59, 'Crear Profesor Archivo', 'Nombre: PINZON RUEDA WILSON ALEXANDER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(60, 'Crear Profesor Archivo', 'Nombre: SUAREZ SERRANO MONICA YINETTE; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(61, 'Crear Profesor Archivo', 'Nombre: OSPINA USAQUEN MIGUEL ANGEL; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(62, 'Crear Profesor Archivo', 'Nombre: NAICIPA OTALORA JAIRO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(63, 'Crear Profesor Archivo', 'Nombre: PINTO CRUZ EDGAR ANTONIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(64, 'Crear Profesor Archivo', 'Nombre: GARAVITO LEON NELSON EDUARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(65, 'Crear Profesor Archivo', 'Nombre: AVENDAÑO AVENDAÑO MARY SOL; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(66, 'Crear Profesor Archivo', 'Nombre: DIAZ OSSA WILMAR ALBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(67, 'Crear Profesor Archivo', 'Nombre: RODRIGUEZ RINCON BRAHIAN FABIANS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(68, 'Crear Profesor Archivo', 'Nombre: RAHIM GARZON GLADYS P. ABDEL; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(69, 'Crear Profesor Archivo', 'Nombre: AVENDAÑO AVENDAÑO CARLOS ALBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(70, 'Crear Profesor Archivo', 'Nombre: PUENTES JAIRO HERNANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(71, 'Crear Profesor Archivo', 'Nombre: PEÑA TRIANA ROSA MARGARITA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(72, 'Crear Profesor Archivo', 'Nombre: RAMIREZ SANCHEZ GLORIA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(73, 'Crear Profesor Archivo', 'Nombre: APONTE GUTIERREZ JUAN CARLOS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(74, 'Crear Profesor Archivo', 'Nombre: BURITICA ARBOLEDA CLARA INES; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(75, 'Crear Profesor Archivo', 'Nombre: IBAÑEZ OLAYA HENRY FELIPE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(76, 'Crear Profesor Archivo', 'Nombre: RODRIGUEZ BARRERA MARIO ALBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(77, 'Crear Profesor Archivo', 'Nombre: MARTINEZ SANTA FERNANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(78, 'Crear Profesor Archivo', 'Nombre: MARTINEZ SARMIENTO FREDY HERNAN; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(79, 'Crear Profesor Archivo', 'Nombre: GIRAL RAMIREZ DIEGO ARMANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(80, 'Crear Profesor Archivo', 'Nombre: NOGUERA VEGA LUIS ANTONIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(81, 'Crear Profesor Archivo', 'Nombre: RUBIANO CRUZ JONATHAN JAIR; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(82, 'Crear Profesor Archivo', 'Nombre: HERNANDEZ MARTINEZ HENRY ALBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(83, 'Crear Profesor Archivo', 'Nombre: ROMERO MESTRE HENRY ALFONSO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(84, 'Crear Profesor Archivo', 'Nombre: ESPEJO MOJICA OSCAR GABRIEL; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(85, 'Crear Profesor Archivo', 'Nombre: MANTILLA BAUTISTA EDGAR JAVIER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(86, 'Crear Profesor Archivo', 'Nombre: CADENA MUÑOZ ERNESTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(87, 'Crear Profesor Archivo', 'Nombre: PEDRAZA MARTINEZ LUIS FERNANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(88, 'Crear Profesor Archivo', 'Nombre: MARTINEZ RICAURTE OSCAR JAVIER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(89, 'Crear Profesor Archivo', 'Nombre: ROJAS CASTELLAR LUIS ALEJANDRO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(90, 'Crear Profesor Archivo', 'Nombre: CORONEL SEGURA CESAR AUGUSTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(91, 'Crear Profesor Archivo', 'Nombre: MEDINA MONROY OSCAR MAURICIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(92, 'Crear Profesor Archivo', 'Nombre: ESLAVA BLANCO HERMES JAVIER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(93, 'Crear Profesor Archivo', 'Nombre: GOMEZ GOMEZ EDGAR LEONARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(94, 'Crear Profesor Archivo', 'Nombre: CELY CALLEJAS JOSE DAVID; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(95, 'Crear Profesor Archivo', 'Nombre: MORENO HERRERA DIEGO ARMANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(96, 'Crear Profesor Archivo', 'Nombre: BLANCO SIERRA FABIAN DARIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(97, 'Crear Profesor Archivo', 'Nombre: ZABALA ALVAREZ JOHN FREDY; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(98, 'Crear Profesor Archivo', 'Nombre: PATERNINA DURAN JESUS MANUEL; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(99, 'Crear Profesor Archivo', 'Nombre: BERDUGO ROMERO EDWING OSWALDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(100, 'Crear Profesor Archivo', 'Nombre: MONTEZUMA OBANDO GERMAN; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(101, 'Crear Profesor Archivo', 'Nombre: LEGUIZAMON PAEZ MIGUEL ANGEL; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(102, 'Crear Profesor Archivo', 'Nombre: ARDILA ISMAEL ANTONIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(103, 'Crear Profesor Archivo', 'Nombre: DE ARMAS COSTA RICARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(104, 'Crear Profesor Archivo', 'Nombre: MONCADA ESPITIA ANDRES; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(105, 'Crear Profesor Archivo', 'Nombre: DAZA TORRES JAVIER ORLANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(106, 'Crear Profesor Archivo', 'Nombre: CASTANG MONTIEL GERARDO ALBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(107, 'Crear Profesor Archivo', 'Nombre: HUERTAS MARQUEZ DIEGO FERNANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(108, 'Crear Profesor Archivo', 'Nombre: CASTAÑO TAMARA RICARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(109, 'Crear Profesor Archivo', 'Nombre: BENAVIDES VEGA OSCAR ENRIQUE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(110, 'Crear Profesor Archivo', 'Nombre: MEJIA VILLAMIL ANDRES ERNESTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(111, 'Crear Profesor Archivo', 'Nombre: HERNANDEZ RODRIGUEZ JORGE EDUARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(112, 'Crear Profesor Archivo', 'Nombre: PATIÑO BERNAL MARLON; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(113, 'Crear Profesor Archivo', 'Nombre: PEREZ TORRES YONATHAN ANDRES; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(114, 'Crear Profesor Archivo', 'Nombre: DEAZA TRIANA NELSON JAVIER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(115, 'Crear Profesor Archivo', 'Nombre: AGREDA BASTIDAS ERNESTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(116, 'Crear Profesor Archivo', 'Nombre: WILCHES CAÑON CARLOS FERNANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(117, 'Crear Profesor Archivo', 'Nombre: NIVIA VARGAS ANGELICA MERCEDES; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(118, 'Crear Profesor Archivo', 'Nombre: CABRALES CONTRERAS HUBER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(119, 'Crear Profesor Archivo', 'Nombre: GARZON CORREA MAGDA LORENA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(120, 'Crear Profesor Archivo', 'Nombre: CRUZ GUAYACUNDO WILMER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(121, 'Crear Profesor Archivo', 'Nombre: MUÑOZ BELLO NICOLAS GABRIEL; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(122, 'Crear Profesor Archivo', 'Nombre: LOPEZ MARTINEZ GERMAN ARTURO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(123, 'Crear Profesor Archivo', 'Nombre: ROMERO ARIZA CARLOS ANDRES; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(124, 'Crear Profesor Archivo', 'Nombre: SUAREZ DIAZ LUIS FRANCISCO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(125, 'Crear Profesor Archivo', 'Nombre: CABRERA SANCHEZ JUAN DAVID; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:36', '::1', 'Linux', 'Chrome', 1),
(126, 'Crear Profesor Archivo', 'Nombre: PARDO AMAYA DIEGO ALEJANDRO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(127, 'Crear Profesor Archivo', 'Nombre: GONZALEZ COLMENARES MAURICIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(128, 'Crear Profesor Archivo', 'Nombre: ALVARADO MORENO ALEXANDER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(129, 'Crear Profesor Archivo', 'Nombre: PEREZ PEREZ PEDRO JAVIER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(130, 'Crear Profesor Archivo', 'Nombre: RODRIGUEZ ARANGO EMILIANO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(131, 'Crear Profesor Archivo', 'Nombre: VELASCO PEÑA MARCO ANTONIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(132, 'Crear Profesor Archivo', 'Nombre: CHALA BUSTAMANTE EDGAR ARTURO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(133, 'Crear Profesor Archivo', 'Nombre: CASTIBLANCO HERNANDE DIEGO ALFREDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(134, 'Crear Profesor Archivo', 'Nombre: MEDINA GAMBA ANDRES FELIPE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(135, 'Crear Profesor Archivo', 'Nombre: PORRAS BOADA RICARDO ENRIQUE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(136, 'Crear Profesor Archivo', 'Nombre: GUASCA GONZALEZ ANDRES GUILLERMO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(137, 'Crear Profesor Archivo', 'Nombre: RODRIGUEZ QUINTERO JAVIER GIOVANNY; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(138, 'Crear Profesor Archivo', 'Nombre: HERNANDEZ GUTIERREZ JAIRO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(139, 'Crear Profesor Archivo', 'Nombre: VACCA GONZALEZ HAROLD; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(140, 'Crear Profesor Archivo', 'Nombre: PANTOJA BENAVIDES JAIME FRANCISCO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(141, 'Crear Profesor Archivo', 'Nombre: PORRAS BOHADA JORGE EDUARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(142, 'Crear Profesor Archivo', 'Nombre: RAMIREZ ESCOBAR JORGE FEDERICO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(143, 'Crear Profesor Archivo', 'Nombre: FONSECA VELASQUEZ ALDEMAR; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(144, 'Crear Profesor Archivo', 'Nombre: RODRIGUEZ MONTANA FERY PATRICIA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(145, 'Crear Profesor Archivo', 'Nombre: GIRALDO RAMOS FRANK NIXON; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(146, 'Crear Profesor Archivo', 'Nombre: ESCOBAR DIAZ ANDRES; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(147, 'Crear Profesor Archivo', 'Nombre: DELGADILLO GOMEZ EDUARDO ALBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(148, 'Crear Profesor Archivo', 'Nombre: HIGUERA APARICIO JOSE MANUEL; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(149, 'Crear Profesor Archivo', 'Nombre: CAMACHO WILLIAM ALEXANDER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(150, 'Crear Profesor Archivo', 'Nombre: GUTIERREZ MONTAÑA RUBEN EDUARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(151, 'Crear Profesor Archivo', 'Nombre: HERNANDEZ MORA JOHN RODRIGO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(152, 'Crear Profesor Archivo', 'Nombre: CARANTON VELOZA JHON FREDDY; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(153, 'Crear Profesor Archivo', 'Nombre: CASTAÑO ARCILA MAURICIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(154, 'Crear Profesor Archivo', 'Nombre: PEREZ MEDINA ELISEO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(155, 'Crear Profesor Archivo', 'Nombre: CASTRILLON CAMACHO ARJUNA; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(156, 'Crear Profesor Archivo', 'Nombre: ZULUAGA ATEHORTUA IVAN DARIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(157, 'Crear Profesor Archivo', 'Nombre: TENORIO BAUTISTA JULIETH ALEXANDRA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(158, 'Crear Profesor Archivo', 'Nombre: MONTEALEGRE MARTINEZ JOSELIN; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(159, 'Crear Profesor Archivo', 'Nombre: CARVAJAL MARQUEZ EDILMO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(160, 'Crear Profesor Archivo', 'Nombre: TORRES SUAREZ SERGIO ANDRES; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(161, 'Crear Profesor Archivo', 'Nombre: CLAVIJO JOYA JULIAN RENE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(162, 'Crear Profesor Archivo', 'Nombre: GAVILANES JOSE DARIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(163, 'Crear Profesor Archivo', 'Nombre: LOPEZ CHACON YERSON ALEJANDRO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(164, 'Crear Profesor Archivo', 'Nombre: BUITRAGO VALERO CARLOS JULIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(165, 'Crear Profesor Archivo', 'Nombre: CHAPARRO ARBOLEDA ARMANDO ALFREDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(166, 'Crear Profesor Archivo', 'Nombre: MORENO TORRES CARLOS HUMBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(167, 'Crear Profesor Archivo', 'Nombre: LEON ESPINOSA EDNA ALEJANDRA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(168, 'Crear Profesor Archivo', 'Nombre: URIBE SUAREZ GERMAN; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(169, 'Crear Profesor Archivo', 'Nombre: RIOS MONTOYA JENNY ALEXANDRA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(170, 'Crear Profesor Archivo', 'Nombre: ULLOA RODRIGUEZ MANUEL ALFREDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(171, 'Crear Profesor Archivo', 'Nombre: DE LA ROSA SILVA LOURDES CLARISSA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(172, 'Crear Profesor Archivo', 'Nombre: SALGADO DURAN OLGA PATRICIA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(173, 'Crear Profesor Archivo', 'Nombre: PINZON LOPEZ HECTOR ALFONSO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(174, 'Crear Profesor Archivo', 'Nombre: PASTRAN BELTRAN CARLOS GREGORIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(175, 'Crear Profesor Archivo', 'Nombre: VILLOTA POSSO HERNANDO ANTONIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(176, 'Crear Profesor Archivo', 'Nombre: MANRIQUE MUÑOZ ALVARO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(177, 'Crear Profesor Archivo', 'Nombre: DIAZ ORTIZ VICTOR HUGO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(178, 'Crear Profesor Archivo', 'Nombre: MEDINA GONZALEZ MAGDA LILIANA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(179, 'Crear Profesor Archivo', 'Nombre: ACEVEDO PEREZ JHON VLADIMIR; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(180, 'Crear Profesor Archivo', 'Nombre: MANJARRES GARCIA GUILLERMO ANTONIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(181, 'Crear Profesor Archivo', 'Nombre: ZAMBRANO CASTRO MARTIN GERMAN; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(182, 'Crear Profesor Archivo', 'Nombre: JAIMES CONTRERAS LUIS ALBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(183, 'Crear Profesor Archivo', 'Nombre: PINEDA JAIMES JORGE ARTURO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(184, 'Crear Profesor Archivo', 'Nombre: FERNANDEZ CASTILLO RAFAEL ENRIQUE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(185, 'Crear Profesor Archivo', 'Nombre: QUITIAN BUSTOS RUTH MERY; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(186, 'Crear Profesor Archivo', 'Nombre: RUA YANEZ LAURA MARCELA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(187, 'Crear Profesor Archivo', 'Nombre: ZAMBRANO BERRIO REYNALDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(188, 'Crear Profesor Archivo', 'Nombre: UBAQUE BRITO KAROL YOBANI; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(189, 'Crear Profesor Archivo', 'Nombre: ESQUIVEL RAMIREZ RODRIGO ELIAS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(190, 'Crear Profesor Archivo', 'Nombre: BUENO PINZON MAURICIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(191, 'Crear Profesor Archivo', 'Nombre: FORERO CLAVIJO JORGE ENRIQUE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(192, 'Crear Profesor Archivo', 'Nombre: VELANDIA RODRIGUEZ CARLOS HERBERTY; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(193, 'Crear Profesor Archivo', 'Nombre: MENA SERNA MILTON; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(194, 'Crear Profesor Archivo', 'Nombre: GARCIA MATEUS EDICSON GABRIEL; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(195, 'Crear Profesor Archivo', 'Nombre: MARTINEZ GONZALEZ RICARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(196, 'Crear Profesor Archivo', 'Nombre: GONZALEZ MALDONADO GEOVANNY; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(197, 'Crear Profesor Archivo', 'Nombre: CELIS MARIN NORVELY; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(198, 'Crear Profesor Archivo', 'Nombre: BARROS SANCHEZ RICARDO DE JESUS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(199, 'Crear Profesor Archivo', 'Nombre: URREGO RIVILLAS LIBIA SUSANA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(200, 'Crear Profesor Archivo', 'Nombre: FINO SANDOVAL RAFAEL ALBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(201, 'Crear Profesor Archivo', 'Nombre: LUENGAS CONTRERAS LELY ADRIANA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(202, 'Crear Profesor Archivo', 'Nombre: RIVERA AGUILAR FREDY ALEXANDER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(203, 'Crear Profesor Archivo', 'Nombre: JACINTO GOMEZ EDWAR; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(204, 'Crear Profesor Archivo', 'Nombre: MONTIEL ARIZA HOLMAN; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(205, 'Crear Profesor Archivo', 'Nombre: RODRIGUEZ LEON NAYIVER; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(206, 'Crear Profesor Archivo', 'Nombre: HERNANDEZ JAIRO ORLANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(207, 'Crear Profesor Archivo', 'Nombre: FONSECA VELASQUEZ JIMMY; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(208, 'Crear Profesor Archivo', 'Nombre: CENDALES ROMERO URIAS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(209, 'Crear Profesor Archivo', 'Nombre: ALVAREZ VIZCAINO NESTOR EDGAR; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(210, 'Crear Profesor Archivo', 'Nombre: MONTANA MESA JORGE ENRIQUE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(211, 'Crear Profesor Archivo', 'Nombre: VILLARRAGA RIAÑO ANDRES FELIPE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(212, 'Crear Profesor Archivo', 'Nombre: TORRES RIVEROS ALVARO ROGELIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(213, 'Crear Profesor Archivo', 'Nombre: ARAUJO OVIEDO CLAUDIA MARINA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(214, 'Crear Profesor Archivo', 'Nombre: BALANTA CASTILLA NEVIS DE JESUS; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(215, 'Crear Profesor Archivo', 'Nombre: URIBE BECERRA JOSE ERNESTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(216, 'Crear Profesor Archivo', 'Nombre: NAVARRO MEJIA DAVID RAFAEL; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(217, 'Crear Profesor Archivo', 'Nombre: VARGAS HERNANDEZ NAZLY; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(218, 'Crear Profesor Archivo', 'Nombre: CAVANZO NISO DORIS GUISELA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(219, 'Crear Profesor Archivo', 'Nombre: DIAZ MONTAÑO EVELYN IVONNE; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(220, 'Crear Profesor Archivo', 'Nombre: ALBA PULIDO RODRIGO; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(221, 'Crear Profesor Archivo', 'Nombre: GUZMAN LAVERDE JORGE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(222, 'Crear Profesor Archivo', 'Nombre: QUINTERO REYES RODRIGO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(223, 'Crear Profesor Archivo', 'Nombre: BULLA PEREIRA EDWIN ALBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(224, 'Crear Profesor Archivo', 'Nombre: RODRIGUEZ MONTAÑA NELSON EDUARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(225, 'Crear Profesor Archivo', 'Nombre: LOPEZ GONZALEZ ROSENDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(226, 'Crear Profesor Archivo', 'Nombre: MADRID SOTO NANCY ESPERANZA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(227, 'Crear Profesor Archivo', 'Nombre: CHAPARRO CHAPARRO FAOLAIN; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(228, 'Crear Profesor Archivo', 'Nombre: MORENO PENAGOS CLAUDIA MABEL; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(229, 'Crear Profesor Archivo', 'Nombre: OLEA SUAREZ DORIS MARLENE; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(230, 'Crear Profesor Archivo', 'Nombre: PARDO HEREDIA ANGELA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(231, 'Crear Profesor Archivo', 'Nombre: GONZALEZ SILVA RONALD; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(232, 'Crear Profesor Archivo', 'Nombre: MURCIA RODRIGUEZ WILLIAM ALEXANDER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(233, 'Crear Profesor Archivo', 'Nombre: ALFEREZ RIVAS LUIS ERNESTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(234, 'Crear Profesor Archivo', 'Nombre: ROJAS QUIROGA ANIBAL NICOLAS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(235, 'Crear Profesor Archivo', 'Nombre: FONSECA MEDARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(236, 'Crear Profesor Archivo', 'Nombre: DIAZ OSORIO MARCELA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(237, 'Crear Profesor Archivo', 'Nombre: ORDOÑEZ RODRIGUEZ PAOLA DOLORES; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(238, 'Crear Profesor Archivo', 'Nombre: AGUDELO CARDENAS ALEXANDER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(239, 'Crear Profesor Archivo', 'Nombre: ACOSTA SOLARTE PABLO ANDRES; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(240, 'Crear Profesor Archivo', 'Nombre: AVELLANEDA LEAL ROSA MYRIAM; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(241, 'Crear Profesor Archivo', 'Nombre: BARRAGAN VIZCAYA FLOR ALBA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(242, 'Crear Profesor Archivo', 'Nombre: SANABRIA VERGARA YOANNY; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(243, 'Crear Profesor Archivo', 'Nombre: PINILLA SUAREZ HECTOR ORLANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(244, 'Crear Profesor Archivo', 'Nombre: ROMERO RODRIGUEZ JORGE ENRIQUE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(245, 'Crear Profesor Archivo', 'Nombre: MORENO HURTADO ALEJANDRO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(246, 'Crear Profesor Archivo', 'Nombre: SICACHA ROJAS GERMAN; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(247, 'Crear Profesor Archivo', 'Nombre: PASTRAN BELTRAN OSWALDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(248, 'Crear Profesor Archivo', 'Nombre: ARIAS HENAO CAMILO ANDRES; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(249, 'Crear Profesor Archivo', 'Nombre: MORENO ACOSTA HENRY; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(250, 'Crear Profesor Archivo', 'Nombre: VANEGAS PARRA HENRY SAMIR; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(251, 'Crear Profesor Archivo', 'Nombre: CORREA MURILLO LUIS HERNANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(252, 'Crear Profesor Archivo', 'Nombre: FORERO CASALLAS JOHN ALEJANDRO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:37', '::1', 'Linux', 'Chrome', 1),
(253, 'Crear Profesor Archivo', 'Nombre: DUEÑAS ROJAS JONNY RICARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(254, 'Crear Profesor Archivo', 'Nombre: MONROY CASTRO JUAN CARLOS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(255, 'Crear Profesor Archivo', 'Nombre: MONROY DIAZ JOSE ISRAEL; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(256, 'Crear Profesor Archivo', 'Nombre: RAMIREZ ACERO JULIO CESAR; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(257, 'Crear Profesor Archivo', 'Nombre: HURTADO CORTES LUINI LEONARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(258, 'Crear Profesor Archivo', 'Nombre: MORENO FLAUTERO LUIS ALEJANDRO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(259, 'Crear Profesor Archivo', 'Nombre: ROCHA CASTELLANOS DAIRO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(260, 'Crear Profesor Archivo', 'Nombre: SALAZAR GUALDRON JUAN CARLOS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(261, 'Crear Profesor Archivo', 'Nombre: PEÑA TRIANA JEAN YECID; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(262, 'Crear Profesor Archivo', 'Nombre: CAÑAS VARON DAVID LEONARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(263, 'Crear Profesor Archivo', 'Nombre: CASTILLO HERNANDEZ JAIRO ERNESTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(264, 'Crear Profesor Archivo', 'Nombre: VELASQUEZ MOYA XIMENA AUDREY; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(265, 'Crear Profesor Archivo', 'Nombre: MARTINEZ CAMARGO DORA MARCELA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(266, 'Crear Profesor Archivo', 'Nombre: RUSINQUE PADILLA ALEJANDRA MARITZA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(267, 'Crear Profesor Archivo', 'Nombre: DIAZ RODRIGUEZ MIRTA ROCIO; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(268, 'Crear Profesor Archivo', 'Nombre: PEREZ CARO HERMENT; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(269, 'Crear Profesor Archivo', 'Nombre: MARTINEZ BUITRAGO DIANA ISABEL; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(270, 'Crear Profesor Archivo', 'Nombre: PALMA VANEGAS NELLY PAOLA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(271, 'Crear Profesor Archivo', 'Nombre: VASQUEZ ARRIETA TOMAS ANTONIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(272, 'Crear Profesor Archivo', 'Nombre: LUGO GONZALEZ LUZ MARINA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(273, 'Crear Profesor Archivo', 'Nombre: CHIQUIZA OCHOA MARIBEL; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(274, 'Crear Profesor Archivo', 'Nombre: BERNAL GARZON EILEEN; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(275, 'Crear Profesor Archivo', 'Nombre: SERNA DIAZ JOSE LEONARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(276, 'Crear Profesor Archivo', 'Nombre: CRUZ RAMIREZ JOHN ALEXANDER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(277, 'Crear Profesor Archivo', 'Nombre: LOZADA ROMERO ROXMERY; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(278, 'Crear Profesor Archivo', 'Nombre: MOSQUERA PALACIOS DARIN JAIRO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(279, 'Crear Profesor Archivo', 'Nombre: REYES MOZO JOSE VICENTE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(280, 'Crear Profesor Archivo', 'Nombre: BERNAL GOMEZ MIREYA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(281, 'Crear Profesor Archivo', 'Nombre: PINZON NUÑEZ SONIA ALEXANDRA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(282, 'Crear Profesor Archivo', 'Nombre: RODRIGUEZ GUERRERO ROCIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(283, 'Crear Profesor Archivo', 'Nombre: SALAS RUIZ ROBERTO EMILIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(284, 'Crear Profesor Archivo', 'Nombre: NOVOA TORRES NORBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(285, 'Crear Profesor Archivo', 'Nombre: CAVANZO NISSO GLORIA ANDREA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(286, 'Crear Profesor Archivo', 'Nombre: BECERRA CORREA NELSON REYNALDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(287, 'Crear Profesor Archivo', 'Nombre: VARGAS SANCHEZ NELSON ARMANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(288, 'Crear Profesor Archivo', 'Nombre: ARCO MUÑOZ NOE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(289, 'Crear Profesor Archivo', 'Nombre: WANUMEN SILVA LUIS FELIPE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(290, 'Crear Profesor Archivo', 'Nombre: GOMEZ MORA MILLER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(291, 'Crear Profesor Archivo', 'Nombre: RODRIGUEZ RODRIGUEZ JORGE ENRIQUE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(292, 'Crear Profesor Archivo', 'Nombre: FUQUENE ARDILA HECTOR JULIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(293, 'Crear Profesor Archivo', 'Nombre: FLOREZ FERNANDEZ HECTOR ARTURO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(294, 'Crear Profesor Archivo', 'Nombre: BAUTISTA HERRERA WILLIAM; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(295, 'Crear Profesor Archivo', 'Nombre: GARCIA QUEVEDO FRANCYA INES; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(296, 'Crear Profesor Archivo', 'Nombre: ROMERO GARCIA MARILUZ; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(297, 'Crear Profesor Archivo', 'Nombre: HERNANDEZ GARCIA CLAUDIA LILIANA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(298, 'Crear Profesor Archivo', 'Nombre: GUEVARA BOLAÑOS JUAN CARLOS; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(299, 'Crear Profesor Archivo', 'Nombre: VANEGAS CARLOS ALBERTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(300, 'Crear Profesor Archivo', 'Nombre: NAVARRO MEJIA WILMAN ENRIQUE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(301, 'Crear Profesor Archivo', 'Nombre: FIERRO CASTAÑO PETER NELSON; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(302, 'Crear Profesor Archivo', 'Nombre: BAREÑO ROMERO EDICSON FERNEY; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(303, 'Crear Profesor Archivo', 'Nombre: ZAMBRANO CAVIEDES JUAN NEPOMUCENO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(304, 'Crear Profesor Archivo', 'Nombre: GORDO MUSKUS RICARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(305, 'Crear Profesor Archivo', 'Nombre: LUGO GONZALEZ ARMANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(306, 'Crear Profesor Archivo', 'Nombre: HERNANDEZ SUAREZ CESAR AUGUSTO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(307, 'Crear Profesor Archivo', 'Nombre: MURILLO RONDON FRED GEOVANNY; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(308, 'Crear Profesor Archivo', 'Nombre: PEREZ SANTOS ALEXANDRA SASHENKA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(309, 'Crear Profesor Archivo', 'Nombre: RAIRAN ANTOLINES JOSE DANILO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1);
INSERT INTO `LogAdministrador` (`idLogAdministrador`, `accion`, `informacion`, `fecha`, `hora`, `ip`, `so`, `explorador`, `administrador_idAdministrador`) VALUES
(310, 'Crear Profesor Archivo', 'Nombre: GUEVARA VELANDIA GERMAN ANTONIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(311, 'Crear Profesor Archivo', 'Nombre: ORTIZ SUAREZ HELMUT EDGARDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(312, 'Crear Profesor Archivo', 'Nombre: HUERTAS SANABRIA JOSE RAFAEL; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(313, 'Crear Profesor Archivo', 'Nombre: LEYTON VASQUEZ HERNANDO EVELIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(314, 'Crear Profesor Archivo', 'Nombre: CAMACHO VELANDIA MARISOL; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(315, 'Crear Profesor Archivo', 'Nombre: GARCES RENDON HUMBERTO ANTONIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(316, 'Crear Profesor Archivo', 'Nombre: CASTELLANOS MORENO FABIO HERNANDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(317, 'Crear Profesor Archivo', 'Nombre: JARAMILLO VILLAMIZAR LENIN OSWALDO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(318, 'Crear Profesor Archivo', 'Nombre: BENITEZ SAZA CLAUDIA ROCIO; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(319, 'Crear Profesor Archivo', 'Nombre: GOMEZ CASTILLO HARVEY; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(320, 'Crear Profesor Archivo', 'Nombre: RUIZ CAICEDO JAIRO ALFONSO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(321, 'Crear Profesor Archivo', 'Nombre: AVENDAÑO AVENDAÑO EUSEBIO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(322, 'Crear Profesor Archivo', 'Nombre: RODRIGUEZ TEQUI YUDY MARCELA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(323, 'Crear Profesor Archivo', 'Nombre: LOPEZ MACIAS JAVIER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(324, 'Crear Profesor Archivo', 'Nombre: TELLO CASTAÑEDA MARTHA LUCIA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(325, 'Crear Profesor Archivo', 'Nombre: CAMARGO CASALLAS ESPERANZA; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(326, 'Crear Profesor Archivo', 'Nombre: JIMENEZ TRIANA ALEXANDER; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(327, 'Crear Profesor Archivo', 'Nombre: INFANTE MORENO WILSON; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(328, 'Crear Profesor Archivo', 'Nombre: NOVOA ROLDAN KRISTEL SOLANGE; Correo: Femenino; Genero: Femenino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(329, 'Crear Profesor Archivo', 'Nombre: BERMUDEZ BOHORQUEZ GIOVANNI RODRIGO; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(330, 'Crear Profesor Archivo', 'Nombre: FANDIÑO JORGE ENRIQUE; Correo: Masculino; Genero: Masculino', '2024-05-02', '21:30:38', '::1', 'Linux', 'Chrome', 1),
(331, 'Crear Curso Archivo', 'Nombre: FISICA II: ELECTROMAGNETISMO', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(332, 'Crear Curso Archivo', 'Nombre: CALCULO MULTIVARIADO', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(333, 'Crear Curso Archivo', 'Nombre: ECUACIONES DIFERENCIALES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(334, 'Crear Curso Archivo', 'Nombre: PROBABILIDAD Y ESTADISTICA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(335, 'Crear Curso Archivo', 'Nombre: ADMINISTRACION', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(336, 'Crear Curso Archivo', 'Nombre: FISICA III: ONDAS Y FISICA MODERNA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(337, 'Crear Curso Archivo', 'Nombre: FORMULACION Y EVALUACION DE PROYECTOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(338, 'Crear Curso Archivo', 'Nombre: INGENIERIA ECONOMICA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(339, 'Crear Curso Archivo', 'Nombre: TALLER DE INVESTIGACION', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(340, 'Crear Curso Archivo', 'Nombre: METODOS NUMERICOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(341, 'Crear Curso Archivo', 'Nombre: INGENIERIA AMBIENTAL', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(342, 'Crear Curso Archivo', 'Nombre: HIDROLOGIA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(343, 'Crear Curso Archivo', 'Nombre: ANALISIS DE ESTRUCTURAS I', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(344, 'Crear Curso Archivo', 'Nombre: EMPRENDIMIENTO EMPRESARIAL', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(345, 'Crear Curso Archivo', 'Nombre: ANALISIS DE ESTRUCTURAS II', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(346, 'Crear Curso Archivo', 'Nombre: DISEÑO Y CONSTRUCCION DE PAVIMENTOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(347, 'Crear Curso Archivo', 'Nombre: TUBERIAS Y BOMBAS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(348, 'Crear Curso Archivo', 'Nombre: DISEÑO DE ESTRUCTURAS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(349, 'Crear Curso Archivo', 'Nombre: DISEÑO DE INSTALACIONES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(350, 'Crear Curso Archivo', 'Nombre: DISEÑO Y CONSTRUCCION DE CANALES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(351, 'Crear Curso Archivo', 'Nombre: FUNDACIONES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(352, 'Crear Curso Archivo', 'Nombre: ACUEDUCTO Y ALCANTARILLADO', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(353, 'Crear Curso Archivo', 'Nombre: DINAMICA ESTRUCTURAL', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(354, 'Crear Curso Archivo', 'Nombre: INGENIERIA DE TRANSITO Y TRANSPORTE', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(355, 'Crear Curso Archivo', 'Nombre: LEGISLACION E INTERVENTORIA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(356, 'Crear Curso Archivo', 'Nombre: MAQUINARIA Y EQUIPOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(357, 'Crear Curso Archivo', 'Nombre: GESTION DE SEGURIDAD Y SALUD OCUPACIONAL', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(358, 'Crear Curso Archivo', 'Nombre: PLANTAS DE TRATAMIENTO DE AGUA POTABLE Y RESIDUAL', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(359, 'Crear Curso Archivo', 'Nombre: ESTRUCTURAS HIDRAULICAS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(360, 'Crear Curso Archivo', 'Nombre: CONSTRUCCION SOSTENIBLE', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(361, 'Crear Curso Archivo', 'Nombre: GEOFISICA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(362, 'Crear Curso Archivo', 'Nombre: TEORIA Y LOGICA DE PROGRAMACION', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(363, 'Crear Curso Archivo', 'Nombre: PROGRAMACION II', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(364, 'Crear Curso Archivo', 'Nombre: ESTABILIDAD DE TALUDES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(365, 'Crear Curso Archivo', 'Nombre: DISEÑO Y CONSTRUCCION DE INFRAESTRUCTURA AEROPORTUORIA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(366, 'Crear Curso Archivo', 'Nombre: TRATAMIENTO DE AGUAS RESIDUALES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(367, 'Crear Curso Archivo', 'Nombre: DISEÑO DE PROCESOS DE PRODUCCION', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(368, 'Crear Curso Archivo', 'Nombre: GESTION DE LA CALIDAD', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(369, 'Crear Curso Archivo', 'Nombre: AUTOMATIZACION', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(370, 'Crear Curso Archivo', 'Nombre: TRATAMIENTO DE RESIDUOS SOLIDOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(371, 'Crear Curso Archivo', 'Nombre: MODELOS DETERMINISTICOS DE LA PRODUCCION', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(372, 'Crear Curso Archivo', 'Nombre: CONTROL DE PROCESOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(373, 'Crear Curso Archivo', 'Nombre: EVALUACION DE IMPACTO AMBIENTAL', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(374, 'Crear Curso Archivo', 'Nombre: SISTEMAS FLEXIBLES DE MANUFACTURA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(375, 'Crear Curso Archivo', 'Nombre: PRODUCCION MAS LIMPIA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(376, 'Crear Curso Archivo', 'Nombre: PARADIGMAS MODERNOS DE LA ADMINISTRACION', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(377, 'Crear Curso Archivo', 'Nombre: DISEÑO DE INSTALACIONES INDUSTRIALES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(378, 'Crear Curso Archivo', 'Nombre: MEDIOS DE COMUNICACIÓN', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(379, 'Crear Curso Archivo', 'Nombre: CULTURAS Y RELIGIONES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(380, 'Crear Curso Archivo', 'Nombre: DISEÑO DE EXPERIMENTOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(381, 'Crear Curso Archivo', 'Nombre: MODELOS ESTOCASTICOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(382, 'Crear Curso Archivo', 'Nombre: GESTION TECNOLOGICA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(383, 'Crear Curso Archivo', 'Nombre: DISEÑO INDUSTRIAL', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(384, 'Crear Curso Archivo', 'Nombre: SIMULACION', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(385, 'Crear Curso Archivo', 'Nombre: DIRECCION DE PRODUCCION', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(386, 'Crear Curso Archivo', 'Nombre: MODELOS DE INTEGRACION LOGISTICA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(387, 'Crear Curso Archivo', 'Nombre: SISTEMAS DE INFORMACION LOGISTICA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(388, 'Crear Curso Archivo', 'Nombre: METODOS DE BUSQUEDA ESTADISTICA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(389, 'Crear Curso Archivo', 'Nombre: MANUFACTURA ESBELTA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(390, 'Crear Curso Archivo', 'Nombre: TEORIA DE LA DECISION', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(391, 'Crear Curso Archivo', 'Nombre: METODOS DE BUSQUEDA ESTOCASTICA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(392, 'Crear Curso Archivo', 'Nombre: HISTORIA Y CULTURA COLOMBIANA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(393, 'Crear Curso Archivo', 'Nombre: FISICA III: ONDAS Y FISICA ', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(394, 'Crear Curso Archivo', 'Nombre: DISEÑO DE SISTEMAS DE PUESTA A TIERRA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(395, 'Crear Curso Archivo', 'Nombre: SUBESTACIONES DE POTENCIA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(396, 'Crear Curso Archivo', 'Nombre: COORDINACION DE AISLAMIENTO', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(397, 'Crear Curso Archivo', 'Nombre: TRABAJO DE GRADO I', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(398, 'Crear Curso Archivo', 'Nombre: TRABAJO DE GRADO II', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(399, 'Crear Curso Archivo', 'Nombre: COSTOS Y PRESUPUESTOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(400, 'Crear Curso Archivo', 'Nombre: REGULACION AMBIENTAL', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(401, 'Crear Curso Archivo', 'Nombre: CULTURA Y SOCIEDAD EN AMERICA LATINA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(402, 'Crear Curso Archivo', 'Nombre: PLANEAMIENTO ENERGETICO', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(403, 'Crear Curso Archivo', 'Nombre: MICROREDES Y REDES INTELIGENTES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(404, 'Crear Curso Archivo', 'Nombre: LABORATORIO DE AISLAMIENTO', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(405, 'Crear Curso Archivo', 'Nombre: CAMPOS ELECTROMAGNETICOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(406, 'Crear Curso Archivo', 'Nombre: TEORIA DE CONTROL', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(407, 'Crear Curso Archivo', 'Nombre: MECANICA DE FLUIDOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(408, 'Crear Curso Archivo', 'Nombre: TERMODINAMICA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(409, 'Crear Curso Archivo', 'Nombre: ELECTRONICA DE POTENCIA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(410, 'Crear Curso Archivo', 'Nombre: ANALISIS DE FALLAS Y PROTECCIONES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(411, 'Crear Curso Archivo', 'Nombre: REDES Y AUTOMATIZACION INDUSTRIAL', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(412, 'Crear Curso Archivo', 'Nombre: GENERACION DE ENERGIA ELECTRICA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(413, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A LA CEM', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(414, 'Crear Curso Archivo', 'Nombre: ENERGIA Y MEDIO AMBIENTE', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(415, 'Crear Curso Archivo', 'Nombre: BASES DE DATOS EN INGENIERIA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(416, 'Crear Curso Archivo', 'Nombre: REDES DE DATOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(417, 'Crear Curso Archivo', 'Nombre: MEDIOS DE TRANSMISION', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(418, 'Crear Curso Archivo', 'Nombre: REDES DE COMUNICACIONES OPTICAS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(419, 'Crear Curso Archivo', 'Nombre: ANTENAS Y PROPAGACION', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(420, 'Crear Curso Archivo', 'Nombre: CRIPTOGRAFIA Y SEGURIDAD EN REDES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(421, 'Crear Curso Archivo', 'Nombre: LEGISLACION DE TELECOMUNICACIONES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(422, 'Crear Curso Archivo', 'Nombre: REDES DE CONVERGENCIA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(423, 'Crear Curso Archivo', 'Nombre: COMUNICACIONES MOVILES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(424, 'Crear Curso Archivo', 'Nombre: TECNOLOGIAS SOBRE IP', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(425, 'Crear Curso Archivo', 'Nombre: MODERNIDAD Y HUMANISMO CIENTIFICO', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(426, 'Crear Curso Archivo', 'Nombre: GESTION DE TECNOLOGIA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(427, 'Crear Curso Archivo', 'Nombre: APLICACIONES PARA DISPOSITIVOS DE TELECOMUNICACIONES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(428, 'Crear Curso Archivo', 'Nombre: FISICA DE ONDAS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(429, 'Crear Curso Archivo', 'Nombre: TRANSMISION DIGITAL', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(430, 'Crear Curso Archivo', 'Nombre: INGENIERIA DE TRAFICO', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(431, 'Crear Curso Archivo', 'Nombre: TEORIA DE LA INFORMACION', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(432, 'Crear Curso Archivo', 'Nombre: PROCESAMIENTO DIGITAL DE SEÑALES DE AUDIO Y VIDEO', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(433, 'Crear Curso Archivo', 'Nombre: REDES INALAMBRICAS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(434, 'Crear Curso Archivo', 'Nombre: SERVICIOS TELEMATICOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(435, 'Crear Curso Archivo', 'Nombre: DISEÑO Y PLANEACION DE REDES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(436, 'Crear Curso Archivo', 'Nombre: MICROONDAS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(437, 'Crear Curso Archivo', 'Nombre: GESTION DEL ESPECTRO RADIOELECTRICO', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(438, 'Crear Curso Archivo', 'Nombre: TELECOMUNICACIONES AERONAUTICAS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(439, 'Crear Curso Archivo', 'Nombre: SISTEMAS DISTRIBUIDOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(440, 'Crear Curso Archivo', 'Nombre: TEORIA GENERAL DE SISTEMAS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(441, 'Crear Curso Archivo', 'Nombre: ANALISIS DE FOURIER', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(442, 'Crear Curso Archivo', 'Nombre: REDES CORPORATIVAS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(443, 'Crear Curso Archivo', 'Nombre: SISTEMAS ABIERTOS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(444, 'Crear Curso Archivo', 'Nombre: PLANIFICACION Y DISEÑO DE REDES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(445, 'Crear Curso Archivo', 'Nombre: REDES DE ALTA VELOCIDAD', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(446, 'Crear Curso Archivo', 'Nombre: SEGURIDAD EN REDES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(447, 'Crear Curso Archivo', 'Nombre: GERENCIA Y AUDITORIA EN REDES', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(448, 'Crear Curso Archivo', 'Nombre: TECNOCIENCIAS', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(449, 'Crear Curso Archivo', 'Nombre: COMPUTACION CUANTICA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(450, 'Crear Curso Archivo', 'Nombre: CRIPTOLOGIA', '2024-05-02', '22:01:55', '::1', 'Linux', 'Chrome', 1),
(451, 'Crear Curso Archivo', 'Nombre: INVESTIGACION DE OPERACIONES', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(452, 'Crear Curso Archivo', 'Nombre: ANALISIS DE DATOS', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(453, 'Crear Curso Archivo', 'Nombre: SEMINARIO DE TELEMATICA', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(454, 'Crear Curso Archivo', 'Nombre: GESTION DE CALIDAD', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(455, 'Crear Curso Archivo', 'Nombre: GESTION DE REDES TELEMATICAS', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(456, 'Crear Curso Archivo', 'Nombre: SISTEMAS DNAMICOS Y DE CONTROL', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(457, 'Crear Curso Archivo', 'Nombre: DISEÑO POR ELEMENTOS FINITOS', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(458, 'Crear Curso Archivo', 'Nombre: TRANSFERENCIA DE CALOR', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(459, 'Crear Curso Archivo', 'Nombre: DISEÑO DE MAQUINAS', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(460, 'Crear Curso Archivo', 'Nombre: MAQUINAS HIDRAULICAS', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(461, 'Crear Curso Archivo', 'Nombre: MAQUINAS TERMICAS', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(462, 'Crear Curso Archivo', 'Nombre: EL LENGUAJE DE LA CIENCIA: TALLER DE TEXTO CIENTIFICO', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(463, 'Crear Curso Archivo', 'Nombre: MOTORES DE COMBUSTION INTERNA', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(464, 'Crear Curso Archivo', 'Nombre: ASEGURAMIENTO METROLOGICO', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(465, 'Crear Curso Archivo', 'Nombre: PRODUCCION DE TEXTOS CIENTIFICOS Y ACADEMICOS', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(466, 'Crear Curso Archivo', 'Nombre: TERMODINAMICA APLICADA', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(467, 'Crear Curso Archivo', 'Nombre: MANTENIMIENTO DE MAQUINAS INDUSTRIALES', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(468, 'Crear Curso Archivo', 'Nombre: DISEÑO EXPERIMENTAL', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(469, 'Crear Curso Archivo', 'Nombre: INGENIERIA DE MANUFACTURA', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(470, 'Crear Curso Archivo', 'Nombre: DISEÑO DE RECIPIENTES A PRESION', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(471, 'Crear Curso Archivo', 'Nombre: SELECCION DE MATERIALES', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(472, 'Crear Curso Archivo', 'Nombre: VIBRACIONES MECANICAS', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(473, 'Crear Curso Archivo', 'Nombre: TEORIA DE CORTE', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(474, 'Crear Curso Archivo', 'Nombre: DISEÑO DE ESTRUCTURAS METALICAS', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(475, 'Crear Curso Archivo', 'Nombre: INGENIERIA DEL AUTOMOVIL', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(476, 'Crear Curso Archivo', 'Nombre: AVANCES EN PROCESOS DE MANUFACTURA', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(477, 'Crear Curso Archivo', 'Nombre: AUTOMATIZACION DE MAQUINARIA', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(478, 'Crear Curso Archivo', 'Nombre: SERVOHIDRAULICA Y SERVONEUMATICA', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(479, 'Crear Curso Archivo', 'Nombre: APLICACIONES DE LA BIOMASA Y OTRAS FUENTES ALTERNAS', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(480, 'Crear Curso Archivo', 'Nombre: DISEÑO DE ESPECIFICACION DE PROCESOS DE SOLDADURA', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(481, 'Crear Curso Archivo', 'Nombre: DISEÑO DE VEHICULOS IMPULSADOS POR POTENCIA HUMANA', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(482, 'Crear Curso Archivo', 'Nombre: MAQUINAS DE ELEVACION Y TRANSPORTE', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(483, 'Crear Curso Archivo', 'Nombre: SIMULACION DE SISTEMAS', '2024-05-02', '22:01:56', '::1', 'Linux', 'Chrome', 1),
(484, 'Crear Curso Archivo', 'Nombre: TICS EN LAS ORGANIZACIONES', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(485, 'Crear Curso Archivo', 'Nombre: MATEMATICAS ESPECIALES', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(486, 'Crear Curso Archivo', 'Nombre: ANALISIS SOCIAL COLOMBIANO', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(487, 'Crear Curso Archivo', 'Nombre: INSTRUMENTACION DE PROCESOS I', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(488, 'Crear Curso Archivo', 'Nombre: INSTRUMENTACION DE PROCESOS II', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(489, 'Crear Curso Archivo', 'Nombre: CONTROL INTELIGENTE', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(490, 'Crear Curso Archivo', 'Nombre: AUTOMATICA DSC', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(491, 'Crear Curso Archivo', 'Nombre: ENERGIAS ALTERNATIVAS', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(492, 'Crear Curso Archivo', 'Nombre: DESARROLLO DE APLICACIONES MULTIPLATAFORMA', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(493, 'Crear Curso Archivo', 'Nombre: PENSAMIENTO CIENTIFICO', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(494, 'Crear Curso Archivo', 'Nombre: PROCESAMIENTO DE IMAGENES MEDICAS', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(495, 'Crear Curso Archivo', 'Nombre: MINERIA DE DATOS', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(496, 'Crear Curso Archivo', 'Nombre: SEÑALES Y SISTEMAS', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(497, 'Crear Curso Archivo', 'Nombre: SISTEMAS DINAMICOS', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(498, 'Crear Curso Archivo', 'Nombre: AUTOMATICA I', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(499, 'Crear Curso Archivo', 'Nombre: TERMODINAMICA Y FLUIDOS', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(500, 'Crear Curso Archivo', 'Nombre: CONTROL DE MOVIMIENTO', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(501, 'Crear Curso Archivo', 'Nombre: SENSORES Y ACTUADORES', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(502, 'Crear Curso Archivo', 'Nombre: AUTOMATICA II', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(503, 'Crear Curso Archivo', 'Nombre: CONTROL I', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(504, 'Crear Curso Archivo', 'Nombre: INSTRUMENTACION INDUSTRIAL', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(505, 'Crear Curso Archivo', 'Nombre: ROBOTICA', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(506, 'Crear Curso Archivo', 'Nombre: AUTOMATICA III', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(507, 'Crear Curso Archivo', 'Nombre: CONTROL II', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(508, 'Crear Curso Archivo', 'Nombre: CONTROL III', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(509, 'Crear Curso Archivo', 'Nombre: INDUSTRIA 4.0 APLICADA', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(510, 'Crear Curso Archivo', 'Nombre: CALCULO DIFERENCIAL', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(511, 'Crear Curso Archivo', 'Nombre: FISICA I: MECANICA NEWTONIANA', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(512, 'Crear Curso Archivo', 'Nombre: CATEDRA FRANCISCO JOSE DE CALDAS', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(513, 'Crear Curso Archivo', 'Nombre: CALCULO INTEGRAL', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(514, 'Crear Curso Archivo', 'Nombre: ALGEBRA LINEAL', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(515, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A LAS CONSTRUCCIONES CIVILES', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(516, 'Crear Curso Archivo', 'Nombre: PRODUCCION Y COMPRENSION DE TEXTOS I', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(517, 'Crear Curso Archivo', 'Nombre: PRODUCCION Y COMPRENSION DE TEXTOS II', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(518, 'Crear Curso Archivo', 'Nombre: CIENCIA TECNOLOGIA Y SOCIEDAD', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(519, 'Crear Curso Archivo', 'Nombre: GEOMETRIA DESCRIPTIVA', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(520, 'Crear Curso Archivo', 'Nombre: CONTABILIDAD', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(521, 'Crear Curso Archivo', 'Nombre: ESTATICA', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(522, 'Crear Curso Archivo', 'Nombre: ETICA Y SOCIEDAD', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(523, 'Crear Curso Archivo', 'Nombre: MECANICA DE SUELOS', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(524, 'Crear Curso Archivo', 'Nombre: RESISTENCIA DE MATERIALES', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(525, 'Crear Curso Archivo', 'Nombre: PROGRAMACION COSTOS Y PRESUPUESTOS', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(526, 'Crear Curso Archivo', 'Nombre: ELECTIVA SOCIOHUMANISTICA', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(527, 'Crear Curso Archivo', 'Nombre: GEOMETRIA EUCLIDIANA', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(528, 'Crear Curso Archivo', 'Nombre: LOGICA PROPOSICIONAL', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(529, 'Crear Curso Archivo', 'Nombre: GEOMETRIA ANALITICA', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(530, 'Crear Curso Archivo', 'Nombre: GEOLOGIA', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(531, 'Crear Curso Archivo', 'Nombre: PLANEACION ESTRATEGICA GERENCIAL', '2024-05-02', '22:03:01', '::1', 'Linux', 'Chrome', 1),
(532, 'Crear Curso Archivo', 'Nombre: GESTION DEL DESARROLLO HUMANO', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(533, 'Crear Curso Archivo', 'Nombre: ESTADISTICA DESCRIPTIVA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(534, 'Crear Curso Archivo', 'Nombre: EXPRESION GRAFICA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(535, 'Crear Curso Archivo', 'Nombre: FUNDAMENTOS DE MATEMATICAS', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(536, 'Crear Curso Archivo', 'Nombre: TOPOGRAFIA I: PLANIMETRIA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(537, 'Crear Curso Archivo', 'Nombre: TECNOLOGIA DEL CONCRETO', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(538, 'Crear Curso Archivo', 'Nombre: TOPOGRAFIA II: ALTIMETRIA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(539, 'Crear Curso Archivo', 'Nombre: DISEÑO GEOMETRICO DE VIAS', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(540, 'Crear Curso Archivo', 'Nombre: MATERIALES DE CONSTRUCCION', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(541, 'Crear Curso Archivo', 'Nombre: SEMINARIO DE ECOLOGIA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(542, 'Crear Curso Archivo', 'Nombre: PATOLOGIA DEL CONCRETO', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(543, 'Crear Curso Archivo', 'Nombre: DISEÑO VIAL COMPUTARIZADO', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(544, 'Crear Curso Archivo', 'Nombre: CONSTRUCCION DE EDIFICACIONES', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(545, 'Crear Curso Archivo', 'Nombre: CONSTRUCCION DE VIAS Y URBANISMO', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(546, 'Crear Curso Archivo', 'Nombre: QUIMICA GENERAL', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(547, 'Crear Curso Archivo', 'Nombre: TECNICA DE LICITACION, SEGUIMIENTO Y CONTROL DE PROYECTOS DE INGENIERIA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(548, 'Crear Curso Archivo', 'Nombre: ELECTRONICA APLICADA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(549, 'Crear Curso Archivo', 'Nombre: MAQUINAS ELECTRICAS', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(550, 'Crear Curso Archivo', 'Nombre: ELECTRONICA INDUSTRIAL', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(551, 'Crear Curso Archivo', 'Nombre: TALLER DE INVESTIGACION I', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(552, 'Crear Curso Archivo', 'Nombre: DISEÑO DIGITAL AVANZADO', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(553, 'Crear Curso Archivo', 'Nombre: ADQUISICION DE DATOS', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(554, 'Crear Curso Archivo', 'Nombre: ACCIONAMIENTOS NEUMATICOS-HIDRAULICOS', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(555, 'Crear Curso Archivo', 'Nombre: INVESTIGACION DE MERCADOS', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(556, 'Crear Curso Archivo', 'Nombre: DIBUJO TECNICO', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(557, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A LA PRODUCCION INDUSTRIAL', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(558, 'Crear Curso Archivo', 'Nombre: QUIMICA INDUSTRIAL', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(559, 'Crear Curso Archivo', 'Nombre: TALLER DE MECANICA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(560, 'Crear Curso Archivo', 'Nombre: MUESTREO Y MEDICION DEL TRABAJO', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(561, 'Crear Curso Archivo', 'Nombre: NEUMATICA E HIDRAULICA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(562, 'Crear Curso Archivo', 'Nombre: GESTION HUMANA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(563, 'Crear Curso Archivo', 'Nombre: HABILIDADES GERENCIALES', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(564, 'Crear Curso Archivo', 'Nombre: CAD/CAM', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(565, 'Crear Curso Archivo', 'Nombre: MANTENIMIENTO INDUSTRIAL', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(566, 'Crear Curso Archivo', 'Nombre: MICROECONOMIA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(567, 'Crear Curso Archivo', 'Nombre: PROCESOS INDUSTRIALES', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(568, 'Crear Curso Archivo', 'Nombre: GESTION AMBIENTAL DE LA PRODUCCION', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(569, 'Crear Curso Archivo', 'Nombre: TALLER DE SOLDADURA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(570, 'Crear Curso Archivo', 'Nombre: TECNOLOGIA Y MEDIO AMBIENTE', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(571, 'Crear Curso Archivo', 'Nombre: COSTOS DE PRODUCCIÓN', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(572, 'Crear Curso Archivo', 'Nombre: SEGURIDAD Y SALUD EN EL TRABAJO', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(573, 'Crear Curso Archivo', 'Nombre: ESTADISTICA Y PROBABILIDAD', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(574, 'Crear Curso Archivo', 'Nombre: ANALISIS FINANCIERO', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(575, 'Crear Curso Archivo', 'Nombre: PLANEACION DE LA PRODUCCION', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(576, 'Crear Curso Archivo', 'Nombre: DISEÑO DE PUESTOS DE TRABAJO', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(577, 'Crear Curso Archivo', 'Nombre: CONTROL ESTADISTICO DE CALIDAD', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(578, 'Crear Curso Archivo', 'Nombre: LOGISTICA INTEGAL', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(579, 'Crear Curso Archivo', 'Nombre: APLICACIONES COMPUTACIONALES', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(580, 'Crear Curso Archivo', 'Nombre: PROGRAMACION LINEAL', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(581, 'Crear Curso Archivo', 'Nombre: MATERIALES INDUSTRIALES', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(582, 'Crear Curso Archivo', 'Nombre: SEGUNDA LENGUA I - INGLES', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(583, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A LA MECANICA INDUSTRIAL', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(584, 'Crear Curso Archivo', 'Nombre: MATERIALES METALICOS', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(585, 'Crear Curso Archivo', 'Nombre: DIBUJO DE ELEMENTOS DE MAQUINAS', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(586, 'Crear Curso Archivo', 'Nombre: FUNDAMENTOS DE PROGRAMACION', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(587, 'Crear Curso Archivo', 'Nombre: MATERIALES POLIMERICOS Y COMPUESTOS', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(588, 'Crear Curso Archivo', 'Nombre: DIBUJO DE TALLER INDUSTRIAL', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(589, 'Crear Curso Archivo', 'Nombre: METROLOGIA DIMENSIONAL', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(590, 'Crear Curso Archivo', 'Nombre: PROCESOS DE MECANIZADO I', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(591, 'Crear Curso Archivo', 'Nombre: PROCESOS DE MECANIZADO II', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(592, 'Crear Curso Archivo', 'Nombre: DINAMICA DE MECANISMOS', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(593, 'Crear Curso Archivo', 'Nombre: MANTENIMIENTO DE MAQUINAS', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(594, 'Crear Curso Archivo', 'Nombre: PROCESOS DE CONFORMADO', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(595, 'Crear Curso Archivo', 'Nombre: ELEMENTOS DE MAQUINAS I', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(596, 'Crear Curso Archivo', 'Nombre: DISEÑO DE PROCESOS DE FABRICACION', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(597, 'Crear Curso Archivo', 'Nombre: ELEMENTOS DE MAQUINAS II', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(598, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A LA OPTIMIZACION', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(599, 'Crear Curso Archivo', 'Nombre: NEUMATICA Y ELECTRONEUMATICA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(600, 'Crear Curso Archivo', 'Nombre: FUNDAMENTOS DE SELECCION DE MATERIALES', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(601, 'Crear Curso Archivo', 'Nombre: APLICACIONES DE LA ENERGIA SOLAR FOTOVOLTAICA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(602, 'Crear Curso Archivo', 'Nombre: SIMULACION VIRTUAL DE SISTEMAS MECANICOS', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(603, 'Crear Curso Archivo', 'Nombre: TRATAMIENTOS TERMICOS', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(604, 'Crear Curso Archivo', 'Nombre: ENERGIAS RENOVABLES', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(605, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A LA ROBOTICA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(606, 'Crear Curso Archivo', 'Nombre: CATEDRA DEMOCRACIA Y CIUDADANIA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(607, 'Crear Curso Archivo', 'Nombre: EMPRENDIMIENTO', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(608, 'Crear Curso Archivo', 'Nombre: CONTABILIDAD GENERAL', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(609, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A ALGORITMOS', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(610, 'Crear Curso Archivo', 'Nombre: LOGICA MATEMATICA', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(611, 'Crear Curso Archivo', 'Nombre: FUNDAMENTOS DE ORGANIZACIÓN', '2024-05-02', '22:03:02', '::1', 'Linux', 'Chrome', 1),
(612, 'Crear Curso Archivo', 'Nombre: FISICA II: ELECTROMAGNETISMO', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(613, 'Crear Curso Archivo', 'Nombre: CALCULO MULTIVARIADO', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(614, 'Crear Curso Archivo', 'Nombre: ECUACIONES DIFERENCIALES', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(615, 'Crear Curso Archivo', 'Nombre: PROBABILIDAD Y ESTADISTICA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(616, 'Crear Curso Archivo', 'Nombre: ADMINISTRACION', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(617, 'Crear Curso Archivo', 'Nombre: FISICA III: ONDAS Y FISICA MODERNA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(618, 'Crear Curso Archivo', 'Nombre: FORMULACION Y EVALUACION DE PROYECTOS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(619, 'Crear Curso Archivo', 'Nombre: INGENIERIA ECONOMICA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(620, 'Crear Curso Archivo', 'Nombre: TALLER DE INVESTIGACION', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(621, 'Crear Curso Archivo', 'Nombre: METODOS NUMERICOS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(622, 'Crear Curso Archivo', 'Nombre: INGENIERIA AMBIENTAL', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(623, 'Crear Curso Archivo', 'Nombre: HIDROLOGIA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(624, 'Crear Curso Archivo', 'Nombre: ANALISIS DE ESTRUCTURAS I', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(625, 'Crear Curso Archivo', 'Nombre: EMPRENDIMIENTO EMPRESARIAL', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(626, 'Crear Curso Archivo', 'Nombre: ANALISIS DE ESTRUCTURAS II', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(627, 'Crear Curso Archivo', 'Nombre: DISEÑO Y CONSTRUCCION DE PAVIMENTOS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(628, 'Crear Curso Archivo', 'Nombre: TUBERIAS Y BOMBAS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(629, 'Crear Curso Archivo', 'Nombre: DISEÑO DE ESTRUCTURAS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(630, 'Crear Curso Archivo', 'Nombre: DISEÑO DE INSTALACIONES', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(631, 'Crear Curso Archivo', 'Nombre: DISEÑO Y CONSTRUCCION DE CANALES', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(632, 'Crear Curso Archivo', 'Nombre: FUNDACIONES', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(633, 'Crear Curso Archivo', 'Nombre: ACUEDUCTO Y ALCANTARILLADO', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(634, 'Crear Curso Archivo', 'Nombre: DINAMICA ESTRUCTURAL', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(635, 'Crear Curso Archivo', 'Nombre: INGENIERIA DE TRANSITO Y TRANSPORTE', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(636, 'Crear Curso Archivo', 'Nombre: LEGISLACION E INTERVENTORIA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(637, 'Crear Curso Archivo', 'Nombre: MAQUINARIA Y EQUIPOS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(638, 'Crear Curso Archivo', 'Nombre: GESTION DE SEGURIDAD Y SALUD OCUPACIONAL', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(639, 'Crear Curso Archivo', 'Nombre: PLANTAS DE TRATAMIENTO DE AGUA POTABLE Y RESIDUAL', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(640, 'Crear Curso Archivo', 'Nombre: ESTRUCTURAS HIDRAULICAS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(641, 'Crear Curso Archivo', 'Nombre: CONSTRUCCION SOSTENIBLE', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(642, 'Crear Curso Archivo', 'Nombre: GEOFISICA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(643, 'Crear Curso Archivo', 'Nombre: TEORIA Y LOGICA DE PROGRAMACION', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(644, 'Crear Curso Archivo', 'Nombre: PROGRAMACION II', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(645, 'Crear Curso Archivo', 'Nombre: ESTABILIDAD DE TALUDES', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(646, 'Crear Curso Archivo', 'Nombre: DISEÑO Y CONSTRUCCION DE INFRAESTRUCTURA AEROPORTUORIA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(647, 'Crear Curso Archivo', 'Nombre: TRATAMIENTO DE AGUAS RESIDUALES', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(648, 'Crear Curso Archivo', 'Nombre: DISEÑO DE PROCESOS DE PRODUCCION', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(649, 'Crear Curso Archivo', 'Nombre: GESTION DE LA CALIDAD', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(650, 'Crear Curso Archivo', 'Nombre: AUTOMATIZACION', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(651, 'Crear Curso Archivo', 'Nombre: TRATAMIENTO DE RESIDUOS SOLIDOS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(652, 'Crear Curso Archivo', 'Nombre: MODELOS DETERMINISTICOS DE LA PRODUCCION', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(653, 'Crear Curso Archivo', 'Nombre: CONTROL DE PROCESOS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(654, 'Crear Curso Archivo', 'Nombre: EVALUACION DE IMPACTO AMBIENTAL', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(655, 'Crear Curso Archivo', 'Nombre: SISTEMAS FLEXIBLES DE MANUFACTURA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(656, 'Crear Curso Archivo', 'Nombre: PRODUCCION MAS LIMPIA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(657, 'Crear Curso Archivo', 'Nombre: PARADIGMAS MODERNOS DE LA ADMINISTRACION', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(658, 'Crear Curso Archivo', 'Nombre: DISEÑO DE INSTALACIONES INDUSTRIALES', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(659, 'Crear Curso Archivo', 'Nombre: MEDIOS DE COMUNICACIÓN', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(660, 'Crear Curso Archivo', 'Nombre: CULTURAS Y RELIGIONES', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(661, 'Crear Curso Archivo', 'Nombre: DISEÑO DE EXPERIMENTOS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(662, 'Crear Curso Archivo', 'Nombre: MODELOS ESTOCASTICOS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(663, 'Crear Curso Archivo', 'Nombre: GESTION TECNOLOGICA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(664, 'Crear Curso Archivo', 'Nombre: DISEÑO INDUSTRIAL', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(665, 'Crear Curso Archivo', 'Nombre: SIMULACION', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(666, 'Crear Curso Archivo', 'Nombre: DIRECCION DE PRODUCCION', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(667, 'Crear Curso Archivo', 'Nombre: MODELOS DE INTEGRACION LOGISTICA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(668, 'Crear Curso Archivo', 'Nombre: SISTEMAS DE INFORMACION LOGISTICA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(669, 'Crear Curso Archivo', 'Nombre: METODOS DE BUSQUEDA ESTADISTICA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(670, 'Crear Curso Archivo', 'Nombre: MANUFACTURA ESBELTA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(671, 'Crear Curso Archivo', 'Nombre: TEORIA DE LA DECISION', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(672, 'Crear Curso Archivo', 'Nombre: METODOS DE BUSQUEDA ESTOCASTICA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(673, 'Crear Curso Archivo', 'Nombre: HISTORIA Y CULTURA COLOMBIANA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(674, 'Crear Curso Archivo', 'Nombre: FISICA III: ONDAS Y FISICA ', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(675, 'Crear Curso Archivo', 'Nombre: DISEÑO DE SISTEMAS DE PUESTA A TIERRA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(676, 'Crear Curso Archivo', 'Nombre: SUBESTACIONES DE POTENCIA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(677, 'Crear Curso Archivo', 'Nombre: COORDINACION DE AISLAMIENTO', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(678, 'Crear Curso Archivo', 'Nombre: TRABAJO DE GRADO I', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(679, 'Crear Curso Archivo', 'Nombre: TRABAJO DE GRADO II', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(680, 'Crear Curso Archivo', 'Nombre: COSTOS Y PRESUPUESTOS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(681, 'Crear Curso Archivo', 'Nombre: REGULACION AMBIENTAL', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(682, 'Crear Curso Archivo', 'Nombre: CULTURA Y SOCIEDAD EN AMERICA LATINA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(683, 'Crear Curso Archivo', 'Nombre: PLANEAMIENTO ENERGETICO', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(684, 'Crear Curso Archivo', 'Nombre: MICROREDES Y REDES INTELIGENTES', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(685, 'Crear Curso Archivo', 'Nombre: LABORATORIO DE AISLAMIENTO', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(686, 'Crear Curso Archivo', 'Nombre: CAMPOS ELECTROMAGNETICOS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(687, 'Crear Curso Archivo', 'Nombre: TEORIA DE CONTROL', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(688, 'Crear Curso Archivo', 'Nombre: MECANICA DE FLUIDOS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(689, 'Crear Curso Archivo', 'Nombre: TERMODINAMICA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(690, 'Crear Curso Archivo', 'Nombre: ELECTRONICA DE POTENCIA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(691, 'Crear Curso Archivo', 'Nombre: ANALISIS DE FALLAS Y PROTECCIONES', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(692, 'Crear Curso Archivo', 'Nombre: REDES Y AUTOMATIZACION INDUSTRIAL', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(693, 'Crear Curso Archivo', 'Nombre: GENERACION DE ENERGIA ELECTRICA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(694, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A LA CEM', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(695, 'Crear Curso Archivo', 'Nombre: ENERGIA Y MEDIO AMBIENTE', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(696, 'Crear Curso Archivo', 'Nombre: BASES DE DATOS EN INGENIERIA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(697, 'Crear Curso Archivo', 'Nombre: REDES DE DATOS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(698, 'Crear Curso Archivo', 'Nombre: MEDIOS DE TRANSMISION', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(699, 'Crear Curso Archivo', 'Nombre: REDES DE COMUNICACIONES OPTICAS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(700, 'Crear Curso Archivo', 'Nombre: ANTENAS Y PROPAGACION', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(701, 'Crear Curso Archivo', 'Nombre: CRIPTOGRAFIA Y SEGURIDAD EN REDES', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(702, 'Crear Curso Archivo', 'Nombre: LEGISLACION DE TELECOMUNICACIONES', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(703, 'Crear Curso Archivo', 'Nombre: REDES DE CONVERGENCIA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(704, 'Crear Curso Archivo', 'Nombre: COMUNICACIONES MOVILES', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(705, 'Crear Curso Archivo', 'Nombre: TECNOLOGIAS SOBRE IP', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(706, 'Crear Curso Archivo', 'Nombre: MODERNIDAD Y HUMANISMO CIENTIFICO', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(707, 'Crear Curso Archivo', 'Nombre: GESTION DE TECNOLOGIA', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(708, 'Crear Curso Archivo', 'Nombre: APLICACIONES PARA DISPOSITIVOS DE TELECOMUNICACIONES', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(709, 'Crear Curso Archivo', 'Nombre: FISICA DE ONDAS', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(710, 'Crear Curso Archivo', 'Nombre: TRANSMISION DIGITAL', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(711, 'Crear Curso Archivo', 'Nombre: INGENIERIA DE TRAFICO', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(712, 'Crear Curso Archivo', 'Nombre: TEORIA DE LA INFORMACION', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(713, 'Crear Curso Archivo', 'Nombre: PROCESAMIENTO DIGITAL DE SEÑALES DE AUDIO Y VIDEO', '2024-05-02', '22:04:40', '::1', 'Linux', 'Chrome', 1),
(714, 'Crear Curso Archivo', 'Nombre: REDES INALAMBRICAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(715, 'Crear Curso Archivo', 'Nombre: SERVICIOS TELEMATICOS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(716, 'Crear Curso Archivo', 'Nombre: DISEÑO Y PLANEACION DE REDES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1);
INSERT INTO `LogAdministrador` (`idLogAdministrador`, `accion`, `informacion`, `fecha`, `hora`, `ip`, `so`, `explorador`, `administrador_idAdministrador`) VALUES
(717, 'Crear Curso Archivo', 'Nombre: MICROONDAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(718, 'Crear Curso Archivo', 'Nombre: GESTION DEL ESPECTRO RADIOELECTRICO', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(719, 'Crear Curso Archivo', 'Nombre: TELECOMUNICACIONES AERONAUTICAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(720, 'Crear Curso Archivo', 'Nombre: SISTEMAS DISTRIBUIDOS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(721, 'Crear Curso Archivo', 'Nombre: TEORIA GENERAL DE SISTEMAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(722, 'Crear Curso Archivo', 'Nombre: ANALISIS DE FOURIER', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(723, 'Crear Curso Archivo', 'Nombre: REDES CORPORATIVAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(724, 'Crear Curso Archivo', 'Nombre: SISTEMAS ABIERTOS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(725, 'Crear Curso Archivo', 'Nombre: PLANIFICACION Y DISEÑO DE REDES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(726, 'Crear Curso Archivo', 'Nombre: REDES DE ALTA VELOCIDAD', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(727, 'Crear Curso Archivo', 'Nombre: SEGURIDAD EN REDES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(728, 'Crear Curso Archivo', 'Nombre: GERENCIA Y AUDITORIA EN REDES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(729, 'Crear Curso Archivo', 'Nombre: TECNOCIENCIAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(730, 'Crear Curso Archivo', 'Nombre: COMPUTACION CUANTICA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(731, 'Crear Curso Archivo', 'Nombre: CRIPTOLOGIA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(732, 'Crear Curso Archivo', 'Nombre: INVESTIGACION DE OPERACIONES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(733, 'Crear Curso Archivo', 'Nombre: ANALISIS DE DATOS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(734, 'Crear Curso Archivo', 'Nombre: SEMINARIO DE TELEMATICA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(735, 'Crear Curso Archivo', 'Nombre: GESTION DE CALIDAD', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(736, 'Crear Curso Archivo', 'Nombre: GESTION DE REDES TELEMATICAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(737, 'Crear Curso Archivo', 'Nombre: SISTEMAS DNAMICOS Y DE CONTROL', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(738, 'Crear Curso Archivo', 'Nombre: DISEÑO POR ELEMENTOS FINITOS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(739, 'Crear Curso Archivo', 'Nombre: TRANSFERENCIA DE CALOR', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(740, 'Crear Curso Archivo', 'Nombre: DISEÑO DE MAQUINAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(741, 'Crear Curso Archivo', 'Nombre: MAQUINAS HIDRAULICAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(742, 'Crear Curso Archivo', 'Nombre: MAQUINAS TERMICAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(743, 'Crear Curso Archivo', 'Nombre: EL LENGUAJE DE LA CIENCIA: TALLER DE TEXTO CIENTIFICO', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(744, 'Crear Curso Archivo', 'Nombre: MOTORES DE COMBUSTION INTERNA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(745, 'Crear Curso Archivo', 'Nombre: ASEGURAMIENTO METROLOGICO', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(746, 'Crear Curso Archivo', 'Nombre: PRODUCCION DE TEXTOS CIENTIFICOS Y ACADEMICOS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(747, 'Crear Curso Archivo', 'Nombre: TERMODINAMICA APLICADA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(748, 'Crear Curso Archivo', 'Nombre: MANTENIMIENTO DE MAQUINAS INDUSTRIALES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(749, 'Crear Curso Archivo', 'Nombre: DISEÑO EXPERIMENTAL', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(750, 'Crear Curso Archivo', 'Nombre: INGENIERIA DE MANUFACTURA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(751, 'Crear Curso Archivo', 'Nombre: DISEÑO DE RECIPIENTES A PRESION', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(752, 'Crear Curso Archivo', 'Nombre: SELECCION DE MATERIALES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(753, 'Crear Curso Archivo', 'Nombre: VIBRACIONES MECANICAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(754, 'Crear Curso Archivo', 'Nombre: TEORIA DE CORTE', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(755, 'Crear Curso Archivo', 'Nombre: DISEÑO DE ESTRUCTURAS METALICAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(756, 'Crear Curso Archivo', 'Nombre: INGENIERIA DEL AUTOMOVIL', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(757, 'Crear Curso Archivo', 'Nombre: AVANCES EN PROCESOS DE MANUFACTURA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(758, 'Crear Curso Archivo', 'Nombre: AUTOMATIZACION DE MAQUINARIA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(759, 'Crear Curso Archivo', 'Nombre: SERVOHIDRAULICA Y SERVONEUMATICA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(760, 'Crear Curso Archivo', 'Nombre: APLICACIONES DE LA BIOMASA Y OTRAS FUENTES ALTERNAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(761, 'Crear Curso Archivo', 'Nombre: DISEÑO DE ESPECIFICACION DE PROCESOS DE SOLDADURA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(762, 'Crear Curso Archivo', 'Nombre: DISEÑO DE VEHICULOS IMPULSADOS POR POTENCIA HUMANA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(763, 'Crear Curso Archivo', 'Nombre: MAQUINAS DE ELEVACION Y TRANSPORTE', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(764, 'Crear Curso Archivo', 'Nombre: SIMULACION DE SISTEMAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(765, 'Crear Curso Archivo', 'Nombre: TICS EN LAS ORGANIZACIONES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(766, 'Crear Curso Archivo', 'Nombre: MATEMATICAS ESPECIALES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(767, 'Crear Curso Archivo', 'Nombre: ANALISIS SOCIAL COLOMBIANO', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(768, 'Crear Curso Archivo', 'Nombre: INSTRUMENTACION DE PROCESOS I', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(769, 'Crear Curso Archivo', 'Nombre: INSTRUMENTACION DE PROCESOS II', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(770, 'Crear Curso Archivo', 'Nombre: CONTROL INTELIGENTE', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(771, 'Crear Curso Archivo', 'Nombre: AUTOMATICA DSC', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(772, 'Crear Curso Archivo', 'Nombre: ENERGIAS ALTERNATIVAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(773, 'Crear Curso Archivo', 'Nombre: DESARROLLO DE APLICACIONES MULTIPLATAFORMA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(774, 'Crear Curso Archivo', 'Nombre: PENSAMIENTO CIENTIFICO', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(775, 'Crear Curso Archivo', 'Nombre: PROCESAMIENTO DE IMAGENES MEDICAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(776, 'Crear Curso Archivo', 'Nombre: MINERIA DE DATOS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(777, 'Crear Curso Archivo', 'Nombre: SEÑALES Y SISTEMAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(778, 'Crear Curso Archivo', 'Nombre: SISTEMAS DINAMICOS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(779, 'Crear Curso Archivo', 'Nombre: AUTOMATICA I', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(780, 'Crear Curso Archivo', 'Nombre: TERMODINAMICA Y FLUIDOS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(781, 'Crear Curso Archivo', 'Nombre: CONTROL DE MOVIMIENTO', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(782, 'Crear Curso Archivo', 'Nombre: SENSORES Y ACTUADORES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(783, 'Crear Curso Archivo', 'Nombre: AUTOMATICA II', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(784, 'Crear Curso Archivo', 'Nombre: CONTROL I', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(785, 'Crear Curso Archivo', 'Nombre: INSTRUMENTACION INDUSTRIAL', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(786, 'Crear Curso Archivo', 'Nombre: ROBOTICA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(787, 'Crear Curso Archivo', 'Nombre: AUTOMATICA III', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(788, 'Crear Curso Archivo', 'Nombre: CONTROL II', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(789, 'Crear Curso Archivo', 'Nombre: CONTROL III', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(790, 'Crear Curso Archivo', 'Nombre: INDUSTRIA 4.0 APLICADA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(791, 'Crear Curso Archivo', 'Nombre: CALCULO DIFERENCIAL', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(792, 'Crear Curso Archivo', 'Nombre: FISICA I: MECANICA NEWTONIANA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(793, 'Crear Curso Archivo', 'Nombre: CATEDRA FRANCISCO JOSE DE CALDAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(794, 'Crear Curso Archivo', 'Nombre: CALCULO INTEGRAL', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(795, 'Crear Curso Archivo', 'Nombre: ALGEBRA LINEAL', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(796, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A LAS CONSTRUCCIONES CIVILES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(797, 'Crear Curso Archivo', 'Nombre: PRODUCCION Y COMPRENSION DE TEXTOS I', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(798, 'Crear Curso Archivo', 'Nombre: PRODUCCION Y COMPRENSION DE TEXTOS II', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(799, 'Crear Curso Archivo', 'Nombre: CIENCIA TECNOLOGIA Y SOCIEDAD', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(800, 'Crear Curso Archivo', 'Nombre: GEOMETRIA DESCRIPTIVA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(801, 'Crear Curso Archivo', 'Nombre: CONTABILIDAD', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(802, 'Crear Curso Archivo', 'Nombre: ESTATICA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(803, 'Crear Curso Archivo', 'Nombre: ETICA Y SOCIEDAD', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(804, 'Crear Curso Archivo', 'Nombre: MECANICA DE SUELOS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(805, 'Crear Curso Archivo', 'Nombre: RESISTENCIA DE MATERIALES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(806, 'Crear Curso Archivo', 'Nombre: PROGRAMACION COSTOS Y PRESUPUESTOS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(807, 'Crear Curso Archivo', 'Nombre: ELECTIVA SOCIOHUMANISTICA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(808, 'Crear Curso Archivo', 'Nombre: GEOMETRIA EUCLIDIANA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(809, 'Crear Curso Archivo', 'Nombre: LOGICA PROPOSICIONAL', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(810, 'Crear Curso Archivo', 'Nombre: GEOMETRIA ANALITICA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(811, 'Crear Curso Archivo', 'Nombre: GEOLOGIA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(812, 'Crear Curso Archivo', 'Nombre: PLANEACION ESTRATEGICA GERENCIAL', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(813, 'Crear Curso Archivo', 'Nombre: GESTION DEL DESARROLLO HUMANO', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(814, 'Crear Curso Archivo', 'Nombre: ESTADISTICA DESCRIPTIVA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(815, 'Crear Curso Archivo', 'Nombre: EXPRESION GRAFICA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(816, 'Crear Curso Archivo', 'Nombre: FUNDAMENTOS DE MATEMATICAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(817, 'Crear Curso Archivo', 'Nombre: TOPOGRAFIA I: PLANIMETRIA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(818, 'Crear Curso Archivo', 'Nombre: TECNOLOGIA DEL CONCRETO', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(819, 'Crear Curso Archivo', 'Nombre: TOPOGRAFIA II: ALTIMETRIA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(820, 'Crear Curso Archivo', 'Nombre: DISEÑO GEOMETRICO DE VIAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(821, 'Crear Curso Archivo', 'Nombre: MATERIALES DE CONSTRUCCION', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(822, 'Crear Curso Archivo', 'Nombre: SEMINARIO DE ECOLOGIA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(823, 'Crear Curso Archivo', 'Nombre: PATOLOGIA DEL CONCRETO', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(824, 'Crear Curso Archivo', 'Nombre: DISEÑO VIAL COMPUTARIZADO', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(825, 'Crear Curso Archivo', 'Nombre: CONSTRUCCION DE EDIFICACIONES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(826, 'Crear Curso Archivo', 'Nombre: CONSTRUCCION DE VIAS Y URBANISMO', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(827, 'Crear Curso Archivo', 'Nombre: QUIMICA GENERAL', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(828, 'Crear Curso Archivo', 'Nombre: TECNICA DE LICITACION, SEGUIMIENTO Y CONTROL DE PROYECTOS DE INGENIERIA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(829, 'Crear Curso Archivo', 'Nombre: ELECTRONICA APLICADA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(830, 'Crear Curso Archivo', 'Nombre: MAQUINAS ELECTRICAS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(831, 'Crear Curso Archivo', 'Nombre: ELECTRONICA INDUSTRIAL', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(832, 'Crear Curso Archivo', 'Nombre: TALLER DE INVESTIGACION I', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(833, 'Crear Curso Archivo', 'Nombre: DISEÑO DIGITAL AVANZADO', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(834, 'Crear Curso Archivo', 'Nombre: ADQUISICION DE DATOS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(835, 'Crear Curso Archivo', 'Nombre: ACCIONAMIENTOS NEUMATICOS-HIDRAULICOS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(836, 'Crear Curso Archivo', 'Nombre: INVESTIGACION DE MERCADOS', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(837, 'Crear Curso Archivo', 'Nombre: DIBUJO TECNICO', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(838, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A LA PRODUCCION INDUSTRIAL', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(839, 'Crear Curso Archivo', 'Nombre: QUIMICA INDUSTRIAL', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(840, 'Crear Curso Archivo', 'Nombre: TALLER DE MECANICA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(841, 'Crear Curso Archivo', 'Nombre: MUESTREO Y MEDICION DEL TRABAJO', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(842, 'Crear Curso Archivo', 'Nombre: NEUMATICA E HIDRAULICA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(843, 'Crear Curso Archivo', 'Nombre: GESTION HUMANA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(844, 'Crear Curso Archivo', 'Nombre: HABILIDADES GERENCIALES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(845, 'Crear Curso Archivo', 'Nombre: CAD/CAM', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(846, 'Crear Curso Archivo', 'Nombre: MANTENIMIENTO INDUSTRIAL', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(847, 'Crear Curso Archivo', 'Nombre: MICROECONOMIA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(848, 'Crear Curso Archivo', 'Nombre: PROCESOS INDUSTRIALES', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(849, 'Crear Curso Archivo', 'Nombre: GESTION AMBIENTAL DE LA PRODUCCION', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(850, 'Crear Curso Archivo', 'Nombre: TALLER DE SOLDADURA', '2024-05-02', '22:04:41', '::1', 'Linux', 'Chrome', 1),
(851, 'Crear Curso Archivo', 'Nombre: TECNOLOGIA Y MEDIO AMBIENTE', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(852, 'Crear Curso Archivo', 'Nombre: COSTOS DE PRODUCCIÓN', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(853, 'Crear Curso Archivo', 'Nombre: SEGURIDAD Y SALUD EN EL TRABAJO', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(854, 'Crear Curso Archivo', 'Nombre: ESTADISTICA Y PROBABILIDAD', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(855, 'Crear Curso Archivo', 'Nombre: ANALISIS FINANCIERO', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(856, 'Crear Curso Archivo', 'Nombre: PLANEACION DE LA PRODUCCION', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(857, 'Crear Curso Archivo', 'Nombre: DISEÑO DE PUESTOS DE TRABAJO', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(858, 'Crear Curso Archivo', 'Nombre: CONTROL ESTADISTICO DE CALIDAD', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(859, 'Crear Curso Archivo', 'Nombre: LOGISTICA INTEGAL', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(860, 'Crear Curso Archivo', 'Nombre: APLICACIONES COMPUTACIONALES', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(861, 'Crear Curso Archivo', 'Nombre: PROGRAMACION LINEAL', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(862, 'Crear Curso Archivo', 'Nombre: MATERIALES INDUSTRIALES', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(863, 'Crear Curso Archivo', 'Nombre: SEGUNDA LENGUA I - INGLES', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(864, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A LA MECANICA INDUSTRIAL', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(865, 'Crear Curso Archivo', 'Nombre: MATERIALES METALICOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(866, 'Crear Curso Archivo', 'Nombre: DIBUJO DE ELEMENTOS DE MAQUINAS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(867, 'Crear Curso Archivo', 'Nombre: FUNDAMENTOS DE PROGRAMACION', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(868, 'Crear Curso Archivo', 'Nombre: MATERIALES POLIMERICOS Y COMPUESTOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(869, 'Crear Curso Archivo', 'Nombre: DIBUJO DE TALLER INDUSTRIAL', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(870, 'Crear Curso Archivo', 'Nombre: METROLOGIA DIMENSIONAL', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(871, 'Crear Curso Archivo', 'Nombre: PROCESOS DE MECANIZADO I', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(872, 'Crear Curso Archivo', 'Nombre: PROCESOS DE MECANIZADO II', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(873, 'Crear Curso Archivo', 'Nombre: DINAMICA DE MECANISMOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(874, 'Crear Curso Archivo', 'Nombre: MANTENIMIENTO DE MAQUINAS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(875, 'Crear Curso Archivo', 'Nombre: PROCESOS DE CONFORMADO', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(876, 'Crear Curso Archivo', 'Nombre: ELEMENTOS DE MAQUINAS I', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(877, 'Crear Curso Archivo', 'Nombre: DISEÑO DE PROCESOS DE FABRICACION', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(878, 'Crear Curso Archivo', 'Nombre: ELEMENTOS DE MAQUINAS II', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(879, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A LA OPTIMIZACION', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(880, 'Crear Curso Archivo', 'Nombre: NEUMATICA Y ELECTRONEUMATICA', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(881, 'Crear Curso Archivo', 'Nombre: FUNDAMENTOS DE SELECCION DE MATERIALES', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(882, 'Crear Curso Archivo', 'Nombre: APLICACIONES DE LA ENERGIA SOLAR FOTOVOLTAICA', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(883, 'Crear Curso Archivo', 'Nombre: SIMULACION VIRTUAL DE SISTEMAS MECANICOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(884, 'Crear Curso Archivo', 'Nombre: TRATAMIENTOS TERMICOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(885, 'Crear Curso Archivo', 'Nombre: ENERGIAS RENOVABLES', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(886, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A LA ROBOTICA', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(887, 'Crear Curso Archivo', 'Nombre: CATEDRA DEMOCRACIA Y CIUDADANIA', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(888, 'Crear Curso Archivo', 'Nombre: EMPRENDIMIENTO', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(889, 'Crear Curso Archivo', 'Nombre: CONTABILIDAD GENERAL', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(890, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A ALGORITMOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(891, 'Crear Curso Archivo', 'Nombre: LOGICA MATEMATICA', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(892, 'Crear Curso Archivo', 'Nombre: FUNDAMENTOS DE ORGANIZACIÓN', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(893, 'Crear Curso Archivo', 'Nombre: ESTRUCTURA DE DATOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(894, 'Crear Curso Archivo', 'Nombre: PROGRAMACION ORIENTADA A OBJETOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(895, 'Crear Curso Archivo', 'Nombre: ANALISIS Y METODOS NUMERICOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(896, 'Crear Curso Archivo', 'Nombre: PROGRAMACION MULTINIVEL', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(897, 'Crear Curso Archivo', 'Nombre: BASES DE DATOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(898, 'Crear Curso Archivo', 'Nombre: PROGRAMACION AVANZADA', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(899, 'Crear Curso Archivo', 'Nombre: BASES DE DATOS AVANZADAS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(900, 'Crear Curso Archivo', 'Nombre: DISEÑO LOGICO', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(901, 'Crear Curso Archivo', 'Nombre: ANALISIS DE SISTEMAS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(902, 'Crear Curso Archivo', 'Nombre: INTELIGENCIA ARTIFICIAL', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(903, 'Crear Curso Archivo', 'Nombre: SISTEMAS OPERACIONALES', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(904, 'Crear Curso Archivo', 'Nombre: TRANSMISION DE DATOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(905, 'Crear Curso Archivo', 'Nombre: PROGRAMACION WEB', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(906, 'Crear Curso Archivo', 'Nombre: INGENIERIA DE SOFTWARE', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(907, 'Crear Curso Archivo', 'Nombre: ARQUITECTURA DE COMPUTADORES', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(908, 'Crear Curso Archivo', 'Nombre: GLOBALIZACION', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(909, 'Crear Curso Archivo', 'Nombre: FUNDAMENTOS DE TELEMATICA', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(910, 'Crear Curso Archivo', 'Nombre: PROGRAMACION POR COMPONENTES', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(911, 'Crear Curso Archivo', 'Nombre: APLICACIONES PARA INTERNET', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(912, 'Crear Curso Archivo', 'Nombre: BASES DE DATOS DISTRIBUIDAS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(913, 'Crear Curso Archivo', 'Nombre: INFORMATICA Y SOCIEDAD', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(914, 'Crear Curso Archivo', 'Nombre: PROGRAMACION', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(915, 'Crear Curso Archivo', 'Nombre: PRINCIPIOS DE ROBOTICA', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(916, 'Crear Curso Archivo', 'Nombre: ECONOMIA', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(917, 'Crear Curso Archivo', 'Nombre: SISTEMAS DE POTENCIA', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(918, 'Crear Curso Archivo', 'Nombre: DISPOSITIVOS SEMICONDUCTORES', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(919, 'Crear Curso Archivo', 'Nombre: MEDIDAS ELECTRICAS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(920, 'Crear Curso Archivo', 'Nombre: REDES ELECTRICAS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(921, 'Crear Curso Archivo', 'Nombre: ARQUITECTURA DE MICROCONTROLADORES', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(922, 'Crear Curso Archivo', 'Nombre: ANALISIS DE CIRCUITOS I', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(923, 'Crear Curso Archivo', 'Nombre: ANALISIS DE CIRCUITOS II', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(924, 'Crear Curso Archivo', 'Nombre: CONVERSION ELECTROMAGNETICA', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(925, 'Crear Curso Archivo', 'Nombre: ANALISIS DE CIRCUITOS III', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(926, 'Crear Curso Archivo', 'Nombre: CIRCUITOS DIGITALES', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(927, 'Crear Curso Archivo', 'Nombre: INSTALACIONES ELECTRICAS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(928, 'Crear Curso Archivo', 'Nombre: FORMULACION DE PROYECTOS TECNOLOGICOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(929, 'Crear Curso Archivo', 'Nombre: AUTOMATISMOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(930, 'Crear Curso Archivo', 'Nombre: INTRODUCCIÓN A LA ELECTRICIDAD', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(931, 'Crear Curso Archivo', 'Nombre: CATEDRA DE CONTEXTO', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(932, 'Crear Curso Archivo', 'Nombre: INFORMATICA Y ALGORITMOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(933, 'Crear Curso Archivo', 'Nombre: INTRODUCCION A LA ELECTRONICA', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(934, 'Crear Curso Archivo', 'Nombre: LENGUAJE DE PROGRAMACION', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(935, 'Crear Curso Archivo', 'Nombre: CIRCUITOS DIGITALES II', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(936, 'Crear Curso Archivo', 'Nombre: ELECTRONICA II', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(937, 'Crear Curso Archivo', 'Nombre: TRABAJO DE GRADO TECNOLOGICO', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(938, 'Crear Curso Archivo', 'Nombre: ENERGIA SOLAR Y MEDIO AMBIENTE', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(939, 'Crear Curso Archivo', 'Nombre: REDES DE COMUNICACIÓN', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(940, 'Crear Curso Archivo', 'Nombre: INTRODUCCION AL CONTROL DE LOS SISTEMAS ROBOTICOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(941, 'Crear Curso Archivo', 'Nombre: CIRCUITOS ELECTRICOS I', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(942, 'Crear Curso Archivo', 'Nombre: CIRCUITOS ELECTRICOS II', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(943, 'Crear Curso Archivo', 'Nombre: ELECTRONICA I', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(944, 'Crear Curso Archivo', 'Nombre: CIRCUITOS DIGITALES I', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(945, 'Crear Curso Archivo', 'Nombre: OPERACIONES Y MANTENIMIENTO DE EQUIPOS INDUSTRIALES', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(946, 'Crear Curso Archivo', 'Nombre: ELECTRONICA INDUSTRITAL', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(947, 'Crear Curso Archivo', 'Nombre: SISTEMAS DE CONTROL', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(948, 'Crear Curso Archivo', 'Nombre: SISTEMAS DE COMUNICACIONES', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(949, 'Crear Curso Archivo', 'Nombre: PROGRAMACION GRAFICA LABVIEW', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1),
(950, 'Crear Curso Archivo', 'Nombre: PROTOTIPOS ELECTRONICOS', '2024-05-02', '22:04:42', '::1', 'Linux', 'Chrome', 1);

-- --------------------------------------------------------

--
-- Table structure for table `LogEstudiante`
--

CREATE TABLE `LogEstudiante` (
  `idLogEstudiante` int(11) NOT NULL,
  `accion` varchar(100) NOT NULL,
  `informacion` text NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(45) NOT NULL,
  `so` varchar(45) NOT NULL,
  `explorador` varchar(45) NOT NULL,
  `estudiante_idEstudiante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Profesor`
--

CREATE TABLE `Profesor` (
  `idProfesor` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `genero_idGenero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Profesor`
--

INSERT INTO `Profesor` (`idProfesor`, `nombre`, `correo`, `genero_idGenero`) VALUES
(1, 'CAICEDO JOSE ALEXANDER', 'jacaicedo@udistrital.edu.co\n', 2),
(2, 'OJEDA MARULANDA DAVID', 'dojedam@udistrital.edu.co\n', 2),
(3, 'RIAÑO PULIDO ANDRES JHOVANY', 'ajrianop@udistrital.edu.co\n', 2),
(4, 'GARATEJO ESCOBAR OLGA CECILIA', 'ocgaratejoe@udistrital.edu.co\n', 1),
(5, 'GARCIA ARRAZOLA ENRIQUE JOSE', 'ejgarciaa@udistrital.edu.co\n', 2),
(6, 'MEJIA ALVAREZ BIBIANA FARLLEY', 'ifmejiaa@udistrital.edu.co\n', 1),
(7, 'HURTADO MOJICA ROGER ANDERSON', 'rahurtadom@udistrital.edu.co\n', 2),
(8, 'CIPAGAUTA LARA ELSY CAROLINA', 'eccipagautal@udistrital.edu.co\n', 1),
(9, 'SANCHEZ COTTE EDGAR HUMBERTO', 'gi-viasypavimentos@udistrital.edu.co\n', 2),
(10, 'SABY BELTRAN JORGE ENRIQUE', 'jesabyb@udistrital.edu.co\n', 2),
(11, 'MEDINA GONZALEZ MAGDA ', 'mlmedinag@udistrital.edu.co\n', 1),
(12, 'SEGURA BOLIVAR JOHN ALEXANDER', 'jasegurab@udistrital.edu.co\n', 2),
(13, 'BARRANCO JONNY FERNELY', 'jfbarranco@udistrital.edu.co\n', 2),
(14, 'FELIZZOLA CONTRERAS RODOLFO', 'rfelizzolac@udistrital.edu.co\n', 2),
(15, 'AREVALO YEISON ANDRES', 'aarevaloy@udistrital.edu.co\n', 2),
(16, 'ZAMBRANO URBANO HAROLD LEON', 'hlzambranou@udistrital.edu.co\n', 2),
(17, 'PATIÑO SANCHEZ DANIEL FRANCISCO', 'dfpatinos@udistrital.edu.co\n', 2),
(18, 'LADINO MORENO EDGAR ORLANDO', 'eoladinom@udistrital.edu.co\n', 2),
(19, 'DIAZ AREVALO JOSE LUIS', 'jldiaza@udistrital.edu.co\n', 2),
(20, 'TERREROS CANTOR LUIS ORLANDO', 'loterrerosc@udistrital.edu.co\n', 2),
(21, 'ZAMUDIO HUERTAS EDUARDO', 'ezamudioh@udistrital.edu.co\n', 2),
(22, 'LOPEZ PALOMINO PAULO MARCELO', 'pmlopezp@udistrital.edu.co\n', 2),
(23, 'CARDOZO CHAUX MARIO JAVIER', 'mjcardozoc@udistrital.edu.co\n', 2),
(24, 'ALVIS FONSECA DIEGO ARMANDO', 'daalvisf@udistrital.edu.co\n', 2),
(25, 'VILLALOBOS MEDINA JUAN DE JESUS', 'jdvillalobosm@udistrital.edu.co\n', 2),
(26, 'LOSADA FIGUEROA CESAR FERNANDO', 'cflosadaf@udistrital.edu.co\n', 2),
(27, 'ALGARRA FAGUA DIANA STELLA', 'dsalgarraf@udistrital.edu.co\n', 1),
(28, 'VARGAS MADRID JUAN RAMON', 'jrvargas@udistrital.edu.co\n', 2),
(29, 'ROCHA SALAMANCA MAURICIO FERNANDO', 'mfrochas@udistrital.edu.co\n', 2),
(30, 'GOMEZ JIMENEZ CARLOS ARTURO', 'cagomezj@udistrital.edu.co\n', 2),
(31, 'MONTAÑA QUINTERO NILZON', 'nmontanaq@udistrital.edu.co\n', 2),
(32, 'LEON GARCIA JUAN CARLOS', 'jcleong@udistrital.edu.co\n', 2),
(33, 'HERRERA LUIS JORGE', 'ljherrera@udistrital.edu.co\n', 2),
(34, 'CHACON CEPEDA NORBERTO', 'nchaconc@udistrital.edu.co\n', 2),
(35, 'ROMAN CASTILLO RUTH ESPERANZA', 'rroman@udistrital.edu.co\n', 2),
(36, 'MARULANDA CELEITA JORGE ', 'jamarulandac@udistrital.edu.co\n', 2),
(37, 'SOLER CUBIDES FREIL EDUARDO', 'fesolerc@udistrital.edu.co\n', 2),
(38, 'LOPEZ LEZAMA JUAN CARLOS', 'jclopezl@udistrital.edu.co\n', 2),
(39, 'RINCON GUALDRON JOHAN ALEXANDER', 'jarincong@udistrital.edu.co\n', 2),
(40, 'RODRIGUEZ MONDRAGON LUIS FERNANDO', 'luferodriguezm@udistrital.edu.co\n', 2),
(41, 'RIVEROS GOMEZ VICTOR HUGO', 'vhriverosg@udistrital.edu.co\n', 2),
(42, 'CARDONA PERDOMO LUZ DINED', 'ldcardonap@udistrital.edu.co\n', 2),
(43, 'HERNANDEZ BELTRAN LEONARDO ANDRES', 'lahernandezb@udistrital.edu.co\n', 2),
(44, 'BONILLA ISAZA RUBEN DARIO', 'rdbonillai@udistrital.edu.co\n', 2),
(45, 'BEJARANO BARRETO EDWARD HERNANDO', 'ehbejaranob@udistrital.edu.co\n', 2),
(46, 'ROMERO SUAREZ WILSON LEONARDO', 'wlromeros@udistrital.edu.co\n', 2),
(47, 'GUERRERO SALAS HUMBERTO', 'hguerreros@udistrital.edu.co\n', 2),
(48, 'RODRIGUEZ FLORIAN ROMAN LEONARDO', 'rlrodriguezf@udistrital.edu.co\n', 2),
(49, 'PINZON RUEDA MARTHA EDITH', 'mepinzonr@udistrital.edu.co\n', 1),
(50, 'MAYORGA MORATO MANUEL ALFONSO', 'mamayorgam@udistrital.edu.co\n', 2),
(51, 'SANCHEZ GONZALEZ DIEGO', 'dsanchezg@udistrital.edu.co\n', 2),
(52, 'TOLEDO BALLEN ALFONSO', 'atoledob@udistrital.edu.co\n', 2),
(53, 'LUGO GONZALEZ CARLOS', 'clugog@udistrital.edu.co\n', 2),
(54, 'ROJAS OBANDO LUIS CARLOS', 'lucrojaso@udistrital.edu.co\n', 2),
(55, 'PARRA PEÑA JAVIER', 'jparrap@udistrital.edu.co\n', 2),
(56, 'GARZON CARREÑO PABLO EMILIO', 'pegarzonc@udistrital.edu.co\n', 2),
(57, 'ROMERO DUQUE GUSTAVO ANDRES', 'garomerod@udistrital.edu.co\n', 2),
(58, 'PINZON RUEDA WILSON ALEXANDER', 'wapinzon@udistrital.edu.co\n', 2),
(59, 'SUAREZ SERRANO MONICA YINETTE', 'mysuarezs@udistrital.edu.co\n', 1),
(60, 'OSPINA USAQUEN MIGUEL ANGEL', 'maospinau@udistrital.edu.co\n', 2),
(61, 'NAICIPA OTALORA JAIRO', 'jnaicipao@udistrital.edu.co\n', 2),
(62, 'PINTO CRUZ EDGAR ANTONIO', 'eapintoc@udistrital.edu.co\n', 2),
(63, 'GARAVITO LEON NELSON EDUARDO', 'negaravitol@udistrital.edu.co\n', 2),
(64, 'AVENDAÑO AVENDAÑO MARY SOL', 'masavendanoa@udistrital.edu.co\n', 1),
(65, 'DIAZ OSSA WILMAR ALBERTO', 'wadiazo@udistrital.edu.co\n', 2),
(66, 'RODRIGUEZ RINCON BRAHIAN FABIANS', 'bfrodriguezr@udistrital.edu.co\n', 2),
(67, 'RAHIM GARZON GLADYS P. ABDEL', 'garahimg@udistrital.edu.co\n', 1),
(68, 'AVENDAÑO AVENDAÑO CARLOS ALBERTO', 'caavendanoa@udistrital.edu.co\n', 2),
(69, 'PUENTES JAIRO HERNANDO', 'jhpuentes@udistrital.edu.co\n', 2),
(70, 'PEÑA TRIANA ROSA MARGARITA', 'rmpenat@udistrital.edu.co\n', 1),
(71, 'RAMIREZ SANCHEZ GLORIA', 'gramirezs@udistrital.edu.co\n', 1),
(72, 'APONTE GUTIERREZ JUAN CARLOS', 'jcaponteg@udistrital.edu.co\n', 2),
(73, 'BURITICA ARBOLEDA CLARA INES', 'ciburiticaa@udistrital.edu.co\n', 1),
(74, 'IBAÑEZ OLAYA HENRY FELIPE', 'hfibanezo@udistrital.edu.co\n', 2),
(75, 'RODRIGUEZ BARRERA MARIO ALBERTO', 'marodriguezb@udistrital.edu.co\n', 2),
(76, 'MARTINEZ SANTA FERNANDO', 'fmartinezs@udistrital.edu.co\n', 2),
(77, 'MARTINEZ SARMIENTO FREDY HERNAN', 'fhmartinezs@udistrital.edu.co\n', 2),
(78, 'GIRAL RAMIREZ DIEGO ARMANDO', 'dagiralr@udistrital.edu.co\n', 2),
(79, 'NOGUERA VEGA LUIS ANTONIO', 'lanoguerav@udistrital.edu.co\n', 2),
(80, 'RUBIANO CRUZ JONATHAN JAIR', 'jjrubianoc@udistrital.edu.co\n', 2),
(81, 'HERNANDEZ MARTINEZ HENRY ALBERTO', 'heahernandezm@udistrital.edu.co\n', 2),
(82, 'ROMERO MESTRE HENRY ALFONSO', 'haromerom@udistrital.edu.co\n', 2),
(83, 'ESPEJO MOJICA OSCAR GABRIEL', 'ogespejom@udistrital.edu.co\n', 2),
(84, 'MANTILLA BAUTISTA EDGAR JAVIER', 'ejmantillab@udistrital.edu.co\n', 2),
(85, 'CADENA MUÑOZ ERNESTO', 'ecadenam@udistrital.edu.co\n', 2),
(86, 'PEDRAZA MARTINEZ LUIS FERNANDO', 'lfpedrazam@udistrital.edu.co\n', 2),
(87, 'MARTINEZ RICAURTE OSCAR JAVIER', 'ojmartinezr@udistrital.edu.co\n', 2),
(88, 'ROJAS CASTELLAR LUIS ALEJANDRO', 'larojasc@udistrital.edu.co\n', 2),
(89, 'CORONEL SEGURA CESAR AUGUSTO', 'cacoronels@udistrital.edu.co\n', 2),
(90, 'MEDINA MONROY OSCAR MAURICIO', 'ommedinam@udistrital.edu.co\n', 2),
(91, 'ESLAVA BLANCO HERMES JAVIER', 'hjeslavab@udistrital.edu.co\n', 2),
(92, 'GOMEZ GOMEZ EDGAR LEONARDO', 'elgomezg@udistrital.edu.co\n', 2),
(93, 'CELY CALLEJAS JOSE DAVID', 'jdcely@udistrital.edu.co\n', 2),
(94, 'MORENO HERRERA DIEGO ARMANDO', 'damorenoh@udistrital.edu.co\n', 2),
(95, 'BLANCO SIERRA FABIAN DARIO', 'fdblancos@udistrital.edu.co\n', 2),
(96, 'ZABALA ALVAREZ JOHN FREDY', 'jfzabalaa@udistrital.edu.co\n', 2),
(97, 'PATERNINA DURAN JESUS MANUEL', 'jmpaterninad@udistrital.edu.co\n', 2),
(98, 'BERDUGO ROMERO EDWING OSWALDO', 'eoberdugor@udistrital.edu.co\n', 2),
(99, 'MONTEZUMA OBANDO GERMAN', 'gmontezumao@udistrital.edu.co\n', 2),
(100, 'LEGUIZAMON PAEZ MIGUEL ANGEL', 'maleguizamonp@udistrital.edu.co\n', 2),
(101, 'ARDILA ISMAEL ANTONIO', 'iaardila@udistrital.edu.co\n', 2),
(102, 'DE ARMAS COSTA RICARDO', 'rjdearmasc@udistrital.edu.co\n', 2),
(103, 'MONCADA ESPITIA ANDRES', 'amoncadae@udistrital.edu.co\n', 2),
(104, 'DAZA TORRES JAVIER ORLANDO', 'jdazat@udistrital.edu.co\n', 2),
(105, 'CASTANG MONTIEL GERARDO ALBERTO', 'gacastangm@udistrital.edu.co\n', 2),
(106, 'HUERTAS MARQUEZ DIEGO FERNANDO', 'dfhuertasm@udistrital.edu.co\n', 2),
(107, 'CASTAÑO TAMARA RICARDO', 'rcastanot@udistrital.edu.co\n', 2),
(108, 'BENAVIDES VEGA OSCAR ENRIQUE', 'oebenavidesv@udistrital.edu.co\n', 2),
(109, 'MEJIA VILLAMIL ANDRES ERNESTO', 'aemejiav@udistrital.edu.co\n', 2),
(110, 'HERNANDEZ RODRIGUEZ JORGE EDUARDO', 'jehernandezr@udistrital.edu.co\n', 2),
(111, 'PATIÑO BERNAL MARLON', 'marlonpb@udistrital.edu.co\n', 2),
(112, 'PEREZ TORRES YONATHAN ANDRES', 'yaperezt@udistrital.edu.co\n', 2),
(113, 'DEAZA TRIANA NELSON JAVIER', 'njdeazat@udistrital.edu.co\n', 2),
(114, 'AGREDA BASTIDAS ERNESTO', 'eagredab@udistrital.edu.co\n', 2),
(115, 'WILCHES CAÑON CARLOS FERNANDO', 'cfwilchesc@udistrital.edu.co\n', 2),
(116, 'NIVIA VARGAS ANGELICA MERCEDES', 'amniviav@udistrital.edu.co\n', 1),
(117, 'CABRALES CONTRERAS HUBER', 'hcabralesc@udistrital.edu.co\n', 2),
(118, 'GARZON CORREA MAGDA LORENA', 'mlgarzonc@udistrital.edu.co\n', 1),
(119, 'CRUZ GUAYACUNDO WILMER', 'wcruzg@udistrital.edu.co\n', 2),
(120, 'MUÑOZ BELLO NICOLAS GABRIEL', 'ngmunozb@udistrital.edu.co\n', 2),
(121, 'LOPEZ MARTINEZ GERMAN ARTURO', 'galopezm@udistrital.edu.co\n', 2),
(122, 'ROMERO ARIZA CARLOS ANDRES', 'caaromeroa@udistrital.edu.co\n', 2),
(123, 'SUAREZ DIAZ LUIS FRANCISCO', 'lfsuarezd@udistrital.edu.co\n', 2),
(124, 'CABRERA SANCHEZ JUAN DAVID', 'jdcabreras@udistrital.edu.co\n', 2),
(125, 'PARDO AMAYA DIEGO ALEJANDRO', 'dpardoa@udistrital.edu.co\n', 2),
(126, 'GONZALEZ COLMENARES MAURICIO', 'mgonzalezc@udistrital.edu.co\n', 2),
(127, 'ALVARADO MORENO ALEXANDER', 'aalvaradom@udistrital.edu.co\n', 2),
(128, 'PEREZ PEREZ PEDRO JAVIER', 'pjperezp@udistrital.edu.co\n', 2),
(129, 'RODRIGUEZ ARANGO EMILIANO', 'erodrigueza@udistrital.edu.co\n', 2),
(130, 'VELASCO PEÑA MARCO ANTONIO', 'mavelascop@udistrital.edu.co\n', 2),
(131, 'CHALA BUSTAMANTE EDGAR ARTURO', 'eachalab@udistrital.edu.co\n', 2),
(132, 'CASTIBLANCO HERNANDE DIEGO ALFREDO', 'diacastiblancoh@udistrital.edu.co\n', 2),
(133, 'MEDINA GAMBA ANDRES FELIPE', 'afmedinag@udistrital.edu.co\n', 2),
(134, 'PORRAS BOADA RICARDO ENRIQUE', 'reporrasb@udistrital.edu.co\n', 2),
(135, 'GUASCA GONZALEZ ANDRES GUILLERMO', 'agguascag@udistrital.edu.co\n', 2),
(136, 'RODRIGUEZ QUINTERO JAVIER GIOVANNY', 'jgrodriguezq@udistrital.edu.co\n', 2),
(137, 'HERNANDEZ GUTIERREZ JAIRO', 'jhernandezg@udistrital.edu.co\n', 2),
(138, 'VACCA GONZALEZ HAROLD', 'hvacca@udistrital.edu.co\n', 2),
(139, 'PANTOJA BENAVIDES JAIME FRANCISCO', 'jfpantojab@udistrital.edu.co\n', 2),
(140, 'PORRAS BOHADA JORGE EDUARDO', 'jeporrasb@udistrital.edu.co\n', 2),
(141, 'RAMIREZ ESCOBAR JORGE FEDERICO', 'jframirez@udistrital.edu.co\n', 2),
(142, 'FONSECA VELASQUEZ ALDEMAR', 'afonseca@udistrital.edu.co\n', 2),
(143, 'RODRIGUEZ MONTANA FERY PATRICIA', 'fprodriguezm@udistrital.edu.co\n', 1),
(144, 'GIRALDO RAMOS FRANK NIXON', 'fngiraldor@udistrital.edu.co\n', 2),
(145, 'ESCOBAR DIAZ ANDRES', 'aescobard@udistrital.edu.co\n', 2),
(146, 'DELGADILLO GOMEZ EDUARDO ALBERTO', 'eadelgadillog@udistrital.edu.co\n', 2),
(147, 'HIGUERA APARICIO JOSE MANUEL', 'jomhigueraa@udistrital.edu.co\n', 2),
(148, 'CAMACHO WILLIAM ALEXANDER', 'wacamacho@udistrital.edu.co\n', 2),
(149, 'GUTIERREZ MONTAÑA RUBEN EDUARDO', 'ruegutierrezm@udistrital.edu.co\n', 2),
(150, 'HERNANDEZ MORA JOHN RODRIGO', 'jrhernandezm@udistrital.edu.co\n', 2),
(151, 'CARANTON VELOZA JHON FREDDY', 'jfcarantonv@udistrital.edu.co\n', 2),
(152, 'CASTAÑO ARCILA MAURICIO', 'mcastanoa@udistrital.edu.co\n', 2),
(153, 'PEREZ MEDINA ELISEO', 'eperezm@udistrital.edu.co\n', 2),
(154, 'CASTRILLON CAMACHO ARJUNA', 'acastrillonc@udistrital.edu.co\n', 2),
(155, 'ZULUAGA ATEHORTUA IVAN DARIO', 'dzulu@udistrital.edu.co\n', 2),
(156, 'TENORIO BAUTISTA JULIETH ALEXANDRA', 'jatenoriob@udistrital.edu.co\n', 1),
(157, 'MONTEALEGRE MARTINEZ JOSELIN', 'jmontealiegrem@udistrital.edu.co\n', 2),
(158, 'CARVAJAL MARQUEZ EDILMO', 'ecarvajalm@udistrital.edu.co\n', 2),
(159, 'TORRES SUAREZ SERGIO ANDRES', 'satorress@udistrital.edu.co\n', 2),
(160, 'CLAVIJO JOYA JULIAN RENE', 'jrclavijoj@udistrital.edu.co\n', 2),
(161, 'GAVILANES JOSE DARIO', 'jdgavilanes@udistrital.edu.co\n', 2),
(162, 'LOPEZ CHACON YERSON ALEJANDRO', 'yalopezc@udistrital.edu.co\n', 2),
(163, 'BUITRAGO VALERO CARLOS JULIO', 'cjbuitrago@udistrital.edu.co\n', 2),
(164, 'CHAPARRO ARBOLEDA ARMANDO ALFREDO', 'aachaparroa@udistrital.edu.co\n', 2),
(165, 'MORENO TORRES CARLOS HUMBERTO', 'chmorenot@udistrital.edu.co\n', 2),
(166, 'LEON ESPINOSA EDNA ALEJANDRA', 'ealeone@udistrital.edu.co\n', 1),
(167, 'URIBE SUAREZ GERMAN', 'guribes@udistrital.edu.co\n', 2),
(168, 'RIOS MONTOYA JENNY ALEXANDRA', 'jariosm@udistrital.edu.co\n', 1),
(169, 'ULLOA RODRIGUEZ MANUEL ALFREDO', 'maulloar@udistrital.edu.co\n', 2),
(170, 'DE LA ROSA SILVA LOURDES CLARISSA', 'lrosas@udistrital.edu.co\n', 1),
(171, 'SALGADO DURAN OLGA PATRICIA', 'opsalgadod@udistrital.edu.co\n', 1),
(172, 'PINZON LOPEZ HECTOR ALFONSO', 'hapinzonl@udistrital.edu.co\n', 2),
(173, 'PASTRAN BELTRAN CARLOS GREGORIO', 'cgpastranb@udistrital.edu.co\n', 2),
(174, 'VILLOTA POSSO HERNANDO ANTONIO', 'havillotap@udistrital.edu.co\n', 2),
(175, 'MANRIQUE MUÑOZ ALVARO', 'amanriquem@udistrital.edu.co\n', 2),
(176, 'DIAZ ORTIZ VICTOR HUGO', 'vdiaz@udistrital.edu.co\n', 2),
(177, 'MEDINA GONZALEZ MAGDA LILIANA', 'mlmedinag@udistrital.edu.co\n', 1),
(178, 'ACEVEDO PEREZ JHON VLADIMIR', 'jvacevedop@udistrital.edu.co\n', 2),
(179, 'MANJARRES GARCIA GUILLERMO ANTONIO', 'gamanjarresg@udistrital.edu.co\n', 2),
(180, 'ZAMBRANO CASTRO MARTIN GERMAN', 'mgzambranoc@udistrital.edu.co\n', 2),
(181, 'JAIMES CONTRERAS LUIS ALBERTO', 'lajaimesc@udistrital.edu.co\n', 2),
(182, 'PINEDA JAIMES JORGE ARTURO', 'japinedaj@udistrital.edu.co\n', 2),
(183, 'FERNANDEZ CASTILLO RAFAEL ENRIQUE', 'refernandezc@udistrital.edu.co\n', 2),
(184, 'QUITIAN BUSTOS RUTH MERY', 'rmquitianb@udistrital.edu.co\n', 1),
(185, 'RUA YANEZ LAURA MARCELA', 'lmruay@udistrital.edu.co\n', 1),
(186, 'ZAMBRANO BERRIO REYNALDO', 'rzambranob@udistrital.edu.co\n', 2),
(187, 'UBAQUE BRITO KAROL YOBANI', 'kyubaqueb@udistrital.edu.co\n', 2),
(188, 'ESQUIVEL RAMIREZ RODRIGO ELIAS', 'reesquivelr@udistrital.edu.co\n', 2),
(189, 'BUENO PINZON MAURICIO', 'mbueno@udistrital.edu.co\n', 2),
(190, 'FORERO CLAVIJO JORGE ENRIQUE', 'joeforeroc@udistrital.edu.co\n', 2),
(191, 'VELANDIA RODRIGUEZ CARLOS HERBERTY', 'chvelandiar@udistrital.edu.co\n', 2),
(192, 'MENA SERNA MILTON', 'mmenas@udistrital.edu.co\n', 2),
(193, 'GARCIA MATEUS EDICSON GABRIEL', 'eggarciam@udistrital.edu.co\n', 2),
(194, 'MARTINEZ GONZALEZ RICARDO', 'rmartinezg@udistrital.edu.co\n', 2),
(195, 'GONZALEZ MALDONADO GEOVANNY', 'ggonzalezm@udistrital.edu.co\n', 2),
(196, 'CELIS MARIN NORVELY', 'ncelism@udistrital.edu.co\n', 2),
(197, 'BARROS SANCHEZ RICARDO DE JESUS', 'rjbarross@udistrital.edu.co\n', 2),
(198, 'URREGO RIVILLAS LIBIA SUSANA', 'lsurregor@udistrital.edu.co\n', 1),
(199, 'FINO SANDOVAL RAFAEL ALBERTO', 'rfinos@udistrital.edu.co\n', 2),
(200, 'LUENGAS CONTRERAS LELY ADRIANA', 'laluengasc@udistrital.edu.co\n', 1),
(201, 'RIVERA AGUILAR FREDY ALEXANDER', 'rariveraa@udistrital.edu.co\n', 2),
(202, 'JACINTO GOMEZ EDWAR', 'ejacintog@udistrital.edu.co\n', 2),
(203, 'MONTIEL ARIZA HOLMAN', 'hmontiela@udistrital.edu.co\n', 2),
(204, 'RODRIGUEZ LEON NAYIVER', 'narodriguezl@udistrital.edu.co\n', 1),
(205, 'HERNANDEZ JAIRO ORLANDO', 'jaohernandez@udistrital.edu.co\n', 2),
(206, 'FONSECA VELASQUEZ JIMMY', 'jfonsecav@udistrital.edu.co\n', 2),
(207, 'CENDALES ROMERO URIAS', 'ucendalesr@udistrital.edu.co\n', 2),
(208, 'ALVAREZ VIZCAINO NESTOR EDGAR', 'nealvarezv@udistrital.edu.co\n', 2),
(209, 'MONTANA MESA JORGE ENRIQUE', 'jemontanam@udistrital.edu.co\n', 2),
(210, 'VILLARRAGA RIAÑO ANDRES FELIPE', 'afvillarragar@udistrital.edu.co\n', 2),
(211, 'TORRES RIVEROS ALVARO ROGELIO', 'artorresr@udistrital.edu.co\n', 2),
(212, 'ARAUJO OVIEDO CLAUDIA MARINA', 'cmaraujoo@udistrital.edu.co\n', 1),
(213, 'BALANTA CASTILLA NEVIS DE JESUS', 'nbalantac@udistrital.edu.co\n', 1),
(214, 'URIBE BECERRA JOSE ERNESTO', 'euribeb@udistrital.edu.co\n', 2),
(215, 'NAVARRO MEJIA DAVID RAFAEL', 'drnavarrom@udistrital.edu.co\n', 2),
(216, 'VARGAS HERNANDEZ NAZLY', 'nvargash@udistrital.edu.co\n', 1),
(217, 'CAVANZO NISO DORIS GUISELA', 'dgcavanzon@udistrital.edu.co\n', 1),
(218, 'DIAZ MONTAÑO EVELYN IVONNE', 'eidiazm@udistrital.edu.co\n', 1),
(219, 'ALBA PULIDO RODRIGO', 'ralbap@udistrital.edu.co\n', 1),
(220, 'GUZMAN LAVERDE JORGE', 'jvguzmanl@udistrital.edu.co\n', 2),
(221, 'QUINTERO REYES RODRIGO', 'rquinteror@udistrital.edu.co\n', 2),
(222, 'BULLA PEREIRA EDWIN ALBERTO', 'eabullap@udistrital.edu.co\n', 2),
(223, 'RODRIGUEZ MONTAÑA NELSON EDUARDO', 'nerodriguezm@udistrital.edu.co\n', 2),
(224, 'LOPEZ GONZALEZ ROSENDO', 'rlopezg@udistrital.edu.co\n', 2),
(225, 'MADRID SOTO NANCY ESPERANZA', 'nemadrids@udistrital.edu.co\n', 1),
(226, 'CHAPARRO CHAPARRO FAOLAIN', 'fchaparroc@udistrital.edu.co\n', 2),
(227, 'MORENO PENAGOS CLAUDIA MABEL', 'cmmorenop@udistrital.edu.co\n', 1),
(228, 'OLEA SUAREZ DORIS MARLENE', 'dmoleas@udistrital.edu.co\n', 1),
(229, 'PARDO HEREDIA ANGELA', 'apardoh@udistrital.edu.co\n', 1),
(230, 'GONZALEZ SILVA RONALD', 'rgonzalezs@udistrital.edu.co\n', 2),
(231, 'MURCIA RODRIGUEZ WILLIAM ALEXANDER', 'wamurciar@udistrital.edu.co\n', 2),
(232, 'ALFEREZ RIVAS LUIS ERNESTO', 'lealferezr@udistrital.edu.co\n', 2),
(233, 'ROJAS QUIROGA ANIBAL NICOLAS', 'narojasq@udistrital.edu.co\n', 2),
(234, 'FONSECA MEDARDO', 'mfonseca@udistrital.edu.co\n', 2),
(235, 'DIAZ OSORIO MARCELA', 'mdiazo@udistrital.edu.co\n', 1),
(236, 'ORDOÑEZ RODRIGUEZ PAOLA DOLORES', 'pdordonezr@udistrital.edu.co\n', 1),
(237, 'AGUDELO CARDENAS ALEXANDER', 'aagudeloc@udistrital.edu.co\n', 2),
(238, 'ACOSTA SOLARTE PABLO ANDRES', 'paacostas@udistrital.edu.co\n', 2),
(239, 'AVELLANEDA LEAL ROSA MYRIAM', 'rmavellanedal@udistrital.edu.co\n', 1),
(240, 'BARRAGAN VIZCAYA FLOR ALBA', 'fabarraganb@udistrital.edu.co\n', 1),
(241, 'SANABRIA VERGARA YOANNY', 'ysanabriav@udistrital.edu.co\n', 2),
(242, 'PINILLA SUAREZ HECTOR ORLANDO', 'hopinillas@udistrital.edu.co\n', 2),
(243, 'ROMERO RODRIGUEZ JORGE ENRIQUE', 'joeromeror@udistrital.edu.co\n', 2),
(244, 'MORENO HURTADO ALEJANDRO', 'amorenoh@udistrital.edu.co\n', 2),
(245, 'SICACHA ROJAS GERMAN', 'gsicachar@udistrital.edu.co\n', 2),
(246, 'PASTRAN BELTRAN OSWALDO', 'opastranb@udistrital.edu.co\n', 2),
(247, 'ARIAS HENAO CAMILO ANDRES', 'carias@udistrital.edu.co\n', 2),
(248, 'MORENO ACOSTA HENRY', 'hmorenoa@udistrital.edu.co\n', 2),
(249, 'VANEGAS PARRA HENRY SAMIR', 'hsvanegasp@udistrital.edu.co\n', 2),
(250, 'CORREA MURILLO LUIS HERNANDO', 'lhcorream@udistrital.edu.co\n', 2),
(251, 'FORERO CASALLAS JOHN ALEJANDRO', 'jaforeroc@udistrital.edu.co\n', 2),
(252, 'DUEÑAS ROJAS JONNY RICARDO', 'jrduenasr@udistrital.edu.co\n', 2),
(253, 'MONROY CASTRO JUAN CARLOS', 'jucmonroyc@udistrital.edu.co\n', 2),
(254, 'MONROY DIAZ JOSE ISRAEL', 'jimonroyd@udistrital.edu.co\n', 2),
(255, 'RAMIREZ ACERO JULIO CESAR', 'jcramireza@udistrital.edu.co\n', 2),
(256, 'HURTADO CORTES LUINI LEONARDO', 'coordinador-otri@udistrital.edu.co\n', 2),
(257, 'MORENO FLAUTERO LUIS ALEJANDRO', 'lamorenof@udistrital.edu.co\n', 2),
(258, 'ROCHA CASTELLANOS DAIRO', 'dgrochac@udistrital.edu.co\n', 2),
(259, 'SALAZAR GUALDRON JUAN CARLOS', 'jcsalazarg@udistrital.edu.co\n', 2),
(260, 'PEÑA TRIANA JEAN YECID', 'jypenat@udistrital.edu.co\n', 2),
(261, 'CAÑAS VARON DAVID LEONARDO', 'dlcanasv@udistrital.edu.co\n', 2),
(262, 'CASTILLO HERNANDEZ JAIRO ERNESTO', 'jcastillo@udistrital.edu.co\n', 2),
(263, 'VELASQUEZ MOYA XIMENA AUDREY', 'xavelasquezm@udistrital.edu.co\n', 1),
(264, 'MARTINEZ CAMARGO DORA MARCELA', 'dmmartinezc@udistrital.edu.co\n', 1),
(265, 'RUSINQUE PADILLA ALEJANDRA MARITZA', 'amrusinquep@udistrital.edu.co\n', 1),
(266, 'DIAZ RODRIGUEZ MIRTA ROCIO', 'mrdiazr@udistrital.edu.co\n', 1),
(267, 'PEREZ CARO HERMENT', 'hperezc@udistrital.edu.co\n', 2),
(268, 'MARTINEZ BUITRAGO DIANA ISABEL', 'dimartinezb@udistrital.edu.co\n', 1),
(269, 'PALMA VANEGAS NELLY PAOLA', 'nppalmav@udistrital.edu.co\n', 1),
(270, 'VASQUEZ ARRIETA TOMAS ANTONIO', 'tavasquez@udistrital.edu.co\n', 2),
(271, 'LUGO GONZALEZ LUZ MARINA', 'lmlugog@udistrital.edu.co\n', 1),
(272, 'CHIQUIZA OCHOA MARIBEL', 'mchiquizao@udistrital.edu.co\n', 1),
(273, 'BERNAL GARZON EILEEN', 'eibernalg@udistrital.edu.co\n', 1),
(274, 'SERNA DIAZ JOSE LEONARDO', 'jlsernad@udistrital.edu.co\n', 2),
(275, 'CRUZ RAMIREZ JOHN ALEXANDER', 'jacruzr@udistrital.edu.co\n', 2),
(276, 'LOZADA ROMERO ROXMERY', 'rmlozadar@udistrital.edu.co\n', 1),
(277, 'MOSQUERA PALACIOS DARIN JAIRO', 'djmosquerap@udistrital.edu.co\n', 2),
(278, 'REYES MOZO JOSE VICENTE', 'jvreyesm@udistrital.edu.co\n', 2),
(279, 'BERNAL GOMEZ MIREYA', 'mbernalg@udistrital.edu.co\n', 1),
(280, 'PINZON NUÑEZ SONIA ALEXANDRA', 'spinzon@udistrital.edu.co\n', 1),
(281, 'RODRIGUEZ GUERRERO ROCIO', 'rrodriguezg@udistrital.edu.co\n', 2),
(282, 'SALAS RUIZ ROBERTO EMILIO', 'resalasr@udistrital.edu.co\n', 2),
(283, 'NOVOA TORRES NORBERTO', 'nnovoat@udistrital.edu.co\n', 2),
(284, 'CAVANZO NISSO GLORIA ANDREA', 'gacavanzon@udistrital.edu.co\n', 1),
(285, 'BECERRA CORREA NELSON REYNALDO', 'nrbecerrac@udistrital.edu.co\n', 2),
(286, 'VARGAS SANCHEZ NELSON ARMANDO', 'navargass@udistrital.edu.co\n', 2),
(287, 'ARCO MUÑOZ NOE', 'narcom@udistrital.edu.co\n', 2),
(288, 'WANUMEN SILVA LUIS FELIPE', 'lwanumen@udistrital.edu.co\n', 2),
(289, 'GOMEZ MORA MILLER', 'mgomezm@udistrital.edu.co\n', 2),
(290, 'RODRIGUEZ RODRIGUEZ JORGE ENRIQUE', 'jerodriguezr@udistrital.edu.co\n', 2),
(291, 'FUQUENE ARDILA HECTOR JULIO', 'hfuquene@udistrital.edu.co\n', 2),
(292, 'FLOREZ FERNANDEZ HECTOR ARTURO', 'haflorezf@udistrital.edu.co\n', 2),
(293, 'BAUTISTA HERRERA WILLIAM', 'wbautistah@udistrital.edu.co\n', 2),
(294, 'GARCIA QUEVEDO FRANCYA INES', 'figarciaq@udistrital.edu.co\n', 1),
(295, 'ROMERO GARCIA MARILUZ', 'mromerog@udistrital.edu.co\n', 1),
(296, 'HERNANDEZ GARCIA CLAUDIA LILIANA', 'clhernandezg@udistrital.edu.co\n', 1),
(297, 'GUEVARA BOLAÑOS JUAN CARLOS', 'jcguevarab@udistrital.edu.co\n', 2),
(298, 'VANEGAS CARLOS ALBERTO', 'cavanegas@udistrital.edu.co\n', 2),
(299, 'NAVARRO MEJIA WILMAN ENRIQUE', 'wnavarro@udistrital.edu.co\n', 2),
(300, 'FIERRO CASTAÑO PETER NELSON', 'pfierroc@udistrital.edu.co\n', 2),
(301, 'BAREÑO ROMERO EDICSON FERNEY', 'efbarenor@udistrital.edu.co\n', 2),
(302, 'ZAMBRANO CAVIEDES JUAN NEPOMUCENO', 'jzambranoc@udistrital.edu.co\n', 2),
(303, 'GORDO MUSKUS RICARDO', 'rgordom@udistrital.edu.co\n', 2),
(304, 'LUGO GONZALEZ ARMANDO', 'alugog@udistrital.edu.co\n', 2),
(305, 'HERNANDEZ SUAREZ CESAR AUGUSTO', 'cahernandezs@udistrital.edu.co\n', 2),
(306, 'MURILLO RONDON FRED GEOVANNY', 'fgmurillor@udistrital.edu.co\n', 2),
(307, 'PEREZ SANTOS ALEXANDRA SASHENKA', 'asperezs@udistrital.edu.co\n', 1),
(308, 'RAIRAN ANTOLINES JOSE DANILO', 'drairan@udistrital.edu.co\n', 2),
(309, 'GUEVARA VELANDIA GERMAN ANTONIO', 'gaguevarav@udistrital.edu.co\n', 2),
(310, 'ORTIZ SUAREZ HELMUT EDGARDO', 'heortizs@udistrital.edu.co\n', 2),
(311, 'HUERTAS SANABRIA JOSE RAFAEL', 'jrhuertass@udistrital.edu.co\n', 2),
(312, 'LEYTON VASQUEZ HERNANDO EVELIO', 'heleytonv@udistrital.edu.co\n', 2),
(313, 'CAMACHO VELANDIA MARISOL', 'mcamachov@udistrital.edu.co\n', 1),
(314, 'GARCES RENDON HUMBERTO ANTONIO', 'hagarcesr@udistrital.edu.co\n', 2),
(315, 'CASTELLANOS MORENO FABIO HERNANDO', 'fcastellanosm@udistrital.edu.co\n', 2),
(316, 'JARAMILLO VILLAMIZAR LENIN OSWALDO', 'lojaramillov@udistrital.edu.co\n', 2),
(317, 'BENITEZ SAZA CLAUDIA ROCIO', 'crbenitezs@udistrital.edu.co\n', 1),
(318, 'GOMEZ CASTILLO HARVEY', 'hagomezca@udistrital.edu.co\n', 2),
(319, 'RUIZ CAICEDO JAIRO ALFONSO', 'jruiz@udistrital.edu.co\n', 2),
(320, 'AVENDAÑO AVENDAÑO EUSEBIO', 'eavendanoa@udistrital.edu.co\n', 2),
(321, 'RODRIGUEZ TEQUI YUDY MARCELA', 'ymrodriguezt@udistrital.edu.co\n', 1),
(322, 'LOPEZ MACIAS JAVIER', 'jlopezm@udistrital.edu.co\n', 2),
(323, 'TELLO CASTAÑEDA MARTHA LUCIA', 'mtelloc@udistrital.edu.co\n', 1),
(324, 'CAMARGO CASALLAS ESPERANZA', 'ecamargoc@udistrital.edu.co\n', 1),
(325, 'JIMENEZ TRIANA ALEXANDER', 'ajimenezt2@udistrital.edu.co\n', 2),
(326, 'INFANTE MORENO WILSON', 'winfantem@udistrital.edu.co\n', 2),
(327, 'NOVOA ROLDAN KRISTEL SOLANGE', 'ksnovoar@udistrital.edu.co\n', 1),
(328, 'BERMUDEZ BOHORQUEZ GIOVANNI RODRIGO', 'gbermudez@udistrital.edu.co\n', 2),
(329, 'FANDIÑO JORGE ENRIQUE', 'jefandinot@udistrital.edu.co\n', 2);

-- --------------------------------------------------------

--
-- Table structure for table `Programa`
--

CREATE TABLE `Programa` (
  `idPrograma` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `facultad_idFacultad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Programa`
--

INSERT INTO `Programa` (`idPrograma`, `nombre`, `facultad_idFacultad`) VALUES
(1, 'Arte Danzario', 1),
(2, 'Artes Escénicas', 1),
(3, 'Artes Musicales', 1),
(4, 'Artes Plásticas y Visuales', 1),
(5, 'Ingeniería Catastral y Geodesia', 4),
(6, 'Ingeniería de Sistemas', 4),
(7, 'Ingeniería Electrónica', 4),
(8, 'Ingeniería Eléctrica', 4),
(9, 'Ingeniería Industrial', 4),
(10, 'Ingeniería Civil (por ciclos propedéuticos)', 6),
(11, 'Ingeniería de Producción (Por ciclos propedéuticos)', 6),
(12, 'Ingeniería Eléctrica (Por ciclos propedéuticos)', 6),
(13, 'Ingeniería en Telecomunicaciones (por ciclos propedéuticos)', 6),
(14, 'Ingeniería en Telemática (por ciclos propedéuticos)', 6),
(15, 'Ingeniería Mecánica (por ciclos propedéuticos)', 6),
(16, 'Ingeniería en Control y Automatización (por ciclos propedéuticos)', 6),
(17, 'Tecnología en Construcciones Civiles (Por ciclos propedéuticos)', 6),
(18, 'Tecnología en Electrónica (Por ciclos propedéuticos)', 6),
(19, 'Tecnología en Gestión de la Producción Industrial (Por ciclos propedéuticos)', 6),
(20, 'Tecnología en Mecánica Industrial (Por ciclos propedéuticos)', 6),
(21, 'Tecnología en Sistematización de Datos (Por ciclos propedéuticos)', 6),
(22, 'Tecnología en Electricidad de Media y Baja Tensión (por ciclos propedéuticos)', 6),
(23, 'Tecnología en Electrónica Industrial (Por ciclos propedéuticos)', 6),
(24, 'Archivística y Gestión de la Información Digital', 3),
(25, 'Comunicación Social y Periodismo', 3),
(26, 'Licenciatura en Biología', 3),
(27, 'Licenciatura en Ciencias Sociales', 3),
(28, 'Licenciatura en Educación Artística', 3),
(29, 'Licenciatura en Educación Infantil', 3),
(30, 'Licenciatura en Física', 3),
(31, 'Licenciatura en Humanidades y Lengua Castellana', 3),
(32, 'Licenciatura en Lenguas Extranjeras con Énfasis en Inglés', 3),
(33, 'Licenciatura en Matemáticas', 3),
(34, 'Licenciatura en Química', 3),
(35, 'Tecnología en Gestión Ambiental y Servicios Públicos', 5),
(36, 'Tecnología en Saneamiento Ambiental', 5),
(37, 'Tecnología en Levantamientos Topográficos', 5),
(38, 'Administración Ambiental', 5),
(39, 'Administración Deportiva', 5),
(40, 'Ingeniería Ambiental', 5),
(41, 'Ingeniería Forestal', 5),
(42, 'Ingeniería Sanitaria', 5),
(43, 'Ingenieria Topografica', 5),
(44, 'Biología', 2),
(45, 'Física', 2),
(46, 'Matemáticas', 2),
(47, 'Química', 2);

-- --------------------------------------------------------

--
-- Table structure for table `RangoDeNota`
--

CREATE TABLE `RangoDeNota` (
  `idRangoDeNota` int(11) NOT NULL,
  `valor` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `RangoDeNota`
--

INSERT INTO `RangoDeNota` (`idRangoDeNota`, `valor`) VALUES
(1, 'Menor que 30'),
(2, 'Entre 30 y 39'),
(3, 'Entre 40 y 49'),
(4, '50');

-- --------------------------------------------------------

--
-- Table structure for table `Referencia`
--

CREATE TABLE `Referencia` (
  `idReferencia` int(11) NOT NULL,
  `periodo` varchar(45) NOT NULL,
  `comentario` text NOT NULL,
  `pros` text DEFAULT NULL,
  `contras` text DEFAULT NULL,
  `fecha` date NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `profesor_idProfesor` int(11) NOT NULL,
  `curso_idCurso` int(11) NOT NULL,
  `estudiante_idEstudiante` int(11) NOT NULL,
  `programa_idPrograma` int(11) NOT NULL,
  `rangoDeNota_idRangoDeNota` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ReferenciaCriterio`
--

CREATE TABLE `ReferenciaCriterio` (
  `idReferenciaCriterio` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `referencia_idReferencia` int(11) NOT NULL,
  `criterio_idCriterio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `TipoDeDenuncia`
--

CREATE TABLE `TipoDeDenuncia` (
  `idTipoDeDenuncia` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Votacion`
--

CREATE TABLE `Votacion` (
  `idVotacion` int(11) NOT NULL,
  `voto` int(11) NOT NULL,
  `referencia_idReferencia` int(11) NOT NULL,
  `estudiante_idEstudiante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Administrador`
--
ALTER TABLE `Administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indexes for table `CategoriaDeCriterio`
--
ALTER TABLE `CategoriaDeCriterio`
  ADD PRIMARY KEY (`idCategoriaDeCriterio`);

--
-- Indexes for table `Criterio`
--
ALTER TABLE `Criterio`
  ADD PRIMARY KEY (`idCriterio`),
  ADD KEY `categoriaDeCriterio_idCategoriaDeCriterio` (`categoriaDeCriterio_idCategoriaDeCriterio`);

--
-- Indexes for table `Curso`
--
ALTER TABLE `Curso`
  ADD PRIMARY KEY (`idCurso`);

--
-- Indexes for table `Denuncia`
--
ALTER TABLE `Denuncia`
  ADD PRIMARY KEY (`idDenuncia`),
  ADD KEY `referencia_idReferencia` (`referencia_idReferencia`),
  ADD KEY `estudiante_idEstudiante` (`estudiante_idEstudiante`),
  ADD KEY `tipoDeDenuncia_idTipoDeDenuncia` (`tipoDeDenuncia_idTipoDeDenuncia`);

--
-- Indexes for table `Estudiante`
--
ALTER TABLE `Estudiante`
  ADD PRIMARY KEY (`idEstudiante`),
  ADD KEY `genero_idGenero` (`genero_idGenero`);

--
-- Indexes for table `Facultad`
--
ALTER TABLE `Facultad`
  ADD PRIMARY KEY (`idFacultad`);

--
-- Indexes for table `Genero`
--
ALTER TABLE `Genero`
  ADD PRIMARY KEY (`idGenero`);

--
-- Indexes for table `LogAdministrador`
--
ALTER TABLE `LogAdministrador`
  ADD PRIMARY KEY (`idLogAdministrador`),
  ADD KEY `administrador_idAdministrador` (`administrador_idAdministrador`);

--
-- Indexes for table `LogEstudiante`
--
ALTER TABLE `LogEstudiante`
  ADD PRIMARY KEY (`idLogEstudiante`),
  ADD KEY `estudiante_idEstudiante` (`estudiante_idEstudiante`);

--
-- Indexes for table `Profesor`
--
ALTER TABLE `Profesor`
  ADD PRIMARY KEY (`idProfesor`),
  ADD KEY `genero_idGenero` (`genero_idGenero`);

--
-- Indexes for table `Programa`
--
ALTER TABLE `Programa`
  ADD PRIMARY KEY (`idPrograma`),
  ADD KEY `facultad_idFacultad` (`facultad_idFacultad`);

--
-- Indexes for table `RangoDeNota`
--
ALTER TABLE `RangoDeNota`
  ADD PRIMARY KEY (`idRangoDeNota`);

--
-- Indexes for table `Referencia`
--
ALTER TABLE `Referencia`
  ADD PRIMARY KEY (`idReferencia`),
  ADD KEY `profesor_idProfesor` (`profesor_idProfesor`),
  ADD KEY `curso_idCurso` (`curso_idCurso`),
  ADD KEY `estudiante_idEstudiante` (`estudiante_idEstudiante`),
  ADD KEY `programa_idPrograma` (`programa_idPrograma`),
  ADD KEY `rangoDeNota_idRangoDeNota` (`rangoDeNota_idRangoDeNota`);

--
-- Indexes for table `ReferenciaCriterio`
--
ALTER TABLE `ReferenciaCriterio`
  ADD PRIMARY KEY (`idReferenciaCriterio`),
  ADD KEY `referencia_idReferencia` (`referencia_idReferencia`),
  ADD KEY `criterio_idCriterio` (`criterio_idCriterio`);

--
-- Indexes for table `TipoDeDenuncia`
--
ALTER TABLE `TipoDeDenuncia`
  ADD PRIMARY KEY (`idTipoDeDenuncia`);

--
-- Indexes for table `Votacion`
--
ALTER TABLE `Votacion`
  ADD PRIMARY KEY (`idVotacion`),
  ADD KEY `referencia_idReferencia` (`referencia_idReferencia`),
  ADD KEY `estudiante_idEstudiante` (`estudiante_idEstudiante`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Administrador`
--
ALTER TABLE `Administrador`
  MODIFY `idAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `CategoriaDeCriterio`
--
ALTER TABLE `CategoriaDeCriterio`
  MODIFY `idCategoriaDeCriterio` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Criterio`
--
ALTER TABLE `Criterio`
  MODIFY `idCriterio` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Curso`
--
ALTER TABLE `Curso`
  MODIFY `idCurso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=340;

--
-- AUTO_INCREMENT for table `Denuncia`
--
ALTER TABLE `Denuncia`
  MODIFY `idDenuncia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Estudiante`
--
ALTER TABLE `Estudiante`
  MODIFY `idEstudiante` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Facultad`
--
ALTER TABLE `Facultad`
  MODIFY `idFacultad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `Genero`
--
ALTER TABLE `Genero`
  MODIFY `idGenero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `LogAdministrador`
--
ALTER TABLE `LogAdministrador`
  MODIFY `idLogAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=951;

--
-- AUTO_INCREMENT for table `LogEstudiante`
--
ALTER TABLE `LogEstudiante`
  MODIFY `idLogEstudiante` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Profesor`
--
ALTER TABLE `Profesor`
  MODIFY `idProfesor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=330;

--
-- AUTO_INCREMENT for table `Programa`
--
ALTER TABLE `Programa`
  MODIFY `idPrograma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `RangoDeNota`
--
ALTER TABLE `RangoDeNota`
  MODIFY `idRangoDeNota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Referencia`
--
ALTER TABLE `Referencia`
  MODIFY `idReferencia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ReferenciaCriterio`
--
ALTER TABLE `ReferenciaCriterio`
  MODIFY `idReferenciaCriterio` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `TipoDeDenuncia`
--
ALTER TABLE `TipoDeDenuncia`
  MODIFY `idTipoDeDenuncia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Votacion`
--
ALTER TABLE `Votacion`
  MODIFY `idVotacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Criterio`
--
ALTER TABLE `Criterio`
  ADD CONSTRAINT `Criterio_ibfk_1` FOREIGN KEY (`categoriaDeCriterio_idCategoriaDeCriterio`) REFERENCES `CategoriaDeCriterio` (`idCategoriaDeCriterio`);

--
-- Constraints for table `Denuncia`
--
ALTER TABLE `Denuncia`
  ADD CONSTRAINT `Denuncia_ibfk_1` FOREIGN KEY (`referencia_idReferencia`) REFERENCES `Referencia` (`idReferencia`),
  ADD CONSTRAINT `Denuncia_ibfk_2` FOREIGN KEY (`estudiante_idEstudiante`) REFERENCES `Estudiante` (`idEstudiante`),
  ADD CONSTRAINT `Denuncia_ibfk_3` FOREIGN KEY (`tipoDeDenuncia_idTipoDeDenuncia`) REFERENCES `TipoDeDenuncia` (`idTipoDeDenuncia`);

--
-- Constraints for table `Estudiante`
--
ALTER TABLE `Estudiante`
  ADD CONSTRAINT `Estudiante_ibfk_1` FOREIGN KEY (`genero_idGenero`) REFERENCES `Genero` (`idGenero`);

--
-- Constraints for table `LogAdministrador`
--
ALTER TABLE `LogAdministrador`
  ADD CONSTRAINT `LogAdministrador_ibfk_1` FOREIGN KEY (`administrador_idAdministrador`) REFERENCES `Administrador` (`idAdministrador`);

--
-- Constraints for table `LogEstudiante`
--
ALTER TABLE `LogEstudiante`
  ADD CONSTRAINT `LogEstudiante_ibfk_1` FOREIGN KEY (`estudiante_idEstudiante`) REFERENCES `Estudiante` (`idEstudiante`);

--
-- Constraints for table `Profesor`
--
ALTER TABLE `Profesor`
  ADD CONSTRAINT `Profesor_ibfk_1` FOREIGN KEY (`genero_idGenero`) REFERENCES `Genero` (`idGenero`);

--
-- Constraints for table `Programa`
--
ALTER TABLE `Programa`
  ADD CONSTRAINT `Programa_ibfk_1` FOREIGN KEY (`facultad_idFacultad`) REFERENCES `Facultad` (`idFacultad`);

--
-- Constraints for table `Referencia`
--
ALTER TABLE `Referencia`
  ADD CONSTRAINT `Referencia_ibfk_1` FOREIGN KEY (`profesor_idProfesor`) REFERENCES `Profesor` (`idProfesor`),
  ADD CONSTRAINT `Referencia_ibfk_2` FOREIGN KEY (`curso_idCurso`) REFERENCES `Curso` (`idCurso`),
  ADD CONSTRAINT `Referencia_ibfk_3` FOREIGN KEY (`estudiante_idEstudiante`) REFERENCES `Estudiante` (`idEstudiante`),
  ADD CONSTRAINT `Referencia_ibfk_4` FOREIGN KEY (`programa_idPrograma`) REFERENCES `Programa` (`idPrograma`),
  ADD CONSTRAINT `Referencia_ibfk_5` FOREIGN KEY (`rangoDeNota_idRangoDeNota`) REFERENCES `RangoDeNota` (`idRangoDeNota`);

--
-- Constraints for table `ReferenciaCriterio`
--
ALTER TABLE `ReferenciaCriterio`
  ADD CONSTRAINT `ReferenciaCriterio_ibfk_1` FOREIGN KEY (`referencia_idReferencia`) REFERENCES `Referencia` (`idReferencia`),
  ADD CONSTRAINT `ReferenciaCriterio_ibfk_2` FOREIGN KEY (`criterio_idCriterio`) REFERENCES `Criterio` (`idCriterio`);

--
-- Constraints for table `Votacion`
--
ALTER TABLE `Votacion`
  ADD CONSTRAINT `Votacion_ibfk_1` FOREIGN KEY (`referencia_idReferencia`) REFERENCES `Referencia` (`idReferencia`),
  ADD CONSTRAINT `Votacion_ibfk_2` FOREIGN KEY (`estudiante_idEstudiante`) REFERENCES `Estudiante` (`idEstudiante`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
