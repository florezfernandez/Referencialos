<?php
$processed=false;
if(isset($_POST['insert'])){
    $user_ip = getenv('REMOTE_ADDR');
    $agent = $_SERVER["HTTP_USER_AGENT"];
    $browser = "-";
    if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
        $browser = "Internet Explorer";
    } else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Chrome";
    } else if (preg_match('/Edge\/\d+/', $agent) ) {
        $browser = "Edge";
    } else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Firefox";
    } else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Opera";
    } else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Safari";
    }    
    $localPath=$_FILES['archivo']['tmp_name'];
    $file = fopen($localPath, "r");
    $cantidadCursosAgregados = 0;
    $cantidadCursosNoAgregados = 0;
    $cursosAgregados = "Cursos agregados:<br>";
    $cursosNoAgregados = "Cursos no agregados:<br>";
    while(($linea = fgets($file)) != null) {
        $nombreCurso = str_replace("\n", "", $linea);
        $newCurso = new CursoExt("", $nombreCurso);
        if(!$newCurso -> existByNombre()){
            $newCurso -> insert();
            $cursosAgregados .= "Curso: " . $nombreCurso . "<br>";
            $cantidadCursosAgregados++;
        }else{
            $cursosNoAgregados .= "Curso: " . $nombreCurso . "<br>";
            $cantidadCursosNoAgregados++;
        }
    }
    if($cantidadCursosAgregados > 0){
        $logAdministrador = new LogAdministrador("","Crear Curso Archivo", $cursosAgregados . "<br>Cantidad: " . $cantidadCursosAgregados, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
        $logAdministrador -> insert();        
    }
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Crear Curso desde Archivo CSV</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ 
					    if($cantidadCursosAgregados > 0){
					        echo "<div class='alert alert-success' >" . $cursosAgregados . "<br>Cantidad: " . $cantidadCursosAgregados . "</div>";					        
					    }
					    if($cantidadCursosNoAgregados > 0){
					        echo "<div class='alert alert-info' >" . $cursosNoAgregados . "<br>Cantidad: " . $cantidadCursosNoAgregados . "</div>";
					    }
					    
					}?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("addons/curso/insertCursoArchivo.php") ?>" class="bootstrap-form needs-validation" enctype="multipart/form-data"  >
						<div class="form-group">
							<label>Archivo CVS (Curso)*</label>
							<input type="file" class="form-control-file" name="archivo" required />
						</div>
						<button type="submit" class="btn btn-info" name="insert">Crear</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
