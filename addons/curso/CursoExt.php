<?php
require_once ("addons/curso/CursoDAOExt.php");


class CursoExt extends Curso {
    private $cursoDAOExt;
    
    function __construct($pIdCurso = "", $pNombre = ""){
        parent::__construct($pIdCurso, $pNombre);
        $this -> cursoDAOExt = new CursoDAOExt($this -> idCurso, $this -> nombre);
        
    }
    
    function existByNombre(){
        $this -> connection -> open();
        $this -> connection -> run($this -> cursoDAOExt -> existByNombre());        
        $numRows = $this -> connection -> numRows();
        $this -> connection -> close();
        return $numRows == 1;
    }
    
}

?>