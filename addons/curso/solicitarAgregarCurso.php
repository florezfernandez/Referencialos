<?php
$processed=false;
$nombre="";
if(isset($_POST['nombre'])){
    $nombre=$_POST['nombre'];
}
$programa="";
if(isset($_POST['programa'])){
    $programa=$_POST['programa'];
}

if(isset($_POST['insert'])){
    $user_ip = getenv('REMOTE_ADDR');
    $agent = $_SERVER["HTTP_USER_AGENT"];
    $browser = "-";
    if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
        $browser = "Internet Explorer";
    } else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Chrome";
    } else if (preg_match('/Edge\/\d+/', $agent) ) {
        $browser = "Edge";
    } else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Firefox";
    } else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Opera";
    } else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Safari";
    }    
    $estudiante = new Estudiante($_SESSION['id']);
    $estudiante -> select();
    $programa = new Programa($programa);
    $programa -> select();
    $subject="Solicitud de Agregacion de Curso";
    $header = array(
        "From" => "Referencialos <contacto@referencialos.org>",
        "Reply-To" => $estudiante -> getNombre(). " " . $estudiante -> getApellido() . "<" . $estudiante -> getCorreo(). ">"
    );
    $message="Curso: " . $nombre . "\n";
    $message.="Programa: " . $programa -> getNombre() . "\n";    
    $message.="Estudiante: " . $estudiante -> getNombre() . " " . $estudiante -> getApellido() . "\n";
    $to = "Referencialos <contacto@referencialos.org>";
    mail($to, $subject, $message, $header);
    $logEstudiante = new LogEstudiante("","Solicitar Agregar Curso", "Nombre: " . $nombre . "; Programa: " . $programa -> getNombre() , date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
    $logEstudiante -> insert();
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Solicitar Agregar Curso</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					    <div class="alert alert-success" >Solicitud enviada correctamente. Un administrador de <i>Referencialos</i> le informará via correo electrónico el resultado de su solicitud.
					    </div>
					<?php } else { ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("addons/curso/solicitarAgregarCurso.php") ?>" class="bootstrap-form needs-validation" enctype="multipart/form-data"  >
						<div class="form-group">
							<label>Nombre*<br><span class="text-muted"><i>Ingrese el nombre del curso que desearía agregar en Referencialos</i></span></label>
							<input type="text" class="form-control" name="nombre" value="<?php echo $nombre ?>" required />
						</div>
						<div class="form-group">
							<label>Programa*<br><span class="text-muted"><i>Seleccione el programa académico del curso que desearía agregar</i></span></label>
							<select class="form-control" name="programa" id="programa" data-placeholder="Seleccione Programa" required >
								<option></option>
								<?php
								$objPrograma = new Programa();
								$programas = $objPrograma -> selectAllOrder("nombre", "asc");
								foreach($programas as $currentPrograma){
									echo "<option value='" . $currentPrograma -> getIdPrograma() . "'";
									if($currentPrograma -> getIdPrograma() == $programa){
										echo " selected";
									}
									echo ">" . $currentPrograma -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" required=>
                          <label class="form-check-label">Confirmo que la información que he ingresado es verdadera. En caso contrario, soy consciente que los administradores de <i>Referencialos</i> podrían inhabilitar mi cuenta.	</label>
                        </div>
						<button type="submit" class="btn btn-info" name="insert">Enviar Solicitud</button>
					</form>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('#programa').select2({});
</script>
