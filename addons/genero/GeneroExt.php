<?php
require_once ("addons/genero/GeneroDAOExt.php");


class GeneroExt extends Genero {
    private $generoDAOExt;
    
    function __construct($pIdGenero = "", $pNombre = ""){
        parent::__construct($pIdGenero, $pNombre);
        $this -> generoDAOExt = new GeneroDAOExt($this -> idGenero, $this -> nombre);
    }

    
    function selectByNombre(){
        $this -> connection -> open();
        $this -> connection -> run($this -> generoDAOExt -> selectByNombre());
        $result = $this -> connection -> fetchRow();
        $this -> connection -> close();
        $this -> idGenero = $result[0];
        $this -> nombre = $result[1];
    }
    
}

?>