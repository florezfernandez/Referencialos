<?php
require_once ("addons/programa/ProgramaDAOExt.php");


class ProgramaExt extends Programa {
    private $programaDAOExt;
    
    function __construct($pIdPrograma = "", $pNombre = "", $pFacultad = ""){
        parent::__construct($pIdPrograma, $pNombre, $pFacultad);
        $this -> programaDAOExt = new ProgramaDAOExt($this -> idPrograma, $this -> nombre, $this -> facultad);
        
    }
    
    function existByNombre(){
        $this -> connection -> open();
        $this -> connection -> run($this -> programaDAOExt -> existByNombre());        
        $numRows = $this -> connection -> numRows();
        $this -> connection -> close();
        return $numRows == 1;
    }
    
}

?>