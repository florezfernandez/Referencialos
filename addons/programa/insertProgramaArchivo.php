<?php
$processed=false;
if(isset($_POST['insert'])){
    $user_ip = getenv('REMOTE_ADDR');
    $agent = $_SERVER["HTTP_USER_AGENT"];
    $browser = "-";
    if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
        $browser = "Internet Explorer";
    } else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Chrome";
    } else if (preg_match('/Edge\/\d+/', $agent) ) {
        $browser = "Edge";
    } else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Firefox";
    } else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Opera";
    } else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Safari";
    }    
    $localPath=$_FILES['archivo']['tmp_name'];
    $file = fopen($localPath, "r");
    $cantidadProgramasAgregados = 0;
    $cantidadProgramasNoAgregados = 0;
    $programasAgregados = "Programas agregados:<br>";
    $programasNoAgregados = "Programas no agregados:<br>";
    while(($linea = fgets($file)) != null) {
        $datos = explode(";", str_replace("\n", "", $linea));
        $facultad = new FacultadExt("", $datos[0]);
        $facultad -> selectByNombre();
        $newPrograma = new ProgramaExt("", $datos[1], $facultad -> getIdFacultad());
        if(!$newPrograma -> existByNombre()){
            $newPrograma -> insert();
            $programasAgregados .= "Programa: " . $datos[1] . " en facultad: " . $datos[0] . "<br>";
            $cantidadProgramasAgregados++;
        }else{
            $programasNoAgregados .= "Programa: " . $datos[1] . " en facultad: " . $datos[0] . "<br>";
            $cantidadProgramasNoAgregados++;
        }
    }
    if($cantidadProgramasAgregados > 0){
        $logAdministrador = new LogAdministrador("","Crear Programa Archivo", $programasAgregados . "<br>Cantidad: " . $cantidadProgramasAgregados, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
        $logAdministrador -> insert();        
    }
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Crear Programa desde Archivo CSV</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ 
					    if($cantidadProgramasAgregados > 0){
					        echo "<div class='alert alert-success' >" . $programasAgregados . "<br>Cantidad: " . $cantidadProgramasAgregados . "</div>";					        
					    }
					    if($cantidadProgramasNoAgregados > 0){
					        echo "<div class='alert alert-info' >" . $programasNoAgregados . "<br>Cantidad: " . $cantidadProgramasNoAgregados . "</div>";
					    }
					    
					}?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("addons/programa/insertProgramaArchivo.php") ?>" class="bootstrap-form needs-validation" enctype="multipart/form-data"  >
						<div class="form-group">
							<label>Archivo CVS (Facultad; Programa)*</label>
							<input type="file" class="form-control-file" name="archivo" required />
						</div>
						<button type="submit" class="btn btn-info" name="insert">Crear</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
