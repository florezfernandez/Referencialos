<?php
$processed=false;
$error = 0;
$idReferencia = $_GET['idReferencia'];
$referencia = new Referencia($idReferencia);
$referencia -> select();
$tipoDeDenuncia="";
if(isset($_POST['tipoDeDenuncia'])){
    $tipoDeDenuncia=$_POST['tipoDeDenuncia'];
}
$argumento="";
if(isset($_POST['argumento'])){
    $argumento=$_POST['argumento'];
}
if(isset($_POST['insert'])){
    if($argumento == ""){
        $error = 1;
    }else{
    $user_ip = getenv('REMOTE_ADDR');
    $agent = $_SERVER["HTTP_USER_AGENT"];
    $browser = "-";
    if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
        $browser = "Internet Explorer";
    } else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Chrome";
    } else if (preg_match('/Edge\/\d+/', $agent) ) {
        $browser = "Edge";
    } else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Firefox";
    } else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Opera";
    } else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Safari";
    }
    $estudiante = new Estudiante($_SESSION['id']);
    $estudiante -> select();
    $tipoDeDenuncia = new TipoDeDenuncia($tipoDeDenuncia);
    $tipoDeDenuncia -> select();
    $newDenuncia = new Denuncia("", $argumento, date("Y-m-d"), $referencia -> getIdReferencia(), $estudiante -> getIdEstudiante(), $tipoDeDenuncia -> getIdTipoDeDenuncia());
    $newDenuncia -> insert();
    $subject="Denuncia de referencia";
    $header = array(
        "From" => "Referencialos <contacto@referencialos.org>",
        "Reply-To" => $estudiante -> getNombre(). " " . $estudiante -> getApellido() . "<" . $estudiante -> getCorreo(). ">"
    );
    $message="El estudiante " . $estudiante -> getNombre() . " " . $estudiante -> getApellido() . " ha hecho una denuncia a la siguiente referencia:\n";
    $message.="Profesor: " . $referencia -> getProfesor() -> getNombre() . "\n";
    $message.="Curso: " . $referencia -> getCurso() -> getNombre() . "\n";
    $message.="Programa: " . $referencia -> getPrograma() -> getNombre() . "\n";
    $message.="Estudiante: " . $referencia -> getEstudiante() -> getNombre() . " " . $referencia -> getEstudiante() -> getApellido() . "\n";
    $message.="Comentario de la referencia: " . $referencia -> getComentario() . "\n";
    $message.="Argumento de la denuncia: " . $argumento . "\n";
    $message.="Tipo de denuncia: " . $tipoDeDenuncia -> getNombre() . "\n";
    $to = "Referencialos <contacto@referencialos.org>";
    mail($to, $subject, $message, $header);
    $logEstudiante = new LogEstudiante("","Denunciar Referencia", "Comentario de la referencia: " . $referencia -> getComentario() . "; Argumento: " . $argumento . "; Tipo de denuncia: " . $tipoDeDenuncia -> getNombre(), date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
    $logEstudiante -> insert();
    $processed=true;
    }
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Denunciar Referencia</h4>
				</div>
				<div class="card-body">
				<?php if($processed){ ?>
					    <div class="alert alert-success" >Su denuncia ha sido enviada correctamente. Un administrador de <i>Referencialos</i> le informará via correo electrónico el resultado de su denuncia.
					    </div>
					<?php } else { if($error == 1){ ?>
					<div class="alert alert-danger" >Por favor ingrese un argumento. 
					</div>                    
                    <?php } ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("addons/denuncia/denunciarReferencia.php") ?>&idReferencia=<?php echo $idReferencia?>" class="bootstrap-form needs-validation" enctype="multipart/form-data"  >
						<div class="form-group">
							<label>Tipo de denuncia*<br><span class="text-muted"><i>Seleccione el motivo de la denuncia</i></span></label>
							<select class="form-control" name="tipoDeDenuncia" id="tipoDeDenuncia" data-placeholder="Seleccione tipo de denuncia" required >
								<option></option>
								<?php
								$objTipoDeDenuncia = new TipoDeDenuncia();
								$tipoDeDenuncias = $objTipoDeDenuncia -> selectAllOrder("nombre", "asc");
								foreach($tipoDeDenuncias as $currentTipoDeDenuncia){
									echo "<option value='" . $currentTipoDeDenuncia -> getIdTipoDeDenuncia() . "'";
									if($currentTipoDeDenuncia -> getIdTipoDeDenuncia() == $tipoDeDenuncia){
										echo " selected";
									}
									echo ">" . $currentTipoDeDenuncia -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
						<label>Argumento*<br><span class="text-muted"><i>Ingrese un argumento describiendo la razon por el cual hará la denuncia</i></span></label>
						<textarea class="form-control" id="argumento" name="argumento" required><?php echo $argumento ?></textarea>						
					</div>				
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" required=>
                          <label class="form-check-label">Confirmo que la información que he ingresado es verdadera. En caso contrario, soy consciente que los administradores de <i>Referencialos</i> podrían inhabilitar mi cuenta.	</label>
                        </div>
						<button type="submit" class="btn btn-info" name="insert">Enviar Denuncia</button>
					</form>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('#tipoDeDenuncia').select2({});
</script>
