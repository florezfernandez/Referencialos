<?php 
$idProfesor = $_GET['idProfesor'];
$profesor = new Profesor($idProfesor);
$profesor -> select();
$referenciaExt = new ReferenciaExt("", "", "", "", "", "", "", $idProfesor);
$results = $referenciaExt -> referenciaByCursoAndProfesor();
if($_SERVER['HTTP_HOST'] == "localhost"){
    $url = "http://" . $_SERVER['HTTP_HOST'] . "/Referencialos/a/?i=" . $idProfesor;
}else{
    $url = "https://" . $_SERVER['HTTP_HOST'] . "/a/?i=" . $idProfesor;
}
?>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Cursos con referencias del profesor <?php echo $profesor -> getNombre() ?></h4><button id="copiar" type="button" class="btn btn-primary" data-toggle='tooltip' data-placement='right' data-original-title='Copiar URL corta en el portapapeles'>Copiar URL</button>
		</div>
		<div class="card-body">
			<div class="container-fluid">
				<div class="row">
					<div class="col">
                        <div class="table-responsive">
                        <h5><?php echo count($results) ?> registros encontrados</h5>
                        <table class="table table-striped table-hover">
                        	<thead>
                        		<tr>
                        			<th><a href="#" data-toggle='tooltip' data-placement='right' data-original-title='Ordenar por nombre' >Nombre <span class='fas fa-sort'></span></a></th>
                        			<th class="text-center"><a href="#" data-toggle='tooltip' data-placement='left' data-original-title='Ordenar por cantidad de referencias' >Cantidad de Referencias <span class='fas fa-sort'></span></a></th>
                        			<th class="text-center"><a href="#" data-toggle='tooltip' data-placement='left' data-original-title='Ordenar por promedio general' >Promedio General <span class='fas fa-sort'></span></a></th>
                        			<th class="text-center"><a href="#" data-toggle='tooltip' data-placement='left' data-original-title='Ordenar por promedio de criterios académicos' >Promedio de Criterios Académicos <span class='fas fa-sort'></span></a></th>
                        			<th class="text-center"><a href="#" data-toggle='tooltip' data-placement='left' data-original-title='Ordenar por promedio de criterios sociales' >Promedio de Criterios Sociales <span class='fas fa-sort'></span></a></th>
                        			<th></th>
                        		</tr>
                        	</thead>
                        	</tbody>
                        		<?php
                        		foreach ($results as $currentResults){ 
                        		    echo "<td>" . $currentResults[1]. "</td>";
                        		    echo "<td class='text-center'>" . $currentResults[2]. "</td>";
                        		    echo "<td class='text-center'>" . $currentResults[3]. "</td>";
                        		    echo "<td class='text-center'>" . $currentResults[4]. "</td>";
                        		    echo "<td class='text-center'>" . $currentResults[5]. "</td>";
                        		    echo "<td class='text-right' nowrap>";
                        		    echo "<a href='?pid=" . base64_encode("addons/referencia/insertReferenciaEstudiante.php") . "&idCurso=" . $currentResults[0] . "&idProfesor=" . $idProfesor . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Crear Referencia' ></span></a> ";
                        		    echo "<a href='?pid=" . base64_encode("addons/referencia/viewReferenciasProfesorCurso.php") . "&idCurso=" . $currentResults[0] . "&idProfesor=" . $idProfesor . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Ver referencias' ></span></a> ";
                        		    echo "</td>";
                        		    echo "</tr>";
                        		}
                        		?>
                        	</tbody>
                        </table>
                        </div>					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('th').click(function(){
    var table = $(this).parents('table').eq(0)
    var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
    this.asc = !this.asc
    if (!this.asc){
    	rows = rows.reverse()
    }
    for (var i = 0; i < rows.length; i++){
    	table.append(rows[i])
    }
})
function comparer(index) {
    return function(a, b) {
        var valA = getCellValue(a, index)
        valB = getCellValue(b, index)
        return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
    }
}
function getCellValue(row, index){ 
	return $(row).children('td').eq(index).text() 
}
$("#copiar").on("click", function() {
	navigator.clipboard.writeText('<?php echo $url ?>');	
	$("#copiar").text("URL copiada");
});
</script>
