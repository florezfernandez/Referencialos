<?php 
$idProfesor = $_GET['idProfesor'];
$idCurso = $_GET['idCurso'];
$curso = new Curso($idCurso);
$curso -> select();
$profesor = new Profesor($idProfesor);
$profesor -> select();
$referenciaExt = new ReferenciaExt("", "", "", "", "", "", "", $idProfesor, $idCurso);
$result = $referenciaExt -> referenciaByProfesorAndCurso();
$resultCriterios = $referenciaExt -> criteriosByProfesorAndCurso();
$referencias = $referenciaExt -> detalleReferenciasByProfesorAndCursoDosPrimeras();
?>
<div class="container">
	<div class="card border-primary">
		<div class="card-header">
			<h4 class="card-title">Profesor: <?php echo $profesor -> getNombre() ?> <br>Curso: <?php echo $curso -> getNombre() ?><br>
			<a href="addonModalWordCloud.php?idProfesor=<?php echo $idProfesor ?>&idCurso=<?php echo $idCurso ?>" data-toggle="modal" data-target="#modalWordCloud"><span class="fas fa-cloud" data-toggle="tooltip" data-placement="top" data-original-title="Ver nube de palabras"></span></a></h4>
			<div class="row">
				<div class="col-lg-4 col-sm-12">
        			<h6>Cantidad de referencias: <strong><?php echo $result[0] ?></strong></h6>
        			<h6>Promedio General: <strong><?php echo $result[1] ?></strong></h6>
        			<h6>Promedio de Criterios Académicos: <strong><?php echo $result[2] ?></strong></h6>
        			<h6>Promedio de Criterios Sociales: <strong><?php echo $result[3] ?></strong></h6>				
				</div>
				<div class="col-lg-4 col-sm-12">
        			<h5>Criterios Académicos</h5>
        			<ul>
        			<?php 
        			foreach ($resultCriterios as $currentResults) {
        			    if($currentResults[2] == 1){
        			        echo "<li>" . $currentResults[0] . ": " . $currentResults[1] . "</li>";
        			    }
        			}
        			?>
        			</ul>
				</div>
				<div class="col-lg-4 col-sm-12">
        			<h5>Criterios Sociales</h5>
        			<ul>
        			<?php 
        			foreach ($resultCriterios as $currentResults) {
        			    if($currentResults[2] == 2){
        			        echo "<li>" . $currentResults[0] . ": " . $currentResults[1] . "</li>";
        			    }
        			}
        			?>
        			</ul>
				</div>
			</div>
		</div>		
		<div class="card-body">
			<div id="results">
			<?php 
			$num = 1;
			foreach ($referencias as $currentReferencia) {
			    echo "<div class='card border-primary'>";
			    echo "<div class='card-header'><h5>Referencia " . $num . "</h5></div>";
			    echo "<div class='card-body'>";
			    echo "<div class='row'>";
			    echo "<div class='col-lg-4 col-sm-12'>";
			    echo "<strong>Periodo:</strong> " . $currentReferencia[4] . "<br>";
			    echo "<strong>Fecha:</strong> " . $currentReferencia[5] . "<br>";
			    echo "<strong>Programa:</strong> " . $currentReferencia[6] . "<br>";
			    echo "<strong>Nota obtenida:</strong> " . $currentReferencia[7] . "<br>";
			    echo "</div>";
			    echo "<div class='col-lg-4 col-sm-12'>";
			    $referencia = new ReferenciaExt($currentReferencia[0]);
			    $criteriosAcademicos = $referencia -> criteriosAcademicosByReferencia();
			    $criteriosSociales = $referencia -> criteriosSocialesByReferencia();
			    echo "<strong>Criterios Académicos:</strong>";
			    echo "<ul>";
			    foreach ($criteriosAcademicos as $currentCriterioAcademico) {
			        echo "<li>" . $currentCriterioAcademico[0] . ": <strong>" . $currentCriterioAcademico[1] . "</strong></li>";
			    };
			    echo "</ul>";			    
			    echo "</div>";
			    echo "<div class='col-lg-4 col-sm-12'>";
			    echo "<strong>Criterios Sociales:</strong>";
			    echo "<ul>";
			    foreach ($criteriosSociales as $currentCriterioSocial) {
			        echo "<li>" . $currentCriterioSocial[0] . ": <strong>" . $currentCriterioSocial[1] . "</strong></li>";
			    };
			    echo "</ul>";			    
			    echo "</div>";
			    echo "</div>";
			    echo "<hr>";
			    echo "<strong>Comentario:</strong><br>";
			    echo $currentReferencia[1];
			    echo "<hr>";
			    echo "<strong>Pros:</strong><br>";
			    echo $currentReferencia[2];
			    echo "<hr>";
			    echo "<strong>Contras:</strong><br>";
			    echo $currentReferencia[3];
			    echo "<hr>";
			    $votacion = new VotacionExt("", "", "", $currentReferencia[0], $_SESSION['id']);			    
			    $votos = array(
			        "-1" => "Estoy en desacuerdo",
			        "0" => "No he votado",
			        "1" => "Estoy de acuerdo"
			    );
			    echo "<span id='votacion" . $currentReferencia[0] . "'><span class='fas fa-thumbs-up' data-toggle='tooltip' data-placement='top' data-original-title='De acuerdo' ></span> " . $votacion -> getCantidadVotos(1) . " - <span class='fas fa-thumbs-down' data-toggle='tooltip' data-placement='top' data-original-title='Desacuerdo' ></span> " . $votacion -> getCantidadVotos(-1) . " - Mi voto: " . $votos[$votacion -> votoByReferenciaAndEstudiante()] . "</span>";			    
			    echo "</div>";
                echo "<div class='card-footer text-center'>";
                echo "<button id='bAcuerdo" . $currentReferencia[0] . "' type='button' class='btn btn-primary' data-toggle='tooltip' data-placement='top' data-original-title='Estoy de acuerdo' ><span class='fas fa-thumbs-up' ></span></button> ";
                echo "<button id='bDesacuerdo" . $currentReferencia[0] . "' type='button' class='btn btn-primary' data-toggle='tooltip' data-placement='top' data-original-title='Estoy en desacuerdo'><span class='fas fa-thumbs-down' ></span></button> ";                
                echo "<a href='?pid=" . base64_encode("addons/denuncia/denunciarReferencia.php") . "&idReferencia=" . $currentReferencia[0] . "' class='btn btn-danger' data-toggle='tooltip' data-placement='top' data-original-title='Denunciar'><span class='fas fa-flag' ></span></a>";
                echo "</div>";
			    echo "</div><br>";
			    $num++;
			}			
			?>		
			</div>	
		</div>
	</div>
</div>
<input type="hidden" id="num" value="3">
<script>
$(document).ready(function(){
<?php 
foreach ($referencias as $currentReferencia) {
    echo "\t$(\"#bAcuerdo" . $currentReferencia[0] . "\").click(function(){\n";
    echo "\t\tpath = \"indexAjax.php?pid=" . base64_encode("addons/votacion/votarAjax.php") . "&idReferencia=" . $currentReferencia[0] . "&idEstudiante=" . $_SESSION['id'] . "&voto=1\";\n";
    echo "\t\t$(\"#votacion" . $currentReferencia[0] . "\").load(path);\n";    
	echo "\t});\n";    
    echo "\t$(\"#bDesacuerdo" . $currentReferencia[0] . "\").click(function(){\n";
    echo "\t\tpath = \"indexAjax.php?pid=" . base64_encode("addons/votacion/votarAjax.php") . "&idReferencia=" . $currentReferencia[0] . "&idEstudiante=" . $_SESSION['id'] . "&voto=-1\";\n";
    echo "\t\t$(\"#votacion" . $currentReferencia[0] . "\").load(path);\n";
    echo "\t});\n";
}
?>
});
</script>
<div class="modal fade" id="modalWordCloud" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
	
    $(window).scroll(function() {
		if($(window).scrollTop() == $(document).height() - $(window).height()) {        	
			var path = "indexAjax.php?pid=<?php echo base64_encode("addons/referencia/appendReferenciaProfesorAjax.php"); ?>&idProfesor="+<?php echo $idProfesor?>+"&idCurso=<?php echo $idCurso ?>&n=" + $("#num").val();
			$("#num").val(parseInt($("#num").val())+1);
			$(function () {
                $.get(path, function (data) {
                    $("#results").append(data);
                });
            });
        }
    });	
</script>