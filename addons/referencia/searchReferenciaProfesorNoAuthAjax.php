<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th>Nombre</th>
			<th class="text-center">Cantidad de referencias</th>
			<th class="text-center">Promedio General</th>
			<th class="text-center">Promedio de Criterios Académicos</th>
			<th class="text-center">Promedio de Criterios Sociales</th>
			<th class="text-center"></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$profesor = new ProfesorExt();
		$searchText = $_GET['search'];
		$searchTexts = explode(" ", $searchText);		
		$profesors = $profesor -> searchMultipleWords($searchTexts);
		$counter = 1;
		foreach ($profesors as $currentProfesor) {
			echo "<tr><td>" . $counter . "</td>";
			$name = $currentProfesor -> getNombre();
			foreach ($searchTexts as $text){
			    $pos = stripos($name, $text);
			    $name = substr($name, 0, $pos) . "<strong>" . substr($name, $pos, strlen($text)) . "</strong>" . substr($name, $pos + strlen($text));			    
			}			
			echo "<td>" . $name . "</td>";
			$referenciaExt = new ReferenciaExt("", "", "", "", "", "", "", $currentProfesor -> getIdProfesor());
			$result = $referenciaExt -> buscarProfesorByReferencia();
			echo "<td class='text-center'>" . $result[0] . "</td>";
			if($result[0] == 0){
			    echo "<td class='text-center'> - </td>";
			    echo "<td class='text-center'> - </td>";
			    echo "<td class='text-center'> - </td>";
			    echo "<td class='text-right' nowrap>";
			}else{
			    echo "<td class='text-center'>" . $result[1] . "</td>";
			    echo "<td class='text-center'>" . $result[2] . "</td>";
			    echo "<td class='text-center'>" . $result[3] . "</td>";
			    echo "<td class='text-right' nowrap>";
			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewSearchReferenciaProfesorNoAuth.php") . "&idProfesor=" . $currentProfesor -> getIdProfesor() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Cursos' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
