<?php
require_once ("addons/referencia/ReferenciaDAOExt.php");


class ReferenciaExt extends Referencia {
    private $referenciaDAOExt;
    
    function __construct($pIdReferencia = "", $pComentario = "", $pPros = "", $pContras = "", $pPeriodo = "", $pFecha = "", $pEstado = "", $pProfesor = "", $pCurso = "", $pEstudiante = "", $pPrograma = "", $pRangoDeNota = ""){
        parent::__construct($pIdReferencia, $pComentario, $pPros, $pContras, $pPeriodo, $pFecha, $pEstado, $pProfesor, $pCurso, $pEstudiante, $pPrograma, $pRangoDeNota);
        $this -> referenciaDAOExt = new ReferenciaDAOExt($this -> idReferencia, $this -> comentario, $this -> pros, $this -> contras, $this -> periodo, $this -> fecha, $this -> estado, $this -> profesor, $this -> curso, $this -> estudiante, $this -> programa, $this -> rangoDeNota);
        
    }
    
    function countByCurso(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> countByCurso());        
        $result = $this -> connection -> fetchRow();
        $this -> connection -> close();
        return $result[0];
    }
    function rankingByCurso(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> rankingByCurso());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){            
            array_push($referencias, array($result[0], $result[1], $result[2], $result[3], $result[4], $result[5]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    
    function referenciaByProfesorAndCurso(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> referenciaByProfesorAndCurso());
        $result = $this -> connection -> fetchRow();
        $referencia = array($result[0], $result[1], $result[2], $result[3]);
        $this -> connection -> close();
        return $referencia;
    }
    
    function criteriosByProfesorAndCurso(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> criteriosByProfesorAndCurso());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1], $result[2]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    
    function detalleReferenciasByProfesorAndCurso(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> detalleReferenciasByProfesorAndCurso());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    function detalleReferenciasByProfesorAndCursoDosPrimeras(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> detalleReferenciasByProfesorAndCursoDosPrimeras());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    function detalleReferenciasByProfesorAndCursoUnaMas($posicion){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> detalleReferenciasByProfesorAndCursoUnaMas($posicion));
        $result = $this -> connection -> fetchRow();
        return $result;
    }
    function buscarProfesorByReferencia(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> buscarProfesorByReferencia());
        $result = $this -> connection -> fetchRow();
        $this -> connection -> close();
        return array($result[0],$result[1],$result[2],$result[3]);
    }
    function referenciaByCursoAndProfesor(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> referenciaByCursoAndProfesor());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1], $result[2], $result[3], $result[4], $result[5]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    function countByPrograma(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> countByPrograma());
        $result = $this -> connection -> fetchRow();
        $this -> connection -> close();
        return $result[0];
    }
    function rankingByPrograma() {
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> rankingByPrograma());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1], $result[2], $result[3], $result[4], $result[5]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    function criteriosAcademicosByReferencia(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> criteriosAcademicosByReferencia());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    function criteriosSocialesByReferencia(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> criteriosSocialesByReferencia());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    function getCorreosEstudiantesConReferencia($idPrograma){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> getCorreosEstudiantesConReferencia($idPrograma));
        $correos = "";
        while ($result = $this -> connection -> fetchRow()){
            $correos .= $result[0] . ", ";
        }
        $this -> connection -> close();
        return substr($correos, 0, strlen($correos)-2);
    }
    function detallePrimerReferenciasByProfesorAndCurso(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> detallePrimerReferenciasByProfesorAndCurso());        
        return $this -> connection -> fetchRow();        
    }
    function getActividadGeneralCincoPrimeras(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> getActividadGeneralCincoPrimeras());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $result[8]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    function getActividadGeneralCincoMas($posicion){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> getActividadGeneralCincoMas($posicion));
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $result[8]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    function getActividadReferenciaEstudiante(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> getActividadReferenciaEstudiante());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    function getActividadVotacionEstudiante(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> getActividadVotacionEstudiante());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $result[8]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    function getActividadDenunciaEstudiante(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> getActividadDenunciaEstudiante());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $result[8], $result[9]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    function cantidadDeReferenciasPorPrograma(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> cantidadDeReferenciasPorPrograma());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    function cantidadDeReferenciasPorCurso(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> cantidadDeReferenciasPorCurso());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    function cantidadDeReferenciasPorProfesor(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> cantidadDeReferenciasPorProfesor());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1]));
        }
        $this -> connection -> close();
        return $referencias;
    }    
    function cantidadDeReferenciasPorRangoDeNota(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> cantidadDeReferenciasPorRangoDeNota());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    function cantidadDeReferenciasPorPeriodo(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> cantidadDeReferenciasPorPeriodo());
        $referencias = array();
        while ($result = $this -> connection -> fetchRow()){
            array_push($referencias, array($result[0], $result[1]));
        }
        $this -> connection -> close();
        return $referencias;
    }
    
    function agregarSolicitudReferencia(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> agregarSolicitudReferencia());
        $this -> connection -> close();
    }
    
    function consultarSolicitudReferencia(){
        $this -> connection -> open();
        $this -> referenciaDAOExt -> consultarSolicitudReferencia();
        $this -> connection -> run($this -> referenciaDAOExt -> consultarSolicitudReferencia());
        $solicitudes = array();
        while ($result = $this -> connection -> fetchRow()){
            $profesorSol = new Profesor($result[1]);
            $profesorSol -> select();
            $cursoSol = new Curso($result[2]);
            $cursoSol -> select();
            $estudianteSol = new Estudiante($result[3]);
            $estudianteSol -> select();
            $programaSol = new Programa($result[4]);
            $programaSol -> select();
            array_push($solicitudes, array($result[0], $profesorSol, $cursoSol, $estudianteSol, $programaSol));
        }
        $this -> connection -> close();
        return $solicitudes;
    }

    function getActividadSolicitudEstudiante(){
        $this -> connection -> open();
        $this -> connection -> run($this -> referenciaDAOExt -> getActividadSolicitudEstudiante());
        $solicitudes = array();
        while ($result = $this -> connection -> fetchRow()){
            $profesorSol = new Profesor($result[1]);
            $profesorSol -> select();
            $cursoSol = new Curso($result[2]);
            $cursoSol -> select();
            $estudianteSol = new Estudiante($result[3]);
            $estudianteSol -> select();
            $programaSol = new Programa($result[4]);
            $programaSol -> select();
            array_push($solicitudes, array($result[0], $profesorSol, $cursoSol, $estudianteSol, $programaSol));
        }
        $this -> connection -> close();
        return $solicitudes;
    }
    
}

?>