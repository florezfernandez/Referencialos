<?php 
$idProfesor = $_GET['idProfesor'];
$idCurso = $_GET['idCurso'];
$curso = new Curso($idCurso);
$curso -> select();
$profesor = new Profesor($idProfesor);
$profesor -> select();
$referenciaExt = new ReferenciaExt("", "", "", "", "", "", "", $idProfesor, $idCurso);
$result = $referenciaExt -> referenciaByProfesorAndCurso();
$resultCriterios = $referenciaExt -> criteriosByProfesorAndCurso();
$primerReferencia = $referenciaExt -> detallePrimerReferenciasByProfesorAndCurso();
?>
<div align="center">
	<?php include("ui/header.php"); ?>
</div>
<div class="container">
	<div class="card border-primary">
		<div class="card-header">
			<h4 class="card-title">Profesor: <?php echo $profesor -> getNombre() ?> <br>Curso: <?php echo $curso -> getNombre() ?></h4>
			<div class="row">
				<div class="col-lg-4 col-sm-12">
        			<h6>Cantidad de referencias: <strong><?php echo $result[0] ?></strong></h6>
        			<h6>Promedio General: <strong><?php echo $result[1] ?></strong></h6>
        			<h6>Promedio de Criterios Académicos: <strong><?php echo $result[2] ?></strong></h6>
        			<h6>Promedio de Criterios Sociales: <strong><?php echo $result[3] ?></strong></h6>				
				</div>
				<div class="col-lg-4 col-sm-12">
        			<h6>Criterios Académicos</h6>
        			<ul>
        			<?php 
        			foreach ($resultCriterios as $currentResults) {
        			    if($currentResults[2] == 1){
        			        echo "<li>" . $currentResults[0] . ": " . $currentResults[1] . "</li>";
        			    }
        			}
        			?>
        			</ul>
				</div>
				<div class="col-lg-4 col-sm-12">
        			<h6>Criterios Sociales</h6>
        			<ul>
        			<?php 
        			foreach ($resultCriterios as $currentResults) {
        			    if($currentResults[2] == 2){
        			        echo "<li>" . $currentResults[0] . ": " . $currentResults[1] . "</li>";
        			    }
        			}
        			?>
        			</ul>
				</div>
			</div>
		</div>
		<div class="card-body">
			<?php 
		    echo "<div class='card border-primary'>";
		    echo "<div class='card-header'><h5>Referencia 1</h5></div>";
		    echo "<div class='card-body'>";
		    echo "<div class='row'>";
		    echo "<div class='col-lg-4 col-sm-12'>";
		    echo "<strong>Periodo:</strong> " . $primerReferencia[4] . "<br>";
		    echo "<strong>Fecha:</strong> " . $primerReferencia[5] . "<br>";
		    echo "<strong>Programa:</strong> " . $primerReferencia[6] . "<br>";
		    echo "<strong>Nota obtenida:</strong> " . $primerReferencia[7] . "<br>";
		    echo "</div>";
		    echo "<div class='col-lg-4 col-sm-12'>";
		    $referencia = new ReferenciaExt($primerReferencia[0]);
		    $criteriosAcademicos = $referencia -> criteriosAcademicosByReferencia();
		    $criteriosSociales = $referencia -> criteriosSocialesByReferencia();
		    echo "<strong>Criterios Académicos:</strong>";
		    echo "<ul>";
		    foreach ($criteriosAcademicos as $currentCriterioAcademico) {
		        echo "<li>" . $currentCriterioAcademico[0] . ": <strong>" . $currentCriterioAcademico[1] . "</strong></li>";
		    };
		    echo "</ul>";			    
		    echo "</div>";
		    echo "<div class='col-lg-4 col-sm-12'>";
		    echo "<strong>Criterios Sociales:</strong>";
		    echo "<ul>";
		    foreach ($criteriosSociales as $currentCriterioSocial) {
		        echo "<li>" . $currentCriterioSocial[0] . ": <strong>" . $currentCriterioSocial[1] . "</strong></li>";
		    };
		    echo "</ul>";			    
		    echo "</div>";
		    echo "</div>";
		    echo "<hr>";
		    echo "<strong>Comentario:</strong><br>";
		    echo $primerReferencia[1];
		    echo "<hr>";
		    echo "<strong>Pros:</strong><br>";
		    echo $primerReferencia[2];
		    echo "<hr>";
		    echo "<strong>Contras:</strong><br>";
		    echo $primerReferencia[3];
            echo "</div>";
		    echo "</div><br>";
			?>
			<p><a class="btn btn-success" href="?">Autentíquese para ver mas referencias</a></p>
		</div>
	</div>
</div>