<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th>Nombre</th>
			<th class="text-center">Cantidad de Referencias</th>
			<th></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$curso = new Curso();
		$searchText = $_GET['search'];
		$cursos = $curso -> search($searchText);
		$counter = 1;
		foreach ($cursos as $currentCurso) {		    
		    echo "<tr><td>" . $counter . "</td>";
		    $pos = stripos($currentCurso -> getNombre(), $searchText);
		    $text = substr($currentCurso -> getNombre(), 0, $pos) . "<strong>" . substr($currentCurso -> getNombre(), $pos, strlen($searchText)) . "</strong>" . substr($currentCurso -> getNombre(), $pos + strlen($searchText));		    
		    echo "<td>" . $text . "</td>";
		    $referenciaExt = new ReferenciaExt("", "", "", "", "", "", "", "", $currentCurso -> getIdCurso());
		    $count = $referenciaExt -> countByCurso();
		    echo "<td class='text-center'>" . $count . "</td>";
			echo "<td class='text-right' nowrap>";
			if($count > 0){
                echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorCurso.php") . "&idCurso=" . $currentCurso -> getIdCurso() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Ver ranking' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
