<?php
$processed=false;
$profesor="";
if(isset($_POST['profesor'])){
    $profesor=$_POST['profesor'];
}
$curso="";
if(isset($_POST['curso'])){
    $curso=$_POST['curso'];
}
$programa="";
if(isset($_POST['programa'])){
    $programa=$_POST['programa'];
}
if(isset($_POST['insert'])){
    $user_ip = getenv('REMOTE_ADDR');
    $agent = $_SERVER["HTTP_USER_AGENT"];
    $browser = "-";
    if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
        $browser = "Internet Explorer";
    } else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Chrome";
    } else if (preg_match('/Edge\/\d+/', $agent) ) {
        $browser = "Edge";
    } else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Firefox";
    } else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Opera";
    } else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
        $browser = "Safari";
    }    
    $estudiante = new Estudiante($_SESSION['id']);
    $estudiante -> select();
    $profesor = new Profesor($profesor);
    $profesor -> select();
    $curso = new Curso($curso);
    $curso -> select();
    $programa = new Programa($programa);
    $programa -> select();
    $referencia = new ReferenciaExt("","","","","",date("Y-m-d"),"",$profesor -> getIdProfesor(), $curso -> getIdCurso(), $estudiante -> getIdEstudiante(), $programa -> getIdPrograma());
    $correos = $referencia -> getCorreosEstudiantesConReferencia($programa -> getIdPrograma());
    $subject="Solicitud de Agregacion de Referencia";
    $header = array(
        "From" => "Referencialos <contacto@referencialos.org>",
        "Bcc" => $correos
    );
    $message="Hola\n\nEl estudiante " . $estudiante -> getNombre() . " " . $estudiante -> getApellido() . " ha solicitado una nueva referencia en la plataforma Referencialos del siguiente profesor:\n";
    $message.="Profesor: " . $profesor -> getNombre() . "\n";
    $message.="Curso: " . $curso -> getNombre() . "\n";
    $message.="Programa: " . $programa -> getNombre() . "\n\n";    
    $message.="Ingresa a Referencialos en https://www.referencialos.org\n\n";    
    $message.="El equipo de Referencialos agradece tu colaboracion. \n";
    $to = "Referencialos <contacto@referencialos.org>";
    mail($to, $subject, $message, $header);
    $logEstudiante = new LogEstudiante("","Solicitar Agregar Referencia", "Profesor: " . $profesor -> getNombre() . "; Curso: " . $curso -> getNombre() . "; Programa: " . $programa -> getNombre() , date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
    $logEstudiante -> insert();
    $referencia -> agregarSolicitudReferencia();
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Solicitar Agregar Referencia</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					    <div class="alert alert-success" >Su solicitud ha sido enviada a los estudiantes del programa <strong><?php echo $programa -> getNombre() ?></strong> que han ingresado previamente alguna referencia.
					    </div>
					<?php } else { ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("addons/referencia/solicitarAgregarReferencia.php") ?>" class="bootstrap-form needs-validation" enctype="multipart/form-data"  >
						<div class="form-group">
							<label>Profesor*<br><span class="text-muted"><i>Seleccione el profesor de quien desearía tener una referencia</i></span></label>
							<select class="form-control" name="profesor" id="profesor" data-placeholder="Seleccione Profesor" required >
								<option></option>
								<?php
								$objProfesor = new Profesor();
								$profesors = $objProfesor -> selectAllOrder("nombre", "asc");
								foreach($profesors as $currentProfesor){
									echo "<option value='" . $currentProfesor -> getIdProfesor() . "'";
									if($currentProfesor -> getIdProfesor() == $profesor){
										echo " selected";
									}
									echo ">" . $currentProfesor -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						
						<div class="form-group">
							<label>Curso*<br><span class="text-muted"><i>Seleccione el curso del cual desearía tener una referencia con el profesor seleccionado</i></span></label>
							<select class="form-control" name="curso" id="curso" data-placeholder="Seleccione Curso" required >
								<option></option>
								<?php
								$objCurso = new Curso();
								$cursos = $objCurso -> selectAllOrder("nombre", "asc");
								foreach($cursos as $currentCurso){
									echo "<option value='" . $currentCurso -> getIdCurso() . "'";
									if($currentCurso -> getIdCurso() == $curso){
										echo " selected";
									}
									echo ">" . $currentCurso -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>						
						<div class="form-group">
							<label>Programa*<br><span class="text-muted"><i>Seleccione el programa académico del profesor y curso del cual desearía tener una referencia</i></span></label>
							<select class="form-control" name="programa" id="programa" data-placeholder="Seleccione Programa" required >
								<option></option>
								<?php
								$objPrograma = new Programa();
								$programas = $objPrograma -> selectAllOrder("nombre", "asc");
								foreach($programas as $currentPrograma){
									echo "<option value='" . $currentPrograma -> getIdPrograma() . "'";
									if($currentPrograma -> getIdPrograma() == $programa){
										echo " selected";
									}
									echo ">" . $currentPrograma -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" required=>
                          <label class="form-check-label">Confirmo que la información que he ingresado es verdadera. En caso contrario, soy consciente que los administradores de <i>Referencialos</i> podrían inhabilitar mi cuenta.	</label>
                        </div>
						<button type="submit" class="btn btn-info" name="insert">Enviar Solicitud</button>
					</form>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('#profesor').select2({});
$('#curso').select2({});
$('#programa').select2({});
</script>
