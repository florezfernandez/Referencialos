<?php
$referencia = new ReferenciaExt();
$referencias = $referencia -> consultarSolicitudReferencia();
?>
<div class="container">
	<div class="card">
		<div class="card-header">
			<h3>Solicitudes de Referencias</h3>
		</div>
		<div class="card-body">
			<?php 
			foreach ($referencias as $currentReferencia){
			    echo "<p>";
			    echo "<small>" . $currentReferencia[0] . "</small><br>";
			    echo "El estudiante " . $currentReferencia[3] -> getNombre() . " " . $currentReferencia[3] -> getApellido() . " ha solicitado una referencia del profesor ";
			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewSearchReferenciaProfesor.php") . "&idProfesor=" . $currentReferencia[1] -> getIdProfesor() . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver referencias del profesor " . $currentReferencia[1] -> getNombre() . "'>" . $currentReferencia[1] -> getNombre() . "</a> ";
			    echo "en el curso ";
			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorCurso.php") . "&idCurso=" . $currentReferencia[2] -> getIdCurso() . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver ranking de profesores del curso " . $currentReferencia[2] -> getNombre() . "'>" . $currentReferencia[2] -> getNombre() . "</a> ";
			    echo "del programa ";
			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorPrograma.php") . "&idPrograma=" . $currentReferencia[4] -> getIdPrograma() . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver ranking de profesores del programa " . $currentReferencia[4] -> getNombre() . "'>" . $currentReferencia[4] -> getNombre() . "</a><br>";
			    echo "<a class='btn btn-primary' href='?pid=" . base64_encode("addons/referencia/insertReferenciaEstudiante.php") . "&idCurso=" . $currentReferencia[2] -> getIdCurso() . "&idProfesor=" . $currentReferencia[1] -> getIdProfesor() . "'><small>Crear referencia</small></a> ";
			    echo "<hr></p>";
			}
			?>
		</div>
	</div>
</div>