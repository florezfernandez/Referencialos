<?php


class ReferenciaDAOExt extends ReferenciaDAO{
       
    function __construct($pIdReferencia = "", $pComentario = "", $pPros = "", $pContras = "", $pPeriodo = "", $pFecha = "", $pEstado = "", $pProfesor = "", $pCurso = "", $pEstudiante = "", $pPrograma = "", $pRangoDeNota = ""){
        parent::__construct($pIdReferencia, $pComentario, $pPros, $pContras, $pPeriodo, $pFecha, $pEstado, $pProfesor, $pCurso, $pEstudiante, $pPrograma, $pRangoDeNota);
    }
    
    function countByCurso(){
        return "select count(idReferencia)
				from Referencia
				where curso_idCurso = '" . $this -> curso . "' and estado = 1";
    }   
    
    function rankingByCurso(){
        return "select p.idProfesor, 
                       p.nombre, 
                       count(distinct(r.idReferencia)) as cuenta, 
                       cast(avg(rc.valor) as decimal(3,2)) as promedio,
                       cast(avg(case when c.categoriaDeCriterio_idCategoriaDeCriterio = 1 then rc.valor end) as decimal(3,2)) as promedioCriterioAcademico,
                       cast(avg(case when c.categoriaDeCriterio_idCategoriaDeCriterio = 2 then rc.valor end) as decimal(3,2)) as promedioCriterioSocial
                from Referencia r join ReferenciaCriterio rc on (r.idReferencia = rc.referencia_idReferencia) 
				                  join Criterio c on (c.idCriterio = rc.criterio_idCriterio)
                                  join Profesor p on (r.profesor_idProfesor = p.idProfesor)
                where r.curso_idCurso = '" . $this -> curso . "' and r.estado = 1
                group by p.idProfesor
                order by promedio desc";
    }   
    
    function referenciaByProfesorAndCurso(){
        return "select count(distinct(r.idReferencia)) as cuenta,
                       cast(avg(rc.valor) as decimal(3,2)) as promedio,
                       cast(avg(case when c.categoriaDeCriterio_idCategoriaDeCriterio = 1 then rc.valor end) as decimal(3,2)) as promedioCriterioAcademico,
                       cast(avg(case when c.categoriaDeCriterio_idCategoriaDeCriterio = 2 then rc.valor end) as decimal(3,2)) as promedioCriterioSocial
                from Referencia r join ReferenciaCriterio rc on (r.idReferencia = rc.referencia_idReferencia)
				                  join Criterio c on (c.idCriterio = rc.criterio_idCriterio)
                where r.curso_idCurso = '" . $this -> curso . "' and r.profesor_idProfesor = '" . $this -> profesor . "' and r.estado = 1";
    }   
    
    function criteriosByProfesorAndCurso(){
        return "select c.nombre, cast(avg(rc.valor) as decimal(3,2)) as promedio, c.categoriaDeCriterio_idCategoriaDeCriterio as categoria
                from Referencia r join ReferenciaCriterio rc on (r.idReferencia = rc.referencia_idReferencia)
                                  join Criterio c on (rc.criterio_idCriterio = c.idCriterio)
                where r.curso_idCurso = '" . $this -> curso . "' and r.profesor_idProfesor = '" . $this -> profesor . "' and r.estado = 1
                group by c.nombre
                order by c.idCriterio asc";
    }   
    function detalleReferenciasByProfesorAndCurso(){
        return "select r.idReferencia, r.comentario, r.pros, r.contras, r.periodo, r.fecha, pr.nombre, rn.valor
                from Referencia r join Programa pr on (r.programa_idPrograma = pr.idPrograma)
                                  join RangoDeNota rn on (r.rangoDeNota_idRangoDeNota = rn.idRangoDeNota)
                where r.curso_idCurso = '" . $this -> curso . "' and r.profesor_idProfesor = '" . $this -> profesor . "' and r.estado = 1
                order by r.fecha desc";
    }
    function detalleReferenciasByProfesorAndCursoDosPrimeras(){
        return "select r.idReferencia, r.comentario, r.pros, r.contras, r.periodo, r.fecha, pr.nombre, rn.valor
                from Referencia r join Programa pr on (r.programa_idPrograma = pr.idPrograma)
                                  join RangoDeNota rn on (r.rangoDeNota_idRangoDeNota = rn.idRangoDeNota)
                where r.curso_idCurso = '" . $this -> curso . "' and r.profesor_idProfesor = '" . $this -> profesor . "' and r.estado = 1
                order by r.fecha desc
                limit 2";
    }
    
    function detalleReferenciasByProfesorAndCursoUnaMas($posicion){
        return "select r.idReferencia, r.comentario, r.pros, r.contras, r.periodo, r.fecha, pr.nombre, rn.valor
                from Referencia r join Programa pr on (r.programa_idPrograma = pr.idPrograma)
                                  join RangoDeNota rn on (r.rangoDeNota_idRangoDeNota = rn.idRangoDeNota)
                where r.curso_idCurso = '" . $this -> curso . "' and r.profesor_idProfesor = '" . $this -> profesor . "' and r.estado = 1
                order by r.fecha desc
                limit " . $posicion . ", 1";
    }
    function buscarProfesorByReferencia(){
        return "select count(distinct(r.idReferencia)) as cuenta,
                       cast(avg(rc.valor) as decimal(3,2)) as promedio,
                       cast(avg(case when c.categoriaDeCriterio_idCategoriaDeCriterio = 1 then rc.valor end) as decimal(3,2)) as promedioCriterioAcademico,
                       cast(avg(case when c.categoriaDeCriterio_idCategoriaDeCriterio = 2 then rc.valor end) as decimal(3,2)) as promedioCriterioSocial
                from Referencia r join ReferenciaCriterio rc on (r.idReferencia = rc.referencia_idReferencia)
				                  join Criterio c on (c.idCriterio = rc.criterio_idCriterio)
                                  join Profesor p on (r.profesor_idProfesor = p.idProfesor)
                where r.profesor_idProfesor = '" . $this -> profesor . "' and r.estado = 1";
    }
    
    function referenciaByCursoAndProfesor(){
        return "select cu.idCurso,
                       cu.nombre as nombre,
					   count(distinct(r.idReferencia)) as cuenta,
                       cast(avg(rc.valor) as decimal(3,2)) as promedio,
                       cast(avg(case when c.categoriaDeCriterio_idCategoriaDeCriterio = 1 then rc.valor end) as decimal(3,2)) as promedioCriterioAcademico,
                       cast(avg(case when c.categoriaDeCriterio_idCategoriaDeCriterio = 2 then rc.valor end) as decimal(3,2)) as promedioCriterioSocial
                from Referencia r join ReferenciaCriterio rc on (r.idReferencia = rc.referencia_idReferencia)
				                  join Criterio c on (c.idCriterio = rc.criterio_idCriterio)
                                  join Curso cu on (cu.idCurso = r.curso_idCurso)
                where r.profesor_idProfesor = '". $this -> profesor ."' and r.estado = 1
                group by r.curso_idCurso
                order by nombre asc";
    }
    
    function countByPrograma(){
        return "select count(idReferencia)
				from Referencia
				where programa_idPrograma = '" . $this -> programa . "' and estado = 1";
    }
    
    function rankingByPrograma() {
        return "select p.idProfesor,
                       p.nombre,
                       count(distinct(r.idReferencia)) as cuenta,
                       cast(avg(rc.valor) as decimal(3,2)) as promedio,
                       cast(avg(case when c.categoriaDeCriterio_idCategoriaDeCriterio = 1 then rc.valor end) as decimal(3,2)) as promedioCriterioAcademico,
                       cast(avg(case when c.categoriaDeCriterio_idCategoriaDeCriterio = 2 then rc.valor end) as decimal(3,2)) as promedioCriterioSocial
                from Referencia r join ReferenciaCriterio rc on (r.idReferencia = rc.referencia_idReferencia)
				                  join Criterio c on (c.idCriterio = rc.criterio_idCriterio)
                                  join Profesor p on (r.profesor_idProfesor = p.idProfesor)
                where r.profesor_idProfesor in (
                    select profesor_idProfesor as idProf
				    from Referencia
				    where programa_idPrograma = " . $this -> programa . "
                )                 
                and r.estado = 1
                group by p.idProfesor
                order by cuenta desc, promedio desc, promedioCriterioAcademico desc";
    }
    function criteriosAcademicosByReferencia(){
        return "select c.nombre, rc.valor
                from ReferenciaCriterio rc join Criterio c on (rc.criterio_idCriterio = c.idCriterio)                                           
                where rc.referencia_idReferencia = '" . $this -> idReferencia . "' and c.categoriaDeCriterio_idCategoriaDeCriterio = '1'                
                order by c.idCriterio asc";
    }
    function criteriosSocialesByReferencia(){
        return "select c.nombre, rc.valor
                from ReferenciaCriterio rc join Criterio c on (rc.criterio_idCriterio = c.idCriterio)
                where rc.referencia_idReferencia = '" . $this -> idReferencia . "' and c.categoriaDeCriterio_idCategoriaDeCriterio = '2'
                order by c.idCriterio asc";
    }
    function getCorreosEstudiantesConReferencia($idPrograma){
        return "select distinct(e.correo)
                from Estudiante e join Referencia r on (e.idEstudiante = r.estudiante_idEstudiante)
                where r.programa_idPrograma = '" . $idPrograma . "' and r.estado = 1";
    }
    function detallePrimerReferenciasByProfesorAndCurso(){
        return "select r.idReferencia, r.comentario, r.pros, r.contras, r.periodo, r.fecha, pr.nombre, rn.valor
                from Referencia r join Programa pr on (r.programa_idPrograma = pr.idPrograma)
                                  join RangoDeNota rn on (r.rangoDeNota_idRangoDeNota = rn.idRangoDeNota)
                where r.curso_idCurso = '" . $this -> curso . "' and r.profesor_idProfesor = '" . $this -> profesor . "' and r.estado = 1
                order by r.fecha desc
                limit 1";
    }
    function getActividadGeneralCincoPrimeras(){
        return "select r.idReferencia, r.comentario, p.idProfesor, p.nombre, c.idCurso, c.nombre, pr.idPrograma, pr.nombre, r.fecha
            from Referencia r join Profesor p on (r.profesor_idProfesor = p.idProfesor)
                                  join Curso c on (r.curso_idCurso = c.idCurso)
                                  join Programa pr on (r.programa_idPrograma = pr.idPrograma)
                where r.estado = 1
                order by r.fecha desc
                limit 5";
    }
    function getActividadGeneralCincoMas($posicion){
        return "select r.idReferencia, r.comentario, p.idProfesor, p.nombre, c.idCurso, c.nombre, pr.idPrograma, pr.nombre, r.fecha
                from Referencia r join Profesor p on (r.profesor_idProfesor = p.idProfesor)
                                  join Curso c on (r.curso_idCurso = c.idCurso)
                                  join Programa pr on (r.programa_idPrograma = pr.idPrograma)
                where r.estado = 1
                order by r.fecha desc
                limit " . $posicion . ", 5";
    }function getActividadReferenciaEstudiante(){
        return "select r.comentario, p.idProfesor, p.nombre, c.idCurso, c.nombre, pr.idPrograma, pr.nombre, r.fecha
                from Referencia r join Profesor p on (r.profesor_idProfesor = p.idProfesor)
                                  join Curso c on (r.curso_idCurso = c.idCurso)
                                  join Programa pr on (r.programa_idPrograma = pr.idPrograma)
                where r.estudiante_idEstudiante = '" . $this -> estudiante . "' and r.estado = 1
                order by r.fecha desc";
    }
    function getActividadVotacionEstudiante(){
        return "select r.comentario, p.idProfesor, p.nombre, c.idCurso, c.nombre, pr.idPrograma, pr.nombre, v.fecha, v.voto
                from Referencia r join Profesor p on (r.profesor_idProfesor = p.idProfesor)
                                  join Curso c on (r.curso_idCurso = c.idCurso)
                                  join Programa pr on (r.programa_idPrograma = pr.idPrograma)
                                  join Votacion v on (r.idReferencia = v.referencia_idReferencia)
                where v.estudiante_idEstudiante = '" . $this -> estudiante . "' and r.estado = 1
                order by v.fecha desc
                limit 5";
    }
    function getActividadDenunciaEstudiante(){
        return "select r.comentario, p.idProfesor, p.nombre, c.idCurso, c.nombre, pr.idPrograma, pr.nombre, d.fecha, d.argumento, td.nombre
                from Referencia r join Profesor p on (r.profesor_idProfesor = p.idProfesor)
                                  join Curso c on (r.curso_idCurso = c.idCurso)
                                  join Programa pr on (r.programa_idPrograma = pr.idPrograma)
                                  join Denuncia d on (r.idReferencia = d.referencia_idReferencia)
                                  join TipoDeDenuncia td on (d.tipoDeDenuncia_idTipoDeDenuncia = td.idTipoDeDenuncia)
                where d.estudiante_idEstudiante = '" . $this -> estudiante . "' and r.estado = 1
                order by d.fecha desc
                limit 5";
    }
    function cantidadDeReferenciasPorPrograma(){
        return "select p.nombre, count(r.idReferencia) as cuenta
                from Referencia r join Programa p on (r.programa_idPrograma = p.idPrograma)
                where r.estado = 1 
                group by p.nombre
                order by cuenta desc, p.nombre asc";
    }
    function cantidadDeReferenciasPorCurso(){
        return "select c.nombre, count(r.idReferencia) as cuenta
                from Referencia r join Curso c on (r.curso_idCurso = c.idCurso)
                where r.estado = 1
                group by c.nombre
                order by cuenta desc, c.nombre asc
                limit 10";
    }
    function cantidadDeReferenciasPorProfesor(){
        return "select p.nombre, count(r.idReferencia) as cuenta
                from Referencia r join Profesor p on (r.profesor_idProfesor = p.idProfesor)
                where r.estado = 1
                group by p.nombre
                order by cuenta desc, p.nombre asc
                limit 10";
    }
    function cantidadDeReferenciasPorRangoDeNota(){
        return "select rn.valor, count(r.idReferencia) as cuenta
                from Referencia r join RangoDeNota rn on (r.rangoDeNota_idRangoDeNota = rn.idRangoDeNota)
                where r.estado = 1
                group by rn.valor
                order by cuenta desc";
    }
    function cantidadDeReferenciasPorPeriodo(){
        return "select periodo, count(idReferencia) as cuenta
                from Referencia
                where estado = 1
                group by periodo
                order by cuenta desc, periodo desc";
    }
    
    function agregarSolicitudReferencia(){
        return "insert into SolicitudReferencia(fecha, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma)
				values('" . $this -> fecha . "', '" . $this -> profesor . "', '" . $this -> curso . "', '" . $this -> estudiante . "', '" . $this -> programa . "')";
    }
    
    function consultarSolicitudReferencia(){
        return "select fecha, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma
                from SolicitudReferencia
                where fecha >= " . date("Y") ."-01-01
                order by fecha desc";
    }

    function getActividadSolicitudEstudiante(){
        return "select fecha, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma
                from SolicitudReferencia
                where estudiante_idEstudiante = '" . $this -> estudiante . "'
                order by fecha desc";
    }
}
?>