<?php
$order = "fecha";
if(isset($_GET['order'])){
	$order = $_GET['order'];
}
$dir = "desc";
if(isset($_GET['dir'])){
	$dir = $_GET['dir'];
}
?>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Consultar mis Referencias</h4>
		</div>
		<div class="card-body">
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr><th></th>
						<th nowrap>Comentario 
						<?php if($order=="periodo" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a href='index.php?pid=<?php echo base64_encode("ui/referencia/selectAllReferencia.php") ?>&order=periodo&dir=asc'>
							<span class='fas fa-sort-amount-up' data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' ></span></a>
						<?php } ?>
						<?php if($order=="periodo" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a href='?pid=<?php echo base64_encode("ui/referencia/selectAllReferencia.php") ?>&order=periodo&dir=desc'>
							<span class='fas fa-sort-amount-down' data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' ></span></a>
						<?php } ?>
						</th>
						<th nowrap>Fecha 
						<?php if($order=="fecha" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a href='index.php?pid=<?php echo base64_encode("ui/referencia/selectAllReferencia.php") ?>&order=fecha&dir=asc'>
							<span class='fas fa-sort-amount-up' data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' ></span></a>
						<?php } ?>
						<?php if($order=="fecha" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a href='?pid=<?php echo base64_encode("ui/referencia/selectAllReferencia.php") ?>&order=fecha&dir=desc'>
							<span class='fas fa-sort-amount-down' data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' ></span></a>
						<?php } ?>
						</th>
						<th nowrap>Estado 
						<?php if($order=="estado" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a href='index.php?pid=<?php echo base64_encode("ui/referencia/selectAllReferencia.php") ?>&order=estado&dir=asc'>
							<span class='fas fa-sort-amount-up' data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' ></span></a>
						<?php } ?>
						<?php if($order=="estado" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a href='?pid=<?php echo base64_encode("ui/referencia/selectAllReferencia.php") ?>&order=estado&dir=desc'>
							<span class='fas fa-sort-amount-down' data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' ></span></a>
						<?php } ?>
						</th>
						<th>Profesor</th>
						<th>Curso</th>
						<th>Programa</th>
						<th>Rango De Nota</th>
						<th nowrap></th>
					</tr>
				</thead>
				</tbody>
					<?php
					$referencia = new Referencia("", "", "", "", "", "", "", "", "", $_SESSION['id'], "", "");
					if($order!="" && $dir!="") {
					    $referencias = $referencia -> selectAllByEstudianteOrder($order, $dir);
					} else {
					    $referencias = $referencia -> selectAllByEstudiante();
					}
					$counter = 1;
					
					foreach ($referencias as $currentReferencia) {
					    $fechaLimite = date("Y-m-d", strtotime("+1 day", strtotime($currentReferencia -> getFecha())));
					    echo "<tr><td>" . $counter . "</td>";
						echo "<td>" . $currentReferencia -> getComentario() . "</td>";
						echo "<td>" . $currentReferencia -> getFecha() . "</td>";
						echo "<td>" . ($currentReferencia -> getEstado()==1?"Habilitado":"Deshabilitado") . "</td>";
						echo "<td><a href='modalProfesor.php?idProfesor=" . $currentReferencia -> getProfesor() -> getIdProfesor() . "' data-toggle='modal' data-target='#modalReferencia' >" . $currentReferencia -> getProfesor() -> getNombre() . "</a></td>";
						echo "<td>" . $currentReferencia -> getCurso() -> getNombre() . "</td>";
						echo "<td><a href='modalPrograma.php?idPrograma=" . $currentReferencia -> getPrograma() -> getIdPrograma() . "' data-toggle='modal' data-target='#modalReferencia' >" . $currentReferencia -> getPrograma() -> getNombre() . "</a></td>";
						echo "<td>" . $currentReferencia -> getRangoDeNota() -> getValor() . "</td>";
						echo "<td class='text-right' nowrap>";
						echo "<a href='addonModalReferenciaEstudiante.php?idReferencia=" . $currentReferencia -> getIdReferencia() . "'  data-toggle='modal' data-target='#modalReferencia' ><span class='fas fa-eye' data-toggle='tooltip' data-placement='left' data-original-title='Ver mas información' ></span></a> ";
						if(date("Y-m-d") <= $fechaLimite){
						    echo "<a href='?pid=" . base64_encode("addons/referencia/updateReferenciaEstudiante.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Referencia hasta " . $fechaLimite . "' ></span></a> ";
						}						
						echo "</td>";
						echo "</tr>";
						$counter++;
					}
					?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalReferencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
