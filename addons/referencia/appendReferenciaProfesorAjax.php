<?php 
$idProfesor = $_GET["idProfesor"];
$idCurso = $_GET["idCurso"];
$pos = $_GET["n"];
$referenciaExt = new ReferenciaExt("", "", "", "", "", "", "", $idProfesor, $idCurso);
$nuevaReferencia = $referenciaExt -> detalleReferenciasByProfesorAndCursoUnaMas($pos-1);
if($nuevaReferencia != null){
    echo "<div class='card border-primary'>";
    echo "<div class='card-header'><h5>Referencia " . $pos . "</h5></div>";
    echo "<div class='card-body'>";
    echo "<div class='row'>";
    echo "<div class='col-lg-4 col-sm-12'>";
    echo "<strong>Periodo:</strong> " . $nuevaReferencia[4] . "<br>";
    echo "<strong>Fecha:</strong> " . $nuevaReferencia[5] . "<br>";
    echo "<strong>Programa:</strong> " . $nuevaReferencia[6] . "<br>";
    echo "<strong>Nota obtenida:</strong> " . $nuevaReferencia[7] . "<br>";
    echo "</div>";
    echo "<div class='col-lg-4 col-sm-12'>";
    $referencia = new ReferenciaExt($nuevaReferencia[0]);
    $criteriosAcademicos = $referencia -> criteriosAcademicosByReferencia();
    $criteriosSociales = $referencia -> criteriosSocialesByReferencia();
    echo "<strong>Criterios Académicos:</strong>";
    echo "<ul>";
    foreach ($criteriosAcademicos as $currentCriterioAcademico) {
        echo "<li>" . $currentCriterioAcademico[0] . ": <strong>" . $currentCriterioAcademico[1] . "</strong></li>";
    };
    echo "</ul>";
    echo "</div>";
    echo "<div class='col-lg-4 col-sm-12'>";
    echo "<strong>Criterios Sociales:</strong>";
    echo "<ul>";
    foreach ($criteriosSociales as $currentCriterioSocial) {
        echo "<li>" . $currentCriterioSocial[0] . ": <strong>" . $currentCriterioSocial[1] . "</strong></li>";
    };
    echo "</ul>";
    echo "</div>";
    echo "</div>";
    echo "<hr>";
    echo "<strong>Comentario:</strong><br>";
    echo $nuevaReferencia[1];
    echo "<hr>";
    echo "<strong>Pros:</strong><br>";
    echo $nuevaReferencia[2];
    echo "<hr>";
    echo "<strong>Contras:</strong><br>";
    echo $nuevaReferencia[3];
    echo "<hr>";
    $votacion = new VotacionExt("", "", "", $nuevaReferencia[0], $_SESSION['id']);
    $votos = array(
        "-1" => "Estoy en desacuerdo",
        "0" => "No he votado",
        "1" => "Estoy de acuerdo"
    );
    echo "<span id='votacion" . $nuevaReferencia[0] . "'><span class='fas fa-thumbs-up' data-toggle='tooltip' data-placement='top' data-original-title='De acuerdo' ></span> " . $votacion -> getCantidadVotos(1) . " - <span class='fas fa-thumbs-down' data-toggle='tooltip' data-placement='top' data-original-title='Desacuerdo' ></span> " . $votacion -> getCantidadVotos(-1) . " - Mi voto: " . $votos[$votacion -> votoByReferenciaAndEstudiante()] . "</span>";
    echo "</div>";
    echo "<div class='card-footer text-center'>";
    echo "<button id='bAcuerdo" . $nuevaReferencia[0] . "' type='button' class='btn btn-primary' data-toggle='tooltip' data-placement='top' data-original-title='Estoy de acuerdo' ><span class='fas fa-thumbs-up' ></span></button> ";
    echo "<button id='bDesacuerdo" . $nuevaReferencia[0] . "' type='button' class='btn btn-primary' data-toggle='tooltip' data-placement='top' data-original-title='Estoy en desacuerdo'><span class='fas fa-thumbs-down' ></span></button> ";
    echo "<a href='?pid=" . base64_encode("addons/denuncia/denunciarReferencia.php") . "&idReferencia=" . $nuevaReferencia[0] . "' class='btn btn-danger' data-toggle='tooltip' data-placement='top' data-original-title='Denunciar'><span class='fas fa-flag' ></span></a>";
    echo "</div>";
    echo "</div><br>";
}
?>
<script>
$(document).ready(function(){
<?php 
echo "\t$(\"#bAcuerdo" . $nuevaReferencia[0] . "\").click(function(){\n";
echo "\t\tpath = \"indexAjax.php?pid=" . base64_encode("addons/votacion/votarAjax.php") . "&idReferencia=" . $nuevaReferencia[0] . "&idEstudiante=" . $_SESSION['id'] . "&voto=1\";\n";
echo "\t\t$(\"#votacion" . $nuevaReferencia[0] . "\").load(path);\n";    
echo "\t});\n";    
echo "\t$(\"#bDesacuerdo" . $nuevaReferencia[0] . "\").click(function(){\n";
echo "\t\tpath = \"indexAjax.php?pid=" . base64_encode("addons/votacion/votarAjax.php") . "&idReferencia=" . $nuevaReferencia[0] . "&idEstudiante=" . $_SESSION['id'] . "&voto=-1\";\n";
echo "\t\t$(\"#votacion" . $nuevaReferencia[0] . "\").load(path);\n";
echo "\t});\n";
?>
});
</script>