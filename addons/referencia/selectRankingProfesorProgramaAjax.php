<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th>Nombre</th>
			<th class="text-center">Cantidad de Referencias</th>
			<th></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$programa = new Programa();
		$searchText = $_GET['search'];
		$programas = $programa -> search($searchText);
		$counter = 1;
		foreach ($programas as $currentPrograma) {		    
		    echo "<tr><td>" . $counter . "</td>";
		    $pos = stripos($currentPrograma -> getNombre(), $searchText);
		    $text = substr($currentPrograma -> getNombre(), 0, $pos) . "<strong>" . substr($currentPrograma -> getNombre(), $pos, strlen($searchText)) . "</strong>" . substr($currentPrograma -> getNombre(), $pos + strlen($searchText));		    
		    echo "<td>" . $text . "</td>";
		    $referenciaExt = new ReferenciaExt("", "", "", "", "", "", "", "", "","", $currentPrograma -> getIdPrograma());
		    $count = $referenciaExt -> countByPrograma();
		    echo "<td class='text-center'>" . $count . "</td>";
			echo "<td class='text-right' nowrap>";
			if($count > 0){
                echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorPrograma.php") . "&idPrograma=" . $currentPrograma -> getIdPrograma() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Ver ranking' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
