<?php
$processed=false;
$error = 0;
$comentario="";
if(isset($_POST['comentario'])){
    $comentario=$_POST['comentario'];
}
$pros="";
if(isset($_POST['pros'])){
    $pros=$_POST['pros'];
}
$contras="";
if(isset($_POST['contras'])){
    $contras=$_POST['contras'];
}
$periodo="";
if(isset($_POST['periodo'])){
    $periodo=$_POST['periodo'];
}
$fecha=date("Y-m-d");
$estado=1;
$profesor="";
if(isset($_GET['idProfesor'])){
    $profesor = $_GET['idProfesor'];
}else if(isset($_POST['profesor'])){
    $profesor=$_POST['profesor'];
}
$curso="";
if(isset($_GET['idCurso'])){
    $curso = $_GET['idCurso'];
}else if(isset($_POST['curso'])){
    $curso=$_POST['curso'];
}
$programa="";
if(isset($_POST['programa'])){
    $programa=$_POST['programa'];
}
$rangoDeNota="";
if(isset($_POST['rangoDeNota'])){
    $rangoDeNota=$_POST['rangoDeNota'];
}
if(isset($_POST['insert'])){
    if($comentario == ""){
        $error = 1;
    }else{
        $newReferencia = new Referencia("", $comentario, $pros, $contras, $periodo, $fecha, $estado, $profesor, $curso, $_SESSION["id"], $programa, $rangoDeNota);
        $idReferencia = $newReferencia -> insert();
        $objCriterio = new Criterio();
        $criterios = $objCriterio -> selectAllOrder("idCriterio", "asc");
        foreach($criterios as $currentCriterio){
            $valor = $_POST["criterio_" . $currentCriterio -> getIdCriterio()];
            $objReferenciaCriterio = new ReferenciaCriterio("", $valor, $idReferencia, $currentCriterio -> getIdCriterio());
            $objReferenciaCriterio -> insert();
        }
        $objProfesor = new Profesor($profesor);
        $objProfesor -> select();
        $nameProfesor = $objProfesor -> getNombre() ;
        $objCurso = new Curso($curso);
        $objCurso -> select();
        $nameCurso = $objCurso -> getNombre() ;
        $objPrograma = new Programa($programa);
        $objPrograma -> select();
        $namePrograma = $objPrograma -> getNombre() ;
        $objRangoDeNota = new RangoDeNota($rangoDeNota);
        $objRangoDeNota -> select();
        $nameRangoDeNota = $objRangoDeNota -> getValor() ;
        $user_ip = getenv('REMOTE_ADDR');
        $agent = $_SERVER["HTTP_USER_AGENT"];
        $browser = "-";
        if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
            $browser = "Internet Explorer";
        } else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
            $browser = "Chrome";
        } else if (preg_match('/Edge\/\d+/', $agent) ) {
            $browser = "Edge";
        } else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
            $browser = "Firefox";
        } else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
            $browser = "Opera";
        } else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
            $browser = "Safari";
        }
        $logEstudiante = new LogEstudiante("","Crear Referencia", "Periodo: " . $periodo . "; Comentario: " . $comentario . "; Pros: " . $pros . "; Contras: " . $contras . "; Fecha: " . $fecha . "; Estado: " . $estado . "; Profesor: " . $nameProfesor . "; Curso: " . $nameCurso . "; Programa: " . $namePrograma . "; Rango De Nota: " . $nameRangoDeNota , date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
        $logEstudiante -> insert();
        $processed=true;
    }
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col">		
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Crear Referencia</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					<div class="alert alert-success" >Datos Ingresados. Puede modificar los datos de su referencia hoy y mañana.
					</div>
					<?php } else { if($error == 1){ ?>
					<div class="alert alert-danger" >Por favor ingrese un comentario. 
					</div>                    
                    <?php } ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("addons/referencia/insertReferenciaEstudiante.php") ?>&profesor=<?php echo $profesor; ?>&curso=<?php echo $curso; ?>" class="bootstrap-form needs-validation"   >
    					<div class="row">
    						<div class="col-lg-6 col-sm-12">
    						<?php if(!isset($_GET['idProfesor'])){?>
        						<div class="form-group">
        							<label>Profesor*<br><span class="text-muted"><i>Seleccione el profesor a quien va a dar la referencia</i></span></label>
        							<select class="form-control" name="profesor" id="profesor" data-placeholder="Seleccione Profesor" required >
        								<option></option>
        								<?php
        								    $objProfesor = new Profesor();
            								$profesors = $objProfesor -> selectAllOrder("nombre", "asc");
            								foreach($profesors as $currentProfesor){
            									echo "<option value='" . $currentProfesor -> getIdProfesor() . "'";
            									if($currentProfesor -> getIdProfesor() == $profesor){
            										echo " selected";
            									}
            									echo ">" . $currentProfesor -> getNombre() . "</option>";
            								}
        								?>
        							</select>        							
        						</div>
        						<?php  } else {?>
        						<div class="form-group">
        							<label>Profesor<br></label>        							
            						<?php 
            						  $objProfesor = new Profesor($profesor);
            						  $objProfesor -> select();?>
            						  <p class="form-control"><?php echo $objProfesor -> getNombre(); $objProfesor -> getIdProfesor();?></p>
            						  <input type="hidden" name="profesor" value="<?php echo $objProfesor -> getIdProfesor(); ?>">
        						  </div>
        						  <?php }?>
        						<?php if(!isset($_GET['idCurso'])){?>
        						<div class="form-group">
        							<label>Curso*<br><span class="text-muted"><i>Seleccione el curso que tomó con el profesor seleccionado</i></span></label>
        							<select class="form-control" name="curso" id="curso" data-placeholder="Seleccione Curso" required >
        								<option></option>
        								<?php
            								$objCurso = new Curso();
            								$cursos = $objCurso -> selectAllOrder("nombre", "asc");
            								foreach($cursos as $currentCurso){
            									echo "<option value='" . $currentCurso -> getIdCurso() . "'";
            									if($currentCurso -> getIdCurso() == $curso){
            										echo " selected";
            									}
            									echo ">" . $currentCurso -> getNombre() . "</option>";
            								}
        								?>
        							</select>
        						</div>						
        						<?php  } else {?>
        						<div class="form-group">
        							<label>Curso<br></label>        							
        						<?php 
        						  $objCurso = new Curso($curso);
        						  $objCurso -> select();?>
        						  <p class="form-control"><?php echo $objCurso -> getNombre();?></p>
        						  <input type="hidden" name="curso" value="<?php echo $objCurso -> getIdCurso(); ?>">
        						</div>
        						<?php }?>
        						<div class="form-group">
        							<label>Programa*<br><span class="text-muted"><i>Seleccione el programa académico que está cursando</i></span></label>
        							<select class="form-control" name="programa" id="programa" data-placeholder="Seleccione Programa" required >
        								<option></option>
        								<?php
        								$objPrograma = new Programa();
        								$programas = $objPrograma -> selectAllOrder("nombre", "asc");
        								foreach($programas as $currentPrograma){
        									echo "<option value='" . $currentPrograma -> getIdPrograma() . "'";
        									if($currentPrograma -> getIdPrograma() == $programa){
        										echo " selected";
        									}
        									echo ">" . $currentPrograma -> getNombre() . "</option>";
        								}
        								?>
        							</select>
        						</div>
        						<div class="form-group">
        							<label>Periodo*<br><span class="text-muted"><i>Seleccione el periodo en que tomó el curso seleccionado con el profesor seleccionado</i></span></label>
        							<select class="form-control" name="periodo" id="periodo" data-placeholder="Seleccione Periodo" required >
        								<option></option>
        								<?php 
        								$ano = date("Y");
        								for($i=$ano; $i>$ano-5; $i--){
        								    for($j=20; $j>=10; $j-=10){
        								        $p = $i . "-" . $j;
        								        echo "<option value='" . $p . "'";
        								        if($p == $periodo){
        								            echo " selected";
        								        }
        								        echo ">". $p . "</option>";
        								    }
        								}
        								?>
        							</select>
        						</div>
        						
        						<div class="form-group">
        							<label>Rango De Nota*<br><span class="text-muted"><i>Seleccione el rango de nota definitiva que obtubo en el curso seleccionado con el profesor seleccionado</i></span></label>
        							<select class="form-control" name="rangoDeNota" id="rangoDeNota" data-placeholder="Seleccione Rango De Nota" required >
        								<option></option>
        								<?php
        								$objRangoDeNota = new RangoDeNota();
        								$rangoDeNotas = $objRangoDeNota -> selectAllOrder("idRangoDeNota", "asc");
        								foreach($rangoDeNotas as $currentRangoDeNota){
        									echo "<option value='" . $currentRangoDeNota -> getIdRangoDeNota() . "'";
        									if($currentRangoDeNota -> getIdRangoDeNota() == $rangoDeNota){
        										echo " selected";
        									}
        									echo ">" . $currentRangoDeNota -> getValor() . "</option>";
        								}
        								?>
        							</select>
        						</div>
    						</div>						
							<div class="col-lg-6 col-sm-12">
        						<div class="form-group">
        							<label>Comentario*<br><span class="text-muted"><i>Ingrese sus comentarios del profesor seleccionado en el curso seleccionado</i></span></label>
        							<textarea id="comentario" name="comentario" ><?php echo $comentario ?></textarea>
        							<script>
        								$('#comentario').summernote({
        									tabsize: 2,
        									height: 100
        								});
        							</script>
        						</div>
        						<div class="form-group">
        							<label>Pros<br><span class="text-muted"><i>Ingrese aspectos a favor</i></span></label>
        							<textarea id="pros" name="pros" ><?php echo $pros ?></textarea>
        							<script>
        								$('#pros').summernote({
        									tabsize: 2,
        									height: 100
        								});
        							</script>
        						</div>
        						<div class="form-group">
        							<label>Contras<br><span class="text-muted"><i>Ingrese aspectos en contra</i></span></label>
        							<textarea id="contras" name="contras" ><?php echo $contras ?></textarea>
        							<script>
        								$('#contras').summernote({
        									tabsize: 2,
        									height: 100
        								});
        							</script>
        						</div>
        					</div>
        				</div>
        				<div class="row">
        					<div class="col">
        						<h6>Califique los siguientes criterios:</h6>
        						<div class="table-responsive">
        						<table class="table table-hover">
    								<thead><tr><th>Categoría</th><th>Criterio</th><th>Descripcion</th><th class='text-center col-1'>Muy en desacuerdo (1)</th><th class='text-center col-1'>En desacuerdo (2)</th><th class='text-center col-1'>Neutral (3)</th><th class='text-center col-1'>De acuerdo (4)</th><th class='text-center col-1'>Muy de acuerdo (5)</th></tr></thead>
    								<tbody class='table-group-divider'>
									<?php
    								$objCriterio = new Criterio();
    								$criterios = $objCriterio -> selectAllOrder("idCriterio", "asc");
    								foreach($criterios as $currentCriterio){
    								    echo "<tr>";
    								    echo "<td>" . $currentCriterio -> getCategoriaDeCriterio() -> getNombre() . "</td><th>" . $currentCriterio -> getNombre() . "</th><td><span class='text-muted'><i>" . $currentCriterio -> getDescripcion() . "</i></span></td>";
    								    for($i=1; $i<=5; $i++){
    								        echo "<td class='text-center'><input type='radio' class='btn-check' name='criterio_" . $currentCriterio -> getIdCriterio() . "' value='" . $i . "' required";
    								        if(isset($_POST["criterio_" . $currentCriterio -> getIdCriterio()]) && $_POST["criterio_" . $currentCriterio -> getIdCriterio()] == $i){
    								            echo " checked ";
    								        }
    								        echo "></td>";
    								    }
    								    echo "</tr>";
    								}
    								
    								?> 
									</tbody>									
								</table>
								</div>
        					</div>
        				</div>
						<button type="submit" class="btn btn-info" name="insert">Crear</button>
					</form>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('#periodo').select2({});
$('#profesor').select2({});
$('#curso').select2({});
$('#programa').select2({});
$('#rangoDeNota').select2({});
</script>
