<?php
$processed = false;
$idEstudiante = base64_decode($_GET["id"]);
$estudianteExt = new EstudianteExt($idEstudiante);
$estudianteExt -> select();
$error = 0;
if($estudianteExt -> getEstado() == 2){
    if($estudianteExt -> getCorreo() != ""){
        $estudianteExt -> activate();
        $processed = true;
    }    
}else if($estudianteExt -> getEstado() == 1){
    $error = 1;
}else{
    $error = 2;
}
?>
<?php 
include "ui/header.php";
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Activar Estudiante</h4>
				</div>
				<div class="card-body">
					<?php if($error == 1){ ?>
					<div class="alert alert-danger" >El estudiante con correo electrónico <?php echo $estudianteExt -> getCorreo() ?> ya habia sido activado.
					</div>
					<?php } else if($error == 2) { ?>
					<div class="alert alert-danger" >El estudiante con correo electrónico <?php echo $estudianteExt -> getCorreo() ?> no puede ser activado porque está deshabilitado. Contacte al administrador.
					</div>
					<?php } else if($processed){ ?>
					<div class="alert alert-success" >El estudiante con correo electrónico <?php echo $estudianteExt -> getCorreo() ?> ha sido activado.
					</div>
					<?php } else { ?>
					<div class="alert alert-danger" >Error de URL. No se activó ningun estudiante.
					</div>
					<?php } ?>
					<div class="form-group">
						<a href="?">Volver al inicio</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
