<?php
$processed=false;
$nombre="";
if(isset($_POST['nombre'])){
	$nombre=$_POST['nombre'];
}
$apellido="";
if(isset($_POST['apellido'])){
	$apellido=$_POST['apellido'];
}
$correo="";
if(isset($_POST['correo'])){
	$correo=$_POST['correo'];
}
$clave="";
if(isset($_POST['clave'])){
	$clave=$_POST['clave'];
}
$periodoDeIngreso="";
if(isset($_POST['periodoDeIngreso'])){
	$periodoDeIngreso=$_POST['periodoDeIngreso'];
}
$fechaDeNacimiento=date("d/m/Y");
if(isset($_POST['fechaDeNacimiento'])){
	$fechaDeNacimiento=$_POST['fechaDeNacimiento'];
}
$genero="";
if(isset($_POST['genero'])){
	$genero=$_POST['genero'];
}
if(isset($_GET['idGenero'])){
	$genero=$_GET['idGenero'];
}
$error = 0;
$dominio = "https://uniandes.referencialos.org/";

if(isset($_POST['insert'])){
    if(substr($correo, strpos($correo, "@")) != "@uniandes.edu.co"){
        $error = 1;
    }else{
        $profesorExt = new ProfesorExt("", "", $correo);
        if($profesorExt -> existByEmail()){
            $error = 2;
        }else{
            $newEstudiante = new Estudiante("", $nombre, $apellido, $correo, $clave, "", "", "", 2, $periodoDeIngreso, $fechaDeNacimiento, date("Y-m-d"), $genero);
            if($newEstudiante -> existCorreo($correo)){
                $error = 2;
            }else{
                $idEstudiante = $newEstudiante -> insert();
                $to=$correo;
                $subject="Activacion de cuenta en Referencialos";
                //TODO Cambiar correo
                $from="FROM: Referencialos <contacto@referencialos.org>";
                $message="Hola " . $nombre . "\n\n";
                $message.="Active su cuenta en el enlace: " . $dominio . "?pid=" . base64_encode("addons/estudiante/activateEstudiante.php") . "&id=" . base64_encode($idEstudiante) . "\n\n";
                $message.="Gracias por participar en Referencialos.org\n";
                //echo $message;
                mail($to, $subject, $message, $from);
                mail("contacto@referencialos.org", $subject, $message, $from);
                $processed=true;
            }
        }
    }
}
include "ui/header.php";
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Registrar Estudiante</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					<div class="alert alert-success" >Estudiante registrado. <br>Recibirá un correo para activar su cuenta. <br>El correo podría recibirlo en <i>spam</i>.
					</div>
					<div class="form-group">
						<a href="?">Volver al inicio</a>
					</div>
					<?php } else if($error == 1) { ?>
					<div class="alert alert-danger" >Estudiante NO registrado. El correo debe ser dominio @uniandes.edu.co.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } else if($error == 2) { ?>
					<div class="alert alert-danger" >Estudiante NO registrado. El correo ya se encuentra registrado.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>
					<?php if(!$processed){ ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("addons/estudiante/registerEstudiante.php") ?>" class="bootstrap-form needs-validation"   >
						<div class="form-group">
							<label>Nombre*</label>
							<input type="text" class="form-control" name="nombre" value="<?php echo $nombre ?>" required />
						</div>
						<div class="form-group">
							<label>Apellido*</label>
							<input type="text" class="form-control" name="apellido" value="<?php echo $apellido ?>" required />
						</div>
						<div class="form-group">
							<label>Correo Institucional*</label>
							<input type="email" class="form-control" name="correo" value="<?php echo $correo ?>"  required />
						</div>
						<div class="form-group">
							<label>Clave*</label>
							<input type="password" class="form-control" name="clave" value="<?php echo $clave ?>" required />
						</div>
						<div class="form-group">
							<label>Periodo De Ingreso*</label>
							<select class="form-control" name="periodoDeIngreso" id="periodoDeIngreso" data-placeholder="Seleccione Periodo De Ingreso" required >
								<option></option>
								<?php 
								$ano = date("Y");
								for($i=$ano; $i>$ano-5; $i--){
								    for($j=20; $j>=10; $j-=10){
								        $p = $i . "-" . $j;
								        echo "<option value='" . $p . "'";
								        if($p == $periodoDeIngreso){
								            echo " selected";
								        }
								        echo ">" . $p . "</option>";
								    }
								}
								
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Fecha De Nacimiento*</label>
							<input type="date" class="form-control" name="fechaDeNacimiento" id="fechaDeNacimiento" value="<?php echo $fechaDeNacimiento ?>" autocomplete="off" required />
						</div>
						<div class="form-group">
							<label>Genero*</label>
							<select class="form-control" name="genero" id="genero" data-placeholder="Seleccione Genero" required >
								<option></option>
								<?php
								$objGenero = new Genero();
								$generos = $objGenero -> selectAllOrder("nombre", "asc");
								foreach($generos as $currentGenero){
									echo "<option value='" . $currentGenero -> getIdGenero() . "'";
									if($currentGenero -> getIdGenero() == $genero){
										echo " selected";
									}
									echo ">" . $currentGenero -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label><span class="text-danger">ADVERTENCIA: Al dar clic en <i>Registrar</i>, usted esta aceptando los <a href="?pid=<?php echo base64_encode("addons/terminos.php") ?>" target="_blank">Terminos de Servicio</a> y la <a href="?pid=<?php echo base64_encode("addons/politica.php") ?>" target="_blank">Política de Privacidad</a></span></label>							
						</div>
						<button type="submit" class="btn btn-info" name="insert">Registrar</button>
						<div class="form-group">
							<a href="?">Volver al inicio</a>
						</div>
											
					</form>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
