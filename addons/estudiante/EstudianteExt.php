<?php
require_once ("addons/estudiante/EstudianteDAOExt.php");


class EstudianteExt extends Estudiante {
    private $estudianteDAOExt;
    
    function __construct($pIdEstudiante = ""){
        parent::__construct($pIdEstudiante);
        $this -> estudianteDAOExt = new EstudianteDAOExt($this -> idEstudiante);
    }
    
    function activate(){
        $this -> connection -> open();
        $this -> connection -> run($this -> estudianteDAOExt -> activate());
        $this -> connection -> close();
    } 
    
}

?>