<?php
require_once ("persistence/TipoDeDenunciaDAO.php");
require_once ("persistence/Connection.php");

class TipoDeDenuncia {
	private $idTipoDeDenuncia;
	private $nombre;
	private $tipoDeDenunciaDAO;
	private $connection;

	function getIdTipoDeDenuncia() {
		return $this -> idTipoDeDenuncia;
	}

	function setIdTipoDeDenuncia($pIdTipoDeDenuncia) {
		$this -> idTipoDeDenuncia = $pIdTipoDeDenuncia;
	}

	function getNombre() {
		return $this -> nombre;
	}

	function setNombre($pNombre) {
		$this -> nombre = $pNombre;
	}

	function __construct($pIdTipoDeDenuncia = "", $pNombre = ""){
		$this -> idTipoDeDenuncia = $pIdTipoDeDenuncia;
		$this -> nombre = $pNombre;
		$this -> tipoDeDenunciaDAO = new TipoDeDenunciaDAO($this -> idTipoDeDenuncia, $this -> nombre);
		$this -> connection = new Connection();
	}

	function insert(){
		$this -> connection -> open();
		$this -> connection -> run($this -> tipoDeDenunciaDAO -> insert());
		$id = $this -> connection -> lastId();;
		$this -> connection -> close();
		return $id;
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> tipoDeDenunciaDAO -> update());
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> tipoDeDenunciaDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idTipoDeDenuncia = $result[0];
		$this -> nombre = $result[1];
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> tipoDeDenunciaDAO -> selectAll());
		$tipoDeDenuncias = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($tipoDeDenuncias, new TipoDeDenuncia($result[0], $result[1]));
		}
		$this -> connection -> close();
		return $tipoDeDenuncias;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> tipoDeDenunciaDAO -> selectAllOrder($order, $dir));
		$tipoDeDenuncias = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($tipoDeDenuncias, new TipoDeDenuncia($result[0], $result[1]));
		}
		$this -> connection -> close();
		return $tipoDeDenuncias;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> tipoDeDenunciaDAO -> search($search));
		$tipoDeDenuncias = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($tipoDeDenuncias, new TipoDeDenuncia($result[0], $result[1]));
		}
		$this -> connection -> close();
		return $tipoDeDenuncias;
	}
}
?>
