<?php
require_once ("persistence/CategoriaDeCriterioDAO.php");
require_once ("persistence/Connection.php");

class CategoriaDeCriterio {
	private $idCategoriaDeCriterio;
	private $nombre;
	private $categoriaDeCriterioDAO;
	private $connection;

	function getIdCategoriaDeCriterio() {
		return $this -> idCategoriaDeCriterio;
	}

	function setIdCategoriaDeCriterio($pIdCategoriaDeCriterio) {
		$this -> idCategoriaDeCriterio = $pIdCategoriaDeCriterio;
	}

	function getNombre() {
		return $this -> nombre;
	}

	function setNombre($pNombre) {
		$this -> nombre = $pNombre;
	}

	function __construct($pIdCategoriaDeCriterio = "", $pNombre = ""){
		$this -> idCategoriaDeCriterio = $pIdCategoriaDeCriterio;
		$this -> nombre = $pNombre;
		$this -> categoriaDeCriterioDAO = new CategoriaDeCriterioDAO($this -> idCategoriaDeCriterio, $this -> nombre);
		$this -> connection = new Connection();
	}

	function insert(){
		$this -> connection -> open();
		$this -> connection -> run($this -> categoriaDeCriterioDAO -> insert());
		$id = $this -> connection -> lastId();;
		$this -> connection -> close();
		return $id;
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> categoriaDeCriterioDAO -> update());
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> categoriaDeCriterioDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idCategoriaDeCriterio = $result[0];
		$this -> nombre = $result[1];
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> categoriaDeCriterioDAO -> selectAll());
		$categoriaDeCriterios = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($categoriaDeCriterios, new CategoriaDeCriterio($result[0], $result[1]));
		}
		$this -> connection -> close();
		return $categoriaDeCriterios;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> categoriaDeCriterioDAO -> selectAllOrder($order, $dir));
		$categoriaDeCriterios = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($categoriaDeCriterios, new CategoriaDeCriterio($result[0], $result[1]));
		}
		$this -> connection -> close();
		return $categoriaDeCriterios;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> categoriaDeCriterioDAO -> search($search));
		$categoriaDeCriterios = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($categoriaDeCriterios, new CategoriaDeCriterio($result[0], $result[1]));
		}
		$this -> connection -> close();
		return $categoriaDeCriterios;
	}
}
?>
