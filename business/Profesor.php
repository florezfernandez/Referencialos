<?php
require_once ("persistence/ProfesorDAO.php");
require_once ("persistence/Connection.php");

class Profesor {
	protected $idProfesor;
	protected $nombre;
	protected $correo;
	protected $genero;
	private $profesorDAO;
	protected $connection;

	function getIdProfesor() {
		return $this -> idProfesor;
	}

	function setIdProfesor($pIdProfesor) {
		$this -> idProfesor = $pIdProfesor;
	}

	function getNombre() {
		return $this -> nombre;
	}

	function setNombre($pNombre) {
		$this -> nombre = $pNombre;
	}

	function getCorreo() {
		return $this -> correo;
	}

	function setCorreo($pCorreo) {
		$this -> correo = $pCorreo;
	}

	function getGenero() {
		return $this -> genero;
	}

	function setGenero($pGenero) {
		$this -> genero = $pGenero;
	}

	function __construct($pIdProfesor = "", $pNombre = "", $pCorreo = "", $pGenero = ""){
		$this -> idProfesor = $pIdProfesor;
		$this -> nombre = $pNombre;
		$this -> correo = $pCorreo;
		$this -> genero = $pGenero;
		$this -> profesorDAO = new ProfesorDAO($this -> idProfesor, $this -> nombre, $this -> correo, $this -> genero);
		$this -> connection = new Connection();
	}

	function insert(){
		$this -> connection -> open();
		$this -> connection -> run($this -> profesorDAO -> insert());
		$id = $this -> connection -> lastId();;
		$this -> connection -> close();
		return $id;
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> profesorDAO -> update());
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> profesorDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idProfesor = $result[0];
		$this -> nombre = $result[1];
		$this -> correo = $result[2];
		$genero = new Genero($result[3]);
		$genero -> select();
		$this -> genero = $genero;
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> profesorDAO -> selectAll());
		$profesors = array();
		while ($result = $this -> connection -> fetchRow()){
			$genero = new Genero($result[3]);
			$genero -> select();
			array_push($profesors, new Profesor($result[0], $result[1], $result[2], $genero));
		}
		$this -> connection -> close();
		return $profesors;
	}

	function selectAllByGenero(){
		$this -> connection -> open();
		$this -> connection -> run($this -> profesorDAO -> selectAllByGenero());
		$profesors = array();
		while ($result = $this -> connection -> fetchRow()){
			$genero = new Genero($result[3]);
			$genero -> select();
			array_push($profesors, new Profesor($result[0], $result[1], $result[2], $genero));
		}
		$this -> connection -> close();
		return $profesors;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> profesorDAO -> selectAllOrder($order, $dir));
		$profesors = array();
		while ($result = $this -> connection -> fetchRow()){
			$genero = new Genero($result[3]);
			$genero -> select();
			array_push($profesors, new Profesor($result[0], $result[1], $result[2], $genero));
		}
		$this -> connection -> close();
		return $profesors;
	}

	function selectAllByGeneroOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> profesorDAO -> selectAllByGeneroOrder($order, $dir));
		$profesors = array();
		while ($result = $this -> connection -> fetchRow()){
			$genero = new Genero($result[3]);
			$genero -> select();
			array_push($profesors, new Profesor($result[0], $result[1], $result[2], $genero));
		}
		$this -> connection -> close();
		return $profesors;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> profesorDAO -> search($search));
		$profesors = array();
		while ($result = $this -> connection -> fetchRow()){
			$genero = new Genero($result[3]);
			$genero -> select();
			array_push($profesors, new Profesor($result[0], $result[1], $result[2], $genero));
		}
		$this -> connection -> close();
		return $profesors;
	}
}
?>
