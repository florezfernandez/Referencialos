<?php
require_once ("persistence/ReferenciaDAO.php");
require_once ("persistence/Connection.php");

class Referencia {
	protected $idReferencia;
	protected $comentario;
	protected $pros;
	protected $contras;
	protected $periodo;
	protected $fecha;
	protected $estado;
	protected $profesor;
	protected $curso;
	protected $estudiante;
	protected $programa;
	protected $rangoDeNota;
	private $referenciaDAO;
	protected $connection;

	function getIdReferencia() {
		return $this -> idReferencia;
	}

	function setIdReferencia($pIdReferencia) {
		$this -> idReferencia = $pIdReferencia;
	}

	function getComentario() {
		return $this -> comentario;
	}

	function setComentario($pComentario) {
		$this -> comentario = $pComentario;
	}

	function getPros() {
		return $this -> pros;
	}

	function setPros($pPros) {
		$this -> pros = $pPros;
	}

	function getContras() {
		return $this -> contras;
	}

	function setContras($pContras) {
		$this -> contras = $pContras;
	}

	function getPeriodo() {
		return $this -> periodo;
	}

	function setPeriodo($pPeriodo) {
		$this -> periodo = $pPeriodo;
	}

	function getFecha() {
		return $this -> fecha;
	}

	function setFecha($pFecha) {
		$this -> fecha = $pFecha;
	}

	function getEstado() {
		return $this -> estado;
	}

	function setEstado($pEstado) {
		$this -> estado = $pEstado;
	}

	function getProfesor() {
		return $this -> profesor;
	}

	function setProfesor($pProfesor) {
		$this -> profesor = $pProfesor;
	}

	function getCurso() {
		return $this -> curso;
	}

	function setCurso($pCurso) {
		$this -> curso = $pCurso;
	}

	function getEstudiante() {
		return $this -> estudiante;
	}

	function setEstudiante($pEstudiante) {
		$this -> estudiante = $pEstudiante;
	}

	function getPrograma() {
		return $this -> programa;
	}

	function setPrograma($pPrograma) {
		$this -> programa = $pPrograma;
	}

	function getRangoDeNota() {
		return $this -> rangoDeNota;
	}

	function setRangoDeNota($pRangoDeNota) {
		$this -> rangoDeNota = $pRangoDeNota;
	}

	function __construct($pIdReferencia = "", $pComentario = "", $pPros = "", $pContras = "", $pPeriodo = "", $pFecha = "", $pEstado = "", $pProfesor = "", $pCurso = "", $pEstudiante = "", $pPrograma = "", $pRangoDeNota = ""){
		$this -> idReferencia = $pIdReferencia;
		$this -> comentario = $pComentario;
		$this -> pros = $pPros;
		$this -> contras = $pContras;
		$this -> periodo = $pPeriodo;
		$this -> fecha = $pFecha;
		$this -> estado = $pEstado;
		$this -> profesor = $pProfesor;
		$this -> curso = $pCurso;
		$this -> estudiante = $pEstudiante;
		$this -> programa = $pPrograma;
		$this -> rangoDeNota = $pRangoDeNota;
		$this -> referenciaDAO = new ReferenciaDAO($this -> idReferencia, $this -> comentario, $this -> pros, $this -> contras, $this -> periodo, $this -> fecha, $this -> estado, $this -> profesor, $this -> curso, $this -> estudiante, $this -> programa, $this -> rangoDeNota);
		$this -> connection = new Connection();
	}

	function insert(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> insert());
		$id = $this -> connection -> lastId();;
		$this -> connection -> close();
		return $id;
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> update());
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idReferencia = $result[0];
		$this -> comentario = $result[1];
		$this -> pros = $result[2];
		$this -> contras = $result[3];
		$this -> periodo = $result[4];
		$this -> fecha = $result[5];
		$this -> estado = $result[6];
		$profesor = new Profesor($result[7]);
		$profesor -> select();
		$this -> profesor = $profesor;
		$curso = new Curso($result[8]);
		$curso -> select();
		$this -> curso = $curso;
		$estudiante = new Estudiante($result[9]);
		$estudiante -> select();
		$this -> estudiante = $estudiante;
		$programa = new Programa($result[10]);
		$programa -> select();
		$this -> programa = $programa;
		$rangoDeNota = new RangoDeNota($result[11]);
		$rangoDeNota -> select();
		$this -> rangoDeNota = $rangoDeNota;
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> selectAll());
		$referencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[7]);
			$profesor -> select();
			$curso = new Curso($result[8]);
			$curso -> select();
			$estudiante = new Estudiante($result[9]);
			$estudiante -> select();
			$programa = new Programa($result[10]);
			$programa -> select();
			$rangoDeNota = new RangoDeNota($result[11]);
			$rangoDeNota -> select();
			array_push($referencias, new Referencia($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $profesor, $curso, $estudiante, $programa, $rangoDeNota));
		}
		$this -> connection -> close();
		return $referencias;
	}

	function selectAllByProfesor(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> selectAllByProfesor());
		$referencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[7]);
			$profesor -> select();
			$curso = new Curso($result[8]);
			$curso -> select();
			$estudiante = new Estudiante($result[9]);
			$estudiante -> select();
			$programa = new Programa($result[10]);
			$programa -> select();
			$rangoDeNota = new RangoDeNota($result[11]);
			$rangoDeNota -> select();
			array_push($referencias, new Referencia($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $profesor, $curso, $estudiante, $programa, $rangoDeNota));
		}
		$this -> connection -> close();
		return $referencias;
	}

	function selectAllByCurso(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> selectAllByCurso());
		$referencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[7]);
			$profesor -> select();
			$curso = new Curso($result[8]);
			$curso -> select();
			$estudiante = new Estudiante($result[9]);
			$estudiante -> select();
			$programa = new Programa($result[10]);
			$programa -> select();
			$rangoDeNota = new RangoDeNota($result[11]);
			$rangoDeNota -> select();
			array_push($referencias, new Referencia($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $profesor, $curso, $estudiante, $programa, $rangoDeNota));
		}
		$this -> connection -> close();
		return $referencias;
	}

	function selectAllByEstudiante(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> selectAllByEstudiante());
		$referencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[7]);
			$profesor -> select();
			$curso = new Curso($result[8]);
			$curso -> select();
			$estudiante = new Estudiante($result[9]);
			$estudiante -> select();
			$programa = new Programa($result[10]);
			$programa -> select();
			$rangoDeNota = new RangoDeNota($result[11]);
			$rangoDeNota -> select();
			array_push($referencias, new Referencia($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $profesor, $curso, $estudiante, $programa, $rangoDeNota));
		}
		$this -> connection -> close();
		return $referencias;
	}

	function selectAllByPrograma(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> selectAllByPrograma());
		$referencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[7]);
			$profesor -> select();
			$curso = new Curso($result[8]);
			$curso -> select();
			$estudiante = new Estudiante($result[9]);
			$estudiante -> select();
			$programa = new Programa($result[10]);
			$programa -> select();
			$rangoDeNota = new RangoDeNota($result[11]);
			$rangoDeNota -> select();
			array_push($referencias, new Referencia($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $profesor, $curso, $estudiante, $programa, $rangoDeNota));
		}
		$this -> connection -> close();
		return $referencias;
	}

	function selectAllByRangoDeNota(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> selectAllByRangoDeNota());
		$referencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[7]);
			$profesor -> select();
			$curso = new Curso($result[8]);
			$curso -> select();
			$estudiante = new Estudiante($result[9]);
			$estudiante -> select();
			$programa = new Programa($result[10]);
			$programa -> select();
			$rangoDeNota = new RangoDeNota($result[11]);
			$rangoDeNota -> select();
			array_push($referencias, new Referencia($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $profesor, $curso, $estudiante, $programa, $rangoDeNota));
		}
		$this -> connection -> close();
		return $referencias;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> selectAllOrder($order, $dir));
		$referencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[7]);
			$profesor -> select();
			$curso = new Curso($result[8]);
			$curso -> select();
			$estudiante = new Estudiante($result[9]);
			$estudiante -> select();
			$programa = new Programa($result[10]);
			$programa -> select();
			$rangoDeNota = new RangoDeNota($result[11]);
			$rangoDeNota -> select();
			array_push($referencias, new Referencia($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $profesor, $curso, $estudiante, $programa, $rangoDeNota));
		}
		$this -> connection -> close();
		return $referencias;
	}

	function selectAllByProfesorOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> selectAllByProfesorOrder($order, $dir));
		$referencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[7]);
			$profesor -> select();
			$curso = new Curso($result[8]);
			$curso -> select();
			$estudiante = new Estudiante($result[9]);
			$estudiante -> select();
			$programa = new Programa($result[10]);
			$programa -> select();
			$rangoDeNota = new RangoDeNota($result[11]);
			$rangoDeNota -> select();
			array_push($referencias, new Referencia($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $profesor, $curso, $estudiante, $programa, $rangoDeNota));
		}
		$this -> connection -> close();
		return $referencias;
	}

	function selectAllByCursoOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> selectAllByCursoOrder($order, $dir));
		$referencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[7]);
			$profesor -> select();
			$curso = new Curso($result[8]);
			$curso -> select();
			$estudiante = new Estudiante($result[9]);
			$estudiante -> select();
			$programa = new Programa($result[10]);
			$programa -> select();
			$rangoDeNota = new RangoDeNota($result[11]);
			$rangoDeNota -> select();
			array_push($referencias, new Referencia($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $profesor, $curso, $estudiante, $programa, $rangoDeNota));
		}
		$this -> connection -> close();
		return $referencias;
	}

	function selectAllByEstudianteOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> selectAllByEstudianteOrder($order, $dir));
		$referencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[7]);
			$profesor -> select();
			$curso = new Curso($result[8]);
			$curso -> select();
			$estudiante = new Estudiante($result[9]);
			$estudiante -> select();
			$programa = new Programa($result[10]);
			$programa -> select();
			$rangoDeNota = new RangoDeNota($result[11]);
			$rangoDeNota -> select();
			array_push($referencias, new Referencia($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $profesor, $curso, $estudiante, $programa, $rangoDeNota));
		}
		$this -> connection -> close();
		return $referencias;
	}

	function selectAllByProgramaOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> selectAllByProgramaOrder($order, $dir));
		$referencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[7]);
			$profesor -> select();
			$curso = new Curso($result[8]);
			$curso -> select();
			$estudiante = new Estudiante($result[9]);
			$estudiante -> select();
			$programa = new Programa($result[10]);
			$programa -> select();
			$rangoDeNota = new RangoDeNota($result[11]);
			$rangoDeNota -> select();
			array_push($referencias, new Referencia($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $profesor, $curso, $estudiante, $programa, $rangoDeNota));
		}
		$this -> connection -> close();
		return $referencias;
	}

	function selectAllByRangoDeNotaOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> selectAllByRangoDeNotaOrder($order, $dir));
		$referencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[7]);
			$profesor -> select();
			$curso = new Curso($result[8]);
			$curso -> select();
			$estudiante = new Estudiante($result[9]);
			$estudiante -> select();
			$programa = new Programa($result[10]);
			$programa -> select();
			$rangoDeNota = new RangoDeNota($result[11]);
			$rangoDeNota -> select();
			array_push($referencias, new Referencia($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $profesor, $curso, $estudiante, $programa, $rangoDeNota));
		}
		$this -> connection -> close();
		return $referencias;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaDAO -> search($search));
		$referencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[7]);
			$profesor -> select();
			$curso = new Curso($result[8]);
			$curso -> select();
			$estudiante = new Estudiante($result[9]);
			$estudiante -> select();
			$programa = new Programa($result[10]);
			$programa -> select();
			$rangoDeNota = new RangoDeNota($result[11]);
			$rangoDeNota -> select();
			array_push($referencias, new Referencia($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $profesor, $curso, $estudiante, $programa, $rangoDeNota));
		}
		$this -> connection -> close();
		return $referencias;
	}
}
?>
