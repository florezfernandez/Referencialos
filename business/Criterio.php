<?php
require_once ("persistence/CriterioDAO.php");
require_once ("persistence/Connection.php");

class Criterio {
	private $idCriterio;
	private $nombre;
	private $descripcion;
	private $categoriaDeCriterio;
	private $criterioDAO;
	private $connection;

	function getIdCriterio() {
		return $this -> idCriterio;
	}

	function setIdCriterio($pIdCriterio) {
		$this -> idCriterio = $pIdCriterio;
	}

	function getNombre() {
		return $this -> nombre;
	}

	function setNombre($pNombre) {
		$this -> nombre = $pNombre;
	}

	function getDescripcion() {
		return $this -> descripcion;
	}

	function setDescripcion($pDescripcion) {
		$this -> descripcion = $pDescripcion;
	}

	function getCategoriaDeCriterio() {
		return $this -> categoriaDeCriterio;
	}

	function setCategoriaDeCriterio($pCategoriaDeCriterio) {
		$this -> categoriaDeCriterio = $pCategoriaDeCriterio;
	}

	function __construct($pIdCriterio = "", $pNombre = "", $pDescripcion = "", $pCategoriaDeCriterio = ""){
		$this -> idCriterio = $pIdCriterio;
		$this -> nombre = $pNombre;
		$this -> descripcion = $pDescripcion;
		$this -> categoriaDeCriterio = $pCategoriaDeCriterio;
		$this -> criterioDAO = new CriterioDAO($this -> idCriterio, $this -> nombre, $this -> descripcion, $this -> categoriaDeCriterio);
		$this -> connection = new Connection();
	}

	function insert(){
		$this -> connection -> open();
		$this -> connection -> run($this -> criterioDAO -> insert());
		$id = $this -> connection -> lastId();;
		$this -> connection -> close();
		return $id;
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> criterioDAO -> update());
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> criterioDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idCriterio = $result[0];
		$this -> nombre = $result[1];
		$this -> descripcion = $result[2];
		$categoriaDeCriterio = new CategoriaDeCriterio($result[3]);
		$categoriaDeCriterio -> select();
		$this -> categoriaDeCriterio = $categoriaDeCriterio;
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> criterioDAO -> selectAll());
		$criterios = array();
		while ($result = $this -> connection -> fetchRow()){
			$categoriaDeCriterio = new CategoriaDeCriterio($result[3]);
			$categoriaDeCriterio -> select();
			array_push($criterios, new Criterio($result[0], $result[1], $result[2], $categoriaDeCriterio));
		}
		$this -> connection -> close();
		return $criterios;
	}

	function selectAllByCategoriaDeCriterio(){
		$this -> connection -> open();
		$this -> connection -> run($this -> criterioDAO -> selectAllByCategoriaDeCriterio());
		$criterios = array();
		while ($result = $this -> connection -> fetchRow()){
			$categoriaDeCriterio = new CategoriaDeCriterio($result[3]);
			$categoriaDeCriterio -> select();
			array_push($criterios, new Criterio($result[0], $result[1], $result[2], $categoriaDeCriterio));
		}
		$this -> connection -> close();
		return $criterios;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> criterioDAO -> selectAllOrder($order, $dir));
		$criterios = array();
		while ($result = $this -> connection -> fetchRow()){
			$categoriaDeCriterio = new CategoriaDeCriterio($result[3]);
			$categoriaDeCriterio -> select();
			array_push($criterios, new Criterio($result[0], $result[1], $result[2], $categoriaDeCriterio));
		}
		$this -> connection -> close();
		return $criterios;
	}

	function selectAllByCategoriaDeCriterioOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> criterioDAO -> selectAllByCategoriaDeCriterioOrder($order, $dir));
		$criterios = array();
		while ($result = $this -> connection -> fetchRow()){
			$categoriaDeCriterio = new CategoriaDeCriterio($result[3]);
			$categoriaDeCriterio -> select();
			array_push($criterios, new Criterio($result[0], $result[1], $result[2], $categoriaDeCriterio));
		}
		$this -> connection -> close();
		return $criterios;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> criterioDAO -> search($search));
		$criterios = array();
		while ($result = $this -> connection -> fetchRow()){
			$categoriaDeCriterio = new CategoriaDeCriterio($result[3]);
			$categoriaDeCriterio -> select();
			array_push($criterios, new Criterio($result[0], $result[1], $result[2], $categoriaDeCriterio));
		}
		$this -> connection -> close();
		return $criterios;
	}
}
?>
