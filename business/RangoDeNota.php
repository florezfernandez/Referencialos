<?php
require_once ("persistence/RangoDeNotaDAO.php");
require_once ("persistence/Connection.php");

class RangoDeNota {
	private $idRangoDeNota;
	private $valor;
	private $rangoDeNotaDAO;
	private $connection;

	function getIdRangoDeNota() {
		return $this -> idRangoDeNota;
	}

	function setIdRangoDeNota($pIdRangoDeNota) {
		$this -> idRangoDeNota = $pIdRangoDeNota;
	}

	function getValor() {
		return $this -> valor;
	}

	function setValor($pValor) {
		$this -> valor = $pValor;
	}

	function __construct($pIdRangoDeNota = "", $pValor = ""){
		$this -> idRangoDeNota = $pIdRangoDeNota;
		$this -> valor = $pValor;
		$this -> rangoDeNotaDAO = new RangoDeNotaDAO($this -> idRangoDeNota, $this -> valor);
		$this -> connection = new Connection();
	}

	function insert(){
		$this -> connection -> open();
		$this -> connection -> run($this -> rangoDeNotaDAO -> insert());
		$id = $this -> connection -> lastId();;
		$this -> connection -> close();
		return $id;
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> rangoDeNotaDAO -> update());
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> rangoDeNotaDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idRangoDeNota = $result[0];
		$this -> valor = $result[1];
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> rangoDeNotaDAO -> selectAll());
		$rangoDeNotas = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($rangoDeNotas, new RangoDeNota($result[0], $result[1]));
		}
		$this -> connection -> close();
		return $rangoDeNotas;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> rangoDeNotaDAO -> selectAllOrder($order, $dir));
		$rangoDeNotas = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($rangoDeNotas, new RangoDeNota($result[0], $result[1]));
		}
		$this -> connection -> close();
		return $rangoDeNotas;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> rangoDeNotaDAO -> search($search));
		$rangoDeNotas = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($rangoDeNotas, new RangoDeNota($result[0], $result[1]));
		}
		$this -> connection -> close();
		return $rangoDeNotas;
	}
}
?>
