<?php
require_once ("persistence/VotacionDAO.php");
require_once ("persistence/Connection.php");

class Votacion {
	protected $idVotacion;
	protected $voto;
	protected $fecha;
	protected $referencia;
	protected $estudiante;
	private $votacionDAO;
	protected $connection;

	function getIdVotacion() {
		return $this -> idVotacion;
	}

	function setIdVotacion($pIdVotacion) {
		$this -> idVotacion = $pIdVotacion;
	}

	function getVoto() {
		return $this -> voto;
	}

	function setVoto($pVoto) {
		$this -> voto = $pVoto;
	}

	function getFecha() {
		return $this -> fecha;
	}

	function setFecha($pFecha) {
		$this -> fecha = $pFecha;
	}

	function getReferencia() {
		return $this -> referencia;
	}

	function setReferencia($pReferencia) {
		$this -> referencia = $pReferencia;
	}

	function getEstudiante() {
		return $this -> estudiante;
	}

	function setEstudiante($pEstudiante) {
		$this -> estudiante = $pEstudiante;
	}

	function __construct($pIdVotacion = "", $pVoto = "", $pFecha = "", $pReferencia = "", $pEstudiante = ""){
		$this -> idVotacion = $pIdVotacion;
		$this -> voto = $pVoto;
		$this -> fecha = $pFecha;
		$this -> referencia = $pReferencia;
		$this -> estudiante = $pEstudiante;
		$this -> votacionDAO = new VotacionDAO($this -> idVotacion, $this -> voto, $this -> fecha, $this -> referencia, $this -> estudiante);
		$this -> connection = new Connection();
	}

	function insert(){
		$this -> connection -> open();
		echo $this -> votacionDAO -> insert();
		$this -> connection -> run($this -> votacionDAO -> insert());
		$id = $this -> connection -> lastId();;
		$this -> connection -> close();
		return $id;
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> votacionDAO -> update());
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> votacionDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idVotacion = $result[0];
		$this -> voto = $result[1];
		$this -> fecha = $result[2];
		$referencia = new Referencia($result[3]);
		$referencia -> select();
		$this -> referencia = $referencia;
		$estudiante = new Estudiante($result[4]);
		$estudiante -> select();
		$this -> estudiante = $estudiante;
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> votacionDAO -> selectAll());
		$votacions = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			array_push($votacions, new Votacion($result[0], $result[1], $result[2], $referencia, $estudiante));
		}
		$this -> connection -> close();
		return $votacions;
	}

	function selectAllByReferencia(){
		$this -> connection -> open();
		$this -> connection -> run($this -> votacionDAO -> selectAllByReferencia());
		$votacions = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			array_push($votacions, new Votacion($result[0], $result[1], $result[2], $referencia, $estudiante));
		}
		$this -> connection -> close();
		return $votacions;
	}

	function selectAllByEstudiante(){
		$this -> connection -> open();
		$this -> connection -> run($this -> votacionDAO -> selectAllByEstudiante());
		$votacions = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			array_push($votacions, new Votacion($result[0], $result[1], $result[2], $referencia, $estudiante));
		}
		$this -> connection -> close();
		return $votacions;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> votacionDAO -> selectAllOrder($order, $dir));
		$votacions = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			array_push($votacions, new Votacion($result[0], $result[1], $result[2], $referencia, $estudiante));
		}
		$this -> connection -> close();
		return $votacions;
	}

	function selectAllByReferenciaOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> votacionDAO -> selectAllByReferenciaOrder($order, $dir));
		$votacions = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			array_push($votacions, new Votacion($result[0], $result[1], $result[2], $referencia, $estudiante));
		}
		$this -> connection -> close();
		return $votacions;
	}

	function selectAllByEstudianteOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> votacionDAO -> selectAllByEstudianteOrder($order, $dir));
		$votacions = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			array_push($votacions, new Votacion($result[0], $result[1], $result[2], $referencia, $estudiante));
		}
		$this -> connection -> close();
		return $votacions;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> votacionDAO -> search($search));
		$votacions = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			array_push($votacions, new Votacion($result[0], $result[1], $result[2], $referencia, $estudiante));
		}
		$this -> connection -> close();
		return $votacions;
	}
}
?>
