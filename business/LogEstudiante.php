<?php
require_once ("persistence/LogEstudianteDAO.php");
require_once ("persistence/Connection.php");

class LogEstudiante {
	private $idLogEstudiante;
	private $accion;
	private $informacion;
	private $fecha;
	private $hora;
	private $ip;
	private $so;
	private $explorador;
	private $estudiante;
	private $logEstudianteDAO;
	private $connection;

	function getIdLogEstudiante() {
		return $this -> idLogEstudiante;
	}

	function setIdLogEstudiante($pIdLogEstudiante) {
		$this -> idLogEstudiante = $pIdLogEstudiante;
	}

	function getAccion() {
		return $this -> accion;
	}

	function setAccion($pAccion) {
		$this -> accion = $pAccion;
	}

	function getInformacion() {
		return $this -> informacion;
	}

	function setInformacion($pInformacion) {
		$this -> informacion = $pInformacion;
	}

	function getFecha() {
		return $this -> fecha;
	}

	function setFecha($pFecha) {
		$this -> fecha = $pFecha;
	}

	function getHora() {
		return $this -> hora;
	}

	function setHora($pHora) {
		$this -> hora = $pHora;
	}

	function getIp() {
		return $this -> ip;
	}

	function setIp($pIp) {
		$this -> ip = $pIp;
	}

	function getSo() {
		return $this -> so;
	}

	function setSo($pSo) {
		$this -> so = $pSo;
	}

	function getExplorador() {
		return $this -> explorador;
	}

	function setExplorador($pExplorador) {
		$this -> explorador = $pExplorador;
	}

	function getEstudiante() {
		return $this -> estudiante;
	}

	function setEstudiante($pEstudiante) {
		$this -> estudiante = $pEstudiante;
	}

	function __construct($pIdLogEstudiante = "", $pAccion = "", $pInformacion = "", $pFecha = "", $pHora = "", $pIp = "", $pSo = "", $pExplorador = "", $pEstudiante = ""){
		$this -> idLogEstudiante = $pIdLogEstudiante;
		$this -> accion = $pAccion;
		$this -> informacion = $pInformacion;
		$this -> fecha = $pFecha;
		$this -> hora = $pHora;
		$this -> ip = $pIp;
		$this -> so = $pSo;
		$this -> explorador = $pExplorador;
		$this -> estudiante = $pEstudiante;
		$this -> logEstudianteDAO = new LogEstudianteDAO($this -> idLogEstudiante, $this -> accion, $this -> informacion, $this -> fecha, $this -> hora, $this -> ip, $this -> so, $this -> explorador, $this -> estudiante);
		$this -> connection = new Connection();
	}

	function insert(){
		$this -> connection -> open();
		$this -> connection -> run($this -> logEstudianteDAO -> insert());
		$id = $this -> connection -> lastId();;
		$this -> connection -> close();
		return $id;
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> logEstudianteDAO -> update());
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> logEstudianteDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idLogEstudiante = $result[0];
		$this -> accion = $result[1];
		$this -> informacion = $result[2];
		$this -> fecha = $result[3];
		$this -> hora = $result[4];
		$this -> ip = $result[5];
		$this -> so = $result[6];
		$this -> explorador = $result[7];
		$estudiante = new Estudiante($result[8]);
		$estudiante -> select();
		$this -> estudiante = $estudiante;
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> logEstudianteDAO -> selectAll());
		$logEstudiantes = array();
		while ($result = $this -> connection -> fetchRow()){
			$estudiante = new Estudiante($result[8]);
			$estudiante -> select();
			array_push($logEstudiantes, new LogEstudiante($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $estudiante));
		}
		$this -> connection -> close();
		return $logEstudiantes;
	}

	function selectAllByEstudiante(){
		$this -> connection -> open();
		$this -> connection -> run($this -> logEstudianteDAO -> selectAllByEstudiante());
		$logEstudiantes = array();
		while ($result = $this -> connection -> fetchRow()){
			$estudiante = new Estudiante($result[8]);
			$estudiante -> select();
			array_push($logEstudiantes, new LogEstudiante($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $estudiante));
		}
		$this -> connection -> close();
		return $logEstudiantes;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> logEstudianteDAO -> selectAllOrder($order, $dir));
		$logEstudiantes = array();
		while ($result = $this -> connection -> fetchRow()){
			$estudiante = new Estudiante($result[8]);
			$estudiante -> select();
			array_push($logEstudiantes, new LogEstudiante($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $estudiante));
		}
		$this -> connection -> close();
		return $logEstudiantes;
	}

	function selectAllByEstudianteOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> logEstudianteDAO -> selectAllByEstudianteOrder($order, $dir));
		$logEstudiantes = array();
		while ($result = $this -> connection -> fetchRow()){
			$estudiante = new Estudiante($result[8]);
			$estudiante -> select();
			array_push($logEstudiantes, new LogEstudiante($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $estudiante));
		}
		$this -> connection -> close();
		return $logEstudiantes;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> logEstudianteDAO -> search($search));
		$logEstudiantes = array();
		while ($result = $this -> connection -> fetchRow()){
			$estudiante = new Estudiante($result[8]);
			$estudiante -> select();
			array_push($logEstudiantes, new LogEstudiante($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $estudiante));
		}
		$this -> connection -> close();
		return $logEstudiantes;
	}
}
?>
