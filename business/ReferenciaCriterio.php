<?php
require_once ("persistence/ReferenciaCriterioDAO.php");
require_once ("persistence/Connection.php");

class ReferenciaCriterio {
	private $idReferenciaCriterio;
	private $valor;
	private $referencia;
	private $criterio;
	private $referenciaCriterioDAO;
	private $connection;

	function getIdReferenciaCriterio() {
		return $this -> idReferenciaCriterio;
	}

	function setIdReferenciaCriterio($pIdReferenciaCriterio) {
		$this -> idReferenciaCriterio = $pIdReferenciaCriterio;
	}

	function getValor() {
		return $this -> valor;
	}

	function setValor($pValor) {
		$this -> valor = $pValor;
	}

	function getReferencia() {
		return $this -> referencia;
	}

	function setReferencia($pReferencia) {
		$this -> referencia = $pReferencia;
	}

	function getCriterio() {
		return $this -> criterio;
	}

	function setCriterio($pCriterio) {
		$this -> criterio = $pCriterio;
	}

	function __construct($pIdReferenciaCriterio = "", $pValor = "", $pReferencia = "", $pCriterio = ""){
		$this -> idReferenciaCriterio = $pIdReferenciaCriterio;
		$this -> valor = $pValor;
		$this -> referencia = $pReferencia;
		$this -> criterio = $pCriterio;
		$this -> referenciaCriterioDAO = new ReferenciaCriterioDAO($this -> idReferenciaCriterio, $this -> valor, $this -> referencia, $this -> criterio);
		$this -> connection = new Connection();
	}

	function insert(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaCriterioDAO -> insert());
		$id = $this -> connection -> lastId();;
		$this -> connection -> close();
		return $id;
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaCriterioDAO -> update());
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaCriterioDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idReferenciaCriterio = $result[0];
		$this -> valor = $result[1];
		$referencia = new Referencia($result[2]);
		$referencia -> select();
		$this -> referencia = $referencia;
		$criterio = new Criterio($result[3]);
		$criterio -> select();
		$this -> criterio = $criterio;
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaCriterioDAO -> selectAll());
		$referenciaCriterios = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[2]);
			$referencia -> select();
			$criterio = new Criterio($result[3]);
			$criterio -> select();
			array_push($referenciaCriterios, new ReferenciaCriterio($result[0], $result[1], $referencia, $criterio));
		}
		$this -> connection -> close();
		return $referenciaCriterios;
	}

	function selectAllByReferencia(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaCriterioDAO -> selectAllByReferencia());
		$referenciaCriterios = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[2]);
			$referencia -> select();
			$criterio = new Criterio($result[3]);
			$criterio -> select();
			array_push($referenciaCriterios, new ReferenciaCriterio($result[0], $result[1], $referencia, $criterio));
		}
		$this -> connection -> close();
		return $referenciaCriterios;
	}

	function selectAllByCriterio(){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaCriterioDAO -> selectAllByCriterio());
		$referenciaCriterios = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[2]);
			$referencia -> select();
			$criterio = new Criterio($result[3]);
			$criterio -> select();
			array_push($referenciaCriterios, new ReferenciaCriterio($result[0], $result[1], $referencia, $criterio));
		}
		$this -> connection -> close();
		return $referenciaCriterios;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaCriterioDAO -> selectAllOrder($order, $dir));
		$referenciaCriterios = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[2]);
			$referencia -> select();
			$criterio = new Criterio($result[3]);
			$criterio -> select();
			array_push($referenciaCriterios, new ReferenciaCriterio($result[0], $result[1], $referencia, $criterio));
		}
		$this -> connection -> close();
		return $referenciaCriterios;
	}

	function selectAllByReferenciaOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaCriterioDAO -> selectAllByReferenciaOrder($order, $dir));
		$referenciaCriterios = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[2]);
			$referencia -> select();
			$criterio = new Criterio($result[3]);
			$criterio -> select();
			array_push($referenciaCriterios, new ReferenciaCriterio($result[0], $result[1], $referencia, $criterio));
		}
		$this -> connection -> close();
		return $referenciaCriterios;
	}

	function selectAllByCriterioOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaCriterioDAO -> selectAllByCriterioOrder($order, $dir));
		$referenciaCriterios = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[2]);
			$referencia -> select();
			$criterio = new Criterio($result[3]);
			$criterio -> select();
			array_push($referenciaCriterios, new ReferenciaCriterio($result[0], $result[1], $referencia, $criterio));
		}
		$this -> connection -> close();
		return $referenciaCriterios;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> referenciaCriterioDAO -> search($search));
		$referenciaCriterios = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[2]);
			$referencia -> select();
			$criterio = new Criterio($result[3]);
			$criterio -> select();
			array_push($referenciaCriterios, new ReferenciaCriterio($result[0], $result[1], $referencia, $criterio));
		}
		$this -> connection -> close();
		return $referenciaCriterios;
	}
}
?>
