<?php
require_once ("persistence/ProgramaDAO.php");
require_once ("persistence/Connection.php");

class Programa {
	protected $idPrograma;
	protected $nombre;
	protected $facultad;
	private $programaDAO;
	protected $connection;

	function getIdPrograma() {
		return $this -> idPrograma;
	}

	function setIdPrograma($pIdPrograma) {
		$this -> idPrograma = $pIdPrograma;
	}

	function getNombre() {
		return $this -> nombre;
	}

	function setNombre($pNombre) {
		$this -> nombre = $pNombre;
	}

	function getFacultad() {
		return $this -> facultad;
	}

	function setFacultad($pFacultad) {
		$this -> facultad = $pFacultad;
	}

	function __construct($pIdPrograma = "", $pNombre = "", $pFacultad = ""){
		$this -> idPrograma = $pIdPrograma;
		$this -> nombre = $pNombre;
		$this -> facultad = $pFacultad;
		$this -> programaDAO = new ProgramaDAO($this -> idPrograma, $this -> nombre, $this -> facultad);
		$this -> connection = new Connection();
	}

	function insert(){
		$this -> connection -> open();
		$this -> connection -> run($this -> programaDAO -> insert());
		$id = $this -> connection -> lastId();;
		$this -> connection -> close();
		return $id;
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> programaDAO -> update());
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> programaDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idPrograma = $result[0];
		$this -> nombre = $result[1];
		$facultad = new Facultad($result[2]);
		$facultad -> select();
		$this -> facultad = $facultad;
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> programaDAO -> selectAll());
		$programas = array();
		while ($result = $this -> connection -> fetchRow()){
			$facultad = new Facultad($result[2]);
			$facultad -> select();
			array_push($programas, new Programa($result[0], $result[1], $facultad));
		}
		$this -> connection -> close();
		return $programas;
	}

	function selectAllByFacultad(){
		$this -> connection -> open();
		$this -> connection -> run($this -> programaDAO -> selectAllByFacultad());
		$programas = array();
		while ($result = $this -> connection -> fetchRow()){
			$facultad = new Facultad($result[2]);
			$facultad -> select();
			array_push($programas, new Programa($result[0], $result[1], $facultad));
		}
		$this -> connection -> close();
		return $programas;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> programaDAO -> selectAllOrder($order, $dir));
		$programas = array();
		while ($result = $this -> connection -> fetchRow()){
			$facultad = new Facultad($result[2]);
			$facultad -> select();
			array_push($programas, new Programa($result[0], $result[1], $facultad));
		}
		$this -> connection -> close();
		return $programas;
	}

	function selectAllByFacultadOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> programaDAO -> selectAllByFacultadOrder($order, $dir));
		$programas = array();
		while ($result = $this -> connection -> fetchRow()){
			$facultad = new Facultad($result[2]);
			$facultad -> select();
			array_push($programas, new Programa($result[0], $result[1], $facultad));
		}
		$this -> connection -> close();
		return $programas;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> programaDAO -> search($search));
		$programas = array();
		while ($result = $this -> connection -> fetchRow()){
			$facultad = new Facultad($result[2]);
			$facultad -> select();
			array_push($programas, new Programa($result[0], $result[1], $facultad));
		}
		$this -> connection -> close();
		return $programas;
	}
}
?>
