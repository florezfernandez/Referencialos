<?php
require_once ("persistence/EstudianteDAO.php");
require_once ("persistence/Connection.php");

class Estudiante {
	protected $idEstudiante;
	protected $nombre;
	protected $apellido;
	protected $correo;
	protected $clave;
	protected $foto;
	protected $telefono;
	protected $celular;
	protected $estado;
	protected $periodoDeIngreso;
	protected $fechaDeNacimiento;
	protected $fechaDeRegistro;
	protected $genero;
	private $estudianteDAO;
	protected $connection;

	function getIdEstudiante() {
		return $this -> idEstudiante;
	}

	function setIdEstudiante($pIdEstudiante) {
		$this -> idEstudiante = $pIdEstudiante;
	}

	function getNombre() {
		return $this -> nombre;
	}

	function setNombre($pNombre) {
		$this -> nombre = $pNombre;
	}

	function getApellido() {
		return $this -> apellido;
	}

	function setApellido($pApellido) {
		$this -> apellido = $pApellido;
	}

	function getCorreo() {
		return $this -> correo;
	}

	function setCorreo($pCorreo) {
		$this -> correo = $pCorreo;
	}

	function getClave() {
		return $this -> clave;
	}

	function setClave($pClave) {
		$this -> clave = $pClave;
	}

	function getFoto() {
		return $this -> foto;
	}

	function setFoto($pFoto) {
		$this -> foto = $pFoto;
	}

	function getTelefono() {
		return $this -> telefono;
	}

	function setTelefono($pTelefono) {
		$this -> telefono = $pTelefono;
	}

	function getCelular() {
		return $this -> celular;
	}

	function setCelular($pCelular) {
		$this -> celular = $pCelular;
	}

	function getEstado() {
		return $this -> estado;
	}

	function setEstado($pEstado) {
		$this -> estado = $pEstado;
	}

	function getPeriodoDeIngreso() {
		return $this -> periodoDeIngreso;
	}

	function setPeriodoDeIngreso($pPeriodoDeIngreso) {
		$this -> periodoDeIngreso = $pPeriodoDeIngreso;
	}

	function getFechaDeNacimiento() {
		return $this -> fechaDeNacimiento;
	}

	function setFechaDeNacimiento($pFechaDeNacimiento) {
		$this -> fechaDeNacimiento = $pFechaDeNacimiento;
	}

	function getFechaDeRegistro() {
		return $this -> fechaDeRegistro;
	}

	function setFechaDeRegistro($pFechaDeRegistro) {
		$this -> fechaDeRegistro = $pFechaDeRegistro;
	}

	function getGenero() {
		return $this -> genero;
	}

	function setGenero($pGenero) {
		$this -> genero = $pGenero;
	}

	function __construct($pIdEstudiante = "", $pNombre = "", $pApellido = "", $pCorreo = "", $pClave = "", $pFoto = "", $pTelefono = "", $pCelular = "", $pEstado = "", $pPeriodoDeIngreso = "", $pFechaDeNacimiento = "", $pFechaDeRegistro = "", $pGenero = ""){
		$this -> idEstudiante = $pIdEstudiante;
		$this -> nombre = $pNombre;
		$this -> apellido = $pApellido;
		$this -> correo = $pCorreo;
		$this -> clave = $pClave;
		$this -> foto = $pFoto;
		$this -> telefono = $pTelefono;
		$this -> celular = $pCelular;
		$this -> estado = $pEstado;
		$this -> periodoDeIngreso = $pPeriodoDeIngreso;
		$this -> fechaDeNacimiento = $pFechaDeNacimiento;
		$this -> fechaDeRegistro = $pFechaDeRegistro;
		$this -> genero = $pGenero;
		$this -> estudianteDAO = new EstudianteDAO($this -> idEstudiante, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> foto, $this -> telefono, $this -> celular, $this -> estado, $this -> periodoDeIngreso, $this -> fechaDeNacimiento, $this -> fechaDeRegistro, $this -> genero);
		$this -> connection = new Connection();
	}

	function logIn($email, $password){
		$this -> connection -> open();
		$this -> connection -> run($this -> estudianteDAO -> logIn($email, $password));
		if($this -> connection -> numRows()==1){
			$result = $this -> connection -> fetchRow();
			$this -> idEstudiante = $result[0];
			$this -> nombre = $result[1];
			$this -> apellido = $result[2];
			$this -> correo = $result[3];
			$this -> clave = $result[4];
			$this -> foto = $result[5];
			$this -> telefono = $result[6];
			$this -> celular = $result[7];
			$this -> estado = $result[8];
			$this -> periodoDeIngreso = $result[9];
			$this -> fechaDeNacimiento = $result[10];
			$this -> fechaDeRegistro = $result[11];
			$genero = new Genero($result[12]);
			$genero -> select();
			$this -> genero = $genero;
			$this -> connection -> close();
			return true;
		}else{
			$this -> connection -> close();
			return false;
		}
	}

	function insert(){
		$this -> connection -> open();
		$this -> connection -> run($this -> estudianteDAO -> insert());
		$id = $this -> connection -> lastId();;
		$this -> connection -> close();
		return $id;
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> estudianteDAO -> update());
		$this -> connection -> close();
	}

	function updateClave($password){
		$this -> connection -> open();
		$this -> connection -> run($this -> estudianteDAO -> updateClave($password));
		$this -> connection -> close();
	}

	function existCorreo($correo){
		$this -> connection -> open();
		$this -> connection -> run($this -> estudianteDAO -> existCorreo($correo));
		if($this -> connection -> numRows()==1){
			$this -> connection -> close();
			return true;
		}else{
			$this -> connection -> close();
			return false;
		}
	}

	function recoverPassword($correo, $password){
		$this -> connection -> open();
		$this -> connection -> run($this -> estudianteDAO -> recoverPassword($correo, $password));
		$this -> connection -> close();
	}

	function updateImageFoto($value){
		$this -> connection -> open();
		$this -> connection -> run($this -> estudianteDAO -> updateImageFoto($value));
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> estudianteDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idEstudiante = $result[0];
		$this -> nombre = $result[1];
		$this -> apellido = $result[2];
		$this -> correo = $result[3];
		$this -> clave = $result[4];
		$this -> foto = $result[5];
		$this -> telefono = $result[6];
		$this -> celular = $result[7];
		$this -> estado = $result[8];
		$this -> periodoDeIngreso = $result[9];
		$this -> fechaDeNacimiento = $result[10];
		$this -> fechaDeRegistro = $result[11];
		$genero = new Genero($result[12]);
		$genero -> select();
		$this -> genero = $genero;
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> estudianteDAO -> selectAll());
		$estudiantes = array();
		while ($result = $this -> connection -> fetchRow()){
			$genero = new Genero($result[12]);
			$genero -> select();
			array_push($estudiantes, new Estudiante($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $result[8], $result[9], $result[10], $result[11], $genero));
		}
		$this -> connection -> close();
		return $estudiantes;
	}

	function selectAllByGenero(){
		$this -> connection -> open();
		$this -> connection -> run($this -> estudianteDAO -> selectAllByGenero());
		$estudiantes = array();
		while ($result = $this -> connection -> fetchRow()){
			$genero = new Genero($result[12]);
			$genero -> select();
			array_push($estudiantes, new Estudiante($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $result[8], $result[9], $result[10], $result[11], $genero));
		}
		$this -> connection -> close();
		return $estudiantes;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> estudianteDAO -> selectAllOrder($order, $dir));
		$estudiantes = array();
		while ($result = $this -> connection -> fetchRow()){
			$genero = new Genero($result[12]);
			$genero -> select();
			array_push($estudiantes, new Estudiante($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $result[8], $result[9], $result[10], $result[11], $genero));
		}
		$this -> connection -> close();
		return $estudiantes;
	}

	function selectAllByGeneroOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> estudianteDAO -> selectAllByGeneroOrder($order, $dir));
		$estudiantes = array();
		while ($result = $this -> connection -> fetchRow()){
			$genero = new Genero($result[12]);
			$genero -> select();
			array_push($estudiantes, new Estudiante($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $result[8], $result[9], $result[10], $result[11], $genero));
		}
		$this -> connection -> close();
		return $estudiantes;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> estudianteDAO -> search($search));
		$estudiantes = array();
		while ($result = $this -> connection -> fetchRow()){
			$genero = new Genero($result[12]);
			$genero -> select();
			array_push($estudiantes, new Estudiante($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $result[8], $result[9], $result[10], $result[11], $genero));
		}
		$this -> connection -> close();
		return $estudiantes;
	}
}
?>
