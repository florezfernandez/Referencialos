<?php
require_once ("persistence/DenunciaDAO.php");
require_once ("persistence/Connection.php");

class Denuncia {
	protected $idDenuncia;
	protected $argumento;
	protected $fecha;
	protected $referencia;
	protected $estudiante;
	protected $tipoDeDenuncia;
	private $denunciaDAO;
	protected $connection;

	function getIdDenuncia() {
		return $this -> idDenuncia;
	}

	function setIdDenuncia($pIdDenuncia) {
		$this -> idDenuncia = $pIdDenuncia;
	}

	function getArgumento() {
		return $this -> argumento;
	}

	function setArgumento($pArgumento) {
		$this -> argumento = $pArgumento;
	}

	function getFecha() {
		return $this -> fecha;
	}

	function setFecha($pFecha) {
		$this -> fecha = $pFecha;
	}

	function getReferencia() {
		return $this -> referencia;
	}

	function setReferencia($pReferencia) {
		$this -> referencia = $pReferencia;
	}

	function getEstudiante() {
		return $this -> estudiante;
	}

	function setEstudiante($pEstudiante) {
		$this -> estudiante = $pEstudiante;
	}

	function getTipoDeDenuncia() {
		return $this -> tipoDeDenuncia;
	}

	function setTipoDeDenuncia($pTipoDeDenuncia) {
		$this -> tipoDeDenuncia = $pTipoDeDenuncia;
	}

	function __construct($pIdDenuncia = "", $pArgumento = "", $pFecha = "", $pReferencia = "", $pEstudiante = "", $pTipoDeDenuncia = ""){
		$this -> idDenuncia = $pIdDenuncia;
		$this -> argumento = $pArgumento;
		$this -> fecha = $pFecha;
		$this -> referencia = $pReferencia;
		$this -> estudiante = $pEstudiante;
		$this -> tipoDeDenuncia = $pTipoDeDenuncia;
		$this -> denunciaDAO = new DenunciaDAO($this -> idDenuncia, $this -> argumento, $this -> fecha, $this -> referencia, $this -> estudiante, $this -> tipoDeDenuncia);
		$this -> connection = new Connection();
	}

	function insert(){
		$this -> connection -> open();
		$this -> connection -> run($this -> denunciaDAO -> insert());
		$id = $this -> connection -> lastId();;
		$this -> connection -> close();
		return $id;
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> denunciaDAO -> update());
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> denunciaDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idDenuncia = $result[0];
		$this -> argumento = $result[1];
		$this -> fecha = $result[2];
		$referencia = new Referencia($result[3]);
		$referencia -> select();
		$this -> referencia = $referencia;
		$estudiante = new Estudiante($result[4]);
		$estudiante -> select();
		$this -> estudiante = $estudiante;
		$tipoDeDenuncia = new TipoDeDenuncia($result[5]);
		$tipoDeDenuncia -> select();
		$this -> tipoDeDenuncia = $tipoDeDenuncia;
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> denunciaDAO -> selectAll());
		$denuncias = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			$tipoDeDenuncia = new TipoDeDenuncia($result[5]);
			$tipoDeDenuncia -> select();
			array_push($denuncias, new Denuncia($result[0], $result[1], $result[2], $referencia, $estudiante, $tipoDeDenuncia));
		}
		$this -> connection -> close();
		return $denuncias;
	}

	function selectAllByReferencia(){
		$this -> connection -> open();
		$this -> connection -> run($this -> denunciaDAO -> selectAllByReferencia());
		$denuncias = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			$tipoDeDenuncia = new TipoDeDenuncia($result[5]);
			$tipoDeDenuncia -> select();
			array_push($denuncias, new Denuncia($result[0], $result[1], $result[2], $referencia, $estudiante, $tipoDeDenuncia));
		}
		$this -> connection -> close();
		return $denuncias;
	}

	function selectAllByEstudiante(){
		$this -> connection -> open();
		$this -> connection -> run($this -> denunciaDAO -> selectAllByEstudiante());
		$denuncias = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			$tipoDeDenuncia = new TipoDeDenuncia($result[5]);
			$tipoDeDenuncia -> select();
			array_push($denuncias, new Denuncia($result[0], $result[1], $result[2], $referencia, $estudiante, $tipoDeDenuncia));
		}
		$this -> connection -> close();
		return $denuncias;
	}

	function selectAllByTipoDeDenuncia(){
		$this -> connection -> open();
		$this -> connection -> run($this -> denunciaDAO -> selectAllByTipoDeDenuncia());
		$denuncias = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			$tipoDeDenuncia = new TipoDeDenuncia($result[5]);
			$tipoDeDenuncia -> select();
			array_push($denuncias, new Denuncia($result[0], $result[1], $result[2], $referencia, $estudiante, $tipoDeDenuncia));
		}
		$this -> connection -> close();
		return $denuncias;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> denunciaDAO -> selectAllOrder($order, $dir));
		$denuncias = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			$tipoDeDenuncia = new TipoDeDenuncia($result[5]);
			$tipoDeDenuncia -> select();
			array_push($denuncias, new Denuncia($result[0], $result[1], $result[2], $referencia, $estudiante, $tipoDeDenuncia));
		}
		$this -> connection -> close();
		return $denuncias;
	}

	function selectAllByReferenciaOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> denunciaDAO -> selectAllByReferenciaOrder($order, $dir));
		$denuncias = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			$tipoDeDenuncia = new TipoDeDenuncia($result[5]);
			$tipoDeDenuncia -> select();
			array_push($denuncias, new Denuncia($result[0], $result[1], $result[2], $referencia, $estudiante, $tipoDeDenuncia));
		}
		$this -> connection -> close();
		return $denuncias;
	}

	function selectAllByEstudianteOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> denunciaDAO -> selectAllByEstudianteOrder($order, $dir));
		$denuncias = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			$tipoDeDenuncia = new TipoDeDenuncia($result[5]);
			$tipoDeDenuncia -> select();
			array_push($denuncias, new Denuncia($result[0], $result[1], $result[2], $referencia, $estudiante, $tipoDeDenuncia));
		}
		$this -> connection -> close();
		return $denuncias;
	}

	function selectAllByTipoDeDenunciaOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> denunciaDAO -> selectAllByTipoDeDenunciaOrder($order, $dir));
		$denuncias = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			$tipoDeDenuncia = new TipoDeDenuncia($result[5]);
			$tipoDeDenuncia -> select();
			array_push($denuncias, new Denuncia($result[0], $result[1], $result[2], $referencia, $estudiante, $tipoDeDenuncia));
		}
		$this -> connection -> close();
		return $denuncias;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> denunciaDAO -> search($search));
		$denuncias = array();
		while ($result = $this -> connection -> fetchRow()){
			$referencia = new Referencia($result[3]);
			$referencia -> select();
			$estudiante = new Estudiante($result[4]);
			$estudiante -> select();
			$tipoDeDenuncia = new TipoDeDenuncia($result[5]);
			$tipoDeDenuncia -> select();
			array_push($denuncias, new Denuncia($result[0], $result[1], $result[2], $referencia, $estudiante, $tipoDeDenuncia));
		}
		$this -> connection -> close();
		return $denuncias;
	}
}
?>
