<?php
require_once ("persistence/GeneroDAO.php");
require_once ("persistence/Connection.php");

class Genero {
	protected $idGenero;
	protected $nombre;
	private $generoDAO;
	protected $connection;

	function getIdGenero() {
		return $this -> idGenero;
	}

	function setIdGenero($pIdGenero) {
		$this -> idGenero = $pIdGenero;
	}

	function getNombre() {
		return $this -> nombre;
	}

	function setNombre($pNombre) {
		$this -> nombre = $pNombre;
	}

	function __construct($pIdGenero = "", $pNombre = ""){
		$this -> idGenero = $pIdGenero;
		$this -> nombre = $pNombre;
		$this -> generoDAO = new GeneroDAO($this -> idGenero, $this -> nombre);
		$this -> connection = new Connection();
	}

	function insert(){
		$this -> connection -> open();
		$this -> connection -> run($this -> generoDAO -> insert());
		$id = $this -> connection -> lastId();;
		$this -> connection -> close();
		return $id;
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> generoDAO -> update());
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> generoDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idGenero = $result[0];
		$this -> nombre = $result[1];
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> generoDAO -> selectAll());
		$generos = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($generos, new Genero($result[0], $result[1]));
		}
		$this -> connection -> close();
		return $generos;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> generoDAO -> selectAllOrder($order, $dir));
		$generos = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($generos, new Genero($result[0], $result[1]));
		}
		$this -> connection -> close();
		return $generos;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> generoDAO -> search($search));
		$generos = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($generos, new Genero($result[0], $result[1]));
		}
		$this -> connection -> close();
		return $generos;
	}
}
?>
