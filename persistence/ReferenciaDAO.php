<?php
class ReferenciaDAO{
	protected $idReferencia;
	protected $comentario;
	protected $pros;
	protected $contras;
	protected $periodo;
	protected $fecha;
	protected $estado;
	protected $profesor;
	protected $curso;
	protected $estudiante;
	protected $programa;
	protected $rangoDeNota;

	function __construct($pIdReferencia = "", $pComentario = "", $pPros = "", $pContras = "", $pPeriodo = "", $pFecha = "", $pEstado = "", $pProfesor = "", $pCurso = "", $pEstudiante = "", $pPrograma = "", $pRangoDeNota = ""){
		$this -> idReferencia = $pIdReferencia;
		$this -> comentario = $pComentario;
		$this -> pros = $pPros;
		$this -> contras = $pContras;
		$this -> periodo = $pPeriodo;
		$this -> fecha = $pFecha;
		$this -> estado = $pEstado;
		$this -> profesor = $pProfesor;
		$this -> curso = $pCurso;
		$this -> estudiante = $pEstudiante;
		$this -> programa = $pPrograma;
		$this -> rangoDeNota = $pRangoDeNota;
	}

	function insert(){
		return "insert into Referencia(comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota)
				values('" . $this -> comentario . "', '" . $this -> pros . "', '" . $this -> contras . "', '" . $this -> periodo . "', '" . $this -> fecha . "', '" . $this -> estado . "', '" . $this -> profesor . "', '" . $this -> curso . "', '" . $this -> estudiante . "', '" . $this -> programa . "', '" . $this -> rangoDeNota . "')";
	}

	function update(){
		return "update Referencia set 
				comentario = '" . $this -> comentario . "',
				pros = '" . $this -> pros . "',
				contras = '" . $this -> contras . "',
				periodo = '" . $this -> periodo . "',
				fecha = '" . $this -> fecha . "',
				estado = '" . $this -> estado . "',
				profesor_idProfesor = '" . $this -> profesor . "',
				curso_idCurso = '" . $this -> curso . "',
				estudiante_idEstudiante = '" . $this -> estudiante . "',
				programa_idPrograma = '" . $this -> programa . "',
				rangoDeNota_idRangoDeNota = '" . $this -> rangoDeNota . "'	
				where idReferencia = '" . $this -> idReferencia . "'";
	}

	function select() {
		return "select idReferencia, comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota
				from Referencia
				where idReferencia = '" . $this -> idReferencia . "'";
	}

	function selectAll() {
		return "select idReferencia, comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota
				from Referencia";
	}

	function selectAllByProfesor() {
		return "select idReferencia, comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota
				from Referencia
				where profesor_idProfesor = '" . $this -> profesor . "'";
	}

	function selectAllByCurso() {
		return "select idReferencia, comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota
				from Referencia
				where curso_idCurso = '" . $this -> curso . "'";
	}

	function selectAllByEstudiante() {
		return "select idReferencia, comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota
				from Referencia
				where estudiante_idEstudiante = '" . $this -> estudiante . "'";
	}

	function selectAllByPrograma() {
		return "select idReferencia, comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota
				from Referencia
				where programa_idPrograma = '" . $this -> programa . "'";
	}

	function selectAllByRangoDeNota() {
		return "select idReferencia, comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota
				from Referencia
				where rangoDeNota_idRangoDeNota = '" . $this -> rangoDeNota . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idReferencia, comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota
				from Referencia
				order by " . $orden . " " . $dir;
	}

	function selectAllByProfesorOrder($orden, $dir) {
		return "select idReferencia, comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota
				from Referencia
				where profesor_idProfesor = '" . $this -> profesor . "'
				order by " . $orden . " " . $dir;
	}

	function selectAllByCursoOrder($orden, $dir) {
		return "select idReferencia, comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota
				from Referencia
				where curso_idCurso = '" . $this -> curso . "'
				order by " . $orden . " " . $dir;
	}

	function selectAllByEstudianteOrder($orden, $dir) {
		return "select idReferencia, comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota
				from Referencia
				where estudiante_idEstudiante = '" . $this -> estudiante . "'
				order by " . $orden . " " . $dir;
	}

	function selectAllByProgramaOrder($orden, $dir) {
		return "select idReferencia, comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota
				from Referencia
				where programa_idPrograma = '" . $this -> programa . "'
				order by " . $orden . " " . $dir;
	}

	function selectAllByRangoDeNotaOrder($orden, $dir) {
		return "select idReferencia, comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota
				from Referencia
				where rangoDeNota_idRangoDeNota = '" . $this -> rangoDeNota . "'
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idReferencia, comentario, pros, contras, periodo, fecha, estado, profesor_idProfesor, curso_idCurso, estudiante_idEstudiante, programa_idPrograma, rangoDeNota_idRangoDeNota
				from Referencia
				where comentario like '%" . $search . "%' or periodo like '%" . $search . "%' or fecha like '%" . $search . "%' or estado like '%" . $search . "%'";
	}
}
?>
