<?php
class ProfesorDAO{
	protected $idProfesor;
	protected $nombre;
	protected $correo;
	protected $genero;

	function __construct($pIdProfesor = "", $pNombre = "", $pCorreo = "", $pGenero = ""){
		$this -> idProfesor = $pIdProfesor;
		$this -> nombre = $pNombre;
		$this -> correo = $pCorreo;
		$this -> genero = $pGenero;
	}

	function insert(){
		return "insert into Profesor(nombre, correo, genero_idGenero)
				values('" . $this -> nombre . "', '" . $this -> correo . "', '" . $this -> genero . "')";
	}

	function update(){
		return "update Profesor set 
				nombre = '" . $this -> nombre . "',
				correo = '" . $this -> correo . "',
				genero_idGenero = '" . $this -> genero . "'	
				where idProfesor = '" . $this -> idProfesor . "'";
	}

	function select() {
		return "select idProfesor, nombre, correo, genero_idGenero
				from Profesor
				where idProfesor = '" . $this -> idProfesor . "'";
	}

	function selectAll() {
		return "select idProfesor, nombre, correo, genero_idGenero
				from Profesor";
	}

	function selectAllByGenero() {
		return "select idProfesor, nombre, correo, genero_idGenero
				from Profesor
				where genero_idGenero = '" . $this -> genero . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idProfesor, nombre, correo, genero_idGenero
				from Profesor
				order by " . $orden . " " . $dir;
	}

	function selectAllByGeneroOrder($orden, $dir) {
		return "select idProfesor, nombre, correo, genero_idGenero
				from Profesor
				where genero_idGenero = '" . $this -> genero . "'
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idProfesor, nombre, correo, genero_idGenero
				from Profesor
				where nombre like '%" . $search . "%' or correo like '%" . $search . "%'";
	}
}
?>
