<?php
class RangoDeNotaDAO{
	private $idRangoDeNota;
	private $valor;

	function __construct($pIdRangoDeNota = "", $pValor = ""){
		$this -> idRangoDeNota = $pIdRangoDeNota;
		$this -> valor = $pValor;
	}

	function insert(){
		return "insert into RangoDeNota(valor)
				values('" . $this -> valor . "')";
	}

	function update(){
		return "update RangoDeNota set 
				valor = '" . $this -> valor . "'	
				where idRangoDeNota = '" . $this -> idRangoDeNota . "'";
	}

	function select() {
		return "select idRangoDeNota, valor
				from RangoDeNota
				where idRangoDeNota = '" . $this -> idRangoDeNota . "'";
	}

	function selectAll() {
		return "select idRangoDeNota, valor
				from RangoDeNota";
	}

	function selectAllOrder($orden, $dir){
		return "select idRangoDeNota, valor
				from RangoDeNota
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idRangoDeNota, valor
				from RangoDeNota
				where valor like '%" . $search . "%'";
	}
}
?>
