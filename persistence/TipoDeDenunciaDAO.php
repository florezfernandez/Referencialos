<?php
class TipoDeDenunciaDAO{
	private $idTipoDeDenuncia;
	private $nombre;

	function __construct($pIdTipoDeDenuncia = "", $pNombre = ""){
		$this -> idTipoDeDenuncia = $pIdTipoDeDenuncia;
		$this -> nombre = $pNombre;
	}

	function insert(){
		return "insert into TipoDeDenuncia(nombre)
				values('" . $this -> nombre . "')";
	}

	function update(){
		return "update TipoDeDenuncia set 
				nombre = '" . $this -> nombre . "'	
				where idTipoDeDenuncia = '" . $this -> idTipoDeDenuncia . "'";
	}

	function select() {
		return "select idTipoDeDenuncia, nombre
				from TipoDeDenuncia
				where idTipoDeDenuncia = '" . $this -> idTipoDeDenuncia . "'";
	}

	function selectAll() {
		return "select idTipoDeDenuncia, nombre
				from TipoDeDenuncia";
	}

	function selectAllOrder($orden, $dir){
		return "select idTipoDeDenuncia, nombre
				from TipoDeDenuncia
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idTipoDeDenuncia, nombre
				from TipoDeDenuncia
				where nombre like '%" . $search . "%'";
	}
}
?>
