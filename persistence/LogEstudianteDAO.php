<?php
class LogEstudianteDAO{
	private $idLogEstudiante;
	private $accion;
	private $informacion;
	private $fecha;
	private $hora;
	private $ip;
	private $so;
	private $explorador;
	private $estudiante;

	function __construct($pIdLogEstudiante = "", $pAccion = "", $pInformacion = "", $pFecha = "", $pHora = "", $pIp = "", $pSo = "", $pExplorador = "", $pEstudiante = ""){
		$this -> idLogEstudiante = $pIdLogEstudiante;
		$this -> accion = $pAccion;
		$this -> informacion = $pInformacion;
		$this -> fecha = $pFecha;
		$this -> hora = $pHora;
		$this -> ip = $pIp;
		$this -> so = $pSo;
		$this -> explorador = $pExplorador;
		$this -> estudiante = $pEstudiante;
	}

	function insert(){
		return "insert into LogEstudiante(accion, informacion, fecha, hora, ip, so, explorador, estudiante_idEstudiante)
				values('" . $this -> accion . "', '" . $this -> informacion . "', '" . $this -> fecha . "', '" . $this -> hora . "', '" . $this -> ip . "', '" . $this -> so . "', '" . $this -> explorador . "', '" . $this -> estudiante . "')";
	}

	function update(){
		return "update LogEstudiante set 
				accion = '" . $this -> accion . "',
				informacion = '" . $this -> informacion . "',
				fecha = '" . $this -> fecha . "',
				hora = '" . $this -> hora . "',
				ip = '" . $this -> ip . "',
				so = '" . $this -> so . "',
				explorador = '" . $this -> explorador . "',
				estudiante_idEstudiante = '" . $this -> estudiante . "'	
				where idLogEstudiante = '" . $this -> idLogEstudiante . "'";
	}

	function select() {
		return "select idLogEstudiante, accion, informacion, fecha, hora, ip, so, explorador, estudiante_idEstudiante
				from LogEstudiante
				where idLogEstudiante = '" . $this -> idLogEstudiante . "'";
	}

	function selectAll() {
		return "select idLogEstudiante, accion, informacion, fecha, hora, ip, so, explorador, estudiante_idEstudiante
				from LogEstudiante";
	}

	function selectAllByEstudiante() {
		return "select idLogEstudiante, accion, informacion, fecha, hora, ip, so, explorador, estudiante_idEstudiante
				from LogEstudiante
				where estudiante_idEstudiante = '" . $this -> estudiante . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idLogEstudiante, accion, informacion, fecha, hora, ip, so, explorador, estudiante_idEstudiante
				from LogEstudiante
				order by " . $orden . " " . $dir;
	}

	function selectAllByEstudianteOrder($orden, $dir) {
		return "select idLogEstudiante, accion, informacion, fecha, hora, ip, so, explorador, estudiante_idEstudiante
				from LogEstudiante
				where estudiante_idEstudiante = '" . $this -> estudiante . "'
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idLogEstudiante, accion, informacion, fecha, hora, ip, so, explorador, estudiante_idEstudiante
				from LogEstudiante
				where accion like '%" . $search . "%' or fecha like '%" . $search . "%' or hora like '%" . $search . "%' or ip like '%" . $search . "%' or so like '%" . $search . "%' or explorador like '%" . $search . "%'
				order by fecha desc, hora desc";
	}
}
?>
