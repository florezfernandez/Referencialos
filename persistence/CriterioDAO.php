<?php
class CriterioDAO{
	private $idCriterio;
	private $nombre;
	private $descripcion;
	private $categoriaDeCriterio;

	function __construct($pIdCriterio = "", $pNombre = "", $pDescripcion = "", $pCategoriaDeCriterio = ""){
		$this -> idCriterio = $pIdCriterio;
		$this -> nombre = $pNombre;
		$this -> descripcion = $pDescripcion;
		$this -> categoriaDeCriterio = $pCategoriaDeCriterio;
	}

	function insert(){
		return "insert into Criterio(nombre, descripcion, categoriaDeCriterio_idCategoriaDeCriterio)
				values('" . $this -> nombre . "', '" . $this -> descripcion . "', '" . $this -> categoriaDeCriterio . "')";
	}

	function update(){
		return "update Criterio set 
				nombre = '" . $this -> nombre . "',
				descripcion = '" . $this -> descripcion . "',
				categoriaDeCriterio_idCategoriaDeCriterio = '" . $this -> categoriaDeCriterio . "'	
				where idCriterio = '" . $this -> idCriterio . "'";
	}

	function select() {
		return "select idCriterio, nombre, descripcion, categoriaDeCriterio_idCategoriaDeCriterio
				from Criterio
				where idCriterio = '" . $this -> idCriterio . "'";
	}

	function selectAll() {
		return "select idCriterio, nombre, descripcion, categoriaDeCriterio_idCategoriaDeCriterio
				from Criterio";
	}

	function selectAllByCategoriaDeCriterio() {
		return "select idCriterio, nombre, descripcion, categoriaDeCriterio_idCategoriaDeCriterio
				from Criterio
				where categoriaDeCriterio_idCategoriaDeCriterio = '" . $this -> categoriaDeCriterio . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idCriterio, nombre, descripcion, categoriaDeCriterio_idCategoriaDeCriterio
				from Criterio
				order by " . $orden . " " . $dir;
	}

	function selectAllByCategoriaDeCriterioOrder($orden, $dir) {
		return "select idCriterio, nombre, descripcion, categoriaDeCriterio_idCategoriaDeCriterio
				from Criterio
				where categoriaDeCriterio_idCategoriaDeCriterio = '" . $this -> categoriaDeCriterio . "'
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idCriterio, nombre, descripcion, categoriaDeCriterio_idCategoriaDeCriterio
				from Criterio
				where nombre like '%" . $search . "%' or descripcion like '%" . $search . "%'";
	}
}
?>
