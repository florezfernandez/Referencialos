<?php
class VotacionDAO{
	protected $idVotacion;
	protected $voto;
	protected $fecha;
	protected $referencia;
	protected $estudiante;

	function __construct($pIdVotacion = "", $pVoto = "", $pFecha = "", $pReferencia = "", $pEstudiante = ""){
		$this -> idVotacion = $pIdVotacion;
		$this -> voto = $pVoto;
		$this -> fecha = $pFecha;
		$this -> referencia = $pReferencia;
		$this -> estudiante = $pEstudiante;
	}

	function insert(){
		return "insert into Votacion(voto, fecha, referencia_idReferencia, estudiante_idEstudiante)
				values('" . $this -> voto . "', '" . $this -> fecha . "', '" . $this -> referencia . "', '" . $this -> estudiante . "')";
	}

	function update(){
		return "update Votacion set 
				voto = '" . $this -> voto . "',
				fecha = '" . $this -> fecha . "',
				referencia_idReferencia = '" . $this -> referencia . "',
				estudiante_idEstudiante = '" . $this -> estudiante . "'	
				where idVotacion = '" . $this -> idVotacion . "'";
	}

	function select() {
		return "select idVotacion, voto, fecha, referencia_idReferencia, estudiante_idEstudiante
				from Votacion
				where idVotacion = '" . $this -> idVotacion . "'";
	}

	function selectAll() {
		return "select idVotacion, voto, fecha, referencia_idReferencia, estudiante_idEstudiante
				from Votacion";
	}

	function selectAllByReferencia() {
		return "select idVotacion, voto, fecha, referencia_idReferencia, estudiante_idEstudiante
				from Votacion
				where referencia_idReferencia = '" . $this -> referencia . "'";
	}

	function selectAllByEstudiante() {
		return "select idVotacion, voto, fecha, referencia_idReferencia, estudiante_idEstudiante
				from Votacion
				where estudiante_idEstudiante = '" . $this -> estudiante . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idVotacion, voto, fecha, referencia_idReferencia, estudiante_idEstudiante
				from Votacion
				order by " . $orden . " " . $dir;
	}

	function selectAllByReferenciaOrder($orden, $dir) {
		return "select idVotacion, voto, fecha, referencia_idReferencia, estudiante_idEstudiante
				from Votacion
				where referencia_idReferencia = '" . $this -> referencia . "'
				order by " . $orden . " " . $dir;
	}

	function selectAllByEstudianteOrder($orden, $dir) {
		return "select idVotacion, voto, fecha, referencia_idReferencia, estudiante_idEstudiante
				from Votacion
				where estudiante_idEstudiante = '" . $this -> estudiante . "'
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idVotacion, voto, fecha, referencia_idReferencia, estudiante_idEstudiante
				from Votacion
				where voto like '%" . $search . "%' or fecha like '%" . $search . "%'";
	}
}
?>
