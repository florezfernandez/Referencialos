<?php
class ProgramaDAO{
	protected $idPrograma;
	protected $nombre;
	protected $facultad;

	function __construct($pIdPrograma = "", $pNombre = "", $pFacultad = ""){
		$this -> idPrograma = $pIdPrograma;
		$this -> nombre = $pNombre;
		$this -> facultad = $pFacultad;
	}

	function insert(){
		return "insert into Programa(nombre, facultad_idFacultad)
				values('" . $this -> nombre . "', '" . $this -> facultad . "')";
	}

	function update(){
		return "update Programa set 
				nombre = '" . $this -> nombre . "',
				facultad_idFacultad = '" . $this -> facultad . "'	
				where idPrograma = '" . $this -> idPrograma . "'";
	}

	function select() {
		return "select idPrograma, nombre, facultad_idFacultad
				from Programa
				where idPrograma = '" . $this -> idPrograma . "'";
	}

	function selectAll() {
		return "select idPrograma, nombre, facultad_idFacultad
				from Programa";
	}

	function selectAllByFacultad() {
		return "select idPrograma, nombre, facultad_idFacultad
				from Programa
				where facultad_idFacultad = '" . $this -> facultad . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idPrograma, nombre, facultad_idFacultad
				from Programa
				order by " . $orden . " " . $dir;
	}

	function selectAllByFacultadOrder($orden, $dir) {
		return "select idPrograma, nombre, facultad_idFacultad
				from Programa
				where facultad_idFacultad = '" . $this -> facultad . "'
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idPrograma, nombre, facultad_idFacultad
				from Programa
				where nombre like '%" . $search . "%'";
	}
}
?>
