<?php
class ReferenciaCriterioDAO{
	private $idReferenciaCriterio;
	private $valor;
	private $referencia;
	private $criterio;

	function __construct($pIdReferenciaCriterio = "", $pValor = "", $pReferencia = "", $pCriterio = ""){
		$this -> idReferenciaCriterio = $pIdReferenciaCriterio;
		$this -> valor = $pValor;
		$this -> referencia = $pReferencia;
		$this -> criterio = $pCriterio;
	}

	function insert(){
		return "insert into ReferenciaCriterio(valor, referencia_idReferencia, criterio_idCriterio)
				values('" . $this -> valor . "', '" . $this -> referencia . "', '" . $this -> criterio . "')";
	}

	function update(){
		return "update ReferenciaCriterio set 
				valor = '" . $this -> valor . "',
				referencia_idReferencia = '" . $this -> referencia . "',
				criterio_idCriterio = '" . $this -> criterio . "'	
				where idReferenciaCriterio = '" . $this -> idReferenciaCriterio . "'";
	}

	function select() {
		return "select idReferenciaCriterio, valor, referencia_idReferencia, criterio_idCriterio
				from ReferenciaCriterio
				where idReferenciaCriterio = '" . $this -> idReferenciaCriterio . "'";
	}

	function selectAll() {
		return "select idReferenciaCriterio, valor, referencia_idReferencia, criterio_idCriterio
				from ReferenciaCriterio";
	}

	function selectAllByReferencia() {
		return "select idReferenciaCriterio, valor, referencia_idReferencia, criterio_idCriterio
				from ReferenciaCriterio
				where referencia_idReferencia = '" . $this -> referencia . "'";
	}

	function selectAllByCriterio() {
		return "select idReferenciaCriterio, valor, referencia_idReferencia, criterio_idCriterio
				from ReferenciaCriterio
				where criterio_idCriterio = '" . $this -> criterio . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idReferenciaCriterio, valor, referencia_idReferencia, criterio_idCriterio
				from ReferenciaCriterio
				order by " . $orden . " " . $dir;
	}

	function selectAllByReferenciaOrder($orden, $dir) {
		return "select idReferenciaCriterio, valor, referencia_idReferencia, criterio_idCriterio
				from ReferenciaCriterio
				where referencia_idReferencia = '" . $this -> referencia . "'
				order by " . $orden . " " . $dir;
	}

	function selectAllByCriterioOrder($orden, $dir) {
		return "select idReferenciaCriterio, valor, referencia_idReferencia, criterio_idCriterio
				from ReferenciaCriterio
				where criterio_idCriterio = '" . $this -> criterio . "'
				order by " . $orden . " " . $dir;
	}
}
?>
