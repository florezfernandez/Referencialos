<?php
class CategoriaDeCriterioDAO{
	private $idCategoriaDeCriterio;
	private $nombre;

	function __construct($pIdCategoriaDeCriterio = "", $pNombre = ""){
		$this -> idCategoriaDeCriterio = $pIdCategoriaDeCriterio;
		$this -> nombre = $pNombre;
	}

	function insert(){
		return "insert into CategoriaDeCriterio(nombre)
				values('" . $this -> nombre . "')";
	}

	function update(){
		return "update CategoriaDeCriterio set 
				nombre = '" . $this -> nombre . "'	
				where idCategoriaDeCriterio = '" . $this -> idCategoriaDeCriterio . "'";
	}

	function select() {
		return "select idCategoriaDeCriterio, nombre
				from CategoriaDeCriterio
				where idCategoriaDeCriterio = '" . $this -> idCategoriaDeCriterio . "'";
	}

	function selectAll() {
		return "select idCategoriaDeCriterio, nombre
				from CategoriaDeCriterio";
	}

	function selectAllOrder($orden, $dir){
		return "select idCategoriaDeCriterio, nombre
				from CategoriaDeCriterio
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idCategoriaDeCriterio, nombre
				from CategoriaDeCriterio
				where nombre like '%" . $search . "%'";
	}
}
?>
