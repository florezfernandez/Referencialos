<?php
class FacultadDAO{
	protected $idFacultad;
	protected $nombre;

	function __construct($pIdFacultad = "", $pNombre = ""){
		$this -> idFacultad = $pIdFacultad;
		$this -> nombre = $pNombre;
	}

	function insert(){
		return "insert into Facultad(nombre)
				values('" . $this -> nombre . "')";
	}

	function update(){
		return "update Facultad set 
				nombre = '" . $this -> nombre . "'	
				where idFacultad = '" . $this -> idFacultad . "'";
	}

	function select() {
		return "select idFacultad, nombre
				from Facultad
				where idFacultad = '" . $this -> idFacultad . "'";
	}

	function selectAll() {
		return "select idFacultad, nombre
				from Facultad";
	}

	function selectAllOrder($orden, $dir){
		return "select idFacultad, nombre
				from Facultad
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idFacultad, nombre
				from Facultad
				where nombre like '%" . $search . "%'";
	}
}
?>
