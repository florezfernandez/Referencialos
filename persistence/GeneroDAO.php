<?php
class GeneroDAO{
	protected $idGenero;
	protected $nombre;

	function __construct($pIdGenero = "", $pNombre = ""){
		$this -> idGenero = $pIdGenero;
		$this -> nombre = $pNombre;
	}

	function insert(){
		return "insert into Genero(nombre)
				values('" . $this -> nombre . "')";
	}

	function update(){
		return "update Genero set 
				nombre = '" . $this -> nombre . "'	
				where idGenero = '" . $this -> idGenero . "'";
	}

	function select() {
		return "select idGenero, nombre
				from Genero
				where idGenero = '" . $this -> idGenero . "'";
	}

	function selectAll() {
		return "select idGenero, nombre
				from Genero";
	}

	function selectAllOrder($orden, $dir){
		return "select idGenero, nombre
				from Genero
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idGenero, nombre
				from Genero
				where nombre like '%" . $search . "%'";
	}
}
?>
