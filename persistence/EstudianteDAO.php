<?php
class EstudianteDAO{
	protected $idEstudiante;
	protected $nombre;
	protected $apellido;
	protected $correo;
	protected $clave;
	protected $foto;
	protected $telefono;
	protected $celular;
	protected $estado;
	protected $periodoDeIngreso;
	protected $fechaDeNacimiento;
	protected $fechaDeRegistro;
	protected $genero;

	function __construct($pIdEstudiante = "", $pNombre = "", $pApellido = "", $pCorreo = "", $pClave = "", $pFoto = "", $pTelefono = "", $pCelular = "", $pEstado = "", $pPeriodoDeIngreso = "", $pFechaDeNacimiento = "", $pFechaDeRegistro = "", $pGenero = ""){
		$this -> idEstudiante = $pIdEstudiante;
		$this -> nombre = $pNombre;
		$this -> apellido = $pApellido;
		$this -> correo = $pCorreo;
		$this -> clave = $pClave;
		$this -> foto = $pFoto;
		$this -> telefono = $pTelefono;
		$this -> celular = $pCelular;
		$this -> estado = $pEstado;
		$this -> periodoDeIngreso = $pPeriodoDeIngreso;
		$this -> fechaDeNacimiento = $pFechaDeNacimiento;
		$this -> fechaDeRegistro = $pFechaDeRegistro;
		$this -> genero = $pGenero;
	}

	function logIn($correo, $clave){
		return "select idEstudiante, nombre, apellido, correo, clave, foto, telefono, celular, estado, periodoDeIngreso, fechaDeNacimiento, fechaDeRegistro, genero_idGenero
				from Estudiante
				where correo = '" . $correo . "' and clave = '" . md5($clave) . "'";
	}

	function insert(){
		return "insert into Estudiante(nombre, apellido, correo, clave, foto, telefono, celular, estado, periodoDeIngreso, fechaDeNacimiento, fechaDeRegistro, genero_idGenero)
				values('" . $this -> nombre . "', '" . $this -> apellido . "', '" . $this -> correo . "', md5('" . $this -> clave . "'), '" . $this -> foto . "', '" . $this -> telefono . "', '" . $this -> celular . "', '" . $this -> estado . "', '" . $this -> periodoDeIngreso . "', '" . $this -> fechaDeNacimiento . "', '" . $this -> fechaDeRegistro . "', '" . $this -> genero . "')";
	}

	function update(){
		return "update Estudiante set 
				nombre = '" . $this -> nombre . "',
				apellido = '" . $this -> apellido . "',
				correo = '" . $this -> correo . "',
				telefono = '" . $this -> telefono . "',
				celular = '" . $this -> celular . "',
				estado = '" . $this -> estado . "',
				periodoDeIngreso = '" . $this -> periodoDeIngreso . "',
				fechaDeNacimiento = '" . $this -> fechaDeNacimiento . "',
				fechaDeRegistro = '" . $this -> fechaDeRegistro . "',
				genero_idGenero = '" . $this -> genero . "'	
				where idEstudiante = '" . $this -> idEstudiante . "'";
	}

	function updateClave($password){
		return "update Estudiante set 
				clave = '" . md5($password) . "'
				where idEstudiante = '" . $this -> idEstudiante . "'";
	}

	function existCorreo($correo){
		return "select idEstudiante, nombre, apellido, correo, clave, foto, telefono, celular, estado, periodoDeIngreso, fechaDeNacimiento, fechaDeRegistro, genero_idGenero
				from Estudiante
				where correo = '" . $correo. "'";
	}

	function recoverPassword($correo, $password){
		return "update Estudiante set 
				clave = '" . md5($password) . "'
				where correo = '" . $correo. "'";
	}

	function updateImageFoto($value){
		return "update Estudiante set 
				foto = '" . $value . "'
				where idEstudiante = '" . $this -> idEstudiante . "'";
	}

	function select() {
		return "select idEstudiante, nombre, apellido, correo, clave, foto, telefono, celular, estado, periodoDeIngreso, fechaDeNacimiento, fechaDeRegistro, genero_idGenero
				from Estudiante
				where idEstudiante = '" . $this -> idEstudiante . "'";
	}

	function selectAll() {
		return "select idEstudiante, nombre, apellido, correo, clave, foto, telefono, celular, estado, periodoDeIngreso, fechaDeNacimiento, fechaDeRegistro, genero_idGenero
				from Estudiante";
	}

	function selectAllByGenero() {
		return "select idEstudiante, nombre, apellido, correo, clave, foto, telefono, celular, estado, periodoDeIngreso, fechaDeNacimiento, fechaDeRegistro, genero_idGenero
				from Estudiante
				where genero_idGenero = '" . $this -> genero . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idEstudiante, nombre, apellido, correo, clave, foto, telefono, celular, estado, periodoDeIngreso, fechaDeNacimiento, fechaDeRegistro, genero_idGenero
				from Estudiante
				order by " . $orden . " " . $dir;
	}

	function selectAllByGeneroOrder($orden, $dir) {
		return "select idEstudiante, nombre, apellido, correo, clave, foto, telefono, celular, estado, periodoDeIngreso, fechaDeNacimiento, fechaDeRegistro, genero_idGenero
				from Estudiante
				where genero_idGenero = '" . $this -> genero . "'
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idEstudiante, nombre, apellido, correo, clave, foto, telefono, celular, estado, periodoDeIngreso, fechaDeNacimiento, fechaDeRegistro, genero_idGenero
				from Estudiante
				where nombre like '%" . $search . "%' or apellido like '%" . $search . "%' or correo like '%" . $search . "%' or telefono like '%" . $search . "%' or celular like '%" . $search . "%' or estado like '%" . $search . "%' or periodoDeIngreso like '%" . $search . "%' or fechaDeNacimiento like '%" . $search . "%' or fechaDeRegistro like '%" . $search . "%'";
	}
}
?>
