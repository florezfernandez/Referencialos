<?php
class DenunciaDAO{
	protected $idDenuncia;
	protected $argumento;
	protected $fecha;
	protected $referencia;
	protected $estudiante;
	protected $tipoDeDenuncia;

	function __construct($pIdDenuncia = "", $pArgumento = "", $pFecha = "", $pReferencia = "", $pEstudiante = "", $pTipoDeDenuncia = ""){
		$this -> idDenuncia = $pIdDenuncia;
		$this -> argumento = $pArgumento;
		$this -> fecha = $pFecha;
		$this -> referencia = $pReferencia;
		$this -> estudiante = $pEstudiante;
		$this -> tipoDeDenuncia = $pTipoDeDenuncia;
	}

	function insert(){
		return "insert into Denuncia(argumento, fecha, referencia_idReferencia, estudiante_idEstudiante, tipoDeDenuncia_idTipoDeDenuncia)
				values('" . $this -> argumento . "', '" . $this -> fecha . "', '" . $this -> referencia . "', '" . $this -> estudiante . "', '" . $this -> tipoDeDenuncia . "')";
	}

	function update(){
		return "update Denuncia set 
				argumento = '" . $this -> argumento . "',
				fecha = '" . $this -> fecha . "',
				referencia_idReferencia = '" . $this -> referencia . "',
				estudiante_idEstudiante = '" . $this -> estudiante . "',
				tipoDeDenuncia_idTipoDeDenuncia = '" . $this -> tipoDeDenuncia . "'	
				where idDenuncia = '" . $this -> idDenuncia . "'";
	}

	function select() {
		return "select idDenuncia, argumento, fecha, referencia_idReferencia, estudiante_idEstudiante, tipoDeDenuncia_idTipoDeDenuncia
				from Denuncia
				where idDenuncia = '" . $this -> idDenuncia . "'";
	}

	function selectAll() {
		return "select idDenuncia, argumento, fecha, referencia_idReferencia, estudiante_idEstudiante, tipoDeDenuncia_idTipoDeDenuncia
				from Denuncia";
	}

	function selectAllByReferencia() {
		return "select idDenuncia, argumento, fecha, referencia_idReferencia, estudiante_idEstudiante, tipoDeDenuncia_idTipoDeDenuncia
				from Denuncia
				where referencia_idReferencia = '" . $this -> referencia . "'";
	}

	function selectAllByEstudiante() {
		return "select idDenuncia, argumento, fecha, referencia_idReferencia, estudiante_idEstudiante, tipoDeDenuncia_idTipoDeDenuncia
				from Denuncia
				where estudiante_idEstudiante = '" . $this -> estudiante . "'";
	}

	function selectAllByTipoDeDenuncia() {
		return "select idDenuncia, argumento, fecha, referencia_idReferencia, estudiante_idEstudiante, tipoDeDenuncia_idTipoDeDenuncia
				from Denuncia
				where tipoDeDenuncia_idTipoDeDenuncia = '" . $this -> tipoDeDenuncia . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idDenuncia, argumento, fecha, referencia_idReferencia, estudiante_idEstudiante, tipoDeDenuncia_idTipoDeDenuncia
				from Denuncia
				order by " . $orden . " " . $dir;
	}

	function selectAllByReferenciaOrder($orden, $dir) {
		return "select idDenuncia, argumento, fecha, referencia_idReferencia, estudiante_idEstudiante, tipoDeDenuncia_idTipoDeDenuncia
				from Denuncia
				where referencia_idReferencia = '" . $this -> referencia . "'
				order by " . $orden . " " . $dir;
	}

	function selectAllByEstudianteOrder($orden, $dir) {
		return "select idDenuncia, argumento, fecha, referencia_idReferencia, estudiante_idEstudiante, tipoDeDenuncia_idTipoDeDenuncia
				from Denuncia
				where estudiante_idEstudiante = '" . $this -> estudiante . "'
				order by " . $orden . " " . $dir;
	}

	function selectAllByTipoDeDenunciaOrder($orden, $dir) {
		return "select idDenuncia, argumento, fecha, referencia_idReferencia, estudiante_idEstudiante, tipoDeDenuncia_idTipoDeDenuncia
				from Denuncia
				where tipoDeDenuncia_idTipoDeDenuncia = '" . $this -> tipoDeDenuncia . "'
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idDenuncia, argumento, fecha, referencia_idReferencia, estudiante_idEstudiante, tipoDeDenuncia_idTipoDeDenuncia
				from Denuncia
				where argumento like '%" . $search . "%' or fecha like '%" . $search . "%'";
	}
}
?>
