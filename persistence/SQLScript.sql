CREATE TABLE Administrador (
	idAdministrador int(11) NOT NULL AUTO_INCREMENT,
	nombre varchar(45) NOT NULL,
	apellido varchar(45) NOT NULL,
	correo varchar(45) NOT NULL,
	clave varchar(45) NOT NULL,
	foto varchar(45) DEFAULT NULL,
	telefono varchar(45) DEFAULT NULL,
	celular varchar(45) DEFAULT NULL,
	estado tinyint NOT NULL,
	PRIMARY KEY (idAdministrador)
);

INSERT INTO Administrador(idAdministrador, nombre, apellido, correo, clave, telefono, celular, estado) VALUES 
	('1', 'Admin', 'Admin', 'admin@udistrital.edu.co', md5('123'), '123', '123', '1'); 

CREATE TABLE LogAdministrador (
	idLogAdministrador int(11) NOT NULL AUTO_INCREMENT,
	accion varchar(100) NOT NULL,
	informacion text NOT NULL,
	fecha date NOT NULL,
	hora time NOT NULL,
	ip varchar(45) NOT NULL,
	so varchar(45) NOT NULL,
	explorador varchar(45) NOT NULL,
	administrador_idAdministrador int(11) NOT NULL,
	PRIMARY KEY (idLogAdministrador)
);

CREATE TABLE LogEstudiante (
	idLogEstudiante int(11) NOT NULL AUTO_INCREMENT,
	accion varchar(100) NOT NULL,
	informacion text NOT NULL,
	fecha date NOT NULL,
	hora time NOT NULL,
	ip varchar(45) NOT NULL,
	so varchar(45) NOT NULL,
	explorador varchar(45) NOT NULL,
	estudiante_idEstudiante int(11) NOT NULL,
	PRIMARY KEY (idLogEstudiante)
);

CREATE TABLE Estudiante (
	idEstudiante int(11) NOT NULL AUTO_INCREMENT,
	nombre varchar(45) NOT NULL,
	apellido varchar(45) NOT NULL,
	correo varchar(45) NOT NULL,
	clave varchar(45) NOT NULL,
	foto varchar(45) DEFAULT NULL,
	telefono varchar(45) DEFAULT NULL,
	celular varchar(45) DEFAULT NULL,
	estado tinyint NOT NULL,
	periodoDeIngreso varchar(45) NOT NULL,
	fechaDeNacimiento date NOT NULL,
	fechaDeRegistro date NOT NULL,
	genero_idGenero int(11) NOT NULL,
	PRIMARY KEY (idEstudiante)
);

CREATE TABLE Genero (
	idGenero int(11) NOT NULL AUTO_INCREMENT,
	nombre varchar(45) NOT NULL,
	PRIMARY KEY (idGenero)
);

CREATE TABLE Facultad (
	idFacultad int(11) NOT NULL AUTO_INCREMENT,
	nombre varchar(100) NOT NULL,
	PRIMARY KEY (idFacultad)
);

CREATE TABLE Programa (
	idPrograma int(11) NOT NULL AUTO_INCREMENT,
	nombre varchar(100) NOT NULL,
	facultad_idFacultad int(11) NOT NULL,
	PRIMARY KEY (idPrograma)
);

CREATE TABLE Curso (
	idCurso int(11) NOT NULL AUTO_INCREMENT,
	nombre varchar(100) NOT NULL,
	PRIMARY KEY (idCurso)
);

CREATE TABLE Profesor (
	idProfesor int(11) NOT NULL AUTO_INCREMENT,
	nombre varchar(100) NOT NULL,
	correo varchar(100) NOT NULL,
	genero_idGenero int(11) NOT NULL,
	PRIMARY KEY (idProfesor)
);

CREATE TABLE RangoDeNota (
	idRangoDeNota int(11) NOT NULL AUTO_INCREMENT,
	valor varchar(45) DEFAULT NULL,
	PRIMARY KEY (idRangoDeNota)
);

CREATE TABLE Referencia (
	idReferencia int(11) NOT NULL AUTO_INCREMENT,
	comentario text NOT NULL,
	pros text DEFAULT NULL,
	contras text DEFAULT NULL,
	periodo varchar(45) NOT NULL,
	fecha date NOT NULL,
	estado tinyint NOT NULL,
	profesor_idProfesor int(11) NOT NULL,
	curso_idCurso int(11) NOT NULL,
	estudiante_idEstudiante int(11) NOT NULL,
	programa_idPrograma int(11) NOT NULL,
	rangoDeNota_idRangoDeNota int(11) NOT NULL,
	PRIMARY KEY (idReferencia)
);

CREATE TABLE Criterio (
	idCriterio int(11) NOT NULL AUTO_INCREMENT,
	nombre varchar(45) NOT NULL,
	descripcion varchar(200) NOT NULL,
	categoriaDeCriterio_idCategoriaDeCriterio int(11) NOT NULL,
	PRIMARY KEY (idCriterio)
);

CREATE TABLE CategoriaDeCriterio (
	idCategoriaDeCriterio int(11) NOT NULL AUTO_INCREMENT,
	nombre varchar(45) NOT NULL,
	PRIMARY KEY (idCategoriaDeCriterio)
);

CREATE TABLE ReferenciaCriterio (
	idReferenciaCriterio int(11) NOT NULL AUTO_INCREMENT,
	valor int NOT NULL,
	referencia_idReferencia int(11) NOT NULL,
	criterio_idCriterio int(11) NOT NULL,
	PRIMARY KEY (idReferenciaCriterio)
);

CREATE TABLE Votacion (
	idVotacion int(11) NOT NULL AUTO_INCREMENT,
	voto int NOT NULL,
	fecha date NOT NULL,
	referencia_idReferencia int(11) NOT NULL,
	estudiante_idEstudiante int(11) NOT NULL,
	PRIMARY KEY (idVotacion)
);

CREATE TABLE TipoDeDenuncia (
	idTipoDeDenuncia int(11) NOT NULL AUTO_INCREMENT,
	nombre varchar(45) NOT NULL,
	PRIMARY KEY (idTipoDeDenuncia)
);

CREATE TABLE Denuncia (
	idDenuncia int(11) NOT NULL AUTO_INCREMENT,
	argumento text NOT NULL,
	fecha date NOT NULL,
	referencia_idReferencia int(11) NOT NULL,
	estudiante_idEstudiante int(11) NOT NULL,
	tipoDeDenuncia_idTipoDeDenuncia int(11) NOT NULL,
	PRIMARY KEY (idDenuncia)
);

ALTER TABLE LogAdministrador
 	ADD FOREIGN KEY (administrador_idAdministrador) REFERENCES Administrador (idAdministrador); 

ALTER TABLE LogEstudiante
 	ADD FOREIGN KEY (estudiante_idEstudiante) REFERENCES Estudiante (idEstudiante); 

ALTER TABLE Estudiante
 	ADD FOREIGN KEY (genero_idGenero) REFERENCES Genero (idGenero); 

ALTER TABLE Programa
 	ADD FOREIGN KEY (facultad_idFacultad) REFERENCES Facultad (idFacultad); 

ALTER TABLE Profesor
 	ADD FOREIGN KEY (genero_idGenero) REFERENCES Genero (idGenero); 

ALTER TABLE Referencia
 	ADD FOREIGN KEY (profesor_idProfesor) REFERENCES Profesor (idProfesor); 

ALTER TABLE Referencia
 	ADD FOREIGN KEY (curso_idCurso) REFERENCES Curso (idCurso); 

ALTER TABLE Referencia
 	ADD FOREIGN KEY (estudiante_idEstudiante) REFERENCES Estudiante (idEstudiante); 

ALTER TABLE Referencia
 	ADD FOREIGN KEY (programa_idPrograma) REFERENCES Programa (idPrograma); 

ALTER TABLE Referencia
 	ADD FOREIGN KEY (rangodenota_idRangoDeNota) REFERENCES RangoDeNota (idRangoDeNota); 

ALTER TABLE Criterio
 	ADD FOREIGN KEY (categoriadecriterio_idCategoriaDeCriterio) REFERENCES CategoriaDeCriterio (idCategoriaDeCriterio); 

ALTER TABLE ReferenciaCriterio
 	ADD FOREIGN KEY (referencia_idReferencia) REFERENCES Referencia (idReferencia); 

ALTER TABLE ReferenciaCriterio
 	ADD FOREIGN KEY (criterio_idCriterio) REFERENCES Criterio (idCriterio); 

ALTER TABLE Votacion
 	ADD FOREIGN KEY (referencia_idReferencia) REFERENCES Referencia (idReferencia); 

ALTER TABLE Votacion
 	ADD FOREIGN KEY (estudiante_idEstudiante) REFERENCES Estudiante (idEstudiante); 

ALTER TABLE Denuncia
 	ADD FOREIGN KEY (referencia_idReferencia) REFERENCES Referencia (idReferencia); 

ALTER TABLE Denuncia
 	ADD FOREIGN KEY (estudiante_idEstudiante) REFERENCES Estudiante (idEstudiante); 

ALTER TABLE Denuncia
 	ADD FOREIGN KEY (tipodedenuncia_idTipoDeDenuncia) REFERENCES TipoDeDenuncia (idTipoDeDenuncia); 

