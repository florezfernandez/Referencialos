<?php
require("business/Administrador.php");
require("business/LogAdministrador.php");
require("business/LogEstudiante.php");
require("business/Estudiante.php");
require("business/Genero.php");
require("business/Facultad.php");
require("business/Programa.php");
require("business/Curso.php");
require("business/Profesor.php");
require("business/RangoDeNota.php");
require("business/Referencia.php");
require("business/Criterio.php");
require("business/CategoriaDeCriterio.php");
require("business/ReferenciaCriterio.php");
require("business/Votacion.php");
require("business/TipoDeDenuncia.php");
require("business/Denuncia.php");
require_once("persistence/Connection.php");
$idEstudiante = $_GET ['idEstudiante'];
$estudiante = new Estudiante($idEstudiante);
$estudiante -> select();
?>
<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	}); 
</script>
<div class="modal-header">
	<h4 class="modal-title">Estudiante</h4>
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
	<table class="table table-striped table-hover">
		<tr>
			<th>Nombre</th>
			<td><?php echo $estudiante -> getNombre() ?></td>
		</tr>
		<tr>
			<th>Apellido</th>
			<td><?php echo $estudiante -> getApellido() ?></td>
		</tr>
		<tr>
			<th>Correo</th>
			<td><?php echo $estudiante -> getCorreo() ?></td>
		</tr>
		<tr>
			<th>Foto</th>
				<td><img class="rounded" src="<?php echo $estudiante -> getFoto() ?>" height="300px" /></td>
		</tr>
		<tr>
			<th>Telefono</th>
			<td><?php echo $estudiante -> getTelefono() ?></td>
		</tr>
		<tr>
			<th>Celular</th>
			<td><?php echo $estudiante -> getCelular() ?></td>
		</tr>
		<tr>
			<th>Estado</th>
			<td><?php echo ($estudiante -> getEstado()==1?"Habilitado":"Deshabilitado") ?> </td>
		</tr>
		<tr>
			<th>Periodo De Ingreso</th>
			<td><?php echo $estudiante -> getPeriodoDeIngreso() ?></td>
		</tr>
		<tr>
			<th>Fecha De Nacimiento</th>
			<td><?php echo $estudiante -> getFechaDeNacimiento() ?></td>
		</tr>
		<tr>
			<th>Fecha De Registro</th>
			<td><?php echo $estudiante -> getFechaDeRegistro() ?></td>
		</tr>
		<tr>
			<th>Genero</th>
			<td><?php echo $estudiante -> getGenero() -> getNombre() ?></td>
		</tr>
	</table>
</div>
