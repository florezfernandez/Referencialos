<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Accion</th>
			<th nowrap>Fecha</th>
			<th nowrap>Hora</th>
			<th nowrap>Ip</th>
			<th nowrap>So</th>
			<th nowrap>Explorador</th>
			<th>Estudiante</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$logEstudiante = new LogEstudiante();
		$searchText = $_GET['search'];
		$logEstudiantes = $logEstudiante -> search($searchText);
		$counter = 1;
		foreach ($logEstudiantes as $currentLogEstudiante) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentLogEstudiante -> getAccion(), $searchText);
			if($pos !== false){
				$text = substr($currentLogEstudiante -> getAccion(), 0, $pos) . "<strong>" . substr($currentLogEstudiante -> getAccion(), $pos, strlen($searchText)) . "</strong>" . substr($currentLogEstudiante -> getAccion(), $pos + strlen($searchText));
			} else {
				$text = $currentLogEstudiante -> getAccion();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentLogEstudiante -> getFecha(), $searchText);
			if($pos !== false){
				$text = substr($currentLogEstudiante -> getFecha(), 0, $pos) . "<strong>" . substr($currentLogEstudiante -> getFecha(), $pos, strlen($searchText)) . "</strong>" . substr($currentLogEstudiante -> getFecha(), $pos + strlen($searchText));
			} else {
				$text = $currentLogEstudiante -> getFecha();
			}
			echo "<td nowrap>" . $text . "</td>";
			$pos = stripos($currentLogEstudiante -> getHora(), $searchText);
			if($pos !== false){
				$text = substr($currentLogEstudiante -> getHora(), 0, $pos) . "<strong>" . substr($currentLogEstudiante -> getHora(), $pos, strlen($searchText)) . "</strong>" . substr($currentLogEstudiante -> getHora(), $pos + strlen($searchText));
			} else {
				$text = $currentLogEstudiante -> getHora();
			}
			echo "<td nowrap>" . $text . "</td>";
			$pos = stripos($currentLogEstudiante -> getIp(), $searchText);
			if($pos !== false){
				$text = substr($currentLogEstudiante -> getIp(), 0, $pos) . "<strong>" . substr($currentLogEstudiante -> getIp(), $pos, strlen($searchText)) . "</strong>" . substr($currentLogEstudiante -> getIp(), $pos + strlen($searchText));
			} else {
				$text = $currentLogEstudiante -> getIp();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentLogEstudiante -> getSo(), $searchText);
			if($pos !== false){
				$text = substr($currentLogEstudiante -> getSo(), 0, $pos) . "<strong>" . substr($currentLogEstudiante -> getSo(), $pos, strlen($searchText)) . "</strong>" . substr($currentLogEstudiante -> getSo(), $pos + strlen($searchText));
			} else {
				$text = $currentLogEstudiante -> getSo();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentLogEstudiante -> getExplorador(), $searchText);
			if($pos !== false){
				$text = substr($currentLogEstudiante -> getExplorador(), 0, $pos) . "<strong>" . substr($currentLogEstudiante -> getExplorador(), $pos, strlen($searchText)) . "</strong>" . substr($currentLogEstudiante -> getExplorador(), $pos + strlen($searchText));
			} else {
				$text = $currentLogEstudiante -> getExplorador();
			}
			echo "<td>" . $text . "</td>";
			echo "<td>" . $currentLogEstudiante -> getEstudiante() -> getNombre() . " " . $currentLogEstudiante -> getEstudiante() -> getApellido() . "</td>";
			echo "<td class='text-right' nowrap>
				<a href='modalLogEstudiante.php?idLogEstudiante=" . $currentLogEstudiante -> getIdLogEstudiante() . "'  data-toggle='modal' data-target='#modalLogEstudiante' >
					<span class='fas fa-eye' data-toggle='tooltip' data-placement='left' data-original-title='Ver mas información' ></span>
				</a>
				</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
<div class="modal fade" id="modalLogEstudiante" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
