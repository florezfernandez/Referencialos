<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Nombre</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$genero = new Genero();
		$searchText = $_GET['search'];
		$generos = $genero -> search($searchText);
		$counter = 1;
		foreach ($generos as $currentGenero) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentGenero -> getNombre(), $searchText);
			if($pos !== false){
				$text = substr($currentGenero -> getNombre(), 0, $pos) . "<strong>" . substr($currentGenero -> getNombre(), $pos, strlen($searchText)) . "</strong>" . substr($currentGenero -> getNombre(), $pos + strlen($searchText));
			} else {
				$text = $currentGenero -> getNombre();
			}
			echo "<td>" . $text . "</td>";
			echo "<td class='text-right' nowrap>";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/genero/updateGenero.php") . "&idGenero=" . $currentGenero -> getIdGenero() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Genero' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") . "&idGenero=" . $currentGenero -> getIdGenero() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Estudiante' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/estudiante/insertEstudiante.php") . "&idGenero=" . $currentGenero -> getIdGenero() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Estudiante' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/profesor/selectAllProfesorByGenero.php") . "&idGenero=" . $currentGenero -> getIdGenero() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Profesor' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/profesor/insertProfesor.php") . "&idGenero=" . $currentGenero -> getIdGenero() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Profesor' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
