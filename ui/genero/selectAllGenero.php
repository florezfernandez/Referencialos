<?php
$order = "";
if(isset($_GET['order'])){
	$order = $_GET['order'];
}
$dir = "";
if(isset($_GET['dir'])){
	$dir = $_GET['dir'];
}
?>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Consultar Genero</h4>
		</div>
		<div class="card-body">
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr><th></th>
						<th nowrap>Nombre 
						<?php if($order=="nombre" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a href='index.php?pid=<?php echo base64_encode("ui/genero/selectAllGenero.php") ?>&order=nombre&dir=asc'>
							<span class='fas fa-sort-amount-up' data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' ></span></a>
						<?php } ?>
						<?php if($order=="nombre" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a href='?pid=<?php echo base64_encode("ui/genero/selectAllGenero.php") ?>&order=nombre&dir=desc'>
							<span class='fas fa-sort-amount-down' data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' ></span></a>
						<?php } ?>
						</th>
						<th nowrap></th>
					</tr>
				</thead>
				</tbody>
					<?php
					$genero = new Genero();
					if($order != "" && $dir != "") {
						$generos = $genero -> selectAllOrder($order, $dir);
					} else {
						$generos = $genero -> selectAll();
					}
					$counter = 1;
					foreach ($generos as $currentGenero) {
						echo "<tr><td>" . $counter . "</td>";
						echo "<td>" . $currentGenero -> getNombre() . "</td>";
						echo "<td class='text-right' nowrap>";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/genero/updateGenero.php") . "&idGenero=" . $currentGenero -> getIdGenero() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Genero' ></span></a> ";
						}
						echo "<a href='?pid=" . base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") . "&idGenero=" . $currentGenero -> getIdGenero() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Estudiante' ></span></a> ";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/estudiante/insertEstudiante.php") . "&idGenero=" . $currentGenero -> getIdGenero() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Estudiante' ></span></a> ";
						}
						echo "<a href='?pid=" . base64_encode("ui/profesor/selectAllProfesorByGenero.php") . "&idGenero=" . $currentGenero -> getIdGenero() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Profesor' ></span></a> ";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/profesor/insertProfesor.php") . "&idGenero=" . $currentGenero -> getIdGenero() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Profesor' ></span></a> ";
						}
						echo "</td>";
						echo "</tr>";
						$counter++;
					}
					?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalGenero" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
