<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Nombre</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$facultad = new Facultad();
		$searchText = $_GET['search'];
		$facultads = $facultad -> search($searchText);
		$counter = 1;
		foreach ($facultads as $currentFacultad) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentFacultad -> getNombre(), $searchText);
			if($pos !== false){
				$text = substr($currentFacultad -> getNombre(), 0, $pos) . "<strong>" . substr($currentFacultad -> getNombre(), $pos, strlen($searchText)) . "</strong>" . substr($currentFacultad -> getNombre(), $pos + strlen($searchText));
			} else {
				$text = $currentFacultad -> getNombre();
			}
			echo "<td>" . $text . "</td>";
			echo "<td class='text-right' nowrap>";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/facultad/updateFacultad.php") . "&idFacultad=" . $currentFacultad -> getIdFacultad() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Facultad' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/programa/selectAllProgramaByFacultad.php") . "&idFacultad=" . $currentFacultad -> getIdFacultad() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Programa' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/programa/insertPrograma.php") . "&idFacultad=" . $currentFacultad -> getIdFacultad() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Programa' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
