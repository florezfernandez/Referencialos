<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Nombre</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$curso = new Curso();
		$searchText = $_GET['search'];
		$cursos = $curso -> search($searchText);
		$counter = 1;
		foreach ($cursos as $currentCurso) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentCurso -> getNombre(), $searchText);
			if($pos !== false){
				$text = substr($currentCurso -> getNombre(), 0, $pos) . "<strong>" . substr($currentCurso -> getNombre(), $pos, strlen($searchText)) . "</strong>" . substr($currentCurso -> getNombre(), $pos + strlen($searchText));
			} else {
				$text = $currentCurso -> getNombre();
			}
			echo "<td>" . $text . "</td>";
			echo "<td class='text-right' nowrap>";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/curso/updateCurso.php") . "&idCurso=" . $currentCurso -> getIdCurso() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Curso' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/referencia/selectAllReferenciaByCurso.php") . "&idCurso=" . $currentCurso -> getIdCurso() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Referencia' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/referencia/insertReferencia.php") . "&idCurso=" . $currentCurso -> getIdCurso() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Referencia' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
