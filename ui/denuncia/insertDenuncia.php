<?php
$processed=false;
$argumento="";
if(isset($_POST['argumento'])){
	$argumento=$_POST['argumento'];
}
$fecha=date("d/m/Y");
if(isset($_POST['fecha'])){
	$fecha=$_POST['fecha'];
}
$referencia="";
if(isset($_POST['referencia'])){
	$referencia=$_POST['referencia'];
}
if(isset($_GET['idReferencia'])){
	$referencia=$_GET['idReferencia'];
}
$estudiante="";
if(isset($_POST['estudiante'])){
	$estudiante=$_POST['estudiante'];
}
if(isset($_GET['idEstudiante'])){
	$estudiante=$_GET['idEstudiante'];
}
$tipoDeDenuncia="";
if(isset($_POST['tipoDeDenuncia'])){
	$tipoDeDenuncia=$_POST['tipoDeDenuncia'];
}
if(isset($_GET['idTipoDeDenuncia'])){
	$tipoDeDenuncia=$_GET['idTipoDeDenuncia'];
}
if(isset($_POST['insert'])){
	$newDenuncia = new Denuncia("", $argumento, $fecha, $referencia, $estudiante, $tipoDeDenuncia);
	$newDenuncia -> insert();
	$objReferencia = new Referencia($referencia);
	$objReferencia -> select();
	$nameReferencia = $objReferencia -> getComentario() ;
	$objEstudiante = new Estudiante($estudiante);
	$objEstudiante -> select();
	$nameEstudiante = $objEstudiante -> getNombre() . " " . $objEstudiante -> getApellido() ;
	$objTipoDeDenuncia = new TipoDeDenuncia($tipoDeDenuncia);
	$objTipoDeDenuncia -> select();
	$nameTipoDeDenuncia = $objTipoDeDenuncia -> getNombre() ;
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	if($_SESSION['entity'] == 'Administrador'){
		$logAdministrador = new LogAdministrador("","Crear Denuncia", "Argumento: " . $argumento . "; Fecha: " . $fecha . "; Referencia: " . $nameReferencia . "; Estudiante: " . $nameEstudiante . "; Tipo De Denuncia: " . $nameTipoDeDenuncia, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrador -> insert();
	}
	else if($_SESSION['entity'] == 'Estudiante'){
		$logEstudiante = new LogEstudiante("","Crear Denuncia", "Argumento: " . $argumento . "; Fecha: " . $fecha . "; Referencia: " . $nameReferencia . "; Estudiante: " . $nameEstudiante . "; Tipo De Denuncia: " . $nameTipoDeDenuncia, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logEstudiante -> insert();
	}
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Crear Denuncia</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					<div class="alert alert-success" >Datos Ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("ui/denuncia/insertDenuncia.php") ?>" class="bootstrap-form needs-validation"   >
						<div class="form-group">
							<label>Argumento*</label>
							<textarea id="argumento" name="argumento" ><?php echo $argumento ?></textarea>
							<script>
								$('#argumento').summernote({
									tabsize: 2,
									height: 100
								});
							</script>
						</div>
						<div class="form-group">
							<label>Fecha*</label>
							<input type="date" class="form-control" name="fecha" id="fecha" value="<?php echo $fecha ?>" autocomplete="off" required />
						</div>
						<div class="form-group">
							<label>Referencia*</label>
							<select class="form-control" name="referencia" id="referencia" data-placeholder="Seleccione Referencia" required >
								<option></option>
								<?php
								$objReferencia = new Referencia();
								$referencias = $objReferencia -> selectAllOrder("comentario", "asc");
								foreach($referencias as $currentReferencia){
									echo "<option value='" . $currentReferencia -> getIdReferencia() . "'";
									if($currentReferencia -> getIdReferencia() == $referencia){
										echo " selected";
									}
									echo ">" . $currentReferencia -> getComentario() . "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Estudiante*</label>
							<select class="form-control" name="estudiante" id="estudiante" data-placeholder="Seleccione Estudiante" required >
								<option></option>
								<?php
								$objEstudiante = new Estudiante();
								$estudiantes = $objEstudiante -> selectAllOrder("nombre", "asc");
								foreach($estudiantes as $currentEstudiante){
									echo "<option value='" . $currentEstudiante -> getIdEstudiante() . "'";
									if($currentEstudiante -> getIdEstudiante() == $estudiante){
										echo " selected";
									}
									echo ">" . $currentEstudiante -> getNombre() . " " . $currentEstudiante -> getApellido() . "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Tipo De Denuncia*</label>
							<select class="form-control" name="tipoDeDenuncia" id="tipoDeDenuncia" data-placeholder="Seleccione Tipo De Denuncia" required >
								<option></option>
								<?php
								$objTipoDeDenuncia = new TipoDeDenuncia();
								$tipoDeDenuncias = $objTipoDeDenuncia -> selectAllOrder("nombre", "asc");
								foreach($tipoDeDenuncias as $currentTipoDeDenuncia){
									echo "<option value='" . $currentTipoDeDenuncia -> getIdTipoDeDenuncia() . "'";
									if($currentTipoDeDenuncia -> getIdTipoDeDenuncia() == $tipoDeDenuncia){
										echo " selected";
									}
									echo ">" . $currentTipoDeDenuncia -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<button type="submit" class="btn btn-info" name="insert">Crear</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('#referencia').select2({});
$('#estudiante').select2({});
$('#tipoDeDenuncia').select2({});
</script>
