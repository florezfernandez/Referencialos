<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Argumento</th>
			<th nowrap>Fecha</th>
			<th>Referencia</th>
			<th>Estudiante</th>
			<th>Tipo De Denuncia</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$denuncia = new Denuncia();
		$searchText = $_GET['search'];
		$denuncias = $denuncia -> search($searchText);
		$counter = 1;
		foreach ($denuncias as $currentDenuncia) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentDenuncia -> getArgumento(), $searchText);
			if($pos !== false){
				$text = substr($currentDenuncia -> getArgumento(), 0, $pos) . "<strong>" . substr($currentDenuncia -> getArgumento(), $pos, strlen($searchText)) . "</strong>" . substr($currentDenuncia -> getArgumento(), $pos + strlen($searchText));
			} else {
				$text = $currentDenuncia -> getArgumento();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentDenuncia -> getFecha(), $searchText);
			if($pos !== false){
				$text = substr($currentDenuncia -> getFecha(), 0, $pos) . "<strong>" . substr($currentDenuncia -> getFecha(), $pos, strlen($searchText)) . "</strong>" . substr($currentDenuncia -> getFecha(), $pos + strlen($searchText));
			} else {
				$text = $currentDenuncia -> getFecha();
			}
			echo "<td>" . $text . "</td>";
			echo "<td>" . $currentDenuncia -> getReferencia() -> getComentario() . "</td>";
			echo "<td>" . $currentDenuncia -> getEstudiante() -> getNombre() . " " . $currentDenuncia -> getEstudiante() -> getApellido() . "</td>";
			echo "<td>" . $currentDenuncia -> getTipoDeDenuncia() -> getNombre() . "</td>";
			echo "<td class='text-right' nowrap>";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/denuncia/updateDenuncia.php") . "&idDenuncia=" . $currentDenuncia -> getIdDenuncia() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Denuncia' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
