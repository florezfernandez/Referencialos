<?php
$order = "";
if(isset($_GET['order'])){
	$order = $_GET['order'];
}
$dir = "";
if(isset($_GET['dir'])){
	$dir = $_GET['dir'];
}
$estudiante = new Estudiante($_GET['idEstudiante']); 
$estudiante -> select();
?>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Consultar Denuncia de Estudiante: <em><?php echo $estudiante -> getNombre() . " " . $estudiante -> getApellido() ?></em></h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr><th></th>
						<th nowrap>Argumento 
						<?php if($order=="argumento" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/denuncia/selectAllDenunciaByEstudiante.php") ?>&idEstudiante=<?php echo $_GET['idEstudiante'] ?>&order=argumento&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="argumento" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/denuncia/selectAllDenunciaByEstudiante.php") ?>&idEstudiante=<?php echo $_GET['idEstudiante'] ?>&order=argumento&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th nowrap>Fecha 
						<?php if($order=="fecha" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/denuncia/selectAllDenunciaByEstudiante.php") ?>&idEstudiante=<?php echo $_GET['idEstudiante'] ?>&order=fecha&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="fecha" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/denuncia/selectAllDenunciaByEstudiante.php") ?>&idEstudiante=<?php echo $_GET['idEstudiante'] ?>&order=fecha&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th>Referencia</th>
						<th>Estudiante</th>
						<th>Tipo De Denuncia</th>
						<th nowrap></th>
					</tr>
				</thead>
				</tbody>
					<?php
					$denuncia = new Denuncia("", "", "", "", $_GET['idEstudiante'], "");
					if($order!="" && $dir!="") {
						$denuncias = $denuncia -> selectAllByEstudianteOrder($order, $dir);
					} else {
						$denuncias = $denuncia -> selectAllByEstudiante();
					}
					$counter = 1;
					foreach ($denuncias as $currentDenuncia) {
						echo "<tr><td>" . $counter . "</td>";
						echo "<td>" . $currentDenuncia -> getArgumento() . "</td>";
						echo "<td>" . $currentDenuncia -> getFecha() . "</td>";
						echo "<td><a href='modalReferencia.php?idReferencia=" . $currentDenuncia -> getReferencia() -> getIdReferencia() . "' data-toggle='modal' data-target='#modalDenuncia' >" . $currentDenuncia -> getReferencia() -> getComentario() . "</a></td>";
						echo "<td><a href='modalEstudiante.php?idEstudiante=" . $currentDenuncia -> getEstudiante() -> getIdEstudiante() . "' data-toggle='modal' data-target='#modalDenuncia' >" . $currentDenuncia -> getEstudiante() -> getNombre() . " " . $currentDenuncia -> getEstudiante() -> getApellido() . "</a></td>";
						echo "<td><a href='modalTipoDeDenuncia.php?idTipoDeDenuncia=" . $currentDenuncia -> getTipoDeDenuncia() -> getIdTipoDeDenuncia() . "' data-toggle='modal' data-target='#modalDenuncia' >" . $currentDenuncia -> getTipoDeDenuncia() -> getNombre() . "</a></td>";
						echo "<td class='text-right' nowrap>";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/denuncia/updateDenuncia.php") . "&idDenuncia=" . $currentDenuncia -> getIdDenuncia() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Denuncia' ></span></a> ";
						}
						echo "</td>";
						echo "</tr>";
						$counter++;
					};
					?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalDenuncia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
