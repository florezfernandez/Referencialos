<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Accion</th>
			<th nowrap>Fecha</th>
			<th nowrap>Hora</th>
			<th nowrap>Ip</th>
			<th nowrap>So</th>
			<th nowrap>Explorador</th>
			<th>Administrador</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$logAdministrador = new LogAdministrador();
		$searchText = $_GET['search'];
		$logAdministradors = $logAdministrador -> search($searchText);
		$counter = 1;
		foreach ($logAdministradors as $currentLogAdministrador) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentLogAdministrador -> getAccion(), $searchText);
			if($pos !== false){
				$text = substr($currentLogAdministrador -> getAccion(), 0, $pos) . "<strong>" . substr($currentLogAdministrador -> getAccion(), $pos, strlen($searchText)) . "</strong>" . substr($currentLogAdministrador -> getAccion(), $pos + strlen($searchText));
			} else {
				$text = $currentLogAdministrador -> getAccion();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentLogAdministrador -> getFecha(), $searchText);
			if($pos !== false){
				$text = substr($currentLogAdministrador -> getFecha(), 0, $pos) . "<strong>" . substr($currentLogAdministrador -> getFecha(), $pos, strlen($searchText)) . "</strong>" . substr($currentLogAdministrador -> getFecha(), $pos + strlen($searchText));
			} else {
				$text = $currentLogAdministrador -> getFecha();
			}
			echo "<td nowrap>" . $text . "</td>";
			$pos = stripos($currentLogAdministrador -> getHora(), $searchText);
			if($pos !== false){
				$text = substr($currentLogAdministrador -> getHora(), 0, $pos) . "<strong>" . substr($currentLogAdministrador -> getHora(), $pos, strlen($searchText)) . "</strong>" . substr($currentLogAdministrador -> getHora(), $pos + strlen($searchText));
			} else {
				$text = $currentLogAdministrador -> getHora();
			}
			echo "<td nowrap>" . $text . "</td>";
			$pos = stripos($currentLogAdministrador -> getIp(), $searchText);
			if($pos !== false){
				$text = substr($currentLogAdministrador -> getIp(), 0, $pos) . "<strong>" . substr($currentLogAdministrador -> getIp(), $pos, strlen($searchText)) . "</strong>" . substr($currentLogAdministrador -> getIp(), $pos + strlen($searchText));
			} else {
				$text = $currentLogAdministrador -> getIp();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentLogAdministrador -> getSo(), $searchText);
			if($pos !== false){
				$text = substr($currentLogAdministrador -> getSo(), 0, $pos) . "<strong>" . substr($currentLogAdministrador -> getSo(), $pos, strlen($searchText)) . "</strong>" . substr($currentLogAdministrador -> getSo(), $pos + strlen($searchText));
			} else {
				$text = $currentLogAdministrador -> getSo();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentLogAdministrador -> getExplorador(), $searchText);
			if($pos !== false){
				$text = substr($currentLogAdministrador -> getExplorador(), 0, $pos) . "<strong>" . substr($currentLogAdministrador -> getExplorador(), $pos, strlen($searchText)) . "</strong>" . substr($currentLogAdministrador -> getExplorador(), $pos + strlen($searchText));
			} else {
				$text = $currentLogAdministrador -> getExplorador();
			}
			echo "<td>" . $text . "</td>";
			echo "<td>" . $currentLogAdministrador -> getAdministrador() -> getNombre() . " " . $currentLogAdministrador -> getAdministrador() -> getApellido() . "</td>";
			echo "<td class='text-right' nowrap>
				<a href='modalLogAdministrador.php?idLogAdministrador=" . $currentLogAdministrador -> getIdLogAdministrador() . "'  data-toggle='modal' data-target='#modalLogAdministrador' >
					<span class='fas fa-eye' data-toggle='tooltip' data-placement='left' data-original-title='Ver mas información' ></span>
				</a>
				</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
<div class="modal fade" id="modalLogAdministrador" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
