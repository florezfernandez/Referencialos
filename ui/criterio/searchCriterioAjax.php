<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Nombre</th>
			<th nowrap>Descripcion</th>
			<th>Categoria De Criterio</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$criterio = new Criterio();
		$searchText = $_GET['search'];
		$criterios = $criterio -> search($searchText);
		$counter = 1;
		foreach ($criterios as $currentCriterio) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentCriterio -> getNombre(), $searchText);
			if($pos !== false){
				$text = substr($currentCriterio -> getNombre(), 0, $pos) . "<strong>" . substr($currentCriterio -> getNombre(), $pos, strlen($searchText)) . "</strong>" . substr($currentCriterio -> getNombre(), $pos + strlen($searchText));
			} else {
				$text = $currentCriterio -> getNombre();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentCriterio -> getDescripcion(), $searchText);
			if($pos !== false){
				$text = substr($currentCriterio -> getDescripcion(), 0, $pos) . "<strong>" . substr($currentCriterio -> getDescripcion(), $pos, strlen($searchText)) . "</strong>" . substr($currentCriterio -> getDescripcion(), $pos + strlen($searchText));
			} else {
				$text = $currentCriterio -> getDescripcion();
			}
			echo "<td>" . $text . "</td>";
			echo "<td>" . $currentCriterio -> getCategoriaDeCriterio() -> getNombre() . "</td>";
			echo "<td class='text-right' nowrap>";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/criterio/updateCriterio.php") . "&idCriterio=" . $currentCriterio -> getIdCriterio() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Criterio' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/referenciaCriterio/selectAllReferenciaCriterioByCriterio.php") . "&idCriterio=" . $currentCriterio -> getIdCriterio() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Referencia Criterio' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/referenciaCriterio/insertReferenciaCriterio.php") . "&idCriterio=" . $currentCriterio -> getIdCriterio() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Referencia Criterio' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
