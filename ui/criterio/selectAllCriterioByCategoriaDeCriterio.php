<?php
$order = "";
if(isset($_GET['order'])){
	$order = $_GET['order'];
}
$dir = "";
if(isset($_GET['dir'])){
	$dir = $_GET['dir'];
}
$categoriaDeCriterio = new CategoriaDeCriterio($_GET['idCategoriaDeCriterio']); 
$categoriaDeCriterio -> select();
?>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Consultar Criterio de Categoria De Criterio: <em><?php echo $categoriaDeCriterio -> getNombre() ?></em></h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr><th></th>
						<th nowrap>Nombre 
						<?php if($order=="nombre" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/criterio/selectAllCriterioByCategoriaDeCriterio.php") ?>&idCategoriaDeCriterio=<?php echo $_GET['idCategoriaDeCriterio'] ?>&order=nombre&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="nombre" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/criterio/selectAllCriterioByCategoriaDeCriterio.php") ?>&idCategoriaDeCriterio=<?php echo $_GET['idCategoriaDeCriterio'] ?>&order=nombre&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th nowrap>Descripcion 
						<?php if($order=="descripcion" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/criterio/selectAllCriterioByCategoriaDeCriterio.php") ?>&idCategoriaDeCriterio=<?php echo $_GET['idCategoriaDeCriterio'] ?>&order=descripcion&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="descripcion" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/criterio/selectAllCriterioByCategoriaDeCriterio.php") ?>&idCategoriaDeCriterio=<?php echo $_GET['idCategoriaDeCriterio'] ?>&order=descripcion&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th>Categoria De Criterio</th>
						<th nowrap></th>
					</tr>
				</thead>
				</tbody>
					<?php
					$criterio = new Criterio("", "", "", $_GET['idCategoriaDeCriterio']);
					if($order!="" && $dir!="") {
						$criterios = $criterio -> selectAllByCategoriaDeCriterioOrder($order, $dir);
					} else {
						$criterios = $criterio -> selectAllByCategoriaDeCriterio();
					}
					$counter = 1;
					foreach ($criterios as $currentCriterio) {
						echo "<tr><td>" . $counter . "</td>";
						echo "<td>" . $currentCriterio -> getNombre() . "</td>";
						echo "<td>" . $currentCriterio -> getDescripcion() . "</td>";
						echo "<td><a href='modalCategoriaDeCriterio.php?idCategoriaDeCriterio=" . $currentCriterio -> getCategoriaDeCriterio() -> getIdCategoriaDeCriterio() . "' data-toggle='modal' data-target='#modalCriterio' >" . $currentCriterio -> getCategoriaDeCriterio() -> getNombre() . "</a></td>";
						echo "<td class='text-right' nowrap>";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/criterio/updateCriterio.php") . "&idCriterio=" . $currentCriterio -> getIdCriterio() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Criterio' ></span></a> ";
						}
						echo "<a href='?pid=" . base64_encode("ui/referenciaCriterio/selectAllReferenciaCriterioByCriterio.php") . "&idCriterio=" . $currentCriterio -> getIdCriterio() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Referencia Criterio' ></span></a> ";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/referenciaCriterio/insertReferenciaCriterio.php") . "&idCriterio=" . $currentCriterio -> getIdCriterio() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Referencia Criterio' ></span></a> ";
						}
						echo "</td>";
						echo "</tr>";
						$counter++;
					};
					?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalCriterio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
