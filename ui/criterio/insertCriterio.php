<?php
$processed=false;
$nombre="";
if(isset($_POST['nombre'])){
	$nombre=$_POST['nombre'];
}
$descripcion="";
if(isset($_POST['descripcion'])){
	$descripcion=$_POST['descripcion'];
}
$categoriaDeCriterio="";
if(isset($_POST['categoriaDeCriterio'])){
	$categoriaDeCriterio=$_POST['categoriaDeCriterio'];
}
if(isset($_GET['idCategoriaDeCriterio'])){
	$categoriaDeCriterio=$_GET['idCategoriaDeCriterio'];
}
if(isset($_POST['insert'])){
	$newCriterio = new Criterio("", $nombre, $descripcion, $categoriaDeCriterio);
	$newCriterio -> insert();
	$objCategoriaDeCriterio = new CategoriaDeCriterio($categoriaDeCriterio);
	$objCategoriaDeCriterio -> select();
	$nameCategoriaDeCriterio = $objCategoriaDeCriterio -> getNombre() ;
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	if($_SESSION['entity'] == 'Administrador'){
		$logAdministrador = new LogAdministrador("","Crear Criterio", "Nombre: " . $nombre . "; Descripcion: " . $descripcion . "; Categoria De Criterio: " . $nameCategoriaDeCriterio, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrador -> insert();
	}
	else if($_SESSION['entity'] == 'Estudiante'){
		$logEstudiante = new LogEstudiante("","Crear Criterio", "Nombre: " . $nombre . "; Descripcion: " . $descripcion . "; Categoria De Criterio: " . $nameCategoriaDeCriterio, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logEstudiante -> insert();
	}
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Crear Criterio</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					<div class="alert alert-success" >Datos Ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("ui/criterio/insertCriterio.php") ?>" class="bootstrap-form needs-validation"   >
						<div class="form-group">
							<label>Nombre*</label>
							<input type="text" class="form-control" name="nombre" value="<?php echo $nombre ?>" required />
						</div>
						<div class="form-group">
							<label>Descripcion*</label>
							<input type="text" class="form-control" name="descripcion" value="<?php echo $descripcion ?>" required />
						</div>
						<div class="form-group">
							<label>Categoria De Criterio*</label>
							<select class="form-control" name="categoriaDeCriterio" id="categoriaDeCriterio" data-placeholder="Seleccione Categoria De Criterio" required >
								<option></option>
								<?php
								$objCategoriaDeCriterio = new CategoriaDeCriterio();
								$categoriaDeCriterios = $objCategoriaDeCriterio -> selectAllOrder("nombre", "asc");
								foreach($categoriaDeCriterios as $currentCategoriaDeCriterio){
									echo "<option value='" . $currentCategoriaDeCriterio -> getIdCategoriaDeCriterio() . "'";
									if($currentCategoriaDeCriterio -> getIdCategoriaDeCriterio() == $categoriaDeCriterio){
										echo " selected";
									}
									echo ">" . $currentCategoriaDeCriterio -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<button type="submit" class="btn btn-info" name="insert">Crear</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('#categoriaDeCriterio').select2({});
</script>
