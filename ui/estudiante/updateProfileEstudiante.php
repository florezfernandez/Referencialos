<?php
$processed=false;
$updateEstudiante = new Estudiante($_SESSION['id']);
$updateEstudiante -> select();
$nombre="";
if(isset($_POST['nombre'])){
	$nombre=$_POST['nombre'];
}
$apellido="";
if(isset($_POST['apellido'])){
	$apellido=$_POST['apellido'];
}
$correo="";
if(isset($_POST['correo'])){
	$correo=$_POST['correo'];
}
$telefono="";
if(isset($_POST['telefono'])){
	$telefono=$_POST['telefono'];
}
$celular="";
if(isset($_POST['celular'])){
	$celular=$_POST['celular'];
}
$periodoDeIngreso="";
if(isset($_POST['periodoDeIngreso'])){
	$periodoDeIngreso=$_POST['periodoDeIngreso'];
}
$fechaDeNacimiento=date("Y-m-d");
if(isset($_POST['fechaDeNacimiento'])){
	$fechaDeNacimiento=$_POST['fechaDeNacimiento'];
}
$fechaDeRegistro=date("Y-m-d");
if(isset($_POST['fechaDeRegistro'])){
	$fechaDeRegistro=$_POST['fechaDeRegistro'];
}
$genero="";
if(isset($_POST['genero'])){
	$genero=$_POST['genero'];
}
if(isset($_POST['update'])){
	$updateEstudiante = new Estudiante($_SESSION['id'], $nombre, $apellido, $correo, "", "", $telefono, $celular, "1", $periodoDeIngreso, $fechaDeNacimiento, $fechaDeRegistro, $genero);
	$updateEstudiante -> update();
	$updateEstudiante -> select();
	$objGenero = new Genero($genero);
	$objGenero -> select();
	$nameGenero = $objGenero -> getNombre() ;
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	$logEstudiante = new LogEstudiante("","Editar Profile Estudiante", "Nombre: " . $nombre . "; Apellido: " . $apellido . "; Correo: " . $correo . "; Telefono: " . $telefono . "; Celular: " . $celular . "; Periodo De Ingreso: " . $periodoDeIngreso . "; Fecha De Nacimiento: " . $fechaDeNacimiento . "; Fecha De Registro: " . $fechaDeRegistro . "; Genero: " . $nameGenero , date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
	$logEstudiante -> insert();
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Editar Profile Estudiante</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					<div class="alert alert-success" >Datos Editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("ui/estudiante/updateProfileEstudiante.php") ?>" class="bootstrap-form needs-validation"   >
						<div class="form-group">
							<label>Nombre*</label>
							<input type="text" class="form-control" name="nombre" value="<?php echo $updateEstudiante -> getNombre() ?>" required />
						</div>
						<div class="form-group">
							<label>Apellido*</label>
							<input type="text" class="form-control" name="apellido" value="<?php echo $updateEstudiante -> getApellido() ?>" required />
						</div>
						<input type="hidden" name="correo" value="<?php echo $updateEstudiante -> getCorreo() ?>" />						
						<div class="form-group">
							<label>Telefono</label>
							<input type="text" class="form-control" name="telefono" value="<?php echo $updateEstudiante -> getTelefono() ?>"/>
						</div>
						<div class="form-group">
							<label>Celular</label>
							<input type="text" class="form-control" name="celular" value="<?php echo $updateEstudiante -> getCelular() ?>"/>
						</div>
						<div class="form-group">
							<label>Periodo De Ingreso*</label>
							<input type="text" class="form-control" name="periodoDeIngreso" value="<?php echo $updateEstudiante -> getPeriodoDeIngreso() ?>" required />
						</div>
						<div class="form-group">
							<label>Fecha De Nacimiento*</label>
							<input type="text" class="form-control" name="fechaDeNacimiento" id="fechaDeNacimiento" value="<?php echo $updateEstudiante -> getFechaDeNacimiento() ?>" autocomplete="off" />
							<script>
								$( "#fechaDeNacimiento" ).datepicker({
									uiLibrary: 'bootstrap4',
									format: 'yyyy-mm-dd'
								});
							</script>
						</div>
						<input type="hidden" name="fechaDeRegistro" value="<?php echo $updateEstudiante -> getFechaDeRegistro() ?>" />
					<div class="form-group">
						<label>Genero*</label>
						<select class="form-control" name="genero">
							<?php
							$objGenero = new Genero();
							$generos = $objGenero -> selectAll();
							foreach($generos as $currentGenero){
								echo "<option value='" . $currentGenero -> getIdGenero() . "'";
								if($currentGenero -> getIdGenero() == $updateEstudiante -> getGenero() -> getIdGenero()){
									echo " selected";
								}
								echo ">" . $currentGenero -> getNombre() . "</option>";
							}
							?>
						</select>
					</div>
						<button type="submit" class="btn btn-info" name="update">Editar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
