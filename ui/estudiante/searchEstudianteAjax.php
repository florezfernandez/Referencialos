<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Nombre</th>
			<th nowrap>Apellido</th>
			<th nowrap>Correo</th>
			<th nowrap>Telefono</th>
			<th nowrap>Celular</th>
			<th nowrap>Estado</th>
			<th nowrap>Periodo De Ingreso</th>
			<th nowrap>Fecha De Nacimiento</th>
			<th nowrap>Fecha De Registro</th>
			<th>Genero</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$estudiante = new Estudiante();
		$searchText = $_GET['search'];
		$estudiantes = $estudiante -> search($searchText);
		$counter = 1;
		foreach ($estudiantes as $currentEstudiante) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentEstudiante -> getNombre(), $searchText);
			if($pos !== false){
				$text = substr($currentEstudiante -> getNombre(), 0, $pos) . "<strong>" . substr($currentEstudiante -> getNombre(), $pos, strlen($searchText)) . "</strong>" . substr($currentEstudiante -> getNombre(), $pos + strlen($searchText));
			} else {
				$text = $currentEstudiante -> getNombre();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentEstudiante -> getApellido(), $searchText);
			if($pos !== false){
				$text = substr($currentEstudiante -> getApellido(), 0, $pos) . "<strong>" . substr($currentEstudiante -> getApellido(), $pos, strlen($searchText)) . "</strong>" . substr($currentEstudiante -> getApellido(), $pos + strlen($searchText));
			} else {
				$text = $currentEstudiante -> getApellido();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentEstudiante -> getCorreo(), $searchText);
			if($pos !== false){
				$text = substr($currentEstudiante -> getCorreo(), 0, $pos) . "<strong>" . substr($currentEstudiante -> getCorreo(), $pos, strlen($searchText)) . "</strong>" . substr($currentEstudiante -> getCorreo(), $pos + strlen($searchText));
			} else {
				$text = $currentEstudiante -> getCorreo();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentEstudiante -> getTelefono(), $searchText);
			if($pos !== false){
				$text = substr($currentEstudiante -> getTelefono(), 0, $pos) . "<strong>" . substr($currentEstudiante -> getTelefono(), $pos, strlen($searchText)) . "</strong>" . substr($currentEstudiante -> getTelefono(), $pos + strlen($searchText));
			} else {
				$text = $currentEstudiante -> getTelefono();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentEstudiante -> getCelular(), $searchText);
			if($pos !== false){
				$text = substr($currentEstudiante -> getCelular(), 0, $pos) . "<strong>" . substr($currentEstudiante -> getCelular(), $pos, strlen($searchText)) . "</strong>" . substr($currentEstudiante -> getCelular(), $pos + strlen($searchText));
			} else {
				$text = $currentEstudiante -> getCelular();
			}
			echo "<td>" . $text . "</td>";
						echo "<td>" . ($currentEstudiante -> getEstado()==1?"Habilitado":"Deshabilitado") . "</td>";
			$pos = stripos($currentEstudiante -> getPeriodoDeIngreso(), $searchText);
			if($pos !== false){
				$text = substr($currentEstudiante -> getPeriodoDeIngreso(), 0, $pos) . "<strong>" . substr($currentEstudiante -> getPeriodoDeIngreso(), $pos, strlen($searchText)) . "</strong>" . substr($currentEstudiante -> getPeriodoDeIngreso(), $pos + strlen($searchText));
			} else {
				$text = $currentEstudiante -> getPeriodoDeIngreso();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentEstudiante -> getFechaDeNacimiento(), $searchText);
			if($pos !== false){
				$text = substr($currentEstudiante -> getFechaDeNacimiento(), 0, $pos) . "<strong>" . substr($currentEstudiante -> getFechaDeNacimiento(), $pos, strlen($searchText)) . "</strong>" . substr($currentEstudiante -> getFechaDeNacimiento(), $pos + strlen($searchText));
			} else {
				$text = $currentEstudiante -> getFechaDeNacimiento();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentEstudiante -> getFechaDeRegistro(), $searchText);
			if($pos !== false){
				$text = substr($currentEstudiante -> getFechaDeRegistro(), 0, $pos) . "<strong>" . substr($currentEstudiante -> getFechaDeRegistro(), $pos, strlen($searchText)) . "</strong>" . substr($currentEstudiante -> getFechaDeRegistro(), $pos + strlen($searchText));
			} else {
				$text = $currentEstudiante -> getFechaDeRegistro();
			}
			echo "<td>" . $text . "</td>";
			echo "<td>" . $currentEstudiante -> getGenero() -> getNombre() . "</td>";
			echo "<td class='text-right' nowrap>";
			echo "<a href='modalEstudiante.php?idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'  data-toggle='modal' data-target='#modalEstudiante' ><span class='fas fa-eye' data-toggle='tooltip' data-placement='left' data-original-title='Ver mas información' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/estudiante/updateEstudiante.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Estudiante' ></span></a> ";
				echo "<a href='?pid=" . base64_encode("ui/estudiante/updateFotoEstudiante.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "&attribute=foto'><span class='fas fa-camera' data-toggle='tooltip' data-placement='left' data-original-title='Editar foto'></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/referencia/selectAllReferenciaByEstudiante.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Referencia' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/referencia/insertReferencia.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Referencia' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/denuncia/selectAllDenunciaByEstudiante.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Denuncia' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/denuncia/insertDenuncia.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Denuncia' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/votacion/selectAllVotacionByEstudiante.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Votacion' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/votacion/insertVotacion.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Votacion' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
<div class="modal fade" id="modalEstudiante" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
