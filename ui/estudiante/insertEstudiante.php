<?php
$processed=false;
$nombre="";
if(isset($_POST['nombre'])){
	$nombre=$_POST['nombre'];
}
$apellido="";
if(isset($_POST['apellido'])){
	$apellido=$_POST['apellido'];
}
$correo="";
if(isset($_POST['correo'])){
	$correo=$_POST['correo'];
}
$clave="";
if(isset($_POST['clave'])){
	$clave=$_POST['clave'];
}
$telefono="";
if(isset($_POST['telefono'])){
	$telefono=$_POST['telefono'];
}
$celular="";
if(isset($_POST['celular'])){
	$celular=$_POST['celular'];
}
$estado="";
if(isset($_POST['estado'])){
	$estado=$_POST['estado'];
}
$periodoDeIngreso="";
if(isset($_POST['periodoDeIngreso'])){
	$periodoDeIngreso=$_POST['periodoDeIngreso'];
}
$fechaDeNacimiento=date("d/m/Y");
if(isset($_POST['fechaDeNacimiento'])){
	$fechaDeNacimiento=$_POST['fechaDeNacimiento'];
}
$fechaDeRegistro=date("d/m/Y");
if(isset($_POST['fechaDeRegistro'])){
	$fechaDeRegistro=$_POST['fechaDeRegistro'];
}
$genero="";
if(isset($_POST['genero'])){
	$genero=$_POST['genero'];
}
if(isset($_GET['idGenero'])){
	$genero=$_GET['idGenero'];
}
if(isset($_POST['insert'])){
	$newEstudiante = new Estudiante("", $nombre, $apellido, $correo, $clave, "", $telefono, $celular, $estado, $periodoDeIngreso, $fechaDeNacimiento, $fechaDeRegistro, $genero);
	$newEstudiante -> insert();
	$objGenero = new Genero($genero);
	$objGenero -> select();
	$nameGenero = $objGenero -> getNombre() ;
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	if($_SESSION['entity'] == 'Administrador'){
		$logAdministrador = new LogAdministrador("","Crear Estudiante", "Nombre: " . $nombre . "; Apellido: " . $apellido . "; Correo: " . $correo . "; Clave: " . $clave . "; Telefono: " . $telefono . "; Celular: " . $celular . "; Estado: " . $estado . "; Periodo De Ingreso: " . $periodoDeIngreso . "; Fecha De Nacimiento: " . $fechaDeNacimiento . "; Fecha De Registro: " . $fechaDeRegistro . "; Genero: " . $nameGenero, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrador -> insert();
	}
	else if($_SESSION['entity'] == 'Estudiante'){
		$logEstudiante = new LogEstudiante("","Crear Estudiante", "Nombre: " . $nombre . "; Apellido: " . $apellido . "; Correo: " . $correo . "; Clave: " . $clave . "; Telefono: " . $telefono . "; Celular: " . $celular . "; Estado: " . $estado . "; Periodo De Ingreso: " . $periodoDeIngreso . "; Fecha De Nacimiento: " . $fechaDeNacimiento . "; Fecha De Registro: " . $fechaDeRegistro . "; Genero: " . $nameGenero, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logEstudiante -> insert();
	}
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Crear Estudiante</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					<div class="alert alert-success" >Datos Ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("ui/estudiante/insertEstudiante.php") ?>" class="bootstrap-form needs-validation"   >
						<div class="form-group">
							<label>Nombre*</label>
							<input type="text" class="form-control" name="nombre" value="<?php echo $nombre ?>" required />
						</div>
						<div class="form-group">
							<label>Apellido*</label>
							<input type="text" class="form-control" name="apellido" value="<?php echo $apellido ?>" required />
						</div>
						<div class="form-group">
							<label>Correo*</label>
							<input type="email" class="form-control" name="correo" value="<?php echo $correo ?>"  required />
						</div>
						<div class="form-group">
							<label>Clave*</label>
							<input type="password" class="form-control" name="clave" value="<?php echo $clave ?>" required />
						</div>
						<div class="form-group">
							<label>Telefono</label>
							<input type="text" class="form-control" name="telefono" value="<?php echo $telefono ?>"/>
						</div>
						<div class="form-group">
							<label>Celular</label>
							<input type="text" class="form-control" name="celular" value="<?php echo $celular ?>"/>
						</div>
						<div class="form-group">
							<label>Estado*</label>
							<div class="form-check">
								<input type="radio" class="form-check-input" name="estado" value="1" checked />
								<label class="form-check-label">Habilitado</label>
							</div>
							<div class="form-check form-check-inline">
								<input type="radio" class="form-check-input" name="estado" value="0" />
								<label class="form-check-label" >Deshabilitado</label>
							</div>
						</div>
						<div class="form-group">
							<label>Periodo De Ingreso*</label>
							<input type="text" class="form-control" name="periodoDeIngreso" value="<?php echo $periodoDeIngreso ?>" required />
						</div>
						<div class="form-group">
							<label>Fecha De Nacimiento*</label>
							<input type="date" class="form-control" name="fechaDeNacimiento" id="fechaDeNacimiento" value="<?php echo $fechaDeNacimiento ?>" autocomplete="off" required />
						</div>
						<div class="form-group">
							<label>Fecha De Registro*</label>
							<input type="date" class="form-control" name="fechaDeRegistro" id="fechaDeRegistro" value="<?php echo $fechaDeRegistro ?>" autocomplete="off" required />
						</div>
						<div class="form-group">
							<label>Genero*</label>
							<select class="form-control" name="genero" id="genero" data-placeholder="Seleccione Genero" required >
								<option></option>
								<?php
								$objGenero = new Genero();
								$generos = $objGenero -> selectAllOrder("nombre", "asc");
								foreach($generos as $currentGenero){
									echo "<option value='" . $currentGenero -> getIdGenero() . "'";
									if($currentGenero -> getIdGenero() == $genero){
										echo " selected";
									}
									echo ">" . $currentGenero -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<button type="submit" class="btn btn-info" name="insert">Crear</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('#genero').select2({});
</script>
