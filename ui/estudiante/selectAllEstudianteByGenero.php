<?php
$order = "apellido";
if(isset($_GET['order'])){
	$order = $_GET['order'];
}
$dir = "asc";
if(isset($_GET['dir'])){
	$dir = $_GET['dir'];
}
$genero = new Genero($_GET['idGenero']); 
$genero -> select();
?>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Consultar Estudiante de Genero: <em><?php echo $genero -> getNombre() ?></em></h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr><th></th>
						<th nowrap>Nombre 
						<?php if($order=="nombre" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=nombre&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="nombre" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=nombre&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th nowrap>Apellido 
						<?php if($order=="apellido" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=apellido&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="apellido" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=apellido&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th nowrap>Correo 
						<?php if($order=="correo" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=correo&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="correo" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=correo&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th nowrap>Telefono 
						<?php if($order=="telefono" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=telefono&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="telefono" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=telefono&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th nowrap>Celular 
						<?php if($order=="celular" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=celular&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="celular" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=celular&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th nowrap>Estado 
						<?php if($order=="estado" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=estado&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="estado" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=estado&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th nowrap>Periodo De Ingreso 
						<?php if($order=="periodoDeIngreso" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=periodoDeIngreso&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="periodoDeIngreso" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=periodoDeIngreso&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th nowrap>Fecha De Nacimiento 
						<?php if($order=="fechaDeNacimiento" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=fechaDeNacimiento&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="fechaDeNacimiento" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=fechaDeNacimiento&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th nowrap>Fecha De Registro 
						<?php if($order=="fechaDeRegistro" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=fechaDeRegistro&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="fechaDeRegistro" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudianteByGenero.php") ?>&idGenero=<?php echo $_GET['idGenero'] ?>&order=fechaDeRegistro&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th>Genero</th>
						<th nowrap></th>
					</tr>
				</thead>
				</tbody>
					<?php
					$estudiante = new Estudiante("", "", "", "", "", "", "", "", "", "", "", "", $_GET['idGenero']);
					if($order!="" && $dir!="") {
						$estudiantes = $estudiante -> selectAllByGeneroOrder($order, $dir);
					} else {
						$estudiantes = $estudiante -> selectAllByGenero();
					}
					$counter = 1;
					foreach ($estudiantes as $currentEstudiante) {
						echo "<tr><td>" . $counter . "</td>";
						echo "<td>" . $currentEstudiante -> getNombre() . "</td>";
						echo "<td>" . $currentEstudiante -> getApellido() . "</td>";
						echo "<td>" . $currentEstudiante -> getCorreo() . "</td>";
						echo "<td>" . $currentEstudiante -> getTelefono() . "</td>";
						echo "<td>" . $currentEstudiante -> getCelular() . "</td>";
						echo "<td>" . ($currentEstudiante -> getEstado()==1?"Habilitado":"Deshabilitado") . "</td>";
						echo "<td>" . $currentEstudiante -> getPeriodoDeIngreso() . "</td>";
						echo "<td>" . $currentEstudiante -> getFechaDeNacimiento() . "</td>";
						echo "<td>" . $currentEstudiante -> getFechaDeRegistro() . "</td>";
						echo "<td><a href='modalGenero.php?idGenero=" . $currentEstudiante -> getGenero() -> getIdGenero() . "' data-toggle='modal' data-target='#modalEstudiante' >" . $currentEstudiante -> getGenero() -> getNombre() . "</a></td>";
						echo "<td class='text-right' nowrap>";
						echo "<a href='modalEstudiante.php?idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'  data-toggle='modal' data-target='#modalEstudiante' ><span class='fas fa-eye' data-toggle='tooltip' data-placement='left' data-original-title='Ver mas información' ></span></a> ";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/estudiante/updateEstudiante.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Estudiante' ></span></a> ";
							echo "<a href='?pid=" . base64_encode("ui/estudiante/updateFotoEstudiante.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "&attribute=foto'><span class='fas fa-camera' data-toggle='tooltip' data-placement='left' data-original-title='Editar foto'></span></a> ";
						}
						echo "<a href='?pid=" . base64_encode("ui/referencia/selectAllReferenciaByEstudiante.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Referencia' ></span></a> ";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/referencia/insertReferencia.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Referencia' ></span></a> ";
						}
						echo "<a href='?pid=" . base64_encode("ui/denuncia/selectAllDenunciaByEstudiante.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Denuncia' ></span></a> ";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/denuncia/insertDenuncia.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Denuncia' ></span></a> ";
						}
						echo "<a href='?pid=" . base64_encode("ui/votacion/selectAllVotacionByEstudiante.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Votacion' ></span></a> ";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/votacion/insertVotacion.php") . "&idEstudiante=" . $currentEstudiante -> getIdEstudiante() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Votacion' ></span></a> ";
						}
						echo "</td>";
						echo "</tr>";
						$counter++;
					};
					?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalEstudiante" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
