<?php
$processed=false;
$idEstudiante = $_GET['idEstudiante'];
$updateEstudiante = new Estudiante($idEstudiante);
$updateEstudiante -> select();
$nombre="";
if(isset($_POST['nombre'])){
	$nombre=$_POST['nombre'];
}
$apellido="";
if(isset($_POST['apellido'])){
	$apellido=$_POST['apellido'];
}
$correo="";
if(isset($_POST['correo'])){
	$correo=$_POST['correo'];
}
$telefono="";
if(isset($_POST['telefono'])){
	$telefono=$_POST['telefono'];
}
$celular="";
if(isset($_POST['celular'])){
	$celular=$_POST['celular'];
}
$estado="";
if(isset($_POST['estado'])){
	$estado=$_POST['estado'];
}
$periodoDeIngreso="";
if(isset($_POST['periodoDeIngreso'])){
	$periodoDeIngreso=$_POST['periodoDeIngreso'];
}
$fechaDeNacimiento=date("d/m/Y");
if(isset($_POST['fechaDeNacimiento'])){
	$fechaDeNacimiento=$_POST['fechaDeNacimiento'];
}
$fechaDeRegistro=date("d/m/Y");
if(isset($_POST['fechaDeRegistro'])){
	$fechaDeRegistro=$_POST['fechaDeRegistro'];
}
$genero="";
if(isset($_POST['genero'])){
	$genero=$_POST['genero'];
}
if(isset($_POST['update'])){
	$updateEstudiante = new Estudiante($idEstudiante, $nombre, $apellido, $correo, "", "", $telefono, $celular, $estado, $periodoDeIngreso, $fechaDeNacimiento, $fechaDeRegistro, $genero);
	$updateEstudiante -> update();
	$updateEstudiante -> select();
	$objGenero = new Genero($genero);
	$objGenero -> select();
	$nameGenero = $objGenero -> getNombre() ;
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	if($_SESSION['entity'] == 'Administrador'){
		$logAdministrador = new LogAdministrador("","Editar Estudiante", "Nombre: " . $nombre . "; Apellido: " . $apellido . "; Correo: " . $correo . "; Telefono: " . $telefono . "; Celular: " . $celular . "; Estado: " . $estado . "; Periodo De Ingreso: " . $periodoDeIngreso . "; Fecha De Nacimiento: " . $fechaDeNacimiento . "; Fecha De Registro: " . $fechaDeRegistro . "; Genero: " . $nameGenero , date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrador -> insert();
	}
	else if($_SESSION['entity'] == 'Estudiante'){
		$logEstudiante = new LogEstudiante("","Editar Estudiante", "Nombre: " . $nombre . "; Apellido: " . $apellido . "; Correo: " . $correo . "; Telefono: " . $telefono . "; Celular: " . $celular . "; Estado: " . $estado . "; Periodo De Ingreso: " . $periodoDeIngreso . "; Fecha De Nacimiento: " . $fechaDeNacimiento . "; Fecha De Registro: " . $fechaDeRegistro . "; Genero: " . $nameGenero , date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logEstudiante -> insert();
	}
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Editar Estudiante</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					<div class="alert alert-success" >Datos Editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("ui/estudiante/updateEstudiante.php") . "&idEstudiante=" . $idEstudiante ?>" class="bootstrap-form needs-validation"   >
						<div class="form-group">
							<label>Nombre*</label>
							<input type="text" class="form-control" name="nombre" value="<?php echo $updateEstudiante -> getNombre() ?>" required />
						</div>
						<div class="form-group">
							<label>Apellido*</label>
							<input type="text" class="form-control" name="apellido" value="<?php echo $updateEstudiante -> getApellido() ?>" required />
						</div>
						<div class="form-group">
							<label>Correo*</label>
							<input type="email" class="form-control" name="correo" value="<?php echo $updateEstudiante -> getCorreo() ?>"  required />
						</div>
						<div class="form-group">
							<label>Telefono</label>
							<input type="text" class="form-control" name="telefono" value="<?php echo $updateEstudiante -> getTelefono() ?>"/>
						</div>
						<div class="form-group">
							<label>Celular</label>
							<input type="text" class="form-control" name="celular" value="<?php echo $updateEstudiante -> getCelular() ?>"/>
						</div>
						<div class="form-group">
							<label>Estado*</label>
							<div class="form-check">
								<input type="radio" class="form-check-input" name="estado" value="1" <?php echo ($updateEstudiante -> getEstado()==1)?"checked":"" ?>/>
								<label class="form-check-label">Habilitado</label>
							</div>
							<div class="form-check form-check-inline">
								<input type="radio" class="form-check-input" name="estado" value="0" <?php echo ($updateEstudiante -> getEstado()==0)?"checked":"" ?>/>
								<label class="form-check-label" >Deshabilitado</label>
							</div>
						</div>
						<div class="form-group">
							<label>Periodo De Ingreso*</label>
							<input type="text" class="form-control" name="periodoDeIngreso" value="<?php echo $updateEstudiante -> getPeriodoDeIngreso() ?>" required />
						</div>
						<div class="form-group">
							<label>Fecha De Nacimiento*</label>
							<input type="date" class="form-control" name="fechaDeNacimiento" id="fechaDeNacimiento" value="<?php echo $updateEstudiante -> getFechaDeNacimiento() ?>" autocomplete="off" required />
						</div>
						<div class="form-group">
							<label>Fecha De Registro*</label>
							<input type="date" class="form-control" name="fechaDeRegistro" id="fechaDeRegistro" value="<?php echo $updateEstudiante -> getFechaDeRegistro() ?>" autocomplete="off" required />
						</div>
						<div class="form-group">
							<label>Genero*</label>
							<select class="form-control" name="genero" id="genero" data-placeholder="Seleccione Genero" required >
								<option></option>
								<?php
								$objGenero = new Genero();
								$generos = $objGenero -> selectAllOrder("nombre", "asc");
								foreach($generos as $currentGenero){
									echo "<option value='" . $currentGenero -> getIdGenero() . "'";
									if($currentGenero -> getIdGenero() == $updateEstudiante -> getGenero() -> getIdGenero()){
										echo " selected";
									}
									echo ">" . $currentGenero -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<button type="submit" class="btn btn-info" name="update">Editar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('#genero').select2({});
</script>
