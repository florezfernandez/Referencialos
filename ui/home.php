<?php
$logInError=false;
$enabledError=false;
if(isset($_POST['logIn'])){
	if(isset($_POST['email']) && isset($_POST['password'])){
		$user_ip = getenv('REMOTE_ADDR');
		$agent = $_SERVER["HTTP_USER_AGENT"];
		$browser = "-";
		if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
			$browser = "Internet Explorer";
		} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
			$browser = "Chrome";
		} else if (preg_match('/Edge\/\d+/', $agent) ) {
			$browser = "Edge";
		} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
			$browser = "Firefox";
		} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
			$browser = "Opera";
		} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
			$browser = "Safari";
		}
		$email=$_POST['email'];
		$password=$_POST['password'];
		$administrador = new Administrador();
		if($administrador -> logIn($email, $password)){
			if($administrador -> getEstado()==1){
				$_SESSION['id']=$administrador -> getIdAdministrador();
				$_SESSION['entity']="Administrador";
				$logAdministrador = new LogAdministrador("", "Log In", "", date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $administrador -> getIdAdministrador());
				$logAdministrador -> insert();
				echo "<script>location.href = '?pid=" . base64_encode("ui/sessionAdministrador.php") . "'</script>"; 
			} else { 
				$enabledError=true; 
			}
		}
		$estudiante = new Estudiante();
		if($estudiante -> logIn($email, $password)){
			if($estudiante -> getEstado()==1){
				$_SESSION['id']=$estudiante -> getIdEstudiante();
				$_SESSION['entity']="Estudiante";
				$logEstudiante = new LogEstudiante("", "Log In", "", date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $estudiante -> getIdEstudiante());
				$logEstudiante -> insert();
				echo "<script>location.href = '?pid=" . base64_encode("ui/sessionEstudiante.php") . "'</script>"; 
			} else { 
				$enabledError=true; 
			}
		}
		$logInError=true;
	}
}
?>
<div align="center">
	<?php include("ui/header.php"); ?>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-9">
			<div class="card">
				<div class="card-header">
					<h4><strong>Referencialos</strong></h4>
				</div>
				<div class="card-body">
					<p>Referencialos es una red social académica que permite registrar referencias de profesores universitarios con el fin de  tener un instrumento que ofrezca un soporte a los estudiantes para la toma de decisiones en el momento de inscribir cursos</p>
					<p><a class="btn btn-success" href="?pid=<?php echo base64_encode("addons/referencia/searchReferenciaProfesorNoAuth.php") ?>">Ver referencias de profesores</a></p>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card">
				<div class="card-header">
					<h4><strong>Autenticar</strong></h4>
				</div>
				<div class="card-body">
					<form id="form" method="post" action="?" class="bootstrap-form needs-validation"  >
						<div class="form-group">
							<div class="input-group" >
								<input type="email" class="form-control" name="email" placeholder="Correo" autocomplete="off" required />
							</div>
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="password" placeholder="Clave" required />
						</div>
						<?php if($enabledError){
							echo "<div class='alert alert-danger' >Usuario Deshabilitado</div>";
						} else if ($logInError){
							echo "<div class='alert alert-danger' >Error de correo o clave</div>";
						} ?>
						<div class="form-group">
							<button type="submit" class="btn btn-info" name="logIn">Autenticar</button>
						</div>
						<div class="form-group">
							<a href="?pid=<?php echo base64_encode("ui/recoverPassword.php") ?>">Recuperar Clave</a>
						</div>
						<div class="form-group">
							<a href="?pid=<?php echo base64_encode("addons/estudiante/registerEstudiante.php") ?>">Registrar Nuevo Estudiante</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
