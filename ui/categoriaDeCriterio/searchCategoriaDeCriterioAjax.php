<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Nombre</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$categoriaDeCriterio = new CategoriaDeCriterio();
		$searchText = $_GET['search'];
		$categoriaDeCriterios = $categoriaDeCriterio -> search($searchText);
		$counter = 1;
		foreach ($categoriaDeCriterios as $currentCategoriaDeCriterio) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentCategoriaDeCriterio -> getNombre(), $searchText);
			if($pos !== false){
				$text = substr($currentCategoriaDeCriterio -> getNombre(), 0, $pos) . "<strong>" . substr($currentCategoriaDeCriterio -> getNombre(), $pos, strlen($searchText)) . "</strong>" . substr($currentCategoriaDeCriterio -> getNombre(), $pos + strlen($searchText));
			} else {
				$text = $currentCategoriaDeCriterio -> getNombre();
			}
			echo "<td>" . $text . "</td>";
			echo "<td class='text-right' nowrap>";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/categoriaDeCriterio/updateCategoriaDeCriterio.php") . "&idCategoriaDeCriterio=" . $currentCategoriaDeCriterio -> getIdCategoriaDeCriterio() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Categoria De Criterio' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/criterio/selectAllCriterioByCategoriaDeCriterio.php") . "&idCategoriaDeCriterio=" . $currentCategoriaDeCriterio -> getIdCategoriaDeCriterio() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Criterio' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/criterio/insertCriterio.php") . "&idCategoriaDeCriterio=" . $currentCategoriaDeCriterio -> getIdCategoriaDeCriterio() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Criterio' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
