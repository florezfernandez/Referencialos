<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Valor</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$rangoDeNota = new RangoDeNota();
		$searchText = $_GET['search'];
		$rangoDeNotas = $rangoDeNota -> search($searchText);
		$counter = 1;
		foreach ($rangoDeNotas as $currentRangoDeNota) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentRangoDeNota -> getValor(), $searchText);
			if($pos !== false){
				$text = substr($currentRangoDeNota -> getValor(), 0, $pos) . "<strong>" . substr($currentRangoDeNota -> getValor(), $pos, strlen($searchText)) . "</strong>" . substr($currentRangoDeNota -> getValor(), $pos + strlen($searchText));
			} else {
				$text = $currentRangoDeNota -> getValor();
			}
			echo "<td>" . $text . "</td>";
			echo "<td class='text-right' nowrap>";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/rangoDeNota/updateRangoDeNota.php") . "&idRangoDeNota=" . $currentRangoDeNota -> getIdRangoDeNota() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Rango De Nota' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/referencia/selectAllReferenciaByRangoDeNota.php") . "&idRangoDeNota=" . $currentRangoDeNota -> getIdRangoDeNota() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Referencia' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/referencia/insertReferencia.php") . "&idRangoDeNota=" . $currentRangoDeNota -> getIdRangoDeNota() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Referencia' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
