<?php
$order = "";
if(isset($_GET['order'])){
	$order = $_GET['order'];
}
$dir = "";
if(isset($_GET['dir'])){
	$dir = $_GET['dir'];
}
$criterio = new Criterio($_GET['idCriterio']); 
$criterio -> select();
?>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Consultar Referencia Criterio de Criterio: <em><?php echo $criterio -> getNombre() ?></em></h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr><th></th>
						<th nowrap>Valor 
						<?php if($order=="valor" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/referenciaCriterio/selectAllReferenciaCriterioByCriterio.php") ?>&idCriterio=<?php echo $_GET['idCriterio'] ?>&order=valor&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="valor" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/referenciaCriterio/selectAllReferenciaCriterioByCriterio.php") ?>&idCriterio=<?php echo $_GET['idCriterio'] ?>&order=valor&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th>Referencia</th>
						<th>Criterio</th>
						<th nowrap></th>
					</tr>
				</thead>
				</tbody>
					<?php
					$referenciaCriterio = new ReferenciaCriterio("", "", "", $_GET['idCriterio']);
					if($order!="" && $dir!="") {
						$referenciaCriterios = $referenciaCriterio -> selectAllByCriterioOrder($order, $dir);
					} else {
						$referenciaCriterios = $referenciaCriterio -> selectAllByCriterio();
					}
					$counter = 1;
					foreach ($referenciaCriterios as $currentReferenciaCriterio) {
						echo "<tr><td>" . $counter . "</td>";
						echo "<td>" . $currentReferenciaCriterio -> getValor() . "</td>";
						echo "<td><a href='modalReferencia.php?idReferencia=" . $currentReferenciaCriterio -> getReferencia() -> getIdReferencia() . "' data-toggle='modal' data-target='#modalReferenciaCriterio' >" . $currentReferenciaCriterio -> getReferencia() -> getComentario() . "</a></td>";
						echo "<td><a href='modalCriterio.php?idCriterio=" . $currentReferenciaCriterio -> getCriterio() -> getIdCriterio() . "' data-toggle='modal' data-target='#modalReferenciaCriterio' >" . $currentReferenciaCriterio -> getCriterio() -> getNombre() . "</a></td>";
						echo "<td class='text-right' nowrap>";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/referenciaCriterio/updateReferenciaCriterio.php") . "&idReferenciaCriterio=" . $currentReferenciaCriterio -> getIdReferenciaCriterio() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Referencia Criterio' ></span></a> ";
						}
						echo "</td>";
						echo "</tr>";
						$counter++;
					};
					?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalReferenciaCriterio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
