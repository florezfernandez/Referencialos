<?php
$processed=false;
$valor="";
if(isset($_POST['valor'])){
	$valor=$_POST['valor'];
}
$referencia="";
if(isset($_POST['referencia'])){
	$referencia=$_POST['referencia'];
}
if(isset($_GET['idReferencia'])){
	$referencia=$_GET['idReferencia'];
}
$criterio="";
if(isset($_POST['criterio'])){
	$criterio=$_POST['criterio'];
}
if(isset($_GET['idCriterio'])){
	$criterio=$_GET['idCriterio'];
}
if(isset($_POST['insert'])){
	$newReferenciaCriterio = new ReferenciaCriterio("", $valor, $referencia, $criterio);
	$newReferenciaCriterio -> insert();
	$objReferencia = new Referencia($referencia);
	$objReferencia -> select();
	$nameReferencia = $objReferencia -> getComentario() ;
	$objCriterio = new Criterio($criterio);
	$objCriterio -> select();
	$nameCriterio = $objCriterio -> getNombre() ;
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	if($_SESSION['entity'] == 'Administrador'){
		$logAdministrador = new LogAdministrador("","Crear Referencia Criterio", "Valor: " . $valor . "; Referencia: " . $nameReferencia . "; Criterio: " . $nameCriterio, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrador -> insert();
	}
	else if($_SESSION['entity'] == 'Estudiante'){
		$logEstudiante = new LogEstudiante("","Crear Referencia Criterio", "Valor: " . $valor . "; Referencia: " . $nameReferencia . "; Criterio: " . $nameCriterio, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logEstudiante -> insert();
	}
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Crear Referencia Criterio</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					<div class="alert alert-success" >Datos Ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("ui/referenciaCriterio/insertReferenciaCriterio.php") ?>" class="bootstrap-form needs-validation"   >
						<div class="form-group">
							<label>Valor*</label>
							<input type="number" class="form-control" name="valor" value="<?php echo $valor ?>" required />
						</div>
						<div class="form-group">
							<label>Referencia*</label>
							<select class="form-control" name="referencia" id="referencia" data-placeholder="Seleccione Referencia" required >
								<option></option>
								<?php
								$objReferencia = new Referencia();
								$referencias = $objReferencia -> selectAllOrder("comentario", "asc");
								foreach($referencias as $currentReferencia){
									echo "<option value='" . $currentReferencia -> getIdReferencia() . "'";
									if($currentReferencia -> getIdReferencia() == $referencia){
										echo " selected";
									}
									echo ">" . $currentReferencia -> getComentario() . "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Criterio*</label>
							<select class="form-control" name="criterio" id="criterio" data-placeholder="Seleccione Criterio" required >
								<option></option>
								<?php
								$objCriterio = new Criterio();
								$criterios = $objCriterio -> selectAllOrder("nombre", "asc");
								foreach($criterios as $currentCriterio){
									echo "<option value='" . $currentCriterio -> getIdCriterio() . "'";
									if($currentCriterio -> getIdCriterio() == $criterio){
										echo " selected";
									}
									echo ">" . $currentCriterio -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<button type="submit" class="btn btn-info" name="insert">Crear</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('#referencia').select2({});
$('#criterio').select2({});
</script>
