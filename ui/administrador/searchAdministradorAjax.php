<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Nombre</th>
			<th nowrap>Apellido</th>
			<th nowrap>Correo</th>
			<th nowrap>Telefono</th>
			<th nowrap>Celular</th>
			<th nowrap>Estado</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$administrador = new Administrador();
		$searchText = $_GET['search'];
		$administradors = $administrador -> search($searchText);
		$counter = 1;
		foreach ($administradors as $currentAdministrador) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentAdministrador -> getNombre(), $searchText);
			if($pos !== false){
				$text = substr($currentAdministrador -> getNombre(), 0, $pos) . "<strong>" . substr($currentAdministrador -> getNombre(), $pos, strlen($searchText)) . "</strong>" . substr($currentAdministrador -> getNombre(), $pos + strlen($searchText));
			} else {
				$text = $currentAdministrador -> getNombre();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentAdministrador -> getApellido(), $searchText);
			if($pos !== false){
				$text = substr($currentAdministrador -> getApellido(), 0, $pos) . "<strong>" . substr($currentAdministrador -> getApellido(), $pos, strlen($searchText)) . "</strong>" . substr($currentAdministrador -> getApellido(), $pos + strlen($searchText));
			} else {
				$text = $currentAdministrador -> getApellido();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentAdministrador -> getCorreo(), $searchText);
			if($pos !== false){
				$text = substr($currentAdministrador -> getCorreo(), 0, $pos) . "<strong>" . substr($currentAdministrador -> getCorreo(), $pos, strlen($searchText)) . "</strong>" . substr($currentAdministrador -> getCorreo(), $pos + strlen($searchText));
			} else {
				$text = $currentAdministrador -> getCorreo();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentAdministrador -> getTelefono(), $searchText);
			if($pos !== false){
				$text = substr($currentAdministrador -> getTelefono(), 0, $pos) . "<strong>" . substr($currentAdministrador -> getTelefono(), $pos, strlen($searchText)) . "</strong>" . substr($currentAdministrador -> getTelefono(), $pos + strlen($searchText));
			} else {
				$text = $currentAdministrador -> getTelefono();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentAdministrador -> getCelular(), $searchText);
			if($pos !== false){
				$text = substr($currentAdministrador -> getCelular(), 0, $pos) . "<strong>" . substr($currentAdministrador -> getCelular(), $pos, strlen($searchText)) . "</strong>" . substr($currentAdministrador -> getCelular(), $pos + strlen($searchText));
			} else {
				$text = $currentAdministrador -> getCelular();
			}
			echo "<td>" . $text . "</td>";
						echo "<td>" . ($currentAdministrador -> getEstado()==1?"Habilitado":"Deshabilitado") . "</td>";
			echo "<td class='text-right' nowrap>";
			echo "<a href='modalAdministrador.php?idAdministrador=" . $currentAdministrador -> getIdAdministrador() . "'  data-toggle='modal' data-target='#modalAdministrador' ><span class='fas fa-eye' data-toggle='tooltip' data-placement='left' data-original-title='Ver mas información' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/administrador/updateAdministrador.php") . "&idAdministrador=" . $currentAdministrador -> getIdAdministrador() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Administrador' ></span></a> ";
				echo "<a href='?pid=" . base64_encode("ui/administrador/updateFotoAdministrador.php") . "&idAdministrador=" . $currentAdministrador -> getIdAdministrador() . "&attribute=foto'><span class='fas fa-camera' data-toggle='tooltip' data-placement='left' data-original-title='Editar foto'></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
<div class="modal fade" id="modalAdministrador" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
