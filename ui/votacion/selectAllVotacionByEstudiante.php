<?php
$order = "";
if(isset($_GET['order'])){
	$order = $_GET['order'];
}
$dir = "";
if(isset($_GET['dir'])){
	$dir = $_GET['dir'];
}
$estudiante = new Estudiante($_GET['idEstudiante']); 
$estudiante -> select();
?>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Consultar Votacion de Estudiante: <em><?php echo $estudiante -> getNombre() . " " . $estudiante -> getApellido() ?></em></h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr><th></th>
						<th nowrap>Voto 
						<?php if($order=="voto" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/votacion/selectAllVotacionByEstudiante.php") ?>&idEstudiante=<?php echo $_GET['idEstudiante'] ?>&order=voto&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="voto" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/votacion/selectAllVotacionByEstudiante.php") ?>&idEstudiante=<?php echo $_GET['idEstudiante'] ?>&order=voto&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th nowrap>Fecha 
						<?php if($order=="fecha" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/votacion/selectAllVotacionByEstudiante.php") ?>&idEstudiante=<?php echo $_GET['idEstudiante'] ?>&order=fecha&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="fecha" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/votacion/selectAllVotacionByEstudiante.php") ?>&idEstudiante=<?php echo $_GET['idEstudiante'] ?>&order=fecha&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th>Referencia</th>
						<th>Estudiante</th>
						<th nowrap></th>
					</tr>
				</thead>
				</tbody>
					<?php
					$votacion = new Votacion("", "", "", "", $_GET['idEstudiante']);
					if($order!="" && $dir!="") {
						$votacions = $votacion -> selectAllByEstudianteOrder($order, $dir);
					} else {
						$votacions = $votacion -> selectAllByEstudiante();
					}
					$counter = 1;
					foreach ($votacions as $currentVotacion) {
						echo "<tr><td>" . $counter . "</td>";
						echo "<td>" . $currentVotacion -> getVoto() . "</td>";
						echo "<td>" . $currentVotacion -> getFecha() . "</td>";
						echo "<td><a href='modalReferencia.php?idReferencia=" . $currentVotacion -> getReferencia() -> getIdReferencia() . "' data-toggle='modal' data-target='#modalVotacion' >" . $currentVotacion -> getReferencia() -> getComentario() . "</a></td>";
						echo "<td><a href='modalEstudiante.php?idEstudiante=" . $currentVotacion -> getEstudiante() -> getIdEstudiante() . "' data-toggle='modal' data-target='#modalVotacion' >" . $currentVotacion -> getEstudiante() -> getNombre() . " " . $currentVotacion -> getEstudiante() -> getApellido() . "</a></td>";
						echo "<td class='text-right' nowrap>";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/votacion/updateVotacion.php") . "&idVotacion=" . $currentVotacion -> getIdVotacion() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Votacion' ></span></a> ";
						}
						echo "</td>";
						echo "</tr>";
						$counter++;
					};
					?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalVotacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
