<?php
$processed=false;
$idVotacion = $_GET['idVotacion'];
$updateVotacion = new Votacion($idVotacion);
$updateVotacion -> select();
$voto="";
if(isset($_POST['voto'])){
	$voto=$_POST['voto'];
}
$fecha=date("d/m/Y");
if(isset($_POST['fecha'])){
	$fecha=$_POST['fecha'];
}
$referencia="";
if(isset($_POST['referencia'])){
	$referencia=$_POST['referencia'];
}
$estudiante="";
if(isset($_POST['estudiante'])){
	$estudiante=$_POST['estudiante'];
}
if(isset($_POST['update'])){
	$updateVotacion = new Votacion($idVotacion, $voto, $fecha, $referencia, $estudiante);
	$updateVotacion -> update();
	$updateVotacion -> select();
	$objReferencia = new Referencia($referencia);
	$objReferencia -> select();
	$nameReferencia = $objReferencia -> getComentario() ;
	$objEstudiante = new Estudiante($estudiante);
	$objEstudiante -> select();
	$nameEstudiante = $objEstudiante -> getNombre() . " " . $objEstudiante -> getApellido() ;
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	if($_SESSION['entity'] == 'Administrador'){
		$logAdministrador = new LogAdministrador("","Editar Votacion", "Voto: " . $voto . "; Fecha: " . $fecha . "; Referencia: " . $nameReferencia . ";; Estudiante: " . $nameEstudiante , date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrador -> insert();
	}
	else if($_SESSION['entity'] == 'Estudiante'){
		$logEstudiante = new LogEstudiante("","Editar Votacion", "Voto: " . $voto . "; Fecha: " . $fecha . "; Referencia: " . $nameReferencia . ";; Estudiante: " . $nameEstudiante , date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logEstudiante -> insert();
	}
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Editar Votacion</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					<div class="alert alert-success" >Datos Editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("ui/votacion/updateVotacion.php") . "&idVotacion=" . $idVotacion ?>" class="bootstrap-form needs-validation"   >
						<div class="form-group">
							<label>Voto*</label>
							<input type="number" class="form-control" name="voto" id="voto" value="<?php echo $updateVotacion -> getVoto() ?>" required />
						</div>
						<div class="form-group">
							<label>Fecha*</label>
							<input type="date" class="form-control" name="fecha" id="fecha" value="<?php echo $updateVotacion -> getFecha() ?>" autocomplete="off" required />
						</div>
						<div class="form-group">
							<label>Referencia*</label>
							<select class="form-control" name="referencia" id="referencia" data-placeholder="Seleccione Referencia" required >
								<option></option>
								<?php
								$objReferencia = new Referencia();
								$referencias = $objReferencia -> selectAllOrder("comentario", "asc");
								foreach($referencias as $currentReferencia){
									echo "<option value='" . $currentReferencia -> getIdReferencia() . "'";
									if($currentReferencia -> getIdReferencia() == $updateVotacion -> getReferencia() -> getIdReferencia()){
										echo " selected";
									}
									echo ">" . $currentReferencia -> getComentario() . "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Estudiante*</label>
							<select class="form-control" name="estudiante" id="estudiante" data-placeholder="Seleccione Estudiante" required >
								<option></option>
								<?php
								$objEstudiante = new Estudiante();
								$estudiantes = $objEstudiante -> selectAllOrder("nombre", "asc");
								foreach($estudiantes as $currentEstudiante){
									echo "<option value='" . $currentEstudiante -> getIdEstudiante() . "'";
									if($currentEstudiante -> getIdEstudiante() == $updateVotacion -> getEstudiante() -> getIdEstudiante()){
										echo " selected";
									}
									echo ">" . $currentEstudiante -> getNombre() . " " . $currentEstudiante -> getApellido() . "</option>";
								}
								?>
							</select>
						</div>
						<button type="submit" class="btn btn-info" name="update">Editar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('#referencia').select2({});
$('#estudiante').select2({});
</script>
