<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Voto</th>
			<th nowrap>Fecha</th>
			<th>Referencia</th>
			<th>Estudiante</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$votacion = new Votacion();
		$searchText = $_GET['search'];
		$votacions = $votacion -> search($searchText);
		$counter = 1;
		foreach ($votacions as $currentVotacion) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentVotacion -> getVoto(), $searchText);
			if($pos !== false){
				$text = substr($currentVotacion -> getVoto(), 0, $pos) . "<strong>" . substr($currentVotacion -> getVoto(), $pos, strlen($searchText)) . "</strong>" . substr($currentVotacion -> getVoto(), $pos + strlen($searchText));
			} else {
				$text = $currentVotacion -> getVoto();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentVotacion -> getFecha(), $searchText);
			if($pos !== false){
				$text = substr($currentVotacion -> getFecha(), 0, $pos) . "<strong>" . substr($currentVotacion -> getFecha(), $pos, strlen($searchText)) . "</strong>" . substr($currentVotacion -> getFecha(), $pos + strlen($searchText));
			} else {
				$text = $currentVotacion -> getFecha();
			}
			echo "<td>" . $text . "</td>";
			echo "<td>" . $currentVotacion -> getReferencia() -> getComentario() . "</td>";
			echo "<td>" . $currentVotacion -> getEstudiante() -> getNombre() . " " . $currentVotacion -> getEstudiante() -> getApellido() . "</td>";
			echo "<td class='text-right' nowrap>";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/votacion/updateVotacion.php") . "&idVotacion=" . $currentVotacion -> getIdVotacion() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Votacion' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
