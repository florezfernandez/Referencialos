<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Comentario</th>
			<th nowrap>Periodo</th>
			<th nowrap>Fecha</th>
			<th nowrap>Estado</th>
			<th>Profesor</th>
			<th>Curso</th>
			<th>Estudiante</th>
			<th>Programa</th>
			<th>Rango De Nota</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$referencia = new Referencia();
		$searchText = $_GET['search'];
		$referencias = $referencia -> search($searchText);
		$counter = 1;
		foreach ($referencias as $currentReferencia) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentReferencia -> getComentario(), $searchText);
			if($pos !== false){
				$text = substr($currentReferencia -> getComentario(), 0, $pos) . "<strong>" . substr($currentReferencia -> getComentario(), $pos, strlen($searchText)) . "</strong>" . substr($currentReferencia -> getComentario(), $pos + strlen($searchText));
			} else {
				$text = $currentReferencia -> getComentario();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentReferencia -> getPeriodo(), $searchText);
			if($pos !== false){
				$text = substr($currentReferencia -> getPeriodo(), 0, $pos) . "<strong>" . substr($currentReferencia -> getPeriodo(), $pos, strlen($searchText)) . "</strong>" . substr($currentReferencia -> getPeriodo(), $pos + strlen($searchText));
			} else {
				$text = $currentReferencia -> getPeriodo();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentReferencia -> getFecha(), $searchText);
			if($pos !== false){
				$text = substr($currentReferencia -> getFecha(), 0, $pos) . "<strong>" . substr($currentReferencia -> getFecha(), $pos, strlen($searchText)) . "</strong>" . substr($currentReferencia -> getFecha(), $pos + strlen($searchText));
			} else {
				$text = $currentReferencia -> getFecha();
			}
			echo "<td>" . $text . "</td>";
						echo "<td>" . ($currentReferencia -> getEstado()==1?"Habilitado":"Deshabilitado") . "</td>";
			echo "<td>" . $currentReferencia -> getProfesor() -> getNombre() . "</td>";
			echo "<td>" . $currentReferencia -> getCurso() -> getNombre() . "</td>";
			echo "<td>" . $currentReferencia -> getEstudiante() -> getNombre() . " " . $currentReferencia -> getEstudiante() -> getApellido() . "</td>";
			echo "<td>" . $currentReferencia -> getPrograma() -> getNombre() . "</td>";
			echo "<td>" . $currentReferencia -> getRangoDeNota() -> getValor() . "</td>";
			echo "<td class='text-right' nowrap>";
			echo "<a href='modalReferencia.php?idReferencia=" . $currentReferencia -> getIdReferencia() . "'  data-toggle='modal' data-target='#modalReferencia' ><span class='fas fa-eye' data-toggle='tooltip' data-placement='left' data-original-title='Ver mas información' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/referencia/updateReferencia.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Referencia' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/referenciaCriterio/selectAllReferenciaCriterioByReferencia.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Referencia Criterio' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/referenciaCriterio/insertReferenciaCriterio.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Referencia Criterio' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/denuncia/selectAllDenunciaByReferencia.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Denuncia' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/denuncia/insertDenuncia.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Denuncia' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/votacion/selectAllVotacionByReferencia.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Votacion' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/votacion/insertVotacion.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Votacion' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
<div class="modal fade" id="modalReferencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
