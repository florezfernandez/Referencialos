<?php
$processed=false;
$comentario="";
if(isset($_POST['comentario'])){
	$comentario=$_POST['comentario'];
}
$pros="";
if(isset($_POST['pros'])){
	$pros=$_POST['pros'];
}
$contras="";
if(isset($_POST['contras'])){
	$contras=$_POST['contras'];
}
$periodo="";
if(isset($_POST['periodo'])){
	$periodo=$_POST['periodo'];
}
$fecha=date("d/m/Y");
if(isset($_POST['fecha'])){
	$fecha=$_POST['fecha'];
}
$estado="";
if(isset($_POST['estado'])){
	$estado=$_POST['estado'];
}
$profesor="";
if(isset($_POST['profesor'])){
	$profesor=$_POST['profesor'];
}
if(isset($_GET['idProfesor'])){
	$profesor=$_GET['idProfesor'];
}
$curso="";
if(isset($_POST['curso'])){
	$curso=$_POST['curso'];
}
if(isset($_GET['idCurso'])){
	$curso=$_GET['idCurso'];
}
$estudiante="";
if(isset($_POST['estudiante'])){
	$estudiante=$_POST['estudiante'];
}
if(isset($_GET['idEstudiante'])){
	$estudiante=$_GET['idEstudiante'];
}
$programa="";
if(isset($_POST['programa'])){
	$programa=$_POST['programa'];
}
if(isset($_GET['idPrograma'])){
	$programa=$_GET['idPrograma'];
}
$rangoDeNota="";
if(isset($_POST['rangoDeNota'])){
	$rangoDeNota=$_POST['rangoDeNota'];
}
if(isset($_GET['idRangoDeNota'])){
	$rangoDeNota=$_GET['idRangoDeNota'];
}
if(isset($_POST['insert'])){
	$newReferencia = new Referencia("", $comentario, $pros, $contras, $periodo, $fecha, $estado, $profesor, $curso, $estudiante, $programa, $rangoDeNota);
	$newReferencia -> insert();
	$objProfesor = new Profesor($profesor);
	$objProfesor -> select();
	$nameProfesor = $objProfesor -> getNombre() ;
	$objCurso = new Curso($curso);
	$objCurso -> select();
	$nameCurso = $objCurso -> getNombre() ;
	$objEstudiante = new Estudiante($estudiante);
	$objEstudiante -> select();
	$nameEstudiante = $objEstudiante -> getNombre() . " " . $objEstudiante -> getApellido() ;
	$objPrograma = new Programa($programa);
	$objPrograma -> select();
	$namePrograma = $objPrograma -> getNombre() ;
	$objRangoDeNota = new RangoDeNota($rangoDeNota);
	$objRangoDeNota -> select();
	$nameRangoDeNota = $objRangoDeNota -> getValor() ;
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	if($_SESSION['entity'] == 'Administrador'){
		$logAdministrador = new LogAdministrador("","Crear Referencia", "Comentario: " . $comentario . "; Pros: " . $pros . "; Contras: " . $contras . "; Periodo: " . $periodo . "; Fecha: " . $fecha . "; Estado: " . $estado . "; Profesor: " . $nameProfesor . "; Curso: " . $nameCurso . "; Estudiante: " . $nameEstudiante . "; Programa: " . $namePrograma . "; Rango De Nota: " . $nameRangoDeNota, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrador -> insert();
	}
	else if($_SESSION['entity'] == 'Estudiante'){
		$logEstudiante = new LogEstudiante("","Crear Referencia", "Comentario: " . $comentario . "; Pros: " . $pros . "; Contras: " . $contras . "; Periodo: " . $periodo . "; Fecha: " . $fecha . "; Estado: " . $estado . "; Profesor: " . $nameProfesor . "; Curso: " . $nameCurso . "; Estudiante: " . $nameEstudiante . "; Programa: " . $namePrograma . "; Rango De Nota: " . $nameRangoDeNota, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logEstudiante -> insert();
	}
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Crear Referencia</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					<div class="alert alert-success" >Datos Ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("ui/referencia/insertReferencia.php") ?>" class="bootstrap-form needs-validation"   >
						<div class="form-group">
							<label>Comentario*</label>
							<textarea id="comentario" name="comentario" ><?php echo $comentario ?></textarea>
							<script>
								$('#comentario').summernote({
									tabsize: 2,
									height: 100
								});
							</script>
						</div>
						<div class="form-group">
							<label>Pros</label>
							<textarea id="pros" name="pros" ><?php echo $pros ?></textarea>
							<script>
								$('#pros').summernote({
									tabsize: 2,
									height: 100
								});
							</script>
						</div>
						<div class="form-group">
							<label>Contras</label>
							<textarea id="contras" name="contras" ><?php echo $contras ?></textarea>
							<script>
								$('#contras').summernote({
									tabsize: 2,
									height: 100
								});
							</script>
						</div>
						<div class="form-group">
							<label>Periodo*</label>
							<input type="text" class="form-control" name="periodo" value="<?php echo $periodo ?>" required />
						</div>
						<div class="form-group">
							<label>Fecha*</label>
							<input type="date" class="form-control" name="fecha" id="fecha" value="<?php echo $fecha ?>" autocomplete="off" required />
						</div>
						<div class="form-group">
							<label>Estado*</label>
							<div class="form-check">
								<input type="radio" class="form-check-input" name="estado" value="1" checked />
								<label class="form-check-label">Habilitado</label>
							</div>
							<div class="form-check form-check-inline">
								<input type="radio" class="form-check-input" name="estado" value="0" />
								<label class="form-check-label" >Deshabilitado</label>
							</div>
						</div>
						<div class="form-group">
							<label>Profesor*</label>
							<select class="form-control" name="profesor" id="profesor" data-placeholder="Seleccione Profesor" required >
								<option></option>
								<?php
								$objProfesor = new Profesor();
								$profesors = $objProfesor -> selectAllOrder("nombre", "asc");
								foreach($profesors as $currentProfesor){
									echo "<option value='" . $currentProfesor -> getIdProfesor() . "'";
									if($currentProfesor -> getIdProfesor() == $profesor){
										echo " selected";
									}
									echo ">" . $currentProfesor -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Curso*</label>
							<select class="form-control" name="curso" id="curso" data-placeholder="Seleccione Curso" required >
								<option></option>
								<?php
								$objCurso = new Curso();
								$cursos = $objCurso -> selectAllOrder("nombre", "asc");
								foreach($cursos as $currentCurso){
									echo "<option value='" . $currentCurso -> getIdCurso() . "'";
									if($currentCurso -> getIdCurso() == $curso){
										echo " selected";
									}
									echo ">" . $currentCurso -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Estudiante*</label>
							<select class="form-control" name="estudiante" id="estudiante" data-placeholder="Seleccione Estudiante" required >
								<option></option>
								<?php
								$objEstudiante = new Estudiante();
								$estudiantes = $objEstudiante -> selectAllOrder("nombre", "asc");
								foreach($estudiantes as $currentEstudiante){
									echo "<option value='" . $currentEstudiante -> getIdEstudiante() . "'";
									if($currentEstudiante -> getIdEstudiante() == $estudiante){
										echo " selected";
									}
									echo ">" . $currentEstudiante -> getNombre() . " " . $currentEstudiante -> getApellido() . "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Programa*</label>
							<select class="form-control" name="programa" id="programa" data-placeholder="Seleccione Programa" required >
								<option></option>
								<?php
								$objPrograma = new Programa();
								$programas = $objPrograma -> selectAllOrder("nombre", "asc");
								foreach($programas as $currentPrograma){
									echo "<option value='" . $currentPrograma -> getIdPrograma() . "'";
									if($currentPrograma -> getIdPrograma() == $programa){
										echo " selected";
									}
									echo ">" . $currentPrograma -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Rango De Nota*</label>
							<select class="form-control" name="rangoDeNota" id="rangoDeNota" data-placeholder="Seleccione Rango De Nota" required >
								<option></option>
								<?php
								$objRangoDeNota = new RangoDeNota();
								$rangoDeNotas = $objRangoDeNota -> selectAllOrder("valor", "asc");
								foreach($rangoDeNotas as $currentRangoDeNota){
									echo "<option value='" . $currentRangoDeNota -> getIdRangoDeNota() . "'";
									if($currentRangoDeNota -> getIdRangoDeNota() == $rangoDeNota){
										echo " selected";
									}
									echo ">" . $currentRangoDeNota -> getValor() . "</option>";
								}
								?>
							</select>
						</div>
						<button type="submit" class="btn btn-info" name="insert">Crear</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('#profesor').select2({});
$('#curso').select2({});
$('#estudiante').select2({});
$('#programa').select2({});
$('#rangoDeNota').select2({});
</script>
