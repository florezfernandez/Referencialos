<?php
$processed=false;
$idReferencia = $_GET['idReferencia'];
$updateReferencia = new Referencia($idReferencia);
$updateReferencia -> select();
$comentario="";
if(isset($_POST['comentario'])){
	$comentario=$_POST['comentario'];
}
$pros="";
if(isset($_POST['pros'])){
	$pros=$_POST['pros'];
}
$contras="";
if(isset($_POST['contras'])){
	$contras=$_POST['contras'];
}
$periodo="";
if(isset($_POST['periodo'])){
	$periodo=$_POST['periodo'];
}
$fecha=date("d/m/Y");
if(isset($_POST['fecha'])){
	$fecha=$_POST['fecha'];
}
$estado="";
if(isset($_POST['estado'])){
	$estado=$_POST['estado'];
}
$profesor="";
if(isset($_POST['profesor'])){
	$profesor=$_POST['profesor'];
}
$curso="";
if(isset($_POST['curso'])){
	$curso=$_POST['curso'];
}
$estudiante="";
if(isset($_POST['estudiante'])){
	$estudiante=$_POST['estudiante'];
}
$programa="";
if(isset($_POST['programa'])){
	$programa=$_POST['programa'];
}
$rangoDeNota="";
if(isset($_POST['rangoDeNota'])){
	$rangoDeNota=$_POST['rangoDeNota'];
}
if(isset($_POST['update'])){
	$updateReferencia = new Referencia($idReferencia, $comentario, $pros, $contras, $periodo, $fecha, $estado, $profesor, $curso, $estudiante, $programa, $rangoDeNota);
	$updateReferencia -> update();
	$updateReferencia -> select();
	$objProfesor = new Profesor($profesor);
	$objProfesor -> select();
	$nameProfesor = $objProfesor -> getNombre() ;
	$objCurso = new Curso($curso);
	$objCurso -> select();
	$nameCurso = $objCurso -> getNombre() ;
	$objEstudiante = new Estudiante($estudiante);
	$objEstudiante -> select();
	$nameEstudiante = $objEstudiante -> getNombre() . " " . $objEstudiante -> getApellido() ;
	$objPrograma = new Programa($programa);
	$objPrograma -> select();
	$namePrograma = $objPrograma -> getNombre() ;
	$objRangoDeNota = new RangoDeNota($rangoDeNota);
	$objRangoDeNota -> select();
	$nameRangoDeNota = $objRangoDeNota -> getValor() ;
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	if($_SESSION['entity'] == 'Administrador'){
		$logAdministrador = new LogAdministrador("","Editar Referencia", "Comentario: " . $comentario . "; Pros: " . $pros . "; Contras: " . $contras . "; Periodo: " . $periodo . "; Fecha: " . $fecha . "; Estado: " . $estado . "; Profesor: " . $nameProfesor . ";; Curso: " . $nameCurso . ";; Estudiante: " . $nameEstudiante . ";; Programa: " . $namePrograma . ";; Rango De Nota: " . $nameRangoDeNota , date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrador -> insert();
	}
	else if($_SESSION['entity'] == 'Estudiante'){
		$logEstudiante = new LogEstudiante("","Editar Referencia", "Comentario: " . $comentario . "; Pros: " . $pros . "; Contras: " . $contras . "; Periodo: " . $periodo . "; Fecha: " . $fecha . "; Estado: " . $estado . "; Profesor: " . $nameProfesor . ";; Curso: " . $nameCurso . ";; Estudiante: " . $nameEstudiante . ";; Programa: " . $namePrograma . ";; Rango De Nota: " . $nameRangoDeNota , date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logEstudiante -> insert();
	}
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Editar Referencia</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					<div class="alert alert-success" >Datos Editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("ui/referencia/updateReferencia.php") . "&idReferencia=" . $idReferencia ?>" class="bootstrap-form needs-validation"   >
						<div class="form-group">
							<label>Comentario*</label>
							<textarea id="comentario" name="comentario" ><?php echo $updateReferencia -> getComentario() ?></textarea>
							<script>
								$('#comentario').summernote({
									tabsize: 2,
									height: 100
								});
							</script>
						</div>
						<div class="form-group">
							<label>Pros</label>
							<textarea id="pros" name="pros" ><?php echo $updateReferencia -> getPros() ?></textarea>
							<script>
								$('#pros').summernote({
									tabsize: 2,
									height: 100
								});
							</script>
						</div>
						<div class="form-group">
							<label>Contras</label>
							<textarea id="contras" name="contras" ><?php echo $updateReferencia -> getContras() ?></textarea>
							<script>
								$('#contras').summernote({
									tabsize: 2,
									height: 100
								});
							</script>
						</div>
						<div class="form-group">
							<label>Periodo*</label>
							<input type="text" class="form-control" name="periodo" value="<?php echo $updateReferencia -> getPeriodo() ?>" required />
						</div>
						<div class="form-group">
							<label>Fecha*</label>
							<input type="date" class="form-control" name="fecha" id="fecha" value="<?php echo $updateReferencia -> getFecha() ?>" autocomplete="off" required />
						</div>
						<div class="form-group">
							<label>Estado*</label>
							<div class="form-check">
								<input type="radio" class="form-check-input" name="estado" value="1" <?php echo ($updateReferencia -> getEstado()==1)?"checked":"" ?>/>
								<label class="form-check-label">Habilitado</label>
							</div>
							<div class="form-check form-check-inline">
								<input type="radio" class="form-check-input" name="estado" value="0" <?php echo ($updateReferencia -> getEstado()==0)?"checked":"" ?>/>
								<label class="form-check-label" >Deshabilitado</label>
							</div>
						</div>
						<div class="form-group">
							<label>Profesor*</label>
							<select class="form-control" name="profesor" id="profesor" data-placeholder="Seleccione Profesor" required >
								<option></option>
								<?php
								$objProfesor = new Profesor();
								$profesors = $objProfesor -> selectAllOrder("nombre", "asc");
								foreach($profesors as $currentProfesor){
									echo "<option value='" . $currentProfesor -> getIdProfesor() . "'";
									if($currentProfesor -> getIdProfesor() == $updateReferencia -> getProfesor() -> getIdProfesor()){
										echo " selected";
									}
									echo ">" . $currentProfesor -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Curso*</label>
							<select class="form-control" name="curso" id="curso" data-placeholder="Seleccione Curso" required >
								<option></option>
								<?php
								$objCurso = new Curso();
								$cursos = $objCurso -> selectAllOrder("nombre", "asc");
								foreach($cursos as $currentCurso){
									echo "<option value='" . $currentCurso -> getIdCurso() . "'";
									if($currentCurso -> getIdCurso() == $updateReferencia -> getCurso() -> getIdCurso()){
										echo " selected";
									}
									echo ">" . $currentCurso -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Estudiante*</label>
							<select class="form-control" name="estudiante" id="estudiante" data-placeholder="Seleccione Estudiante" required >
								<option></option>
								<?php
								$objEstudiante = new Estudiante();
								$estudiantes = $objEstudiante -> selectAllOrder("nombre", "asc");
								foreach($estudiantes as $currentEstudiante){
									echo "<option value='" . $currentEstudiante -> getIdEstudiante() . "'";
									if($currentEstudiante -> getIdEstudiante() == $updateReferencia -> getEstudiante() -> getIdEstudiante()){
										echo " selected";
									}
									echo ">" . $currentEstudiante -> getNombre() . " " . $currentEstudiante -> getApellido() . "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Programa*</label>
							<select class="form-control" name="programa" id="programa" data-placeholder="Seleccione Programa" required >
								<option></option>
								<?php
								$objPrograma = new Programa();
								$programas = $objPrograma -> selectAllOrder("nombre", "asc");
								foreach($programas as $currentPrograma){
									echo "<option value='" . $currentPrograma -> getIdPrograma() . "'";
									if($currentPrograma -> getIdPrograma() == $updateReferencia -> getPrograma() -> getIdPrograma()){
										echo " selected";
									}
									echo ">" . $currentPrograma -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Rango De Nota*</label>
							<select class="form-control" name="rangoDeNota" id="rangoDeNota" data-placeholder="Seleccione Rango De Nota" required >
								<option></option>
								<?php
								$objRangoDeNota = new RangoDeNota();
								$rangoDeNotas = $objRangoDeNota -> selectAllOrder("valor", "asc");
								foreach($rangoDeNotas as $currentRangoDeNota){
									echo "<option value='" . $currentRangoDeNota -> getIdRangoDeNota() . "'";
									if($currentRangoDeNota -> getIdRangoDeNota() == $updateReferencia -> getRangoDeNota() -> getIdRangoDeNota()){
										echo " selected";
									}
									echo ">" . $currentRangoDeNota -> getValor() . "</option>";
								}
								?>
							</select>
						</div>
						<button type="submit" class="btn btn-info" name="update">Editar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('#profesor').select2({});
$('#curso').select2({});
$('#estudiante').select2({});
$('#programa').select2({});
$('#rangoDeNota').select2({});
</script>
