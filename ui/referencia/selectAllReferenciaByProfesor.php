<?php
$order = "";
if(isset($_GET['order'])){
	$order = $_GET['order'];
}
$dir = "";
if(isset($_GET['dir'])){
	$dir = $_GET['dir'];
}
$profesor = new Profesor($_GET['idProfesor']); 
$profesor -> select();
?>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Consultar Referencia de Profesor: <em><?php echo $profesor -> getNombre() ?></em></h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr><th></th>
						<th nowrap>Comentario 
						<?php if($order=="comentario" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/referencia/selectAllReferenciaByProfesor.php") ?>&idProfesor=<?php echo $_GET['idProfesor'] ?>&order=comentario&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="comentario" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/referencia/selectAllReferenciaByProfesor.php") ?>&idProfesor=<?php echo $_GET['idProfesor'] ?>&order=comentario&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th nowrap>Periodo 
						<?php if($order=="periodo" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/referencia/selectAllReferenciaByProfesor.php") ?>&idProfesor=<?php echo $_GET['idProfesor'] ?>&order=periodo&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="periodo" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/referencia/selectAllReferenciaByProfesor.php") ?>&idProfesor=<?php echo $_GET['idProfesor'] ?>&order=periodo&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th nowrap>Fecha 
						<?php if($order=="fecha" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/referencia/selectAllReferenciaByProfesor.php") ?>&idProfesor=<?php echo $_GET['idProfesor'] ?>&order=fecha&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="fecha" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/referencia/selectAllReferenciaByProfesor.php") ?>&idProfesor=<?php echo $_GET['idProfesor'] ?>&order=fecha&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th nowrap>Estado 
						<?php if($order=="estado" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/referencia/selectAllReferenciaByProfesor.php") ?>&idProfesor=<?php echo $_GET['idProfesor'] ?>&order=estado&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="estado" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/referencia/selectAllReferenciaByProfesor.php") ?>&idProfesor=<?php echo $_GET['idProfesor'] ?>&order=estado&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th>Profesor</th>
						<th>Curso</th>
						<th>Estudiante</th>
						<th>Programa</th>
						<th>Rango De Nota</th>
						<th nowrap></th>
					</tr>
				</thead>
				</tbody>
					<?php
					$referencia = new Referencia("", "", "", "", "", "", "", $_GET['idProfesor'], "", "", "", "");
					if($order!="" && $dir!="") {
						$referencias = $referencia -> selectAllByProfesorOrder($order, $dir);
					} else {
						$referencias = $referencia -> selectAllByProfesor();
					}
					$counter = 1;
					foreach ($referencias as $currentReferencia) {
						echo "<tr><td>" . $counter . "</td>";
						echo "<td>" . $currentReferencia -> getComentario() . "</td>";
						echo "<td>" . $currentReferencia -> getPeriodo() . "</td>";
						echo "<td>" . $currentReferencia -> getFecha() . "</td>";
						echo "<td>" . ($currentReferencia -> getEstado()==1?"Habilitado":"Deshabilitado") . "</td>";
						echo "<td><a href='modalProfesor.php?idProfesor=" . $currentReferencia -> getProfesor() -> getIdProfesor() . "' data-toggle='modal' data-target='#modalReferencia' >" . $currentReferencia -> getProfesor() -> getNombre() . "</a></td>";
						echo "<td><a href='modalCurso.php?idCurso=" . $currentReferencia -> getCurso() -> getIdCurso() . "' data-toggle='modal' data-target='#modalReferencia' >" . $currentReferencia -> getCurso() -> getNombre() . "</a></td>";
						echo "<td><a href='modalEstudiante.php?idEstudiante=" . $currentReferencia -> getEstudiante() -> getIdEstudiante() . "' data-toggle='modal' data-target='#modalReferencia' >" . $currentReferencia -> getEstudiante() -> getNombre() . " " . $currentReferencia -> getEstudiante() -> getApellido() . "</a></td>";
						echo "<td><a href='modalPrograma.php?idPrograma=" . $currentReferencia -> getPrograma() -> getIdPrograma() . "' data-toggle='modal' data-target='#modalReferencia' >" . $currentReferencia -> getPrograma() -> getNombre() . "</a></td>";
						echo "<td><a href='modalRangoDeNota.php?idRangoDeNota=" . $currentReferencia -> getRangoDeNota() -> getIdRangoDeNota() . "' data-toggle='modal' data-target='#modalReferencia' >" . $currentReferencia -> getRangoDeNota() -> getValor() . "</a></td>";
						echo "<td class='text-right' nowrap>";
						echo "<a href='modalReferencia.php?idReferencia=" . $currentReferencia -> getIdReferencia() . "'  data-toggle='modal' data-target='#modalReferencia' ><span class='fas fa-eye' data-toggle='tooltip' data-placement='left' data-original-title='Ver mas información' ></span></a> ";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/referencia/updateReferencia.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Referencia' ></span></a> ";
						}
						echo "<a href='?pid=" . base64_encode("ui/referenciaCriterio/selectAllReferenciaCriterioByReferencia.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Referencia Criterio' ></span></a> ";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/referenciaCriterio/insertReferenciaCriterio.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Referencia Criterio' ></span></a> ";
						}
						echo "<a href='?pid=" . base64_encode("ui/denuncia/selectAllDenunciaByReferencia.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Denuncia' ></span></a> ";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/denuncia/insertDenuncia.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Denuncia' ></span></a> ";
						}
						echo "<a href='?pid=" . base64_encode("ui/votacion/selectAllVotacionByReferencia.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Votacion' ></span></a> ";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/votacion/insertVotacion.php") . "&idReferencia=" . $currentReferencia -> getIdReferencia() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Votacion' ></span></a> ";
						}
						echo "</td>";
						echo "</tr>";
						$counter++;
					};
					?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalReferencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
