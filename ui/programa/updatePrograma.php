<?php
$processed=false;
$idPrograma = $_GET['idPrograma'];
$updatePrograma = new Programa($idPrograma);
$updatePrograma -> select();
$nombre="";
if(isset($_POST['nombre'])){
	$nombre=$_POST['nombre'];
}
$facultad="";
if(isset($_POST['facultad'])){
	$facultad=$_POST['facultad'];
}
if(isset($_POST['update'])){
	$updatePrograma = new Programa($idPrograma, $nombre, $facultad);
	$updatePrograma -> update();
	$updatePrograma -> select();
	$objFacultad = new Facultad($facultad);
	$objFacultad -> select();
	$nameFacultad = $objFacultad -> getNombre() ;
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	if($_SESSION['entity'] == 'Administrador'){
		$logAdministrador = new LogAdministrador("","Editar Programa", "Nombre: " . $nombre . "; Facultad: " . $nameFacultad , date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrador -> insert();
	}
	else if($_SESSION['entity'] == 'Estudiante'){
		$logEstudiante = new LogEstudiante("","Editar Programa", "Nombre: " . $nombre . "; Facultad: " . $nameFacultad , date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logEstudiante -> insert();
	}
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Editar Programa</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					<div class="alert alert-success" >Datos Editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("ui/programa/updatePrograma.php") . "&idPrograma=" . $idPrograma ?>" class="bootstrap-form needs-validation"   >
						<div class="form-group">
							<label>Nombre*</label>
							<input type="text" class="form-control" name="nombre" value="<?php echo $updatePrograma -> getNombre() ?>" required />
						</div>
						<div class="form-group">
							<label>Facultad*</label>
							<select class="form-control" name="facultad" id="facultad" data-placeholder="Seleccione Facultad" required >
								<option></option>
								<?php
								$objFacultad = new Facultad();
								$facultads = $objFacultad -> selectAllOrder("nombre", "asc");
								foreach($facultads as $currentFacultad){
									echo "<option value='" . $currentFacultad -> getIdFacultad() . "'";
									if($currentFacultad -> getIdFacultad() == $updatePrograma -> getFacultad() -> getIdFacultad()){
										echo " selected";
									}
									echo ">" . $currentFacultad -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<button type="submit" class="btn btn-info" name="update">Editar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('#facultad').select2({});
</script>
