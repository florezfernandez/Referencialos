<?php
$order = "";
if(isset($_GET['order'])){
	$order = $_GET['order'];
}
$dir = "";
if(isset($_GET['dir'])){
	$dir = $_GET['dir'];
}
$facultad = new Facultad($_GET['idFacultad']); 
$facultad -> select();
?>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Consultar Programa de Facultad: <em><?php echo $facultad -> getNombre() ?></em></h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr><th></th>
						<th nowrap>Nombre 
						<?php if($order=="nombre" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' href='?pid=<?php echo base64_encode("ui/programa/selectAllProgramaByFacultad.php") ?>&idFacultad=<?php echo $_GET['idFacultad'] ?>&order=nombre&dir=asc'>
							<span class='fas fa-sort-amount-up'></span></a>
						<?php } ?>
						<?php if($order=="nombre" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' href='?pid=<?php echo base64_encode("ui/programa/selectAllProgramaByFacultad.php") ?>&idFacultad=<?php echo $_GET['idFacultad'] ?>&order=nombre&dir=desc'>
							<span class='fas fa-sort-amount-down'></span></a>
						<?php } ?>
						</th>
						<th>Facultad</th>
						<th nowrap></th>
					</tr>
				</thead>
				</tbody>
					<?php
					$programa = new Programa("", "", $_GET['idFacultad']);
					if($order!="" && $dir!="") {
						$programas = $programa -> selectAllByFacultadOrder($order, $dir);
					} else {
						$programas = $programa -> selectAllByFacultad();
					}
					$counter = 1;
					foreach ($programas as $currentPrograma) {
						echo "<tr><td>" . $counter . "</td>";
						echo "<td>" . $currentPrograma -> getNombre() . "</td>";
						echo "<td><a href='modalFacultad.php?idFacultad=" . $currentPrograma -> getFacultad() -> getIdFacultad() . "' data-toggle='modal' data-target='#modalPrograma' >" . $currentPrograma -> getFacultad() -> getNombre() . "</a></td>";
						echo "<td class='text-right' nowrap>";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/programa/updatePrograma.php") . "&idPrograma=" . $currentPrograma -> getIdPrograma() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Programa' ></span></a> ";
						}
						echo "<a href='?pid=" . base64_encode("ui/referencia/selectAllReferenciaByPrograma.php") . "&idPrograma=" . $currentPrograma -> getIdPrograma() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Referencia' ></span></a> ";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/referencia/insertReferencia.php") . "&idPrograma=" . $currentPrograma -> getIdPrograma() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Referencia' ></span></a> ";
						}
						echo "</td>";
						echo "</tr>";
						$counter++;
					};
					?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalPrograma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
