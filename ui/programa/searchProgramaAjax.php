<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Nombre</th>
			<th>Facultad</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$programa = new Programa();
		$searchText = $_GET['search'];
		$programas = $programa -> search($searchText);
		$counter = 1;
		foreach ($programas as $currentPrograma) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentPrograma -> getNombre(), $searchText);
			if($pos !== false){
				$text = substr($currentPrograma -> getNombre(), 0, $pos) . "<strong>" . substr($currentPrograma -> getNombre(), $pos, strlen($searchText)) . "</strong>" . substr($currentPrograma -> getNombre(), $pos + strlen($searchText));
			} else {
				$text = $currentPrograma -> getNombre();
			}
			echo "<td>" . $text . "</td>";
			echo "<td>" . $currentPrograma -> getFacultad() -> getNombre() . "</td>";
			echo "<td class='text-right' nowrap>";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/programa/updatePrograma.php") . "&idPrograma=" . $currentPrograma -> getIdPrograma() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Programa' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/referencia/selectAllReferenciaByPrograma.php") . "&idPrograma=" . $currentPrograma -> getIdPrograma() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Referencia' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/referencia/insertReferencia.php") . "&idPrograma=" . $currentPrograma -> getIdPrograma() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Referencia' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
