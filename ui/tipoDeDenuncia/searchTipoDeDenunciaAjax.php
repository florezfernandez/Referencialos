<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Nombre</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$tipoDeDenuncia = new TipoDeDenuncia();
		$searchText = $_GET['search'];
		$tipoDeDenuncias = $tipoDeDenuncia -> search($searchText);
		$counter = 1;
		foreach ($tipoDeDenuncias as $currentTipoDeDenuncia) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentTipoDeDenuncia -> getNombre(), $searchText);
			if($pos !== false){
				$text = substr($currentTipoDeDenuncia -> getNombre(), 0, $pos) . "<strong>" . substr($currentTipoDeDenuncia -> getNombre(), $pos, strlen($searchText)) . "</strong>" . substr($currentTipoDeDenuncia -> getNombre(), $pos + strlen($searchText));
			} else {
				$text = $currentTipoDeDenuncia -> getNombre();
			}
			echo "<td>" . $text . "</td>";
			echo "<td class='text-right' nowrap>";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/tipoDeDenuncia/updateTipoDeDenuncia.php") . "&idTipoDeDenuncia=" . $currentTipoDeDenuncia -> getIdTipoDeDenuncia() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Tipo De Denuncia' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/denuncia/selectAllDenunciaByTipoDeDenuncia.php") . "&idTipoDeDenuncia=" . $currentTipoDeDenuncia -> getIdTipoDeDenuncia() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Denuncia' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/denuncia/insertDenuncia.php") . "&idTipoDeDenuncia=" . $currentTipoDeDenuncia -> getIdTipoDeDenuncia() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Denuncia' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
