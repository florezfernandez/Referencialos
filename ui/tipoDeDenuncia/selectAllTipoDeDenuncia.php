<?php
$order = "";
if(isset($_GET['order'])){
	$order = $_GET['order'];
}
$dir = "";
if(isset($_GET['dir'])){
	$dir = $_GET['dir'];
}
?>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Consultar Tipo De Denuncia</h4>
		</div>
		<div class="card-body">
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr><th></th>
						<th nowrap>Nombre 
						<?php if($order=="nombre" && $dir=="asc") { ?>
							<span class='fas fa-sort-up'></span>
						<?php } else { ?>
							<a href='index.php?pid=<?php echo base64_encode("ui/tipoDeDenuncia/selectAllTipoDeDenuncia.php") ?>&order=nombre&dir=asc'>
							<span class='fas fa-sort-amount-up' data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Ascendente' ></span></a>
						<?php } ?>
						<?php if($order=="nombre" && $dir=="desc") { ?>
							<span class='fas fa-sort-down'></span>
						<?php } else { ?>
							<a href='?pid=<?php echo base64_encode("ui/tipoDeDenuncia/selectAllTipoDeDenuncia.php") ?>&order=nombre&dir=desc'>
							<span class='fas fa-sort-amount-down' data-toggle='tooltip' data-placement='right' data-original-title='Ordenar Descendente' ></span></a>
						<?php } ?>
						</th>
						<th nowrap></th>
					</tr>
				</thead>
				</tbody>
					<?php
					$tipoDeDenuncia = new TipoDeDenuncia();
					if($order != "" && $dir != "") {
						$tipoDeDenuncias = $tipoDeDenuncia -> selectAllOrder($order, $dir);
					} else {
						$tipoDeDenuncias = $tipoDeDenuncia -> selectAll();
					}
					$counter = 1;
					foreach ($tipoDeDenuncias as $currentTipoDeDenuncia) {
						echo "<tr><td>" . $counter . "</td>";
						echo "<td>" . $currentTipoDeDenuncia -> getNombre() . "</td>";
						echo "<td class='text-right' nowrap>";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/tipoDeDenuncia/updateTipoDeDenuncia.php") . "&idTipoDeDenuncia=" . $currentTipoDeDenuncia -> getIdTipoDeDenuncia() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Tipo De Denuncia' ></span></a> ";
						}
						echo "<a href='?pid=" . base64_encode("ui/denuncia/selectAllDenunciaByTipoDeDenuncia.php") . "&idTipoDeDenuncia=" . $currentTipoDeDenuncia -> getIdTipoDeDenuncia() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Denuncia' ></span></a> ";
						if($_SESSION['entity'] == 'Administrador') {
							echo "<a href='?pid=" . base64_encode("ui/denuncia/insertDenuncia.php") . "&idTipoDeDenuncia=" . $currentTipoDeDenuncia -> getIdTipoDeDenuncia() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Denuncia' ></span></a> ";
						}
						echo "</td>";
						echo "</tr>";
						$counter++;
					}
					?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalTipoDeDenuncia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
