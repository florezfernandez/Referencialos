<script charset="utf-8">
	$(function () { 
		$("[data-toggle='tooltip']").tooltip(); 
	});
</script>
<div class="table-responsive">
<table class="table table-striped table-hover">
	<thead>
		<tr><th></th>
			<th nowrap>Nombre</th>
			<th nowrap>Correo</th>
			<th>Genero</th>
			<th nowrap></th>
		</tr>
	</thead>
	</tbody>
		<?php
		$profesor = new Profesor();
		$searchText = $_GET['search'];
		$profesors = $profesor -> search($searchText);
		$counter = 1;
		foreach ($profesors as $currentProfesor) {
			echo "<tr><td>" . $counter . "</td>";
			$pos = stripos($currentProfesor -> getNombre(), $searchText);
			if($pos !== false){
				$text = substr($currentProfesor -> getNombre(), 0, $pos) . "<strong>" . substr($currentProfesor -> getNombre(), $pos, strlen($searchText)) . "</strong>" . substr($currentProfesor -> getNombre(), $pos + strlen($searchText));
			} else {
				$text = $currentProfesor -> getNombre();
			}
			echo "<td>" . $text . "</td>";
			$pos = stripos($currentProfesor -> getCorreo(), $searchText);
			if($pos !== false){
				$text = substr($currentProfesor -> getCorreo(), 0, $pos) . "<strong>" . substr($currentProfesor -> getCorreo(), $pos, strlen($searchText)) . "</strong>" . substr($currentProfesor -> getCorreo(), $pos + strlen($searchText));
			} else {
				$text = $currentProfesor -> getCorreo();
			}
			echo "<td>" . $text . "</td>";
			echo "<td>" . $currentProfesor -> getGenero() -> getNombre() . "</td>";
			echo "<td class='text-right' nowrap>";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/profesor/updateProfesor.php") . "&idProfesor=" . $currentProfesor -> getIdProfesor() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='Editar Profesor' ></span></a> ";
			}
			echo "<a href='?pid=" . base64_encode("ui/referencia/selectAllReferenciaByProfesor.php") . "&idProfesor=" . $currentProfesor -> getIdProfesor() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='Consultar Referencia' ></span></a> ";
			if($_GET['entity'] == 'Administrador') {
				echo "<a href='?pid=" . base64_encode("ui/referencia/insertReferencia.php") . "&idProfesor=" . $currentProfesor -> getIdProfesor() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='Crear Referencia' ></span></a> ";
			}
			echo "</td>";
			echo "</tr>";
			$counter++;
		}
		?>
	</tbody>
</table>
</div>
