<?php
$processed=false;
$idProfesor = $_GET['idProfesor'];
$updateProfesor = new Profesor($idProfesor);
$updateProfesor -> select();
$nombre="";
if(isset($_POST['nombre'])){
	$nombre=$_POST['nombre'];
}
$correo="";
if(isset($_POST['correo'])){
	$correo=$_POST['correo'];
}
$genero="";
if(isset($_POST['genero'])){
	$genero=$_POST['genero'];
}
if(isset($_POST['update'])){
	$updateProfesor = new Profesor($idProfesor, $nombre, $correo, $genero);
	$updateProfesor -> update();
	$updateProfesor -> select();
	$objGenero = new Genero($genero);
	$objGenero -> select();
	$nameGenero = $objGenero -> getNombre() ;
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	if($_SESSION['entity'] == 'Administrador'){
		$logAdministrador = new LogAdministrador("","Editar Profesor", "Nombre: " . $nombre . "; Correo: " . $correo . "; Genero: " . $nameGenero , date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrador -> insert();
	}
	else if($_SESSION['entity'] == 'Estudiante'){
		$logEstudiante = new LogEstudiante("","Editar Profesor", "Nombre: " . $nombre . "; Correo: " . $correo . "; Genero: " . $nameGenero , date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logEstudiante -> insert();
	}
	$processed=true;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Editar Profesor</h4>
				</div>
				<div class="card-body">
					<?php if($processed){ ?>
					<div class="alert alert-success" >Datos Editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>
					<form id="form" method="post" action="?pid=<?php echo base64_encode("ui/profesor/updateProfesor.php") . "&idProfesor=" . $idProfesor ?>" class="bootstrap-form needs-validation"   >
						<div class="form-group">
							<label>Nombre*</label>
							<input type="text" class="form-control" name="nombre" value="<?php echo $updateProfesor -> getNombre() ?>" required />
						</div>
						<div class="form-group">
							<label>Correo*</label>
							<input type="email" class="form-control" name="correo" value="<?php echo $updateProfesor -> getCorreo() ?>"  required />
						</div>
						<div class="form-group">
							<label>Genero*</label>
							<select class="form-control" name="genero" id="genero" data-placeholder="Seleccione Genero" required >
								<option></option>
								<?php
								$objGenero = new Genero();
								$generos = $objGenero -> selectAllOrder("nombre", "asc");
								foreach($generos as $currentGenero){
									echo "<option value='" . $currentGenero -> getIdGenero() . "'";
									if($currentGenero -> getIdGenero() == $updateProfesor -> getGenero() -> getIdGenero()){
										echo " selected";
									}
									echo ">" . $currentGenero -> getNombre() . "</option>";
								}
								?>
							</select>
						</div>
						<button type="submit" class="btn btn-info" name="update">Editar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('#genero').select2({});
</script>
