<?php
$administrador = new Administrador($_SESSION['id']);
$administrador -> select();
?>
<nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark mb-3" >
	<a class="navbar-brand" href="?pid=<?php echo base64_encode("ui/sessionAdministrador.php") ?>"><span class="fas fa-home" aria-hidden="true"></span></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"> <span class="navbar-toggler-icon"></span></button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Crear</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/administrador/insertAdministrador.php") ?>">Administrador</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/estudiante/insertEstudiante.php") ?>">Estudiante</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/genero/insertGenero.php") ?>">Genero</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/facultad/insertFacultad.php") ?>">Facultad</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/programa/insertPrograma.php") ?>">Programa</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/programa/insertProgramaArchivo.php") ?>">Programa desde Archivo</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/curso/insertCurso.php") ?>">Curso</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/curso/insertCursoArchivo.php") ?>">Curso desde Archivo</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/rangoDeNota/insertRangoDeNota.php") ?>">Rango De Nota</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/profesor/insertProfesor.php") ?>">Profesor</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/profesor/insertProfesorArchivo.php") ?>">Profesor desde Archivo</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/criterio/insertCriterio.php") ?>">Criterio</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/categoriaDeCriterio/insertCategoriaDeCriterio.php") ?>">Categoria De Criterio</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/votacion/insertVotacion.php") ?>">Votacion</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/tipoDeDenuncia/insertTipoDeDenuncia.php") ?>">Tipo De Denuncia</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/denuncia/insertDenuncia.php") ?>">Denuncia</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Consultar</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/administrador/selectAllAdministrador.php") ?>">Administrador</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/estudiante/selectAllEstudiante.php") ?>">Estudiante</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/genero/selectAllGenero.php") ?>">Genero</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/facultad/selectAllFacultad.php") ?>">Facultad</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/programa/selectAllPrograma.php") ?>">Programa</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/curso/selectAllCurso.php") ?>">Curso</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/profesor/selectAllProfesor.php") ?>">Profesor</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/rangoDeNota/selectAllRangoDeNota.php") ?>">Rango De Nota</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/referencia/selectAllReferencia.php") ?>">Referencia</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/criterio/selectAllCriterio.php") ?>">Criterio</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/categoriaDeCriterio/selectAllCategoriaDeCriterio.php") ?>">Categoria De Criterio</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/votacion/selectAllVotacion.php") ?>">Votacion</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/tipoDeDenuncia/selectAllTipoDeDenuncia.php") ?>">Tipo De Denuncia</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/denuncia/selectAllDenuncia.php") ?>">Denuncia</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Buscar</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/administrador/searchAdministrador.php") ?>">Administrador</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/estudiante/searchEstudiante.php") ?>">Estudiante</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/genero/searchGenero.php") ?>">Genero</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/facultad/searchFacultad.php") ?>">Facultad</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/programa/searchPrograma.php") ?>">Programa</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/curso/searchCurso.php") ?>">Curso</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/profesor/searchProfesor.php") ?>">Profesor</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/rangoDeNota/searchRangoDeNota.php") ?>">Rango De Nota</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/referencia/searchReferencia.php") ?>">Referencia</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/criterio/searchCriterio.php") ?>">Criterio</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/categoriaDeCriterio/searchCategoriaDeCriterio.php") ?>">Categoria De Criterio</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/votacion/searchVotacion.php") ?>">Votacion</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/tipoDeDenuncia/searchTipoDeDenuncia.php") ?>">Tipo De Denuncia</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/denuncia/searchDenuncia.php") ?>">Denuncia</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Log</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/logAdministrador/searchLogAdministrador.php") ?>">Log Administrador</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/logEstudiante/searchLogEstudiante.php") ?>">Log Estudiante</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Profesores</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/referencia/searchReferenciaProfesor.php") ?>">Buscar referencias de profesores</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/referencia/selectRankingProfesorCurso.php") ?>">Consultar ranking de profesores por curso</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/referencia/selectRankingProfesorPrograma.php") ?>">Consultar ranking de profesores por programa</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Actividad</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/actividad/actividadGeneral.php") ?>">Actividad General</a>
				</div>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="?pid=<?php echo base64_encode("addons/reportes/reportes.php") ?>">Reportes</a>
			</li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#"  data-toggle="dropdown">Administrador: <?php echo $administrador -> getNombre() . " " . $administrador -> getApellido() ?><span class="caret"></span></a>
				<div class="dropdown-menu" >
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/administrador/updateProfileAdministrador.php") ?>">Editar Perfil</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/administrador/updatePasswordAdministrador.php") ?>">Editar Clave</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/administrador/updateProfilePictureAdministrador.php") ?>">Editar Foto</a>
				</div>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="?logOut=1">Salir</a>
			</li>
		</ul>
	</div>
</nav>
