<?php
$estudiante = new Estudiante($_SESSION['id']);
$estudiante -> select();

$referencia = new ReferenciaExt();
$referencias = $referencia -> consultarSolicitudReferencia();
if(count($referencias) > 0){
?>
<div class="container">
	<div class="card">
		<div class="card-header">
			<h3>Solicitudes de Referencias</h3>
		</div>
		<div class="card-body">
			<?php 
			foreach ($referencias as $currentReferencia){
			    echo "<p>";
			    echo "<small>" . $currentReferencia[0] . "</small><br>";
			    echo "El estudiante " . $currentReferencia[3] -> getNombre() . " " . $currentReferencia[3] -> getApellido() . " ha solicitado una referencia del profesor ";
			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewSearchReferenciaProfesor.php") . "&idProfesor=" . $currentReferencia[1] -> getIdProfesor() . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver referencias del profesor " . $currentReferencia[1] -> getNombre() . "'>" . $currentReferencia[1] -> getNombre() . "</a> ";
			    echo "en el curso ";
			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorCurso.php") . "&idCurso=" . $currentReferencia[2] -> getIdCurso() . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver ranking de profesores del curso " . $currentReferencia[2] -> getNombre() . "'>" . $currentReferencia[2] -> getNombre() . "</a> ";
			    echo "del programa ";
			    echo "<a href='?pid=" . base64_encode("addons/referencia/viewRankingProfesorPrograma.php") . "&idPrograma=" . $currentReferencia[4] -> getIdPrograma() . "' data-toggle='tooltip' data-placement='top' data-original-title='Ver ranking de profesores del programa " . $currentReferencia[4] -> getNombre() . "'>" . $currentReferencia[4] -> getNombre() . "</a><br>";
			    echo "<a class='btn btn-primary' href='?pid=" . base64_encode("addons/referencia/insertReferenciaEstudiante.php") . "&idCurso=" . $currentReferencia[2] -> getIdCurso() . "&idProfesor=" . $currentReferencia[1] -> getIdProfesor() . "'><small>Crear referencia</small></a> ";
			    echo "<hr></p>";
			}
			?>
		</div>
	</div>
</div>
<?php } else { ?>
<div class="container">
	<div>
		<div class="card-header">
			<h3>Perfil</h3>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-md-3">
					<img src="<?php echo ($estudiante -> getFoto()!="")?$estudiante -> getFoto():"http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/512/user2-2-icon.png"; ?>" width="100%" class="rounded">
				</div>
				<div class="col-md-9">
					<div class="table-responsive-sm">
						<table class="table table-striped table-hover">
							<tr>
								<th>Nombre</th>
								<td><?php echo $estudiante -> getNombre() ?></td>
							</tr>
							<tr>
								<th>Apellido</th>
								<td><?php echo $estudiante -> getApellido() ?></td>
							</tr>
							<tr>
								<th>Correo</th>
								<td><?php echo $estudiante -> getCorreo() ?></td>
							</tr>
							<tr>
								<th>Telefono</th>
								<td><?php echo $estudiante -> getTelefono() ?></td>
							</tr>
							<tr>
								<th>Celular</th>
								<td><?php echo $estudiante -> getCelular() ?></td>
							</tr>
							<tr>
								<th>Estado</th>
								<td><?php echo ($estudiante -> getEstado()==1)?"Habilitado":"Deshabilitado"; ?></td>
							</tr>
							<tr>
								<th>Periodo De Ingreso</th>
								<td><?php echo $estudiante -> getPeriodoDeIngreso() ?></td>
							</tr>
							<tr>
								<th>Fecha De Nacimiento</th>
								<td><?php echo $estudiante -> getFechaDeNacimiento() ?></td>
							</tr>
							<tr>
								<th>Fecha De Registro</th>
								<td><?php echo $estudiante -> getFechaDeRegistro() ?></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer">
		<p><?php echo "Su rol es: Estudiante"; ?></p>
		</div>
	</div>
</div>
<?php } ?>