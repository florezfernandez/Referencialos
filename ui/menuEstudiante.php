<?php
$estudiante = new Estudiante($_SESSION['id']);
$estudiante -> select();
?>
<nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark mb-3" >
	<a class="navbar-brand" href="?pid=<?php echo base64_encode("ui/sessionEstudiante.php") ?>"><span class="fas fa-home" aria-hidden="true"></span></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"> <span class="navbar-toggler-icon"></span></button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Profesores</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/referencia/searchReferenciaProfesor.php") ?>">Buscar referencias de profesores</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/referencia/selectRankingProfesorCurso.php") ?>">Consultar ranking de profesores por curso</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/referencia/selectRankingProfesorPrograma.php") ?>">Consultar ranking de profesores por programa</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Referencias</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/referencia/insertReferenciaEstudiante.php") ?>">Crear nueva referencia</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/referencia/selectAllReferenciaEstudiante.php") ?>">Consultar mis referencias</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Solicitudes</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/profesor/solicitarAgregarProfesor.php") ?>">Agregar Profesor</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/curso/solicitarAgregarCurso.php") ?>">Agregar Curso</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/referencia/solicitarAgregarReferencia.php") ?>">Agregar Referencia</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Actividad</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/actividad/actividadGeneral.php") ?>">Actividad General</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("addons/actividad/actividadEstudiante.php") ?>">Mi Actividad</a>
				</div>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="?pid=<?php echo base64_encode("addons/reportes/reportes.php") ?>">Reportes</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="?pid=<?php echo base64_encode("addons/contacto/contacto.php") ?>">Contactenos</a>
			</li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#"  data-toggle="dropdown">Estudiante: <?php echo $estudiante -> getNombre() . " " . $estudiante -> getApellido() ?><span class="caret"></span></a>
				<div class="dropdown-menu" >
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/estudiante/updateProfileEstudiante.php") ?>">Editar Perfil</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/estudiante/updatePasswordEstudiante.php") ?>">Editar Clave</a>
					<a class="dropdown-item" href="?pid=<?php echo base64_encode("ui/estudiante/updateProfilePictureEstudiante.php") ?>">Editar Foto</a>
				</div>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="?logOut=1">Salir</a>
			</li>
		</ul>
	</div>
</nav>
